<?php

return [

    'valuation_assortment' => 'Avaliação de Sortimento',

    'list_clients' => 'Lista de clientes',

    'choice_client' => 'Escolha um cliente',

    'choice_period' => 'Escolha um período',

    'average_revenue' => 'Receita Média',
    'average_margin' => 'Margem Média',

    'tag' => [
        'label' => [
            'sku' => 'TOTAL DE SKU\'s',
            'amount' => 'QUANTIDADE MÉDIA',
            'revenue' => 'RECEITA MÉDIA',
            'margin' => 'MARGEM MÉDIA',
        ],
        'tip' => [
            'sku' => 'Quantidade de itens diferentes da loja no período.',
            'amount' => 'Quantidade de itens vendidos no período.',
            'revenue' => 'Quantidade em reais do lucro médio da loja no período.',
            'margin' => 'Quantidade em reais do lucro médio da loja no período.'
        ]
    ],

    'valuation_categories' => 'Avaliação das Categorias',
    'monitoring_assortment' => 'Acompanhamento de Sortimento',

    'step' => [
        'init' => 'Início',
        'categories' => 'Categorias',
        'subcategories' => 'Subcategorias',
        'brands' => 'Marcas',
        'itens' => 'Itens',
        'cockpit' => 'Cockpit'
    ],

    'title' => [
        'init' => 'Avaliação de Sortimento',
        'categories' => 'Posicionamento das Categorias',
        'subcategories' => 'Posicionamento das Subcategorias',
        'brands' => 'Avaliação de Marcas',
        'itens' => 'Avaliação de Itens',
        'cockpit' => 'Cockipt'
    ],

    'init' => [
        'phrase_1' => 'A Avaliação de Sortimento é composta de 4 etapas',
        'phrase_2' => 'Como a gestão é feita obedecendo a divisão das categorias da loja, o primeiro passo é escolher as categorias que serão trabalhadas.',
        'phrase_3' => 'Para facilitar a escolha, posicionamos as categorias de acordo com a sua rentabilidade em reais e dividimos em 4 grupos.',
        'step_1' => 'Escolha da Categoria',
        'step_2' => 'Escolha da Subcategoria',
        'step_3' => 'Avaliação das Marcas',
        'step_4' => 'Avaliação dos Itens',
    ],

    'participation_category' => 'PARTICIPAÇÃO DA CATEGORIA',
    'count_category' => 'Qtd. de categorias',
    'count_item' => 'Qtd. de itens',

    'category' => [
        'popover' => [
            'participation' => 'Trata-se da participação da categoria dentro da rentabilidade total da loja.',
        ],
        'composition' => 'Composição da categoria',
        'tip' => [
            'sku' => 'Quantidade de diferentes itens que fazem parte dessa categoria.',
            'revenue' => 'Quantidade em reais do total de vendas desta categoria.',
            'margin' => 'Quantidade em reais do lucro desta categoria.',
            'composition' => 'Veja aqui as subcategorias que formam essa categoria.'
        ],
        'groups' => [
            'a' => [
                'description' => 'Trata-se do grupo de categorias mais rentáveis da loja e que são responsáveis por 50% da margem bruta obtida. São as categorias mais importantes da loja.'
            ]
        ]
    ],

    'participation_subcategory' => 'PARTICIPAÇÃO DA SUBCATEGORIA',
    'count_subcategory' => 'Qtd. de subcategorias',

    'subcategory' => [
        'popover' => [
            'participation' => 'Trata-se da participação da subcategoria dentro da rentabilidade total da loja.',
        ],
        'composition' => 'Composição da subcategoria',
        'tip' => [
            'sku' => 'Quantidade de diferentes itens que fazem parte dessa subcategoria.',
            'revenue' => 'Quantidade em reais do total de vendas desta subcategoria.',
            'margin' => 'Quantidade em reais do lucro desta subcategoria.',
            'composition' => 'Veja aqui as subcategorias que formam essa subcategoria.'
        ],
        'groups' => [
            'a' => [
                'description' => 'Trata-se do grupo de subcategorias mais rentáveis da loja e que são responsáveis por 50% da margem bruta obtida. São as categorias mais importantes da loja.'
            ]
        ]
    ],

    'groups' => [
        'a' => [
            'title' => 'Grupo A',
            'description' => 'Trata-se do grupo mais rentável da loja e que são responsáveis por 50% da margem bruta obtida. São os mais importantes da loja.',
        ],
        'b' => [
            'title' => 'Grupo B',
            'description' => 'Trata-se do segundo grupo mais rentável da loja, responsável por 25% da margem bruta obtida.',
        ],
        'c' => [
            'title' => 'Grupo C',
            'description' => 'Trata-se do terceiro grupo em rentabilidade da loja, responsável por apenas 15% da margem bruta obtida.',
        ],
        'd' => [
            'title' => 'Grupo D',
            'description' => 'Trata-se do último grupo em rentabilidade da loja, responsável por apenas 10% da margem bruta obtida.',
        ]
    ],

    'brand' => [
        'info' => [
            'margin' => 'Margem da sucategoria.',
            'group' => 'Grupo da subcategoria, de acordo com a rentabilidade.'
        ],
        'popover' => [
            'margin' => 'Diferença entre o preço de venda do produto e seu custo.',
        ],
        'tip' => [
            'sku' => 'Quantidade de diferentes itens que fazem parte dessa marca.',
            'revenue' => 'Quantidade em reais do total de vendas desta marca.',
            'margin' => 'Quantidade em reais do lucro dessa marca.',
        ],
        'status' => [
            0 => 'AVALIAR PERMANÊNCIA DA MARCA',
            1 => 'MANTER MARCA'
        ],
        'alert' => [
            'margin' => 'Esta marca está com a margem média abaixo da margem média da subcategoria!'
        ],
        'phrase_1' => 'Aqui podemos ver qual o posicionamento de cada marca, bem como a importância desta subcategoria.',
        'phrase_2' => 'Como a gestão é feita obedecendo a divisão das categorias da loja, o primeiro passo é escolher as categorias que serão trabalhadasEssa avaliação nos permite visualizar quais são as marcas mais rentáveis e que merecem uma maior exposição e quais são as marcas que devem ser avaliadas quanto à sua permanência.',
    ],

    'select_category' => 'Escolha uma subcategoria para prosseguir!',

    'item' => [
        'info' => [
            'position' => 'Posição da subcategoria na loja.',
            'margin' => 'Margem da sucategoria.',
            'group' => 'Grupo da subcategoria, de acordo com a rentabilidade.'
        ],
        'label' => [
            'price' => 'PREÇO UNIT.',
            'amount' => 'QTD. MÉDIA',
        ],
        'popover' => [
            'importance' => 'Importância do item no desempenho total da loja.',
        ],
        'tip' => [
            'amount' => 'Quantidade total desse item.',
            'price' => 'Preço de venda desse item.',
            'revenue' => 'Quantidade em reais do total de vendas desse item.',
            'margin' => 'Quantidade em reais do lucro desse item.',
        ],
        'alert' => [
            'margin' => 'Este item está com a margem média abaixo da margem média da subcategoria!'
        ],
        'phrase_1' => 'A Avaliação de itens leva em consideração duas coisas',
        'phrase_2' => 'Analise de desempenho do item dentro da subcategoria (rentabilidade, volume e MB%)',
        'phrase_3' => 'Análise do item levando em consideração a importancia da subcatecoria no desempenho total da loja',
    ],

    'cockipt' => [
        'column_subcategory' => 'S<br>U<br>B<br>C<br>A<br>T<br>E<br>G<br>O<br>R<br>I<br>A<br>S',
        'importance' => [
            1 => 'Extremamente Importante',
            2 => 'Muito Importante',
            3 => 'Importante',
            4 => 'Intermediário',
            5 => 'Pouco Importante',
            6 => 'Avaliar Exclusão',
            7 => 'Excluir',
        ],
        'info' => [
            'click' => 'Clique nos números para visualizar os itens pela importância',
            'select_importance' => 'Escolha uma importância para visualizar os itens!',
        ],
        'label' => [
            'price' => 'PREÇO UNIT.',
            'amount' => 'QTD. MÉDIA',
            'group' => 'GRUPO DA SUBCATEGORIA',
            'class' => 'CLASSIFICAÇÃO DO ITEM',
            'category' => 'Qtd. de categorias',
            'subcategory' => 'Qtd. de subcategorias',
            'brand' => 'Qtd. de marcas',
            'item' => 'Qtd. de itens',
        ],
        'tip' => [
            'amount' => 'Quantidade total desse item.',
            'price' => 'Preço de venda desse item.',
            'revenue' => 'Quantidade em reais do total de vendas desse item.',
            'margin' => 'Porcentagem do lucro desse item.',
            'group' => 'Todos os itens fazem parte de subcategorias desse grupo.',
            'class' => 'Todos os itens tem essa classificação.',
        ],
    ]


];