<?php

return [

    'assortment' => 'Sortimento',

    'back' => 'Voltar',

    'brand' => 'Marca',

    'category' => 'Categoria',

    'close' => 'Fechar',

    'code' => 'Código',

    'conclude' => 'Concluir',

    'group' => 'Grupo',

    'keep' => 'Manter',

    'loading' => 'Carregando',

    'margin' => 'Margem',

    'matrix' => 'Matriz',

    'months' => 'Meses',

    'next' => 'Próximo',

    'participation' => 'Participação',

    'percentage' => 'Porcentagem',

    'position' => 'Posição',

    'previous' => 'Anterior',

    'product'  => '{0} Produto|Produtos',

    'rate' => 'Avaliar',

    'revenue' => 'Receita',

    'select_subcategory' => 'Selecione a subcategoria',

    'subcategory' => 'Subcategoria'




];