<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'options'   => 'Opções',
    'import'    => 'Importar',
    'export'    => 'Exportar',
    'delete'    => 'Excluir selecionados',

    'pages'  => 'Página :current de :last',
    'viewing' => 'Visualizando :count, no total de :total',

    'go'    => 'Ir',
    'go_title' => 'Ir para a página',

    'empty' => 'Nada para mostrar aqui',


];