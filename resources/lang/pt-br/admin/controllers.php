<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'home'  => [
        'title' => 'Painel',
    ],

    'admin_system_sites'    => [
        'title' => 'Sites'

    ],

    'admin_system_accounts' => [
        'title' => 'Contas'
    ],

    'admin_system_users'    => [
        'title' => 'Usuários'
    ],

    'admin_system_roles'    => [
        'title' => 'Perfis'
    ],

    'admin' => [

        'change_site' => 'Mudar site',

        'contents' => [
            'pages'    => [
                'title' => 'Páginas',
                'mode_create' => 'Criando nova página',
                'mode_edit' => 'Editando página',
                'mode_show' => 'Visualizando página',
                'index' => 'Lista'
            ],
        ],
    ],

    // Contents

    'admin_contents_pages'    => [
        'title' => 'Páginas',
        'mode_create' => 'Criando nova página',
        'mode_edit' => 'Editando página',
        'mode_show' => 'Visualizando página',
    ],

    'admin_contents_categories' => [
        'title' => 'Categorias',
        'mode_create' => 'Criando nova categoria de conteúdo',
        'mode_edit' => 'Editando categoria de conteúdo',
        'mode_show' => 'Visualizando categoria de conteúdo',
    ],

    'admin_contents_templates' => [
        'title' => 'Modelos',
        'mode_create' => 'Criando novo modelo',
        'mode_edit' => 'Editando modelo',
        'mode_show' => 'Visualizando modelo',
    ],

    // Tourism

    'admin_tourism_categories' => [
        'title' => 'Categorias de viagem',
        'mode_create' => 'Criando nova categoria de viagem',
        'mode_edit' => 'Editando categoria de viagem',
        'mode_show' => 'Visualizando categoria de viagem',
    ],

    'admin_tourism_regions' => [
        'title' => 'Regiões',
        'mode_create' => 'Criando nova região',
        'mode_edit' => 'Editando região',
        'mode_show' => 'Visualizando região',
    ],

    'admin_tourism_subregions' => [
        'title' => 'Sub-regiões',
        'mode_create' => 'Criando nova sub-região',
        'mode_edit' => 'Editando sub-região',
        'mode_show' => 'Visualizando sub-região',
    ],

    'admin_tourism_destinations' => [
        'title' => 'Destinos'
    ],

    'admin_tourism_attractions' => [
        'title' => 'Atrações',
    ],

    'admin_tourism_attraction_types' => [
        'title' => 'Tipos de atração'
    ],

    'admin_tourism_packages' => [
        'title' => 'Pacotes',
        'mode_create' => 'Criando novo pacote',
        'mode_edit' => 'Editando pacote',
        'mode_show' => 'Visualizando pacote',
    ],

    'admin_tourism_services' => [
        'title' => 'Serviços',
        'mode_create' => 'Criando novo serviço',
        'mode_edit' => 'Editando serviço',
        'mode_show' => 'Visualizando serviço',
    ],

    'admin_tourism_notes' => [
        'title' => 'Notas',
        'mode_create' => 'Criando nova nota',
        'mode_edit' => 'Editando nota',
        'mode_show' => 'Visualizando nota',
    ],

    'admin_tourism_payments' => [
        'title' => 'Formas de pagamento',
        'mode_create' => 'Criando nova forma de pagamento',
        'mode_edit' => 'Editando forma de pagamento',
        'mode_show' => 'Visualizando forma de pagamento',
    ],

    'admin_tourism_origins' => [
        'title' => 'Origens',
        'mode_create' => 'Criando nova origem',
        'mode_edit' => 'Editando origem',
        'mode_show' => 'Visualizando origem',
    ],

    'admin_tourism_testimonials' => [
        'title' => 'Avaliações',
        'mode_create' => 'Criando nova avaliação',
        'mode_edit' => 'Editando avaliação',
        'mode_show' => 'Visualizando avaliação',
    ],

    'admin_tourism_intentions' => [
        'title' => 'Reservas',
        'mode_create' => 'Criando nova reserva',
        'mode_edit' => 'Editando reserva',
        'mode_show' => 'Visualizando reserva',
    ],

    'admin_tourism_intention_config' => [
        'title' => 'Configuração de Reservas',
        'mode_create' => 'Criando nova reserva',
        'mode_edit' => 'Editando reserva',
        'mode_show' => 'Visualizando reserva',
    ],

    // Planogram

    'admin_planogram_areas' => [
        'title' => 'Áreas',
        'mode_create' => 'Criando nova área',
        'mode_edit' => 'Editando área',
        'mode_show' => 'Visualizando área'
    ],

    'admin_planogram_categories' => [
        'title' => 'Categorias',
        'mode_create' => 'Criando nova categoria',
        'mode_edit' => 'Editando categoria',
        'mode_show' => 'Visualizando categoria',
    ],

    'admin_planogram_subcategories' => [
        'title' => 'Subcategorias',
        'mode_create' => 'Criando nova subcategoria',
        'mode_edit' => 'Editando subcategoria',
        'mode_show' => 'Visualizando subcategoria',
        'mode_import' => 'Importar Subcategorias',
    ],

    'admin_planogram_brands' => [
        'title' => 'Marcas',
        'mode_create' => 'Criando nova marca',
        'mode_edit' => 'Editando marca',
        'mode_show' => 'Visualizando marca',
        'mode_import' => 'Importar marcas',
    ],

    'admin_planogram_items' => [
        'title' => 'Mix',
        'mode_create' => 'Criando novo item',
        'mode_edit' => 'Editando item',
        'mode_show' => 'Visualizando item',
        'mode_import' => 'Importar itens',
    ],

    'admin_planogram_planograms' => [
        'title' => 'Planogramas',
        'mode_create' => 'Criando novo planograma',
        'mode_edit' => 'Editando planograma',
        'mode_show' => 'Visualizando planograma'
    ],

    // Assortment

    'admin_assortment_bases' => [
        'title' => 'Bases de sortimento',
        'mode_create' => 'Criando nova base de sortimento',
        'mode_edit' => 'Editando base de sortimento',
        'mode_show' => 'Visualizando base de sortimento',
        'mode_import' => 'Importar base de sortimento',
    ],

    'admin_assortment_clients' => [
        'title' => 'Clientes de sortimento',
        'mode_create' => 'Criando novo cliente de sortimento',
        'mode_edit' => 'Editando cliente de sortimento',
        'mode_show' => 'Visualizando cliente de sortimento'
    ],

    'admin_assortment_tags' => [
        'title' => 'Coleção de meses',
        'mode_create' => 'Criando nova coleção de meses',
        'mode_edit' => 'Editando coleção de meses',
        'mode_show' => 'Visualizando coleção de sortimento'
    ],

    // Mailing

    'admin_mailing_contacts' => [
        'title' => 'Contatos',
        'mode_create' => 'Criando novo contato',
        'mode_edit' => 'Editando contato',
        'mode_show' => 'Visualizando contato',
        'mode_import' => 'Importar contato',
    ],

    // Form

    'admin_form_forms' => [
        'title' => 'Formulários',
        'mode_create' => 'Criando novo formulário',
        'mode_edit' => 'Editando formulário',
        'mode_show' => 'Visualizando formulário',
        'mode_import' => 'Importar formulário',
    ],

    // Auth

    'admin_auth_users' => [
        'title' => 'Usuários',
        'mode_create' => 'Criando novo usuário',
        'mode_edit' => 'Editando usuário',
        'mode_show' => 'Visualizando usuário'
    ],


];