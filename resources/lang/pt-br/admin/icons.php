<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'menus' => [
        'main' => [
            'home' => '',
            'publication' => 'md md md-description',
            'personalization' => 'md md-web',
            'administration' => 'md md-settings',
            'tourism'       => 'md md-flight',
            'planogram'     => 'md md-subtitles',
            'assortment'    => 'md md-assignment',
            'mailing'       => 'md md-quick-contacts-mail',
            'auth'       => 'md md-assignment-ind',
        ]
    ],

    'controllers' => [
        'admin_system_accounts' => 'md md-account-balance-wallet',
        'admin_system_users' => 'md md-account-box',
        'admin_system_roles' => 'md md-assignment-turned-in',
        'admin_system_sites' => 'md md-public',
        'admin_contents_pages' => 'md md-description',
        'admin_contents_templates' => 'md md-web',
        'admin_auth_users' => 'md md-assignment-ind'
    ]


];