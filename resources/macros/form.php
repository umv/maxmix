<?php

Form::macro(
    'selectEmpty',
    function($name, $options = [], $selected = null, $attributes = [], $disabled = [])
    {
        $html = '<select name="' . $name . '"';
        foreach ($attributes as $attribute => $value)
        {
            $html .= ' ' . $attribute . '="' . $value . '"';
        }
        $html .= '>';

        $html .= '<option value="">Selecione</option>';

        foreach ($options as $value => $text)
        {
            $html .= '<option value="' . $value . '"' .
                ($value == $selected ? ' selected="selected"' : '') .
                (in_array($value, $disabled) ? ' disabled="disabled"' : '') . '>' .
                $text . '</option>';
        }

        $html .= '</select>';

        return $html;
    }
);