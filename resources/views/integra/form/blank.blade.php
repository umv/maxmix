<table width="650" border="0" align="center" cellpadding="2" cellspacing="5" style="border: 1px solid #000">
    @foreach($input as $key => $item)
        <tr>
            <th align="right" width="30%" bgcolor="#e3e3e3">
                <p style="color:#333333;font-family:Arial, Helvetica, sans-serif;font-size:16px;">
                {{ strtoupper($key) }}
                </p>
            </th>
            <td align="left">
                <p style="color:#333333;font-family:Arial, Helvetica, sans-serif;font-size:16px;">
                    {{ $item }}
                </p>
            </td>
        </tr>
    @endforeach
</table>