@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-sm">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-4">
                    <h4>Dados da coleção de meses</h4>
                    @if( $mode === 'create' || $mode === 'edit' )
                    <article class="margin-bottom-xxl">
                        <p>
                            Selecione os meses da coleção.
                        </p>
                    </article>
                    @endif
                </div>
                <div class="col-lg-offset-1 col-sm-8">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body">
                                <div class="form-group floating-label{{ $errors->first('client_id', ' has-error') }}">
                                    <label for="view">Cliente</label>
                                    {!! Form::select('client_id', $clients, Input::old('client_id', $item->client_id), ['class' => 'form-control select2', 'id' => 'client_id']) !!}
                                    <span class="help-block">{{ $errors->first('client_id', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                    <input type="text" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                    <label for="name">Nome</label>
                                    <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('dates', ' has-error') }}">
                                    {!! Form::select('dates[]', [], Input::old('dates'), ['class' => 'form-control select2', 'id' => 'dates', 'multiple' => 'multiple']) !!}
                                    <label for="dates">Meses</label>
                                    <span class="help-block">{{ $errors->first('dates', ':message') }}</span>
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection

@push('scripts')
<script src="{{ asset('assets/admin/js/libs/inputmask/jquery.inputmask.bundle.min.js') }}"></script>
@endpush

@push('scripts_ready')

    $(".inputmask").inputmask();

    @if($dates)
        var dates_selected = ["{!! implode('","', array_keys($item->getDate())) !!}"];
    @endif

    $('#client_id').on('change', function(){

        //console.log(this.value);

        $.post('{{route('admin.assortment.tags.client')}}', {client_id:this.value}, function(response){

            if(response.success == 1){

                //console.log(response.data);

                var options = '';

                $.each(response.data, function( index, value ) {
                    var selected = (dates_selected.indexOf(index) >= 0) ? 'selected="selected"' : '';
                    options += '<option value="'+index+'" '+selected+'>'+value+'</option>';
                });

                console.log(options);

                $('#dates').html(options);
                $('#dates').val(dates_selected).trigger("change");
                //$('#dates').html(options);

            } else {

                alert("Nenhum mês encontrado na base deste cliente");

            }

        });

    });

    $('#client_id').trigger("change");

@endpush