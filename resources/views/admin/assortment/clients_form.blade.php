@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-sm">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-4">
                    <h4>Dados do cliente</h4>
                    @if( $mode === 'create' || $mode === 'edit' )
                    <article class="margin-bottom-xxl">
                        <p>
                            Insira as informações básicas do cliente.
                        </p>
                    </article>
                    @endif
                </div>
                <div class="col-lg-offset-1 col-sm-8">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body">
                                <div class="form-group floating-label{{ $errors->first('code', ' has-error') }}">
                                    <input type="text" class="form-control" name="code" id="code" value="{{ Input::old('code', $item->code) }}">
                                    <label for="code">Código</label>
                                    <span class="help-block">{{ $errors->first('code', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('cnpj', ' has-error') }}">
                                    <input type="text" class="form-control inputmask" name="cnpj" id="cnpj" data-inputmask="'mask': '99.999.999/9999-99'" value="{{ Input::old('cnpj', $item->cnpj) }}">
                                    <label for="cnpj">CNPJ</label>
                                    <span class="help-block">{{ $errors->first('cnpj', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                    <input type="text" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                    <label for="name">Nome</label>
                                    <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('users', ' has-error') }}">
                                    {!! Form::select('users[]', $users, Input::old('users', $item->users->lists('id')->all()), ['class' => 'form-control select2', 'id' => 'users', 'multiple' => 'multiple']) !!}
                                    <label for="users">Usuários</label>
                                    <span class="help-block">{{ $errors->first('users', ':message') }}</span>
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection

@push('scripts')
<script src="{{ asset('assets/admin/js/libs/inputmask/jquery.inputmask.bundle.min.js') }}"></script>
@endpush

@push('scripts_ready')
$(".inputmask").inputmask();
@endpush