@extends('admin.layouts.master')

@push('styles')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.7.3/summernote.css" rel="stylesheet">
@endpush

@section('content')

    @include('admin.layouts.inc.content_header')
    <section class="contain-lgg">
        <div class="section-header">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->title : null }}</small></h2>
                </div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-sm-12">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                            <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body no-padding style-primary-bright">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="force-padding">
                                            <!--<div class="form-group floating-label">
                                                <div class="holder">
                                                    <div class="overlay overlay-shade-top">
                                                        <strong class="text-default-bright">IMAGEM CAPA</strong>
                                                    </div>
                                                    <img class="img-responsive" src="http://lorempixel.com/400/250/sports" alt="">
                                                </div>
                                            </div>
                                            <div class="form-group floating-label">
                                                <div class="holder">
                                                    <div class="overlay overlay-shade-top">
                                                        <strong class="text-default-bright">IMAGEM DESTACADA</strong>
                                                    </div>
                                                    <img class="img-responsive" src="http://lorempixel.com/400/250/sports" alt="">
                                                </div>
                                            </div>-->
                                            <div class="form-group floating-label{{ $errors->first('page_id', ' has-error') }}">
                                                <label for="page_id">Página Superior</label>
                                                {!! Form::selectEmpty('page_id', $pages, Input::old('page_id', $item->page_id), ['class' => 'form-control', 'id' => 'page_id']) !!}
                                                <span class="help-block">{{ $errors->first('page_id', ':message') }}</span>
                                            </div>
                                            <div class="form-group floating-label{{ $errors->first('template_id', ' has-error') }}">
                                                <label for="view">Modelo</label>
                                                {!! Form::selectEmpty('template_id', $templates, Input::old('template_id', $item->template_id), ['class' => 'form-control', 'id' => 'template_id']) !!}
                                                <span class="help-block">{{ $errors->first('template_id', ':message') }}</span>
                                            </div>
                                            @if( Sentinel::getUser()->owner )
                                            <div class="form-group floating-label{{ $errors->first('tag', ' has-error') }}">
                                                <input type="text" class="form-control" name="tag" id="tag" value="{{ Input::old('tag', $item->tag) }}">
                                                <label for="tag">TAG de identificação (admin)</label>
                                                <span class="help-block">{{ $errors->first('tag', ':message') }}</span>
                                            </div>
                                            @endif
                                            <div class="form-group">
                                                <div class="checkbox checkbox-inline checkbox-styled">
                                                    <label>
                                                        {!! Form::checkbox('active', true, Input::old('active', $item->active)) !!} <span>Ativo</span>
                                                    </label>
                                                </div>
                                                <label for="active">Status</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="force-padding style-default-bright">
                                            <div class="form-group floating-label{{ $errors->first('title', ' has-error') }}">
                                                <input type="text" class="form-control" name="title" id="title" value="{{ Input::old('title', $item->title) }}">
                                                <label for="title">Título</label>
                                                <span class="help-block">{{ $errors->first('title', ':message') }}</span>
                                            </div>
                                            <div class="form-group">
                                                <label>URL</label>
                                                <p id="slug" class="form-control-static input-sm"></p>
                                            </div>
                                            {{--<div class="form-group floating-label{{ $errors->first('slug', ' has-error') }}">
                                                <input type="text" class="form-control" name="slug" id="slug" value="{{ Input::old('slug', $item->slug) }}">
                                                <label for="slug">Slug</label>
                                                <span class="help-block">{{ $errors->first('slug', ':message') }}</span>
                                            </div>--}}
                                            {{--<div class="form-group floating-label{{ $errors->first('slug', ' has-error') }}">
                                                <input type="hidden" class="form-control" name="slug" id="slug" value="{{ Input::old('slug', $item->slug) }}">
                                                <label for="slug">Slug</label>
                                                <p class="form-control-static">{{ Input::old('slug', $item->slug) }}</p>
                                                <span class="help-block">{{ $errors->first('slug', ':message') }}</span>
                                            </div>--}}
                                            <div class="form-group floating-label{{ $errors->first('description', ' has-error') }}">
                                                <textarea class="form-control" name="description" id="description" rows="2" maxlength="160">{{ Input::old('description', $item->description) }}</textarea>
                                                <label for="description">Descrição</label>
                                                <span class="help-block">{{ $errors->first('description', ':message') }}</span>
                                            </div>

                                            {{--<div class="input-group">
                                              <span class="input-group-btn">
                                                <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                                    <i class="fa fa-picture-o"></i> Choose
                                                </a>
                                              </span>
                                                <input id="thumbnail" class="form-control" type="text" name="filepath">
                                            </div>
                                            <img id="holder" style="margin-top:15px;max-height:100px;">--}}

                                            {{--<textarea name="content" class="form-control my-editor">{!! old('content', $item->content) !!}</textarea>--}}




                                            <div class="form-group floating-label{{ $errors->first('content', ' has-error') }}">
                                                <label for="content">Conteúdo</label>
                                                <br>
                                                @if( $mode === 'show')
                                                    <div>{!! html_entity_decode($item->content) !!}</div>
                                                @else
                                                    <textarea name="content" id="summernote">{{ (Input::old('content', $item->content)) }}</textarea>
                                                @endif
                                                <span class="help-block">{{ $errors->first('content', ':message') }}</span>
                                            </div>
                                         </div>
                                    </div>

                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.7.3/summernote.js"></script>
<script src="{{asset('assets/admin/js/libs/summernote/summernote-pt-BR.js')}}"></script>

{{--<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>--}}

{{--<script src="/vendor/laravel-filemanager/js/lfm.js"></script>--}}

{{--<script>
    $('textarea.my-editor').ckeditor({
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
    });
</script>--}}

<script>
    function getUrl()
    {
        $('#slug').html('<i class="fa fa-circle-o-notch fa-spin"></i>');

        var id = {{ ($item->id) ? $item->id : 'null' }};
        var slug = $('#title').val();

        $.post( "{{ route('admin.contents.pages.geturl') }}", {id:id, slug:slug}, function(data){
            $('#slug').text(data);
        });

    }
</script>
@endpush

@push('scripts_ready')

if(mode !== 'show')
{
    $('#summernote').summernote({
        lang: 'pt-BR',
        height: '150px'
    });
}

$(document).on('change keyup', '#title', function(){
    getUrl();
});

@if($item->id)
    $('#title').trigger('change');
@endif

@endpush