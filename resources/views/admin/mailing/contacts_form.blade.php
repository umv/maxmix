@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-sm">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-4">
                    <h4>Dados do contato</h4>
                    @if( $mode === 'create' || $mode === 'edit' )
                        <article class="margin-bottom-xxl">
                            <p>
                                Preencha com as informações do contato.
                            </p>
                        </article>
                    @endif
                </div>
                <div class="col-lg-offset-1 col-sm-8">
                    @if( $mode === 'create' || $mode === 'edit' )
                        <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                            @endif
                            @if( $mode !== 'create' )
                                <input name="_method" type="hidden" value="PUT">
                            @endif
                            {!! csrf_field() !!}
                            <div class="card @if($mode === 'show') card-bordered form @endif">
                                <div class="card-body">
                                    <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                        <input type="text" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                        <label for="name">Nome</label>
                                        <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                    </div>
                                    <div class="form-group floating-label{{ $errors->first('email', ' has-error') }}">
                                        <input type="text" class="form-control" name="email" id="email" value="{{ Input::old('email', $item->email) }}">
                                        <label for="email">E-mail</label>
                                        <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox checkbox-inline checkbox-styled">
                                            <label>
                                                {!! Form::checkbox('active', true, Input::old('active', $item->active)) !!} <span>Ativo</span>
                                            </label>
                                        </div>
                                        <label for="active">Status</label>
                                    </div>
                                </div>
                                @if( $mode === 'create' || $mode === 'edit' )
                                    <div class="card-actionbar style-default-light action-bar">
                                        <div class="card-actionbar-row">
                                            <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                        </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection