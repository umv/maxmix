@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-lg">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? 'Criando novo site' : (($mode == 'edit') ? 'Editando site' : 'Visualizando site') }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-4">
                    <h4>Dados do site</h4>
                    @if( $mode === 'create' || $mode === 'edit' )
                        <article class="margin-bottom-xxl">
                            <p>
                                Insira os dados do site.
                            </p>
                        </article>
                    @endif
                </div>
                <div class="col-lg-offset-1 col-sm-8">
                    @if( $mode === 'create' || $mode === 'edit' )
                        <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                            @endif
                            @if( $mode !== 'create' )
                                <input name="_method" type="hidden" value="PUT">
                            @endif
                            {!! csrf_field() !!}
                            <div class="card @if($mode === 'show') card-bordered form @endif">
                                <div class="card-body style-primary-dark form-inverse">
                                    <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                        <input type="text" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                        <label for="name">Nome</label>
                                        <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group floating-label{{ $errors->first('account_id', ' has-error') }}">
                                                <label for="account_id">Conta</label>
                                                {!! Form::selectEmpty('account_id', $accounts, Input::old('account_id', $item->account_id), ['class' => 'form-control', 'id' => 'account_id']) !!}
                                                <span class="help-block">{{ $errors->first('account_id', ':message') }}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group floating-label{{ $errors->first('slug', ' has-error') }}">
                                                <input type="text" class="form-control" name="slug" id="slug" value="{{ Input::old('slug', $item->slug) }}">
                                                <label for="slug">Slug</label>
                                                <span class="help-block">{{ $errors->first('slug', ':message') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group floating-label{{ $errors->first('url', ' has-error') }}">
                                        <input type="text" class="form-control" name="url" id="url" value="{{ Input::old('url', $item->url) }}">
                                        <label for="url">URL</label>
                                        <span class="help-block">{{ $errors->first('url', ':message') }}</span>
                                    </div>
                                </div>
                                <div class="card-head style-primary-dark">
                                    <ul class="nav nav-tabs tabs-text-contrast tabs-accent-dark" data-toggle="tabs">
                                        <li class="active"><a href="#tab-seo">SEO</a></li>
                                        <li><a href="#tab-social">Links</a></li>
                                    </ul>
                                </div>
                                <div class="card-body tab-content">
                                    <div class="tab-pane active" id="tab-seo">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group floating-label{{ $errors->first('title', ' has-error') }}">
                                                    <input type="text" class="form-control" name="title" id="title" value="{{ Input::old('title', $item->title) }}">
                                                    <label for="title">Nome</label>
                                                    <span class="help-block">{{ $errors->first('title', ':message') }}</span>
                                                </div>
                                                <div class="form-group floating-label{{ $errors->first('description', ' has-error') }}">
                                                    <textarea class="form-control autosize" name="description" id="description">{{ Input::old('description', $item->description) }}</textarea>
                                                    <label for="description">Descrição</label>
                                                    <span class="help-block">{{ $errors->first('description', ':message') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-social">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group floating-label{{ $errors->first('url_facebook', ' has-error') }}">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-facebook-official fa-lg"></i></span>
                                                        <div class="input-group-content">
                                                            <input type="text" class="form-control" name="url_facebook" id="url_facebook" value="{{ Input::old('url_facebook', $item->url_facebook) }}">
                                                            <label for="url_facebook">Facebook</label>
                                                            <span class="help-block">{{ $errors->first('url_facebook', ':message') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group floating-label{{ $errors->first('url_instagram', ' has-error') }}">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-instagram fa-lg"></i></span>
                                                        <div class="input-group-content">
                                                            <input type="text" class="form-control" name="url_instagram" id="url_instagram" value="{{ Input::old('url_instagram', $item->url_instagram) }}">
                                                            <label for="url_instagram">Instagram</label>
                                                            <span class="help-block">{{ $errors->first('url_instagram', ':message') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group floating-label{{ $errors->first('url_youtube', ' has-error') }}">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-youtube fa-lg"></i></span>
                                                        <div class="input-group-content">
                                                            <input type="text" class="form-control" name="url_youtube" id="url_youtube" value="{{ Input::old('url_youtube', $item->url_youtube) }}">
                                                            <label for="url_youtube">Youtube</label>
                                                            <span class="help-block">{{ $errors->first('url_youtube', ':message') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group floating-label{{ $errors->first('url_google', ' has-error') }}">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-google fa-lg"></i></span>
                                                        <div class="input-group-content">
                                                            <input type="text" class="form-control" name="url_google" id="url_google" value="{{ Input::old('url_google', $item->url_google) }}">
                                                            <label for="url_google">Google</label>
                                                            <span class="help-block">{{ $errors->first('url_google', ':message') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group floating-label{{ $errors->first('url_blog', ' has-error') }}">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-wordpress fa-lg"></i></span>
                                                        <div class="input-group-content">
                                                            <input type="text" class="form-control" name="url_blog" id="url_blog" value="{{ Input::old('url_blog', $item->url_blog) }}">
                                                            <label for="url_blog">Blog</label>
                                                            <span class="help-block">{{ $errors->first('url_blog', ':message') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if( $mode === 'create' || $mode === 'edit' )
                                    <div class="card-actionbar style-default-light action-bar">
                                        <div class="card-actionbar-row">
                                            <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                        </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection