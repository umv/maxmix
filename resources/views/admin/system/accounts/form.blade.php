@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-lg">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? 'Criando nova conta' : (($mode == 'edit') ? 'Editando conta' : 'Visualizando conta') }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->number : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <h4>Dados da conta</h4>
                    @if( $mode === 'create' || $mode === 'edit' )
                    <article class="margin-bottom-xxl">
                        <p>
                            Preencha o formulário ao lado com os dados da conta.
                        </p>
                    </article>
                    @endif
                </div>
                <div class="col-lg-offset-1 col-md-8">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                            <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body">
                                <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                    <input type="text" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                    <label for="name">Nome</label>
                                    <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('number', ' has-error') }}">
                                    <input type="text" class="form-control" name="number" id="number" value="{{ Input::old('number', $item->number) }}">
                                    <label for="number">Número da conta</label>
                                    <span class="help-block">{{ $errors->first('number', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('account_id', ' has-error') }}">
                                    <label for="account_id">Conta</label>
                                    {!! Form::selectEmpty('account_id', $accounts, Input::old('account_id', $item->account_id), ['class' => 'form-control', 'id' => 'account_id']) !!}
                                    <span class="help-block">{{ $errors->first('account_id', ':message') }}</span>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox checkbox-inline checkbox-styled">
                                        <label>
                                            {!! Form::checkbox('active', true, Input::old('active', $item->active)) !!} <span>Ativa</span>
                                        </label>
                                    </div>
                                    <label for="active">Status</label>
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection

@push('scripts_showy')

@endpush