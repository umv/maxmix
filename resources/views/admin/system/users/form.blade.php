@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-lg">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? 'Criando novo usuário' : (($mode == 'edit') ? 'Editando usuário' : 'Visualizando usuário') }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->first_name.' '.$item->last_name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <h4>Dados do usuário</h4>
                    @if( $mode === 'create' || $mode === 'edit' )
                    <article class="margin-bottom-xxl">
                        <p>
                            Preencha o formulário ao lado com os dados do usuário.
                        </p>
                    </article>
                    @endif
                </div>
                <div class="col-lg-offset-1 col-md-8">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                            <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group floating-label{{ $errors->first('first_name', ' has-error') }}">
                                            <input type="text" class="form-control" name="first_name" id="first_name" value="{{ Input::old('first_name', $item->first_name) }}" placeholdeer="Digite o primeiro nome do usuário.">
                                            <label for="first_name">Primeiro Nome</label>
                                            <span class="help-block">{{ $errors->first('first_name', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group floating-label{{ $errors->first('last_name', ' has-error') }}">
                                            <input type="text" class="form-control" name="last_name" id="last_name" value="{{ Input::old('last_name', $item->last_name) }}" placeholdeer="Digite o último nome do usuário.">
                                            <label for="name">Último Nome</label>
                                            <span class="help-block">{{ $errors->first('last_name', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group floating-label{{ $errors->first('email', ' has-error') }}">
                                            <input type="text" class="form-control" name="email" id="email" value="{{ Input::old('email', $item->email) }}" placeholdeer="Digite o e-mail.">
                                            <label for="email">E-mail</label>
                                            <span class="help-block">{{{ $errors->first('email', ':message') }}}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group floating-label{{ $errors->first('password', ' has-error') }}">
                                            <input type="password" class="form-control" name="password" id="password" value="" placeholdeer="Digite a senha (somente se você quiser modificar).">
                                            <label for="password">Senha (somente se você quiser modificar)</label>
                                            <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group floating-label{{ $errors->first('accounts', ' has-error') }}">
                                            {!! Form::select('accounts[]', $accounts, Input::old('accounts', $item->accounts->lists('id')->all()), ['class' => 'form-control select2', 'id' => 'accounts', 'multiple' => 'multiple']) !!}
                                            <label for="accounts">Conta</label>
                                            <span class="help-block">{{ $errors->first('accounts', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group floating-label{{ $errors->first('roles', ' has-error') }}">
                                            {!! Form::select('roles[]', $roles, Input::old('roles', $item->roles->lists('id')->all()), ['class' => 'form-control select2', 'id' => 'roles', 'multiple' => 'multiple']) !!}
                                            <label for="roles">Perfil</label>
                                            <span class="help-block">{{ $errors->first('roles', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox checkbox-inline checkbox-styled">
                                        <label>
                                            {!! Form::checkbox('active', true, Input::old('active', $item->active)) !!} <span>Ativo</span>
                                        </label>
                                    </div>
                                    <label for="active">Status</label>
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection