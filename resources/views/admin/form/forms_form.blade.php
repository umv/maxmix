@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        {{--*/
            $identifier = ($item->slug) ? $item->slug : strtoupper(uniqid(substr($site->slug, 0, 3)));
        /*--}}

        <div class="section-body contain-md">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body style-primary-dark form-inverse">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                            <input type="text" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                            <label for="name">Nome</label>
                                            <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('slug', ' has-error') }}">
                                            <input type="text" class="form-control" name="slug" id="slug" value="{{ Input::old('slug', $identifier ) }}">
                                            <label for="slug">Indentificador</label>
                                            <span class="help-block">{{ $errors->first('slug', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="radio radio-inline radio-styled">
                                                <label>
                                                    {!! Form::radio('response', 1, ($item->response == 1) ? true : false ) !!} <span>Cópia do formulário por e-mail</span>
                                                </label>
                                                <br>
                                                <label>
                                                    {!! Form::radio('response', 2, ($item->response != 1) ? true : false ) !!} <span>Somente aviso de recebimento</span>
                                                </label>
                                            </div>
                                            <label for="active">Como deseja receber os dados?</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="checkbox checkbox-inline checkbox-styled">
                                                        <label>
                                                            {!! Form::checkbox('confirmation', true, Input::old('confirmation', $item->confirmation)) !!} <span>Sim</span>
                                                        </label>
                                                    </div>
                                                    <label for="active">E-mail de confirmação</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="checkbox checkbox-inline checkbox-styled">
                                                        <label>
                                                            {!! Form::checkbox('active', true, Input::old('active', $item->active)) !!} <span>Ativo</span>
                                                        </label>
                                                    </div>
                                                    <label for="active">Status</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-head style-primary-dark">
                                <ul class="nav nav-tabs tabs-text-contrast tabs-accent-dark" data-toggle="tabs">
                                    <li class="active"><a href="#tab-data">DADOS</a></li>
                                    <li><a href="#tab-form">FORMULARIO</a></li>
                                    <li><a href="#tab-recipient">EMAIL DO DESTINATÁRIO</a></li>
                                    <li><a href="#tab-confirmation">EMAIL DE CONFIRMAÇÃO</a></li>
                                </ul>
                            </div>
                            <div class="card-body tab-content">
                                <div class="tab-pane active" id="tab-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group floating-label{{ $errors->first('subject', ' has-error') }}">
                                                <input type="text" class="form-control" name="subject" id="subject" value="{{ Input::old('subject', $item->subject) }}">
                                                <label for="subject">Assunto</label>
                                                <span class="help-block">{{ $errors->first('subject', ':message') }}</span>
                                            </div>
                                            <div class="form-group floating-label{{ $errors->first('recipients', ' has-error') }}">
                                                <input type="text" data-role="tagsinput" class="form-control" name="recipients" id="recipients" value="{{ Input::old('recipients', $item->recipients) }}">
                                                <label for="recipients">Destinatários</label>
                                                <span class="help-block">{{ $errors->first('recipients', ':message') }}</span>
                                            </div>
                                            <div class="form-group floating-label{{ $errors->first('text_success', ' has-error') }}">
                                                <input type="text" class="form-control" name="text_success" id="text_success" value="{{ Input::old('text_success', $item->text_success) }}">
                                                <label for="text_success"><i class="md md-ok"></i> Mensagem de sucesso</label>
                                                <span class="help-block">{{ $errors->first('text_success', ':message') }}</span>
                                            </div>
                                            <div class="form-group floating-label{{ $errors->first('text_error', ' has-error') }}">
                                                <input type="text" class="form-control" name="text_error" id="text_error" value="{{ Input::old('text_error', $item->text_error) }}">
                                                <label for="text_error">Mensagem de erro</label>
                                                <span class="help-block">{{ $errors->first('text_error', ':message') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-form">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group floating-label{{ $errors->first('code_form', ' has-error') }}">
                                                <label>Código HTML do formulário</label>
                                                <br>
                                                <div id="code_form">{{ htmlentities($item->code_form) }}</div>
                                                <textarea name="code_form" class="hide"></textarea>
                                                <span class="help-block">{{ $errors->first('code_form', ':message') }}</span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-recipient">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group floating-label{{ $errors->first('code_recipient', ' has-error') }}">
                                                <label for="code_recipient">E-mail que será enviado para os destinatários</label>
                                                <br>
                                                @if( $mode === 'show')
                                                    <div>{!! html_entity_decode($item->code_recipient) !!}</div>
                                                @else
                                                    <textarea name="code_recipient" id="code_recipient">{{ (Input::old('code_recipient', $item->code_recipient)) }}</textarea>
                                                @endif
                                                <span class="help-block">{{ $errors->first('code_recipient', ':message') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-confirmation">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group floating-label{{ $errors->first('code_confirmation', ' has-error') }}">
                                                <label for="code_recipient">E-mail que será enviado como confirmação</label>
                                                <br>
                                                @if( $mode === 'show')
                                                    <div>{!! html_entity_decode($item->code_confirmation) !!}</div>
                                                @else
                                                    <textarea name="code_confirmation" id="code_confirmation">{{ (Input::old('code_confirmation', $item->code_confirmation)) }}</textarea>
                                                @endif
                                                <span class="help-block">{{ $errors->first('code_confirmation', ':message') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection

@push('styles')
<link href="{{asset('assets/admin/css/libs/summernote/summernote.min.css')}}" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/libs/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" />
@endpush

@push('scripts')
<script src="{{asset('assets/admin/js/libs/summernote/summernote.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/summernote/summernote-pt-BR.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/emmet/emmet.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/ace/ace.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/ace/ext-emmet.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/microtemplating/microtemplating.min.js')}}"></script>
<script>
    var code_form = ace.edit("code_form");
    code_form.setTheme("ace/theme/monokai");
    code_form.session.setMode("ace/mode/html");
    code_form.setOption("enableEmmet", true);
    code_form.setAutoScrollEditorIntoView(true);
    code_form.setOption("maxLines", 30);
    @if(!$item->code_form)
    code_form.getSession().setValue("<form method='post' enctype='multipart/form-data' name='integra-form-{{ $identifier}}'>\r\n\r\n</form>");
    @endif

</script>
@endpush

@push('scripts_ready')

if(mode !== 'show')
{
    $('#code_recipient, #code_confirmation').summernote({
        lang: 'pt-BR',
        height: '150px'
    });
}

$( "form.form" ).submit(function( event ) {
    $('textarea[name=code_form]').val( code_form.getSession().getValue() );
});
@endpush