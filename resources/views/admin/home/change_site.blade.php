@extends('admin.login.layout')

@section('conteudo')
<div class="img-backdrop shadow-gray" style="background-image: url('../../assets/admin/img/cover-admin.jpg');"></div>
<div class="spacer"></div>
<div class="card contain-xs style-transparent">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <img class="img-circle shadow-gray" src="../../assets/admin/img/favicon.jpg" alt="Integra" />
                <h2>Olá, Samuel Edson.<br><small>Bem vindo de volta!</small></h2>
                <div class="card">
                    <div class="card-body no-padding">
                        <ul class="list divider-full-bleed">
                            @foreach( session()->get('config.sites') as $site )
                                <li class="tile">
                                    <a class="tile-content ink-reaction" href="{{ route('admin.change_site', $site['id']) }}">
                                        <div class="tile-icon">
                                            <img src="../../assets/admin/img/avatar2.jpg?1422538624" alt="">
                                        </div>
                                        <div class="tile-text">{{ $site['name'] }}</div>
                                    </a>
                                    <a class="btn btn-flat ink-reaction" href="{{ route('admin.change_site', $site['id']) }}">
                                        <i class="md md-forward"></i>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div><!--end .card-body -->
                </div>
            </div><!--end .col -->
        </div><!--end .row -->
    </div><!--end .card-body -->
</div><!--end .card -->

@endsection

@push('scripts_ready')
//$.cookie("username", "{{ Sentinel::getUser()->email }}", { path: '/admin', expires: 7 });
@endpush