<!-- BEGIN HEADER-->
<header id="header" >
    <div class="headerbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="headerbar-left">
            <ul class="header-nav header-nav-options">
                <li class="header-nav-brand" >
                    <div class="brand-holder">
                        <a href="{{ url('/admin') }}">
                            {{--<span class="text-lg text-bold text-primary">INTEGRA <sup>alpha5</sup></span>--}}
                            <img src="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/img/logo.png')}}" alt="INTEGRA CMS">
                        </a>
                    </div>
                </li>
                <li>
                    <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="headerbar-right">

            <ul class="header-nav header-nav-profile">
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
                        <img src="http://www.gravatar.com/avatar/{{ md5(Sentinel::getUser()->email) }}?d=mm" alt="" />
								<span class="profile-info">
									{{ Sentinel::getUser()->first_name }} {{ Sentinel::getUser()->last_name }}
                                    <small>{{ session()->get('site')->name }}</small>
								</span>
                    </a>
                    <ul class="dropdown-menu animation-dock">
                        @forelse(session()->get('config.sites') as $site)
                        <li {!! (session()->get('site')->id == $site['id']) ? 'class="active"' : '' !!}><a href="{{ route('admin.change_site', $site['id']) }}">{{ $site['name'] }}</a></li>
                        @endforeach
                        <li class="divider"></li>
                        <li class="dropdown-header">Personalizar</li>
                        <li>
                            <div>
                                <div class="col-xs-2">
                                    <a href="{{ route('admin.system.users.changetheme', '3') }}">
                                        <i class="md md-stop" style="color:#3f51b5; font-size: 1.8em;"></i>
                                    </a>
                                </div>
                                <div class="col-xs-2">
                                    <a href="{{ route('admin.system.users.changetheme', '1') }}">
                                        <i class="md md-stop" style="color:#2196f3; font-size: 1.8em;"></i>
                                    </a>
                                </div>
                                <div class="col-xs-2">
                                    <a href="{{ route('admin.system.users.changetheme', '4') }}">
                                        <i class="md md-stop" style="color:#8BC34A; font-size: 1.8em;"></i>
                                    </a>
                                </div>
                                <div class="col-xs-2">
                                    <a href="{{ route('admin.system.users.changetheme', 'default') }}">
                                        <i class="md md-stop" style="color:#ff9c01; font-size: 1.8em;"></i>
                                    </a>
                                </div>
                                <div class="col-xs-2">
                                    <a href="{{ route('admin.system.users.changetheme', '2') }}">
                                        <i class="md md-stop" style="color:#ff5722; font-size: 1.8em;"></i>
                                    </a>
                                </div>
                                <div class="col-xs-2">
                                    <a href="{{ route('admin.system.users.changetheme', '5') }}">
                                        <i class="md md-stop" style="color:#EB0038; font-size: 1.8em;"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </li>

                        {{--<li><a href="../../html/pages/profile.html">My profile</a></li>
                        <li><a href="../../html/pages/blog/post.html">My blog <span class="badge style-danger pull-right">16</span></a></li>
                        <li><a href="../../html/pages/calendar.html">My appointments</a></li>--}}

                        @if (Sentinel::check() && Sentinel::getUser()->hasAccess('admin'))
                            <li class="divider"></li>
                            <li {!! (Request::is('users*') ? 'class="active"' : '') !!}><a href="{{ action('\\Sentinel\Controllers\UserController@index') }}">Usuários</a></li>
                            <li {!! (Request::is('groups*') ? 'class="active"' : '') !!}><a href="{{ action('\\Sentinel\Controllers\GroupController@index') }}">Grupos</a></li>
                        @endif
                        <li class="divider"></li>
                        {{--<li><a href="../../html/pages/locked.html"><i class="fa fa-fw fa-lock"></i> Bloquear</a></li>--}}
                        <li><a href="{{ route('logout') }}"><i class="fa fa-fw fa-power-off text-danger"></i> Sair</a></li>
                    </ul><!--end .dropdown-menu -->
                </li><!--end .dropdown -->
            </ul><!--end .header-nav-profile -->
            {{--<ul class="header-nav header-nav-toggle">
                <li>
                    <a class="btn btn-icon-toggle btn-default" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                </li>
            </ul>--}}
        </div><!--end #header-navbar-collapse -->
    </div>
</header>
<!-- END HEADER-->
