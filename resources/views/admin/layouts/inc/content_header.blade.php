<div class="section-header" style="background: #F9F9F9; border-bottom: 1px solid #DADBDB; padding: 14px 15px;">
    <h2 class="pull-left"><i class="{{ trans('admin/icons.controllers.'.$config->getRouteSlug()) }}"></i> {{ trans('admin/controllers.'.$config->getRouteSlug().'.title') }}</h2>
    <div class="pull-right">
        <a href="{{ route($config->getRouteIndex()) }}" class="btn btn-default-light ink-reaction"><i class="md md-arrow-back"></i> {{ trans('admin/controllers.'.$config->getRouteSlug().'.title') }}</a>
        @if( $mode === 'show' )
        <a href="{{ route($config->getRouteCreate()) }}" class="btn btn-default-light ink-reaction">{{ trans('admin/system.add') }}</a>
        <a href="{{ route($config->getRouteEdit(), $item->id) }}" class="btn btn-primary ink-reaction"><i class="md md-edit"></i> {{ trans('admin/system.edit') }}</a>
        @endif
    </div>
</div>