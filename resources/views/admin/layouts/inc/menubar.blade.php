<!-- BEGIN MENUBAR-->
<div id="menubar" class="menubar menubar-inverse">
    <div class="menubar-fixed-panel">
        <div>
            <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="expanded">
            <a href="{{ url('/admin') }}">
                {{--<span class="text-lg text-bold text-primary ">INTEGRA <sup>alpha5</sup></span>--}}
                <img src="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/img/logo.png')}}" alt="INTEGRA CMS">
            </a>
        </div>
    </div>
    <div class="menubar-scroll-panel">

        <!-- BEGIN MAIN MENU -->
        <ul id="main-menu" class="gui-controls">

            <!-- BEGIN DASHBOARD -->
            <li>
                <a href="{{ url('/admin') }}" >
                    <div class="gui-icon"><i class="md md-home"></i></div>
                    <span class="title">Painel</span>
                </a>
            </li><!--end /menu-li -->
            <!-- END DASHBOARD -->

            <li class="gui-folder">
                <a>
                    <div class="gui-icon"><i class="{{ trans('admin/icons.menus.main.publication') }}"></i></div>
                    <span class="title">Conteúdos</span>
                </a>
                <ul>
                    <li><a href="{{ route('admin.contents.pages.index') }}" class="{{ (Request::is('admin/pages*') ? 'active' : '') }}"><span class="title">Páginas</span></a></li>
                    <li class="gui-folder">
                        <a>
                            <span class="title">Personalização</span>
                        </a>
                        <ul>
                            <li><a href="{{ route('admin.contents.templates.index') }}" class="{{ (Request::is('admin/templates*') ? 'active' : '') }}"><span class="title">Modelos</span></a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            @if(Sentinel::getUser()->hasAccess('admin.tourism.*'))
            <li class="gui-folder">
                <a>
                    <div class="gui-icon"><i class="{{ trans('admin/icons.menus.main.tourism') }}"></i></div>
                    <span class="title">Turismo</span>
                </a>
                <ul>
                    <li class="gui-folder">
                        <a>
                            <span class="title">Reservas</span>
                        </a>
                        <ul>
                            <li><a href="{{ route('admin.tourism.intentions.index') }}" class="{{ (Request::is('admin/tourism/intentions*') ? 'active' : '') }}"><span class="title">Lista de reservas</span></a></li>
                            <li><a href="{{ route('admin.tourism.intention.config.index') }}" class="{{ (Request::is('admin/tourism/intention/config*') ? 'active' : '') }}"><span class="title">Configurações</span></a></li>
                        </ul>
                    </li>
                    <li class="gui-folder">
                        <a>
                            <span class="title">Pacotes</span>
                        </a>
                        <ul>
                            <li><a href="{{ route('admin.tourism.packages.index') }}" class="{{ (Request::is('admin/tourism/packages*') ? 'active' : '') }}"><span class="title">Lista de pacotes</span></a></li>
                            <li><a href="{{ route('admin.tourism.payments.index') }}" class="{{ (Request::is('admin/tourism/payments*') ? 'active' : '') }}"><span class="title">Formas de pagamento</span></a></li>
                            <li><a href="{{ route('admin.tourism.services.index') }}" class="{{ (Request::is('admin/tourism/services*') ? 'active' : '') }}"><span class="title">Serviços</span></a></li>
                            <li><a href="{{ route('admin.tourism.notes.index') }}" class="{{ (Request::is('admin/tourism/notes*') ? 'active' : '') }}"><span class="title">Notas</span></a></li>
                        </ul>
                    </li>
                    <li class="gui-folder">
                        <a>
                            <span class="title">Destinos</span>
                        </a>
                        <ul>
                            <li><a href="{{ route('admin.tourism.destinations.index') }}" class="{{ (Request::is('admin/tourism/destinations*') ? 'active' : '') }}"><span class="title">Lista de destinos</span></a></li>
                            <li><a href="{{ route('admin.tourism.testimonials.index') }}" class="{{ (Request::is('admin/tourism/testimonials*') ? 'active' : '') }}"><span class="title">Avaliações</span></a></li>
                            <li><a href="{{ route('admin.tourism.attractions.index') }}" class="{{ (Request::is('admin/tourism/attractions*') ? 'active' : '') }}"><span class="title">Atrações</span></a></li>
                            <li><a href="{{ route('admin.tourism.attraction.types.index') }}" class="{{ (Request::is('admin/tourism/attraction/types*') ? 'active' : '') }}"><span class="title">Tipos de atrações</span></a></li>
                        </ul>
                    </li>
                    <li class="gui-folder">
                        <a>
                            <span class="title">Mais</span>
                        </a>
                        <ul>
                            <li><a href="{{ route('admin.tourism.categories.index') }}" class="{{ (Request::is('admin/tourism/categories*') ? 'active' : '') }}"><span class="title">Categorias de viagem</span></a></li>
                            <li><a href="{{ route('admin.tourism.regions.index') }}" class="{{ (Request::is('admin/tourism/regions*') ? 'active' : '') }}"><span class="title">Regiões</span></a></li>
                            <li><a href="{{ route('admin.tourism.subregions.index') }}" class="{{ (Request::is('admin/tourism/subregions*') ? 'active' : '') }}"><span class="title">Subregiões</span></a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            @endif

            @if(session()->get('site')->id != 4)
            <li class="gui-folder">
                <a>
                    <div class="gui-icon"><i class="{{ trans('admin/icons.menus.main.assortment') }}"></i></div>
                    <span class="title">Sortimento</span>
                </a>
                <ul>
                    <li><a href="{{ route('admin.assortment.clients.index') }}" class="{{ (Request::is('admin/assortment/clients*') ? 'active' : '') }}"><span class="title">Clientes</span></a></li>
                    <li><a href="{{ route('admin.assortment.tags.index') }}" class="{{ (Request::is('admin/assortment/tags*') ? 'active' : '') }}"><span class="title">Tags</span></a></li>
                    <li class="gui-folder">
                        <a>
                            <span class="title">Bases</span>
                        </a>
                        <ul>
                            <li><a href="{{ route('admin.assortment.bases.index') }}" class="{{ (Request::is('admin/assortment/bases*') ? 'active' : '') }}"><span class="title">Lista de Bases</span></a></li>
                            <li><a href="{{ route('admin.assortment.bases.import') }}" class="{{ (Request::is('admin/assortment/bases/import*') ? 'active' : '') }}"><span class="title">Importar</span></a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="gui-folder">
                <a>
                    <div class="gui-icon"><i class="{{ trans('admin/icons.menus.main.planogram') }}"></i></div>
                    <span class="title">Planogramas</span>
                </a>
                <ul>
                    <li><a href="{{ route('admin.planogram.planograms.index') }}" class="{{ (Request::is('admin/planogram/planograms*') ? 'active' : '') }}"><span class="title">Planogramas</span></a></li>
                    <li class="gui-folder">
                        <a>
                            <span class="title">Mix</span>
                        </a>
                        <ul>
                            <li><a href="{{ route('admin.planogram.items.index') }}" class="{{ (Request::is('admin/planogram/items*') ? 'active' : '') }}"><span class="title">Lista de Itens</span></a></li>
                            <li><a href="{{ route('admin.planogram.items.import') }}" class="{{ (Request::is('admin/planogram/items/import*') ? 'active' : '') }}"><span class="title">Importar</span></a></li>
                        </ul>
                    </li>
                    <li class="gui-folder">
                        <a>
                            <span class="title">Marcas</span>
                        </a>
                        <ul>
                            <li><a href="{{ route('admin.planogram.brands.index') }}" class="{{ (Request::is('admin/planogram/brands*') ? 'active' : '') }}"><span class="title">Lista de Marcas</span></a></li>
                            <li><a href="{{ route('admin.planogram.brands.import') }}" class="{{ (Request::is('admin/planogram/brands/import*') ? 'active' : '') }}"><span class="title">Importar</span></a></li>
                        </ul>
                    </li>
                    {{--<li class="gui-folder">
                        <a>
                            <span class="title">Subcategorias</span>
                        </a>
                        <ul>
                            <li><a href="{{ route('admin.planogram.subcategories.index') }}" class="{{ (Request::is('admin/planogram/subcategories*') ? 'active' : '') }}"><span class="title">Lista de Subcategorias</span></a></li>
                            <li><a href="{{ route('admin.planogram.subcategories.import') }}" class="{{ (Request::is('admin/planogram/subcategories/import*') ? 'active' : '') }}"><span class="title">Importar</span></a></li>
                        </ul>
                    </li>--}}
                    <li><a href="{{ route('admin.planogram.subcategories.index') }}" class="{{ (Request::is('admin/planogram/subcategories*') ? 'active' : '') }}"><span class="title">Subcategorias</span></a></li>
                    <li><a href="{{ route('admin.planogram.categories.index') }}" class="{{ (Request::is('admin/planogram/categories*') ? 'active' : '') }}"><span class="title">Categorias</span></a></li>
                    <li><a href="{{ route('admin.planogram.areas.index') }}" class="{{ (Request::is('admin/planogram/areas*') ? 'active' : '') }}"><span class="title">Áreas</span></a></li>
                </ul>
            </li>
            @endif

            <li class="gui-folder">
                <a>
                    <div class="gui-icon"><i class="{{ trans('admin/icons.menus.main.mailing') }}"></i></div>
                    <span class="title">Mailing</span>
                </a>
                <ul>
                    <li><a href="{{ route('admin.mailing.contacts.index') }}" class="{{ (Request::is('admin/mailing/contacts*') ? 'active' : '') }}"><span class="title">Contatos</span></a></li>
                </ul>
            </li>

            <li class="gui-folder">
                <a>
                    <div class="gui-icon"><i class="{{ trans('admin/icons.menus.main.auth') }}"></i></div>
                    <span class="title">Autenticação</span>
                </a>
                <ul>
                    <li><a href="{{ route('admin.auth.users.index') }}" class="{{ (Request::is('admin/auth/users*') ? 'active' : '') }}"><span class="title">Usuários</span></a></li>
                </ul>
            </li>

            <li class="gui-folder">
                <a>
                    <div class="gui-icon"><i class="{{ trans('admin/icons.menus.main.administration') }}"></i></div>
                    <span class="title">Administração</span>
                </a>
                <ul>
                    <li><a href="{{ route('admin.system.accounts.index') }}" class="{{ (Request::is('admin/system/accounts*') ? 'active' : '') }}"><span class="title">Contas</span></a></li>
                    <li><a href="{{ route('admin.system.roles.index') }}" class="{{ (Request::is('admin/system/roles*') ? 'active' : '') }}"><span class="title">Perfis</span></a></li>
                    <li><a href="{{ route('admin.system.users.index') }}" class="{{ (Request::is('admin/system/users*') ? 'active' : '') }}"><span class="title">Usuários</span></a></li>
                    <li><a href="{{ route('admin.system.sites.index') }}" class="{{ (Request::is('admin/system/sites*') ? 'active' : '') }}"><span class="title">Sites</span></a></li>
                </ul>
            </li>



        </ul><!--end .main-menu -->
        <!-- END MAIN MENU -->

        <div class="menubar-foot-panel">
            <small class="no-linebreak hidden-folded">
                <span class="opacity-75">Copyright &copy; {{date('Y')}}</span> <strong><a href="http://www.wingmidia.com.br" target="_blank">Wing Mídia</a></strong>
            </small>
        </div>
    </div><!--end .menubar-scroll-panel-->
</div><!--end #menubar-->
<!-- END MENUBAR -->