<!DOCTYPE html>
<html lang="pt-BR">
<head>

    <title>@section('title')Material Admin - Blank page@show</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="{{asset('assets/admin/img/favicon.png')}}">
    @stack('metas')
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    {{--*/ $theme = 1; /*--}}
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/bootstrap.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/materialadmin.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/font-awesome.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/material-design-iconic-font.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/libs/toastr/toastr.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/libs/select2/select2.css')}}" />
    @stack('styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/app.css')}}" />
    <!-- END STYLESHEETS -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="{{asset('assets/admin/js/libs/utils/html5shiv.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/libs/utils/respond.min.js')}}"></script>
    <![endif]-->
</head>
<body class="menubar-hoverable header-fixed menubar-first">

@include('admin.layouts.inc.header')

<!-- BEGIN BASE-->
<div id="base">

    @include('admin.layouts.inc.offcanvas_left')

    <!-- BEGIN CONTENT-->
    <div id="content">

        @yield('content')

    </div><!--end #content-->
    <!-- END CONTENT -->

    @include('admin.layouts.inc.menubar')

    @include('admin.layouts.inc.offcanvas_right')

</div><!--end #base-->
<!-- END BASE -->

<script src="{{asset('assets/admin/js/libs/jquery/jquery-1.11.2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/spin.js/spin.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/autosize/jquery.autosize.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/nanoscroller/jquery.nanoscroller.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/toastr/toastr.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/App.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppNavigation.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppOffcanvas.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppCard.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppForm.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppNavSearch.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppVendor.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/select2/select2_locale_pt-BR.js')}}"></script>
<script src="{{asset('assets/admin/js/core/demo/Demo.js')}}"></script>
<script>

    function slugfy(text){
        return text.toString().toLowerCase().trim()
                .replace(/&/g, '-and-')
                .replace(/[\s\W-]+/g, '-');
    }

    toastr.options = {
        "closeButton": true,
        "progressBar": false,
        "debug": false,
        "positionClass": 'toast-bottom-left',
        "showDuration": 330,
        "hideDuration": 330,
        "timeOut":  5000,
        "extendedTimeOut": 1000,
        "showEasing": 'swing',
        "hideEasing": 'swing',
        "showMethod": 'slideDown',
        "hideMethod": 'slideUp',
        "onclick": null
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
@stack('scripts')
<script type="application/javascript">
    $( document ).ready(function() {

        $(".select2").select2();

        var mode = '{{ (isset($mode)) ? $mode : ''}}';

        if(mode === 'show')
        {
            $('#content input, #content select, #content button, #content textarea').prop("disabled", true);
        }

        @if(isset(Session::get('info')['save']))
        toastr.success('Salvo com sucesso!', 'Tudo certo!');
        @elseif(isset(Session::get('info')['error']))
        toastr.error('Não foi possível salvar!', 'Ops!');
        @endif

        @if(isset(Session::get('toast')['msg']))
        toastr.info("{!! base64_decode(Session::get('toast')['msg']) !!}", 'AVISO');
        @endif

        /*$(document).on('keyup', 'input[data-integra=slug]', function(){
            var target = $(this).data('target');
            $(target).val(slugfy(this.value));
        });

        $(document).on('change', '*[data-integra=slug-fix]', function(){
            var target = $(this).data('target');
            $(target).text(slugfy($(this).children("option:selected").text()));
        });*/

        @stack('scripts_ready')

    });
</script>
</body>
</html>