@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-md">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-4">
                    <h4>Dados da avaliação</h4>
                    @if( $mode === 'create' || $mode === 'edit' )
                    <article class="margin-bottom-xxl">
                        <p>
                            Preencha com os dados da avaliação.
                        </p>
                    </article>
                    @endif
                </div>
                <div class="col-lg-offset-1 col-sm-8">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body">
                                <div class="form-group floating-label{{ $errors->first('destination_id', ' has-error') }}">
                                    <label for="view">Destino</label>
                                    {!! Form::selectEmpty('destination_id', $destinations, Input::old('destination_id', $item->destination_id), ['class' => 'form-control', 'id' => 'destination_id']) !!}
                                    <span class="help-block">{{ $errors->first('destination_id', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                    <input type="text" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                    <label for="name">Nome</label>
                                    <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('email', ' has-error') }}">
                                    <input type="text" class="form-control" name="email" id="email" value="{{ Input::old('email', $item->email) }}">
                                    <label for="email">E-mail</label>
                                    <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('city_id', ' has-error') }}">
                                    <label for="city_id">Cidade</label>
                                    <input class='form-control select2-city' id="city_id" name="city_id" type='text' value="{{ Input::old('city_id', $item->city_id) }}" placeholder='Selecione a origem' />
                                    <span class="help-block">{{ $errors->first('city_id', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('rating', ' has-error') }}">
                                    <input type="number" min="0" max="5" class="form-control" name="rating" id="rating" value="{{ Input::old('rating', $item->rating) }}">
                                    <label for="rating">Nota</label>
                                    <span class="help-block">{{ $errors->first('rating', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('description', ' has-error') }}">
                                    <textarea class="form-control autosize" name="description" id="description">{{ Input::old('description', $item->description) }}</textarea>
                                    <label for="description">Descrição</label>
                                    <span class="help-block">{{ $errors->first('description', ':message') }}</span>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox checkbox-inline checkbox-styled">
                                        <label>
                                            {!! Form::checkbox('active', true, Input::old('active', $item->active)) !!} <span>Ativo</span>
                                        </label>
                                    </div>
                                    <label for="active">Status</label>
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection

@push('scripts')
<script>
    function repoFormatResult(item) {
        return item.name + ' - ' + item.abbreviation;
    }

    function repoFormatSelection(item) {
        return item.name + ' - ' + item.abbreviation;
    }
</script>
@endpush

@push('scripts_ready')

    $(".select2-city").select2({
        placeholder: "Procure por uma cidade",
        minimumInputLength: 2,
        ajax: {
            url: "{{ route('admin.general.cities.getcity') }}",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, page) {
                return {
                    q: term
                };
            },
            results: function (data, page) {
                return {
                    results: data
                };
            },
            cache: true
        },
        initSelection: function(element, callback) {
            var id = $(element).val();
            if (id !== "") {
                $.post("{{ route('admin.general.cities.getcity') }}", {
                    id:id
                }).done(function(data) {
                    callback(data);
                    $('#' + $(element).attr('id') + '-label').text(data.name + ' - ' + data.abbreviation);
                });
            }
        },
        formatResult: repoFormatResult,
        formatSelection: repoFormatSelection,
        escapeMarkup: function (m) { return m; }
    });

    $(".select2-city").on("change", function(e) {
        var id = '#' + $(this).attr('id') + '-label';
        $(id).text(e.added.name + ' - ' + e.added.abbreviation);
    });


@endpush