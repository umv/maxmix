@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-md">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body style-primary-dark form-inverse">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                            <input type="text" data-integra="slug" data-target="#slug" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                            <label for="name">Nome</label>
                                            <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('destination_id', ' has-error') }}">
                                            <label for="view">Destino</label>
                                            {!! Form::select('destination_id', $destinations, Input::old('destination_id', $item->destination_id), ['class' => 'form-control select2', 'id' => 'destination_id']) !!}
                                            <span class="help-block">{{ $errors->first('destination_id', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>URL</label>
                                            <p id="slug-category" class="form-control-static input-sm"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-inline checkbox-styled">
                                                <label>
                                                    {!! Form::checkbox('active', true, Input::old('active', $item->active)) !!} <span>Ativo</span>
                                                </label>
                                            </div>
                                            <label for="active">Status</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-head style-primary-dark">
                                <ul class="nav nav-tabs tabs-text-contrast tabs-accent-dark" data-toggle="tabs">
                                    <li class="active"><a href="#tab-data">DADOS</a></li>
                                    <li><a href="#tab-services">SERVIÇOS</a></li>
                                    <li><a href="#tab-dates">PERÍODOS</a></li>
                                    <li><a href="#tab-photos">FOTOS</a></li>
                                </ul>
                            </div>
                            <div class="card-body tab-content">
                                <div class="tab-pane active" id="tab-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group floating-label{{ $errors->first('url_video', ' has-error') }}">
                                                <input type="text" class="form-control" name="url_video" id="url_video" value="{{ Input::old('url_video', $item->url_video) }}">
                                                <label for="url_video">URL do vídeo</label>
                                                <span class="help-block">{{ $errors->first('url_video', ':message') }}</span>
                                            </div>
                                            <div class="form-group floating-label{{ $errors->first('description', ' has-error') }}">
                                                <label for="description">Descrição</label>
                                                <br>
                                                @if( $mode === 'show')
                                                    <div>{!! html_entity_decode($item->description) !!}</div>
                                                @else
                                                    <textarea name="description" id="summernote">{{ (Input::old('description', $item->description)) }}</textarea>
                                                @endif
                                                <span class="help-block">{{ $errors->first('description', ':message') }}</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Notas</label>
                                                <br>
                                                @foreach($notes as $note)
                                                    <div class="checkbox checkbox-styled">
                                                        <label>
                                                            {!! Form::checkbox('notes[]', $note->id, in_array($note->id, $item->notes->lists('id')->toArray()) ) !!}
                                                            <span>{{ $note->name }}</span>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-services">
                                    <div class="row">
                                        <div class="col-md-12">

                                            @foreach($services as $service)

                                                <div class="row">
                                                    <div class="col-md-3 col-md-offset-1">
                                                        <strong>{{ $service->name }}</strong>
                                                    </div>
                                                    <div class="col-md-8">
                                                        @foreach($service->filters as $filter)
                                                            <div class="checkbox checkbox-styled">
                                                                <label>
                                                                    {!! Form::checkbox("filters[{$service->id}][]", $filter->id, in_array($filter->id, $item->filters->lists('id')->toArray()) ) !!} {{ $filter->name }}
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>

                                                <hr>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-dates">
                                    <ul class="list-unstyled" id="periodo-list">
                                        @foreach($item->dates as $key => $date)
                                        {{--*/
                                            $key = $key+1;
                                        /*--}}
                                        <li id="periodo-list-item-{{$key}}" class="clearfix" style="display: list-item;">
                                            <div class="card style-default-light">
                                                <div class="card-head">
                                                    <div class="tools">
                                                        <div class="btn-group">
                                                            <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                                            <button type="button" class="btn btn-icon-toggle btn-delete-periodo-list-item" data-item="{{$key}}"><i class="md md-delete"></i></button>
                                                        </div>
                                                    </div>
                                                    <header class="form-inverse">
                                                        <i class="fa fa-fw fa-tag"></i>
                                                        Saindo de <span id="date-{{$key}}-city-label"></span>
                                                    </header>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group {{ $errors->first('date[$key][city_id]', ' has-error') }}">
                                                                <label for="date[{{$key}}][city_id]">Origem</label>
                                                                <input name="date[{{$key}}][id]" type='hidden' value="{{ $date->id }}" />
                                                                <input class='form-control select2-city' id="date-{{$key}}-city" name="date[{{$key}}][city_id]" type='text' value="{{ $date->city->id }}" placeholder='Selecione a origem' />
                                                                <span class="help-block">{{ $errors->first('date[$key][city_id]', ':message') }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body style-default-bright">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group {{ $errors->first('date[$key][date_departure]', ' has-error') }} control-width-normal">
                                                                <div class="input-group date">
                                                                    <div class="input-group-content">
                                                                        <input type="text" class="form-control datepicker" name="date[{{$key}}][date_departure]" value="{{ Input::old('date[$key][date_departure]', date('d/m/Y', strtotime($date->date_departure)) ) }}">
                                                                        <label>Data de ida</label>
                                                                        <span class="help-block">{{ $errors->first('date[$key][date_departure]', ':message') }}</span>
                                                                    </div>
                                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group {{ $errors->first('date[$key][date_arrival]', ' has-error') }} control-width-normal">
                                                                <div class="input-group date">
                                                                    <div class="input-group-content">
                                                                        <input type="text" class="form-control datepicker" name="date[{{$key}}][date_arrival]" value="{{ Input::old('date[$key][date_arrival]', date('d/m/Y', strtotime($date->date_arrival))) }}">
                                                                        <label>Data de volta</label>
                                                                        <span class="help-block">{{ $errors->first('date[$key][date_arrival]', ':message') }}</span>
                                                                    </div>
                                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                                                    <div class="input-group-content">
                                                                        <input type="text" data-mask="R$ 000.000.000.000.000,00" data-mask-reverse="true" class="form-control" name="date[{{$key}}][price_default]" value="{{ Input::old('date[$key][price_default]', number_format($date->price_default, 2, ',','.')) }}">
                                                                        <label for="username10">Preço antigo</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                                                    <div class="input-group-content">
                                                                        <input type="text" data-mask="R$ 000.000.000.000.000,00" data-mask-reverse="true" class="form-control" name="date[{{$key}}][price_promotional]" value="{{ Input::old('date[$key][price_promotional]', number_format($date->price_promotional, 2, ',','.')) }}">
                                                                        <label for="username10">Preço atual</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group {{ $errors->first('date[$key][disabled_at]', ' has-error') }} control-width-normal">
                                                                <div class="input-group date">
                                                                    <div class="input-group-content">
                                                                        <input type="text" class="form-control datepicker" name="date[{{$key}}][disabled_at]" value="{{ Input::old('date[$key][disabled_at]',  date('d/m/Y', strtotime($date->disabled_at))) }}">
                                                                        <label>Data final da oferta</label>
                                                                        <span class="help-block">{{ $errors->first('date[$key][disabled_at]', ':message') }}</span>
                                                                    </div>
                                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <input type="number" min="0" class="form-control" name="date[{{$key}}][amount_people]" value="{{ Input::old('date[$key][amount_people]', $date->amount_people) }}">
                                                                        <label>Quant. Pessoas</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <div class="checkbox checkbox-inline checkbox-styled">
                                                                            <label>
                                                                                {!! Form::checkbox("date[{$key}][active]", true, Input::old("date[{$key}][active]", $date->active)) !!} <span>Ativo</span>
                                                                            </label>
                                                                        </div>
                                                                        <label for="active">Status</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group {{ $errors->first('date[$key][description]', ' has-error') }}">
                                                        <textarea class="form-control autosize" name="date[{{$key}}][description]">{{ Input::old('date[$key][description]', $date->description) }}</textarea>
                                                        <label for="date[{{$key}}][description]">Descrição</label>
                                                        <span class="help-block">{{ $errors->first('date[$key][description]', ':message') }}</span>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="style-default-light">
                                                                <div class="card-head card-head-xs">
                                                                    <div class="tools">
                                                                        <div class="btn-group">
                                                                            <a class="btn btn-icon-toggle" data-toggle="collapse" data-target="#collapse-payment-{{$key}}"><i class="fa fa-angle-down"></i></a>
                                                                        </div>
                                                                    </div>
                                                                    <header class="text-muted">
                                                                        FORMAS DE PAGAMENTO
                                                                    </header>
                                                                </div>
                                                                <div class="style-default-light force-padding collapse" id="collapse-payment-{{$key}}">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            @foreach($payments as $payment)
                                                                                @if($payment->active)
                                                                                <input type="hidden" name="date[{{$key}}][payments][{{$payment->id}}][id]" value="{{ Input::old("date[{$key}][payments][{$payment->id}][id]", $payment->id) }}">
                                                                                <label>{{ $payment->name }}</label>
                                                                                <div class="row">
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control" data-mask="00,00%" name="date[{{$key}}][payments][{{$payment->id}}][input_value]" value="{{ Input::old("date[{$key}][payments][{$payment->id}][input_value]", number_format($date->payments->find($payment->id)['pivot']['input_value'], 2, ',', '.')) }}">
                                                                                            <label for="date[{{$key}}][payments][{{$payment->id}}][input_value]">Porcent. de entrada</label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control" data-mask="00,00%" name="date[{{$key}}][payments][{{$payment->id}}][discount_value]" value="{{ Input::old("date[{$key}][payments][{$payment->id}][discount_value]", number_format($date->payments->find($payment->id)['pivot']['discount_value'], 2, ',', '.')) }}">
                                                                                            <label for="date[{{$key}}][payments][{{$payment->id}}][discount_value]">Porcent. de desconto</label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <input type="number" min="0" class="form-control" name="date[{{$key}}][payments][{{$payment->id}}][min_parcel]" value="{{ Input::old("date[{$key}][payments][{$payment->id}][min_parcel]", $date->payments->find($payment->id)['pivot']['min_parcel']) }}">
                                                                                            <label for="date[{{$key}}][payments][{{$payment->id}}][min_parcel]">Min. Parc.</label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <input type="number" min="0" class="form-control" name="date[{{$key}}][payments][{{$payment->id}}][max_parcel]" value="{{ Input::old("date[{$key}][payments][{$payment->id}][max_parcel]", $date->payments->find($payment->id)['pivot']['max_parcel']) }}">
                                                                                            <label for="date[{{$key}}][payments][{{$payment->id}}][max_parcel]">Max. Parc.</label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2 text-center">
                                                                                        <div class="form-group">
                                                                                            <div class="checkbox checkbox-inline checkbox-styled">
                                                                                                <label>
                                                                                                    {!! Form::checkbox("date[{$key}][payments][{$payment->id}][active]", true, Input::old("date[{$key}][payments][{$payment->id}][active]", $date->payments->find($payment->id)['pivot']['active'])) !!} <span>Ativo</span>
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                @endif
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-raised btn-default-bright" data-duplicate="periodoTmpl" data-target="#periodo-list">ADICIONAR NOVO PERÍODO</button>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-photos">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            @if( $mode !== 'create' )
                                                <div class="panel-group" id="dropzone-accordion" role="tablist" aria-multiselectable="true">
                                                    <div class="card panel">
                                                        <div class="card-head collapsed" data-toggle="collapse" data-parent="#dropzone-accordion" data-target="#dropzone-accordion-1">
                                                            <header>Adicionar</header>
                                                            <div class="tools">
                                                                <a class="btn btn-icon-toggle"><i class="fa fa-plus"></i></a>
                                                            </div>
                                                        </div>
                                                        <div id="dropzone-accordion-1" class="collapse">
                                                            <div class="card-body">
                                                                <div class="dropzone" id="package-dropzone">
                                                                    <div class="dz-message">
                                                                        <h3>Solte os arquivos aqui ou clique para enviar.</h3>
                                                                        <p class="text-xxxl"><i class="md md-cloud-upload text-primary"></i></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card panel">
                                                        <div class="card-head collapsed" data-toggle="collapse" data-parent="#dropzone-accordion" data-target="#dropzone-accordion-2">
                                                            <header>Fotos</header>
                                                            <div class="tools">
                                                                <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                                            </div>
                                                        </div>
                                                        <div id="dropzone-accordion-2" class="collapse">
                                                            <div class="card-body">
                                                                <div id="box-package-photos" class="dropzone">

                                                                </div>
                                                                <div class="dropzone">
                                                                    <strong>FOTOS DO DESTINO</strong>
                                                                    <hr>
                                                                    @foreach($item->destination->photos as $photo)
                                                                        <div id="img-package-photo-{{ $photo->id }}" class="dz-preview dz-processing dz-image-preview dz-complete">
                                                                            <div class="dz-image">
                                                                                <img data-dz-thumbnail="" alt="{{ $photo->slug }}" src="{{ asset($path_image_destination) }}/{{ $photo->slug }}?{{ date('U') }}" style="max-height: 120px;">
                                                                            </div>
                                                                            <div class="dz-details">
                                                                                <div class="dz-size">
                                                                                    <br>
                                                                                    <a href="{{ route('admin.tourism.destinations.edit', $item->destination->id) }}" target="_blank" class="btn ink-reaction btn-floating-action btn-xs btn-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Ir para o destino"><i class="md md-open-in-new"></i></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="card style-primary-bright">
                                                    <p class="text-xxxxl text-center"><i class="md md-image"></i></p>
                                                    <p class="text-muted text-center">
                                                        Crie o pacote para adicionar fotos na galeria.
                                                    </p>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </form>
            </div>
        </div>

    </section>

    <div class="modal fade" id="cropModal" tabindex="-1" role="dialog" aria-labelledby="cropModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="cropModalLabel">Editar imagem</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div id="box-crop-photo" class="size-12">

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="img-preview size-5 border-gray border-lg" style="float: left;  margin-right: 10px;  margin-bottom: 10px;  overflow: hidden;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-crop-save-photo">Salvar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

@endsection

@push('styles')
<link href="{{asset('assets/admin/css/libs/summernote/summernote.min.css')}}" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/libs/bootstrap-datepicker/datepicker3.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/libs/dropzone/dropzone-theme.css')}}" />
<link href="{{ asset('assets/admin/js/bower_components/cropper/dist/cropper.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{asset('assets/admin/js/libs/summernote/summernote.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/summernote/summernote-pt-BR.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/moment/moment-with-langs.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/jquery-mask-plugin/jquery.mask.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/microtemplating/microtemplating.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/dropzone/dropzone.min.js')}}"></script>
<script src="{{asset('assets/admin/js/bower_components/cropper/dist/cropper.min.js')}}"></script>
<script type="text/html" id="emptyPhotosTmpl">
    <p class="text-xxxxl text-center"><i class="md md-image"></i></p>
    <p class="text-muted text-center">
        Adicione fotos na galeria.
    </p>
</script>
<script type="text/html" id="cropPhotoTmpl">
    <img src="<%=img%>" id="image-crop-photo" class="img-responsive" />
</script>
<script type="text/html" id="photoTmpl">
    <% for ( var i = 0; i < photos.length; i++ ) { %>
    <div id="img-package-photo-<%=photos[i].id%>" class="dz-preview dz-processing dz-image-preview dz-complete">
        <div class="dz-image">
            <img data-dz-thumbnail="" alt="<%=photos[i].slug%>" src="{{ asset($path_image) }}/<%=photos[i].slug%>?<%=time%>" style="max-height: 120px;">
        </div>
        <div class="dz-details">
            <div class="dz-size">
                <br>
                <% if(photos[i].cover == 0){ %>
                <button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-default btn-cover-photo" data-photo="<%=photos[i].id%>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Capa do álbum"><i class="md md-star-outline"></i></button>
                <% } else { %>
                <button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-warning" data-photo="<%=photos[i].id%>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Capa do álbum"><i class="md md-star"></i></button>
                <% } %>
                <button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-default-dark btn-crop-photo" data-photo="<%=photos[i].id%>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Recortar"><i class="md md-crop"></i></button>
                <button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-danger btn-delete-photo" data-photo="<%=photos[i].id%>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Remover"><i class="md md-delete"></i></button>
            </div>
        </div>
    </div>
    <% } %>
</script>
<script type="text/html" id="periodoTmpl">
    <li id="periodo-list-item-<%=index%>" class="clearfix" style="display: list-item;">
        {{--*/
            $key = $item->dates->count()+1;
        /*--}}
        <div class="card style-default-light">
            <div class="card-head">
                <div class="tools">
                    <div class="btn-group">
                        {{--<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>--}}
                        <a class="btn btn-icon-toggle btn-delete-periodo-list-item" data-item="<%=index%>"><i class="md md-delete"></i></a>
                    </div>
                </div>
                <header class="form-inverse">
                    <i class="fa fa-fw fa-tag"></i>
                    Saindo de <span id="date-<%=index%>-city-label"></span>
                </header>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="date[<%=index%>][city_id]">Origem</label>
                            <input name="date[<%=index%>][id]" type="hidden" value="" />
                            <input class="form-control select2-city" id="date-<%=index%>-city" name="date[<%=index%>][city_id]" type="text" placeholder="Selecione a origem" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body style-default-bright">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group control-width-normal">
                            <div class="input-group date">
                                <div class="input-group-content">
                                    <input type="text" class="form-control datepicker" name="date[<%=index%>][date_departure]">
                                    <label>Data de ida</label>
                                </div>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="form-group control-width-normal">
                            <div class="input-group date">
                                <div class="input-group-content">
                                    <input type="text" class="form-control datepicker" name="date[<%=index%>][date_arrival]">
                                    <label>Data de volta</label>
                                </div>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                <div class="input-group-content">
                                    <input type="text" data-mask="R$ 000.000.000.000.000,00" data-mask-reverse="true" class="form-control" name="date[<%=index%>][price_default]">
                                    <label for="username10">Preço antigo</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                <div class="input-group-content">
                                    <input type="text" data-mask="R$ 000.000.000.000.000,00" data-mask-reverse="true" class="form-control" name="date[<%=index%>][price_promotional]">
                                    <label for="username10">Preço atual</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group control-width-normal">
                            <div class="input-group date">
                                <div class="input-group-content">
                                    <input type="text" class="form-control datepicker" name="date[<%=index%>][disabled_at]">
                                    <label>Data final da oferta</label>
                                </div>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="number" min="0" class="form-control" name="date[<%=index%>][amount_people]">
                                    <label>Quant. Pessoas</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="checkbox checkbox-inline checkbox-styled">
                                        <label>
                                            <input checked="checked" name="date[<%=index%>][active]" type="checkbox" value="1">
                                            <span>Ativo</span>
                                        </label>
                                    </div>
                                    <label for="active">Status</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <textarea class="form-control autosize" name="date[<%=index%>][description]"></textarea>
                    <label for="date[<%=index%>][description]">Descrição</label>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="style-default-light">
                            <div class="card-head card-head-xs">
                                <div class="tools">
                                    <div class="btn-group">
                                        <a class="btn btn-icon-toggle" data-toggle="collapse" data-target="#collapse-payment-<%=index%>"><i class="fa fa-angle-down"></i></a>
                                    </div>
                                </div>
                                <header class="text-muted">
                                    FORMAS DE PAGAMENTO
                                </header>
                            </div>
                            <div class="style-default-light force-padding collapse" id="collapse-payment-<%=index%>">
                                <div class="row">
                                    <div class="col-md-12">
                                        @foreach($payments as $payment)
                                            @if($payment->active)
                                            <input type="hidden" name="date[<%=index%>][payments][{{$payment->id}}][id]" value="{{ $payment->id }}">
                                            <label>{{ $payment->name }}</label>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" data-mask="00,00%" name="date[<%=index%>][payments][{{$payment->id}}][input_value]" value="{{ $payment->input_value }}">
                                                        <label for="date[<%=index%>][payments][{{$payment->id}}][input_value]">Porcent. de entrada</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" data-mask="00,00%" name="date[<%=index%>][payments][{{$payment->id}}][discount_value]" value="{{ $payment->discount_value }}">
                                                        <label for="date[<%=index%>][payments][{{$payment->id}}][discount_value]">Porcent. de desconto</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="number" min="0" class="form-control" name="date[{{$key}}][payments][{{$payment->id}}][min_parcel]" value="{{ $payment->min_parcel }}">
                                                        <label for="date[<%=index%>][payments][{{$payment->id}}][min_parcel]">Min. Parc.</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="number" min="0" class="form-control" name="date[<%=index%>][payments][{{$payment->id}}][max_parcel]" value="{{ $payment->max_parcel }}">
                                                        <label for="date[<%=index%>][payments][{{$payment->id}}][max_parcel]">Max. Parc.</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 text-center">
                                                    <div class="form-group">
                                                        <div class="checkbox checkbox-inline checkbox-styled">
                                                            <label>
                                                                <input checked="checked" name="date[<%=index%>][payments][{{$payment->id}}][active]" type="checkbox" value="1">
                                                                <span>Ativo</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>
</script>
<script>
    function init_package()
    {
        $.applyDataMask();

        //$('[data-toggle=collapse]').collapse();

        $('.datepicker').datepicker(
        {
            autoclose: true,
            todayHighlight: true,
            format: "dd/mm/yyyy",
            language: "pt-BR",
            weekStart: 1,
            todayBtn: "linked"
        });

        $(".select2-city").select2({
            placeholder: "Procure por uma cidade",
            minimumInputLength: 2,
            ajax: {
                url: "{{ route('admin.general.cities.getcity') }}",
                type: "POST",
                dataType: 'json',
                delay: 250,
                data: function (term, page) {
                    return {
                        q: term
                    };
                },
                results: function (data, page) {
                    return {
                        results: data
                        {{--results: $.map(data, function (item) {
                            return {
                                text: item.name + ' - ' + item.abbreviation,
                                id: item.id
                            }
                        })--}}
                    };
                },
                cache: true
            },
            initSelection: function(element, callback) {
                var id = $(element).val();
                if (id !== "") {
                    $.post("{{ route('admin.general.cities.getcity') }}", {
                        id:id
                    }).done(function(data) {
                        callback(data);
                        $('#' + $(element).attr('id') + '-label').text(data.name + ' - ' + data.abbreviation);
                    });
                }
            },
            formatResult: repoFormatResult,
            formatSelection: repoFormatSelection,
            escapeMarkup: function (m) { return m; }
        });

        $(".select2-city").on("change", function(e) {
            var id = '#' + $(this).attr('id') + '-label';
            $(id).text(e.added.name + ' - ' + e.added.abbreviation);
        });

        $(".btn-delete-periodo-list-item").on("click", function(){
            if(confirm("Tem certeza que deseja excluir este período?"))
            {
                var item = '#periodo-list-item-' + $(this).data('item');
                $(item).hide("fast", function() {
                    $(item).remove();
                });
            }
        });
    }

    function getUrl()
    {
        $('#slug-category').html('<i class="fa fa-circle-o-notch fa-spin"></i>');

        var id = {{ ($item->id) ? $item->id : 'null' }};
        var name = $('#name').val();

        $.post( "{{ route('admin.tourism.packages.geturl') }}", {id:id, name:name}, function(data){
            $('#slug-category').text(data);
        });

    }

    function repoFormatResult(item) {
        return item.name + ' - ' + item.abbreviation;
    }

    function repoFormatSelection(item) {
        return item.name + ' - ' + item.abbreviation;
    }

    var crop_photo = null;

    Dropzone.autoDiscover = false;

    function initDropzone()
    {
        var destinationDropzone = new Dropzone("#package-dropzone", {
            url: "{{ route('admin.tourism.packages.uploadphoto', $item->id) }}",
            paramName: "file",
            maxFilesize: 5,
            method: "post",
            headers: { "X-CSRF-TOKEN": "{{ csrf_token() }}" },
            dictCancelUpload: 'Cancelar',
            success: function(file) {
                toastr.success('Nome: ' + file.name, 'Foto enviada!');
            },
            error: function(file) {
                toastr.error('Nome: ' + file.name, 'Falha ao enviar foto');
            },
            queuecomplete: function(){
                destinationDropzone.removeAllFiles(true);
                $('#dropzone-accordion-1').collapse('hide');
                getPhotos();
            }
        });

    }

    function getPhotos()
    {
        $.ajax("{{ route('admin.tourism.packages.getphotos', $item->id)  }}")
            .done(function( data ) {
                if(data.length > 0){
                    var date = new Date();
                    $('#box-package-photos').html( tmpl("photoTmpl", {photos:data, time:date.getTime()}) );
                    $('#dropzone-accordion-2').collapse('show');
                } else {
                    $('#box-package-photos').html( tmpl("emptyPhotosTmpl") );
                    $('#dropzone-accordion-1').collapse('show');
                }

            });
    }
</script>

@endpush

@push('scripts_ready')

init_package();

$(document).on('change keyup', '#name', function(){
    getUrl();
});

moment.locale('pt-br');

@if($item->id)
    $('#name').trigger('change');
    getPhotos();
    initDropzone();
@endif

if(mode !== 'show')
{
    $('#summernote').summernote({
        lang: 'pt-BR',
        height: '150px'
    });
}

$('[data-duplicate]').on('click', function(e) {
    var item = $(this);
    var templateId = item.data('duplicate');
    var target = $(item.data('target'));
    var index = (target.data('index') > 0) ? target.data('index') : target.children().length + 1;
    target.data('index', index + 1);
    var clonedContent = tmpl(templateId, {index: index});
    var newContent = $(clonedContent).appendTo(target).hide().slideDown('fast');
    init_package();
});

$( document ).on( "click", ".btn-crop-photo", function() {

    var data = $(this).data('photo');

    var img = $('#img-package-photo-'+data+' .dz-image img').attr('src');

    $('#box-crop-photo').html(tmpl("cropPhotoTmpl", {img:img}));

    $('#cropModal').modal('show');

    $('#image-crop-photo').cropper({
      aspectRatio: 4 / 3,
      preview: '.img-preview',
      crop: function(e) {
        crop_photo = e;
      }
    });

});

$( document ).on( "click", ".btn-cover-photo", function() {
    var data = $(this).data('photo');
    $.post( "{{route('admin.tourism.packages.coverphoto')}}", {data:data} )
        .done(function() {
            getPhotos();
            toastr.success('Foto alterada com sucesso!', 'Capa');
        });
});

$( document ).on( "click", ".btn-crop-save-photo", function() {
    //console.log(crop_photo);
    var data = {src: $('#image-crop-photo').attr('src'), y:crop_photo.y, x:crop_photo.x, width:crop_photo.width , height:crop_photo.height , rotate:crop_photo.rotate , scaleX:crop_photo.scaleX , scaleY:crop_photo.scaleY }
    $.post( "{{route('admin.tourism.packages.cropphoto')}}", {data:data} )
        .done(function() {
            getPhotos();
            $('#cropModal').modal('hide');
            toastr.success('Foto alterada com sucesso!', 'Recorte');
        });
});

$( document ).on( "click", ".btn-delete-photo", function() {

    if(confirm("Deseja realmente excluir esta imagem?"))
    {
        var data = $(this).data('photo');

        console.log(data);

        $.post( "{{route('admin.tourism.packages.deletephoto')}}", {data:data} )
            .done(function() {
                $('#img-package-photo-'+data).empty();
                getPhotos();
            });

    }
});

@endpush