@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-md">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? 'Criando nova atração' : (($mode == 'edit') ? 'Editando Atração' : 'Visualizando atração') }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body no-padding style-primary-bright">
                                <div class="row">
                                    <div class="row-height">
                                        <div class="col-md-8 col-md-push-4 col-md-height col-md-top">
                                            <div class="row-height">
                                                <div class="force-padding style-default-bright col-md-height">
                                                    <div class="form-group floating-label{{ $errors->first('destination_id', ' has-error') }}">
                                                        <label for="view">Destino</label>
                                                        {!! Form::selectEmpty('destination_id', $destinations, Input::old('destination_id', $item->destination_id), ['class' => 'form-control', 'id' => 'destination_id']) !!}
                                                        <span class="help-block">{{ $errors->first('destination_id', ':message') }}</span>
                                                    </div>
                                                    <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                                        <input type="text" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                                        <label for="name">Nome</label>
                                                        <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                                    </div>
                                                    <div class="form-group floating-label{{ $errors->first('description', ' has-error') }}">
                                                        <textarea class="form-control autosize" name="description" id="description">{{ Input::old('description', $item->description) }}</textarea>
                                                        <label for="description">Descrição</label>
                                                        <span class="help-block">{{ $errors->first('description', ':message') }}</span>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-md-pull-8 col-md-height col-md-top">
                                            <div class="force-padding">
                                                <div class="form-group floating-label{{ $errors->first('attraction_type_id', ' has-error') }}">
                                                    <label for="view">Tipo de atração</label>
                                                    {!! Form::selectEmpty('attraction_type_id', $attraction_types, Input::old('attraction_type_id', $item->attraction_type_id), ['class' => 'form-control', 'id' => 'attraction_type_id']) !!}
                                                    <span class="help-block">{{ $errors->first('attraction_type_id', ':message') }}</span>
                                                </div>
                                                <div class="form-group floating-label">
                                                    <div class="holder">
                                                        <div class="overlay overlay-shade-top">
                                                            <strong class="text-default-bright">IMAGEM DESTACADA</strong>
                                                        </div>
                                                        <input type="hidden" id="image-1" class="img-image-up" data-image="1" name="image" value="{{ ($item->image) ? asset($path_image.$item->image) : null }}">
                                                        <img id="img-image-up-preview-1" class="img-responsive" src="{{ ($item->image) ? asset($path_image.$item->image) : asset('assets/admin/img/image-default.png') }}?{{ date('U') }}">
                                                        @if($item->image)
                                                        <button type="button" data-image="1" id="btn-image-up-delete-1" class="btn ink-reaction btn-floating-action btn-xs btn-danger stick-top-right btn-image-up-delete" data-toggle="tooltip" data-placement="top" title="Remover"><i class="md md-delete"></i></button>
                                                        @endif
                                                    </div>
                                                    <input type="file" data-image="1" class="btn btn-sm btn-block btn-accent-dark ink-reaction btn-image-up-file" placeholder="Enviar Imagem" />
                                                </div>
                                                <div class="form-group">
                                                    <div class="checkbox checkbox-inline checkbox-styled">
                                                        <label>
                                                            {!! Form::checkbox('active', true, Input::old('active', $item->active)) !!} <span>Ativo</span>
                                                        </label>
                                                    </div>
                                                    <label for="active">Status</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection

@push('styles')
<link href="{{ asset('assets/admin/js/bower_components/cropper/dist/cropper.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{ asset('assets/admin/js/bower_components/cropper/dist/cropper.min.js') }}"></script>
<script>

    function loadImageFile(input) {
        var i = $(input).data('image');
        var image = $('#img-image-up-preview-'+i);
        image.cropper('destroy');
        if (input.files && input.files[0]) {

            var reader = new FileReader(), rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

            reader.onload = function (e) {
                image.attr('src', e.target.result);
                image.cropper({
                    aspectRatio: 4 / 3,
                    crop: function(e) {
                        $('#image-'+i).val(image.cropper('getCroppedCanvas').toDataURL());
                    }
                });
            };

            if (!rFilter.test(input.files[0].type)) {
                toastr.error('Você deve selecionar um arquivo de imagem válido!');
                return;
            }

            reader.readAsDataURL(input.files[0]);

            $('#btn-image-up-delete-'+i).hide();

        } else {
            $('#image-'+i).val('').trigger('change');;
        }
    }
</script>
@endpush

@push('scripts_ready')

$(".btn-image-up-file").change(function(){
    loadImageFile(this);
});

$(".img-image-up").on('change', function(){
    var i = $(this).data('image');
    if(this.value == ''){
        $('#img-image-up-preview-'+i).attr('src', '{{ asset('assets/admin/img/image-default.png') }}');
        $('#btn-image-up-delete-'+i).hide();
    }
});

$('.btn-image-up-delete').on('click', function(){
    var i = $(this).data('image');
    $('#image-'+i).val('').trigger('change');
});

@endpush