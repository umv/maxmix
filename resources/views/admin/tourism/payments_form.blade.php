@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-sm">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-4">
                    <h4>Dados do pagamento</h4>
                    @if( $mode === 'create' || $mode === 'edit' )
                    <article class="margin-bottom-xxl">
                        <p>
                            Preencha com as informações do método de pagamento.
                        </p>
                    </article>
                    @endif
                </div>
                <div class="col-lg-offset-1 col-sm-8">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body">
                                <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                    <input type="text" data-integra="slug" data-target="#slug" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                    <label for="name">Nome</label>
                                    <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                </div>
                                <p class="text-muted">
                                    <strong>VALORES PADRÃO</strong>
                                </p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('input_value', ' has-error') }}">
                                            <input type="text" class="form-control" data-inputmask="'mask': '99,99%'" name="input_value" id="input_value" value="{{ Input::old('input_value', $item->input_value) }}">
                                            <label for="input_value">Porcent. de entrada</label>
                                            <span class="help-block">{{ $errors->first('input_value', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('discount_value', ' has-error') }}">
                                            <input type="text" class="form-control" data-inputmask="'mask': '99,99%'" name="discount_value" id="discount_value" value="{{ Input::old('discount_value', $item->discount_value) }}">
                                            <label for="discount_value">Porcent. de desconto</label>
                                            <span class="help-block">{{ $errors->first('discount_value', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('min_parcel', ' has-error') }}">
                                            <input type="number" min="0" class="form-control" name="min_parcel" id="min_parcel" value="{{ Input::old('min_parcel', $item->min_parcel) }}">
                                            <label for="min_parcel">Min. Parcelas</label>
                                            <span class="help-block">{{ $errors->first('min_parcel', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('max_parcel', ' has-error') }}">
                                            <input type="number" min="0" class="form-control" name="max_parcel" id="max_parcel" value="{{ Input::old('max_parcel', $item->max_parcel) }}">
                                            <label for="max_parcel">Max. Parcelas</label>
                                            <span class="help-block">{{ $errors->first('max_parcel', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox checkbox-inline checkbox-styled">
                                        <label>
                                            {!! Form::checkbox('active', true, Input::old('active', $item->active)) !!} <span>Ativo</span>
                                        </label>
                                    </div>
                                    <label for="active">Status</label>
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection

@push('scripts')
<script src="{{asset('assets/admin/js/libs/inputmask/jquery.inputmask.bundle.min.js')}}"></script>
@endpush

@push('scripts_ready')
$(":input").inputmask();
@endpush