@extends('admin.layouts.master')

@push('styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/libs/bootstrap-datepicker/datepicker3.css')}}" />
@endpush

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-lg">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                            <input type="text" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                            <label for="name">Nome</label>
                                            <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('email', ' has-error') }}">
                                            <input type="text" class="form-control" name="email" id="email" value="{{ Input::old('email', $item->email) }}">
                                            <label for="email">E-mail</label>
                                            <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('phone', ' has-error') }}">
                                            <input type="text" class="form-control" name="phone" id="phone" value="{{ Input::old('phone', $item->phone) }}">
                                            <label for="phone">Telefone</label>
                                            <span class="help-block">{{ $errors->first('phone', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('cell_phone', ' has-error') }}">
                                            <input type="text" class="form-control" name="cell_phone" id="cell_phone" value="{{ Input::old('cell_phone', $item->cell_phone) }}">
                                            <label for="cell_phone">Celular</label>
                                            <span class="help-block">{{ $errors->first('cell_phone', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('date_departure', ' has-error') }}">
                                            <input type="text" class="form-control datepicker" name="date_departure" id="date_departure" value="{{ Input::old('date_departure', date('d/m/Y', strtotime($item->date_departure)) ) }}">
                                            <label for="date_departure">Data de ida</label>
                                            <span class="help-block">{{ $errors->first('date_departure', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('date_arrival', ' has-error') }}">
                                            <input type="text" class="form-control datepicker" name="date_arrival" id="date_arrival" value="{{ Input::old('date_arrival', date('d/m/Y', strtotime($item->date_arrival)) ) }}">
                                            <label for="date_arrival">Date de volta</label>
                                            <span class="help-block">{{ $errors->first('date_arrival', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group floating-label{{ $errors->first('amount_adult', ' has-error') }}">
                                            <input type="text" class="form-control" name="amount_adult" id="amount_adult" value="{{ Input::old('amount_adult', $item->amount_adult) }}">
                                            <label for="amount_adult">Adultos</label>
                                            <span class="help-block">{{ $errors->first('amount_adult', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group floating-label{{ $errors->first('amount_child', ' has-error') }}">
                                            <input type="text" class="form-control" name="amount_child" id="amount_child" value="{{ Input::old('amount_child', $item->amount_child) }}">
                                            <label for="amount_child">Crianças</label>
                                            <span class="help-block">{{ $errors->first('amount_child', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group floating-label{{ $errors->first('amount_baby', ' has-error') }}">
                                            <input type="text" class="form-control" name="amount_baby" id="amount_baby" value="{{ Input::old('amount_baby', $item->amount_baby) }}">
                                            <label for="amount_baby">Bebês</label>
                                            <span class="help-block">{{ $errors->first('amount_baby', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-inline checkbox-styled">
                                                <label>
                                                    {!! Form::checkbox('active', true, Input::old('active', $item->active)) !!} <span>Verificado</span>
                                                </label>
                                            </div>
                                            <label for="active">Status</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="card @if($mode === 'show') card-bordered form @endif">
                        <div class="card-body">
                            @if($item->origin_id)<p>Comos nos conheceu: <strong>{{ $item->origin->name }}</strong></p><hr>@endif
                            @if($item->destination_id || $item->destinantion_name)
                            <h4>Destino</h4>
                            {{ ($item->destination_id) ? $item->destination->name : $item->destination_name }}
                            @endif
                            <hr>
                            @if($item->date_id)
                            <h4>Pacote</h4>
                            {{ $item->date->package->name }}
                            <br>
                            {{ date('D d M', strtotime($item->date->date_departure)) }} - {{ date('D d M', strtotime($item->date->date_arrival)) }}
                            <br>
                            Saindo de {{ $item->date->city->name }} - {{ $item->date->city->state->abbreviation }}
                            <br><br>
                            <b>R$ {{ number_format((($item->date->price_promotional) ? $item->date->price_promotional : $item->date->price_default),2,',','.') }}</b>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection

@push('scripts')
<script src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.js')}}"></script>
@endpush

@push('scripts_ready')
$('.datepicker').datepicker({
    autoclose: true,
    todayHighlight: true,
    format: "dd/mm/yyyy",
    language: "pt-BR",
    weekStart: 1,
    todayBtn: "linked"
});
@endpush