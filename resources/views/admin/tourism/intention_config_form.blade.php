@extends('admin.layouts.master')

@push('styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/libs/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" />
@endpush

@section('content')

    <section>

        <div class="section-body contain-md">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="text-primary pull-left">
                        {{ trans('admin/controllers.'.$config->getRouteSlug().'.title') }}
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('email_name', ' has-error') }}">
                                            <input type="text" class="form-control" name="email_name" id="email_name" value="{{ Input::old('email_name', $item->email_name) }}">
                                            <label for="email_name">Nome do e-mail de envio</label>
                                            <span class="help-block">{{ $errors->first('email_name', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('email_sender', ' has-error') }}">
                                            <input type="text" class="form-control" name="email_sender" id="email_sender" value="{{ Input::old('email_sender', $item->email_sender) }}">
                                            <label for="email_sender">E-mail de envio</label>
                                            <span class="help-block">{{ $errors->first('email_sender', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('subject', ' has-error') }}">
                                            <input type="text" class="form-control" name="subject" id="subject" value="{{ Input::old('subject', $item->subject) }}">
                                            <label for="subject">Assunto da mensagem</label>
                                            <span class="help-block">{{ $errors->first('subject', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('template', ' has-error') }}">
                                            <input type="text" class="form-control" name="template" id="template" value="{{ Input::old('template', $item->template) }}">
                                            <label for="template">Template do e-mail</label>
                                            <span class="help-block">{{ $errors->first('template', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('senders', ' has-error') }}">
                                    <input type="text" data-role="tagsinput" class="form-control" name="senders" id="senders" value="{{ Input::old('senders', $item->senders) }}">
                                    <label for="senders">Destinatários</label>
                                    <span class="help-block">{{ $errors->first('senders', ':message') }}</span>
                                </div>

                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection

@push('scripts')
<script src="{{asset('assets/admin/js/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
@endpush