@extends('admin.layouts.master')

@push('styles')
<link href="{{ asset('assets/admin/js/bower_components/cropper/dist/cropper.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-md">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body no-padding style-primary-bright">
                                <div class="row">
                                    <div class="row-height">
                                        <div class="col-md-8 col-md-push-4 col-md-height col-md-top">
                                            <div class="row-height">
                                                <div class="force-padding style-default-bright col-md-height">
                                                    <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                                        <input type="text" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                                        <label for="name">Nome do serviço</label>
                                                        <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                                    </div>
                                                    <div class="form-group floating-label{{ $errors->first('description', ' has-error') }}">
                                                        <textarea class="form-control autosize" name="description" id="description">{{ Input::old('description', $item->description) }}</textarea>
                                                        <label for="description">Descrição</label>
                                                        <span class="help-block">{{ $errors->first('description', ':message') }}</span>
                                                    </div>
                                                    <div class="form-group floating-label{{ $errors->first('title', ' has-error') }}">
                                                        <input type="text" class="form-control" name="title" id="title" value="{{ Input::old('title', $item->title) }}">
                                                        <label for="title">Título do filtro</label>
                                                        <span class="help-block">{{ $errors->first('title', ':message') }}</span>
                                                    </div>
                                                    <div class="style-default-light force-padding">
                                                        <label>FILTROS</label>
                                                        <br>
                                                        <ul class="list-unstyled" id="filter-list">
                                                        @foreach($item->filters as $key => $filter)
                                                            {{--*/
                                                                $key = $key+1;
                                                            /*--}}
                                                            <li id="filter-list-item-{{$key}}" class="clearfix" style="display: list-item;">
                                                                <div class="form-group {{ $errors->first('name', ' has-error') }}">
                                                                    <input type="hidden" name="filters[{{$key}}][id]" value="{{ Input::old("filters[{$key}][id]", $filter->id) }}">
                                                                    <input type="text" class="form-control" name="filters[{{$key}}][name]" value="{{ Input::old("filters[{$key}][name]", $filter->name) }}">
                                                                    <label for="filters[{{$key}}][name]">#{{$key}}</label>
                                                                    <span class="help-block">{{ $errors->first("filters[{$key}][name]", ':message') }}</span>
                                                                    <button type="button" data-item="{{$key}}" class="btn ink-reaction btn-floating-action btn-xs btn-danger stick-top-right btn-delete-filter-list-item"><i class="md md-delete"></i></button>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                        </ul>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn-raised btn-default-bright" data-duplicate="filterTmpl" data-target="#filter-list">ADICIONAR NOVO FILTRO</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-md-pull-8 col-md-height col-md-top">
                                            <div class="force-padding">
                                                <div class="form-group floating-label">
                                                    <div class="holder style-default-bright">
                                                        <div class="overlay overlay-shade-top">
                                                            <strong class="text-default-bright">ÍCONE</strong>
                                                        </div>
                                                        <input type="hidden" id="image-1" class="img-image-up" data-image="1" name="image" value="{{ ($item->image) ? asset($path_image.$item->image) : null }}">
                                                        <img id="img-image-up-preview-1" class="img-responsive" src="{{ ($item->image) ? asset($path_image.$item->image) : asset('assets/admin/img/image-default.png') }}?{{ date('U') }}">
                                                        @if($item->image)
                                                        <button type="button" data-image="1" id="btn-image-up-delete-1" class="btn ink-reaction btn-floating-action btn-xs btn-danger stick-top-right btn-image-up-delete" data-toggle="tooltip" data-placement="top" title="Remover"><i class="md md-delete"></i></button>
                                                        @endif
                                                    </div>
                                                    <input type="file" data-image="1" class="btn btn-sm btn-block btn-accent-dark ink-reaction btn-image-up-file" placeholder="Enviar Imagem" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection

@push('scripts')
<script src="{{ asset('assets/admin/js/bower_components/cropper/dist/cropper.min.js') }}"></script>
<script src="{{asset('assets/admin/js/libs/microtemplating/microtemplating.min.js')}}"></script>
<script type="text/html" id="filterTmpl">
    <li id="filter-list-item-<%=index%>" class="clearfix" style="display: list-item;">
        <div class="form-group">
            <input type="hidden" name="filters[<%=index%>][id]" }}">
            <input type="text" class="form-control" name="filters[<%=index%>][name]">
            <label for="filters[<%=index%>][name]">#<%=index%></label>
            <button type="button" data-item="<%=index%>" class="btn ink-reaction btn-floating-action btn-xs btn-danger stick-top-right btn-delete-filter-list-item"><i class="md md-delete"></i></button>
        </div>
    </li>
</script>
<script>


    function loadImageFile(input) {
        var i = $(input).data('image');
        var image = $('#img-image-up-preview-'+i);
        image.cropper('destroy');
        if (input.files && input.files[0]) {

            var reader = new FileReader(), rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

            reader.onload = function (e) {
                image.attr('src', e.target.result);
                image.cropper({
                    autoCropArea: 1,
                    //aspectRatio: 1 / 1,
                    crop: function(e) {
                        $('#image-'+i).val(image.cropper('getCroppedCanvas').toDataURL());
                    }
                });
            };

            if (!rFilter.test(input.files[0].type)) {
                toastr.error('Você deve selecionar um arquivo de imagem válido!');
                return;
            }

            reader.readAsDataURL(input.files[0]);

            $('#btn-image-up-delete-'+i).hide();

        } else {
            $('#image-'+i).val('').trigger('change');;
        }
    }
</script>
@endpush

@push('scripts_ready')

$(".btn-image-up-file").change(function(){
    loadImageFile(this);
});

$(".img-image-up").on('change', function(){
    var i = $(this).data('image');
    if(this.value == ''){
        $('#img-image-up-preview-'+i).attr('src', '{{ asset('assets/admin/img/image-default.png') }}');
        $('#btn-image-up-delete-'+i).hide();
    }
});

$('.btn-image-up-delete').on('click', function(){
    var i = $(this).data('image');
    $('#image-'+i).val('').trigger('change');
});

$('[data-duplicate]').on('click', function(e) {
    var item = $(this);
    var templateId = item.data('duplicate');
    var target = $(item.data('target'));
    var index = (target.data('index') > 0) ? target.data('index') : target.children().length + 1;
    target.data('index', index + 1);
    var clonedContent = tmpl(templateId, {index: index});
    var newContent = $(clonedContent).appendTo(target).hide().slideDown('fast');
});

$(".btn-delete-filter-list-item").on("click", function(){
    if(confirm("Tem certeza que deseja excluir este filtro?"))
    {
        var item = '#filter-list-item-' + $(this).data('item');
        $(item).hide("fast", function() {
            $(item).remove();
        });
    }
});

@endpush