@extends('admin.layouts.master')

@push('styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/libs/dropzone/dropzone-theme.css')}}" />
<link href="{{ asset('assets/admin/js/bower_components/cropper/dist/cropper.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-lg">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="text-primary pull-left">{{ ($mode == 'create') ? 'Criando novo destino' : (($mode == 'edit') ? 'Editando Destino' : 'Visualizando destino') }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        @if( $mode === 'create' || $mode === 'edit' )
                            <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                                @endif
                                @if( $mode !== 'create' )
                                    <input name="_method" type="hidden" value="PUT">
                                @endif
                                {!! csrf_field() !!}
                                <div class="card @if($mode === 'show') card-bordered form @endif">
                                    <div class="card-body style-primary-dark form-inverse">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group floating-label{{ $errors->first('subregion_id', ' has-error') }}">
                                                            <label for="view">Sub-região</label>
                                                            {!! Form::select('subregion_id', $subregions, Input::old('subregion_id', $item->subregion_id), ['class' => 'form-control select2', 'id' => 'subregion_id']) !!}
                                                            <span class="help-block">{{ $errors->first('subregion_id', ':message') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                                            <input type="text" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                                            <label for="name">Nome</label>
                                                            <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>URL</label>
                                                            <p id="slug-destination" class="form-control-static input-sm"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div class="checkbox checkbox-inline checkbox-styled">
                                                                <label>
                                                                    {!! Form::checkbox('active', true, Input::old('active', $item->active)) !!} <span>Ativo</span>
                                                                </label>
                                                            </div>
                                                            <label for="active">Status</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-head style-primary-dark">
                                        <ul class="nav nav-tabs tabs-text-contrast tabs-accent-dark" data-toggle="tabs">
                                            <li class="active"><a href="#tab-about">SOBRE</a></li>
                                            <li><a href="#tab-when-to-go">QUANDO IR</a></li>
                                            <li><a href="#tab-what-to-do">O QUE FAZER</a></li>
                                        </ul>
                                    </div>
                                    <div class="card-body tab-content">
                                        <div class="tab-pane active" id="tab-about">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group floating-label{{ $errors->first('about', ' has-error') }}">
                                                        <textarea class="form-control autosize" name="about" id="about">{{ Input::old('about', $item->about) }}</textarea>
                                                        <label for="about">Sobre</label>
                                                        <span class="help-block">{{ $errors->first('about', ':message') }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab-when-to-go">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group floating-label{{ $errors->first('when_to_go', ' has-error') }}">
                                                        <textarea class="form-control autosize" name="when_to_go" id="when_to_go">{{ Input::old('when_to_go', $item->when_to_go) }}</textarea>
                                                        <label for="when_to_go">Quando ir</label>
                                                        <span class="help-block">{{ $errors->first('when_to_go', ':message') }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab-what-to-do">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group floating-label{{ $errors->first('what_to_do', ' has-error') }}">
                                                        <textarea class="form-control autosize" name="what_to_do" id="what_to_do">{{ Input::old('what_to_do', $item->what_to_do) }}</textarea>
                                                        <label for="what_to_do">O que fazer</label>
                                                        <span class="help-block">{{ $errors->first('what_to_do', ':message') }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if( $mode === 'create' || $mode === 'edit' )
                                        <div class="card-actionbar style-default-light action-bar">
                                            <div class="card-actionbar-row">
                                                <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                            </div>
                                        </div>
                                        @endif
                                                <!-- END FORM FOOTER -->
                                </div><!--end .card -->
                                @if( $mode === 'create' || $mode === 'edit' )
                            </form>
                        @endif
                    </div>
                </div>

            </div>
            <div class="col-md-6">

                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="text-primary">Galeria</h2>
                        @if( $mode !== 'create' )
                        <div class="panel-group" id="dropzone-accordion" role="tablist" aria-multiselectable="true">
                            <div class="card panel">
                                <div class="card-head collapsed" data-toggle="collapse" data-parent="#dropzone-accordion" data-target="#dropzone-accordion-1">
                                    <header>Adicionar</header>
                                    <div class="tools">
                                        <a class="btn btn-icon-toggle"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                                <div id="dropzone-accordion-1" class="collapse">
                                    <div class="card-body">
                                        <form action="{{ route('admin.tourism.destinations.uploadphoto', $item->id) }}" class="dropzone" id="destination-dropzone" enctype="multipart/form-data">
                                            <div class="dz-message">
                                                <h3>Solte os arquivos aqui ou clique para enviar.</h3>
                                                <p class="text-xxxl"><i class="md md-cloud-upload text-primary"></i></p>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card panel">
                                <div class="card-head collapsed" data-toggle="collapse" data-parent="#dropzone-accordion" data-target="#dropzone-accordion-2">
                                    <header>Fotos</header>
                                    <div class="tools">
                                        <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                    </div>
                                </div>
                                <div id="dropzone-accordion-2" class="collapse">
                                    <div class="card-body">
                                        <div id="box-destination-photos" class="dropzone">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                            <div class="card style-primary-bright">
                                <p class="text-xxxxl text-center"><i class="md md-image"></i></p>
                                <p class="text-muted text-center">
                                    Crie o destino para adicionar fotos na galeria.
                                </p>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>

    </section>

    <div class="modal fade" id="cropModal" tabindex="-1" role="dialog" aria-labelledby="cropModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="cropModalLabel">Editar imagem</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div id="box-crop-photo" class="size-12">

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="img-preview size-5 border-gray border-lg" style="float: left;  margin-right: 10px;  margin-bottom: 10px;  overflow: hidden;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-crop-save-photo">Salvar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

@endsection

@push('scripts')
<script src="{{asset('assets/admin/js/libs/dropzone/dropzone.min.js')}}"></script>
<script src="{{asset('assets/admin/js/bower_components/cropper/dist/cropper.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/microtemplating/microtemplating.min.js')}}"></script>
<script type="text/html" id="emptyPhotosTmpl">
    <p class="text-xxxxl text-center"><i class="md md-image"></i></p>
    <p class="text-muted text-center">
        Adicione fotos na galeria.
    </p>
</script>
<script type="text/html" id="cropPhotoTmpl">
    <img src="<%=img%>" id="image-crop-photo" class="img-responsive" />
</script>
<script type="text/html" id="photoTmpl">
    <% for ( var i = 0; i < photos.length; i++ ) { %>
    <div id="img-destination-photo-<%=photos[i].id%>" class="dz-preview dz-processing dz-image-preview dz-complete">
        <div class="dz-image">
            <img data-dz-thumbnail="" alt="<%=photos[i].slug%>" src="{{ asset($path_image) }}/<%=photos[i].slug%>?<%=time%>" style="max-height: 120px;">
        </div>
        <div class="dz-details">
            <div class="dz-size">
                <br>
                <% if(photos[i].cover == 0){ %>
                <button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-default btn-cover-photo" data-photo="<%=photos[i].id%>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Capa do álbum"><i class="md md-star-outline"></i></button>
                <% } else { %>
                <button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-warning" data-photo="<%=photos[i].id%>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Capa do álbum"><i class="md md-star"></i></button>
                <% } %>
                <button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-default-dark btn-crop-photo" data-photo="<%=photos[i].id%>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Recortar"><i class="md md-crop"></i></button>
                <button type="button" class="btn ink-reaction btn-floating-action btn-xs btn-danger btn-delete-photo" data-photo="<%=photos[i].id%>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Remover"><i class="md md-delete"></i></button>
            </div>
        </div>
    </div>
    <% } %>
</script>
<script>

    var crop_photo = null;

    Dropzone.autoDiscover = false;

    function initDropzone()
    {
        var destinationDropzone = new Dropzone("#destination-dropzone", {
            url: "{{ route('admin.tourism.destinations.uploadphoto', $item->id) }}",
            paramName: "file",
            maxFilesize: 5,
            method: "post",
            headers: { "X-CSRF-TOKEN": "{{ csrf_token() }}" },
            dictCancelUpload: 'Cancelar',
            success: function(file) {
                toastr.success('Nome: ' + file.name, 'Foto enviada!');
            },
            error: function(file) {
                toastr.error('Nome: ' + file.name, 'Falha ao enviar foto');
            },
            queuecomplete: function(){
                destinationDropzone.removeAllFiles(true);
                $('#dropzone-accordion-1').collapse('hide');
                getPhotos();
            }
        });

    }

    function getPhotos()
    {
        $.ajax("{{ route('admin.tourism.destinations.getphotos', $item->id)  }}")
            .done(function( data ) {
                if(data.length > 0){
                    var date = new Date();
                    $('#box-destination-photos').html( tmpl("photoTmpl", {photos:data, time:date.getTime()}) );
                    $('#dropzone-accordion-2').collapse('show');
                } else {
                    $('#box-destination-photos').html( tmpl("emptyPhotosTmpl") );
                    $('#dropzone-accordion-1').collapse('show');
                }

            });
    }

    function getUrl()
    {
        $('#slug-destination').html('<i class="fa fa-circle-o-notch fa-spin"></i>');

        var id = {{ ($item->id) ? $item->id : 'null' }};
        var subregion_id = $('#subregion_id').val();
        var name = $('#name').val();

        $.post( "{{ route('admin.tourism.destinations.geturl') }}", {id:id, subregion_id:subregion_id, name:name}, function(data){
            $('#slug-destination').text(data);
        });

    }

</script>
@endpush

@push('scripts_ready')

$(document).on('change keyup', '#subregion_id, #name', function(){
    getUrl();
});

@if($item->id)
$('#subregion_id').trigger('change');
getPhotos();
initDropzone();
@endif

$('#dropzone-accordion').on('hidden.bs.collapse', function (e) {
    console.log(e.target.id);
});

$( document ).on( "click", ".btn-crop-photo", function() {

    var data = $(this).data('photo');

    var img = $('#img-destination-photo-'+data+' .dz-image img').attr('src');

    $('#box-crop-photo').html(tmpl("cropPhotoTmpl", {img:img}));

    $('#cropModal').modal('show');

    $('#image-crop-photo').cropper({
      aspectRatio: 4 / 3,
      preview: '.img-preview',
      crop: function(e) {
        crop_photo = e;
      }
    });

});

$( document ).on( "click", ".btn-cover-photo", function() {
    var data = $(this).data('photo');
    $.post( "{{route('admin.tourism.destinations.coverphoto')}}", {data:data} )
        .done(function() {
            getPhotos();
        });
});

$( document ).on( "click", ".btn-crop-save-photo", function() {
    //console.log(crop_photo);
    var data = {src: $('#image-crop-photo').attr('src'), y:crop_photo.y, x:crop_photo.x, width:crop_photo.width , height:crop_photo.height , rotate:crop_photo.rotate , scaleX:crop_photo.scaleX , scaleY:crop_photo.scaleY }
    $.post( "{{route('admin.tourism.destinations.cropphoto')}}", {data:data} )
        .done(function() {
            getPhotos();
            $('#cropModal').modal('hide');
        });
});

$( document ).on( "click", ".btn-delete-photo", function() {

    if(confirm("Deseja realmente excluir esta imagem?"))
    {
        var data = $(this).data('photo');

        console.log(data);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.post( "{{route('admin.tourism.destinations.deletephoto')}}", {data:data} )
            .done(function() {
                $('#img-destination-photo-'+data).empty();
                getPhotos();
            });

    }
});

@endpush