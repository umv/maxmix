<!DOCTYPE html>
<html lang="en">
<head>
    <title>Integra - Alpha 5</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/materialadmin.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/bootstrap.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/materialadmin.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/font-awesome.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/material-design-iconic-font.min.css')}}" />
    <!-- END STYLESHEETS -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="{{asset('assets/admin/js/libs/utils/html5shiv.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/libs/utils/respond.min.js')}}"></script>
    <![endif]-->
</head>
<body class="menubar-hoverable header-fixed ">

<!-- BEGIN LOGIN SECTION -->
<section class="section-account">
    {{--<div class="img-backdrop" style="background-image: url('../../assets/admin/img/img16.jpg')"></div>
    <div class="spacer"></div>--}}
    @yield('conteudo')
</section>
<!-- END LOGIN SECTION -->

<!-- BEGIN JAVASCRIPT -->
<script src="{{asset('assets/admin/js/libs/jquery/jquery-1.11.2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/spin.js/spin.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/autosize/jquery.autosize.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/nanoscroller/jquery.nanoscroller.min.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/App.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppNavigation.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppOffcanvas.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppCard.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppForm.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppNavSearch.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppVendor.js')}}"></script>
<script src="{{asset('assets/admin/js/core/demo/Demo.js')}}"></script>
<!-- END JAVASCRIPT -->
@stack('scripts')
<script type="application/javascript">
    $( document ).ready(function() {

        @stack('scripts_ready')

    });
</script>
</body>


</html>
