@extends('admin.login.layout')

@section('conteudo')
    <br><br>
    <div class="width-9" style="margin: 0 auto;">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <img src="{{asset('assets/admin/img/logo.png')}}" alt="INTEGRA CMS" class="img-responsive" style="display: initial;">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">

                        <br/>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> Encontramos algums problemas com seus dados.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form floating-label" action="{{ route('login') }}" accept-charset="utf-8" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <input type="text" class="form-control" id="username" name="email" value="{{ old('email') }}">
                                <label for="username">Usuário</label>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password" name="password">
                                <label for="password">Senha</label>
                                <p class="help-block"><a href="{{ url('/password/email') }}">Esqueceu sua senha?</a></p>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-xs-6 text-left">
                                    <div class="checkbox checkbox-inline checkbox-styled">
                                        <label>
                                            <input type="checkbox" name="remember"> <span class="text-sm">Permanecer conectado</span>
                                        </label>
                                    </div>
                                </div><!--end .col -->
                                <div class="col-xs-6 text-right">
                                    <button class="btn btn-default-dark btn-raised" type="submit">Entrar</button>
                                </div><!--end .col -->
                            </div><!--end .row -->
                        </form>
                    </div><!--end .col -->
                    {{--<div class="col-sm-5 col-sm-offset-1 text-center">--}}
                    {{--<br><br>--}}
                    {{--<h3 class="text-light">--}}
                    {{--Não tem uma conta?--}}
                    {{--</h3>--}}
                    {{--<a class="btn btn-block btn-raised btn-primary" href="{{ url('/auth/register') }}">Registre-se aqui</a>--}}
                    {{--<br><br>--}}
                    {{--<h3 class="text-light">--}}
                    {{--ou--}}
                    {{--</h3>--}}
                    {{--<p>--}}
                    {{--<a href="#" class="btn btn-block btn-raised btn-info"><i class="fa fa-facebook pull-left"></i>Login com Facebook</a>--}}
                    {{--</p>--}}
                    {{--<p>--}}
                    {{--<a href="#" class="btn btn-block btn-raised btn-info"><i class="fa fa-twitter pull-left"></i>Login com Twitter</a>--}}
                    {{--</p>--}}
                    {{--</div><!--end .col -->--}}
                </div><!--end .row -->
            </div><!--end .card-body -->
        </div><!--end .card -->
    </div>

@endsection