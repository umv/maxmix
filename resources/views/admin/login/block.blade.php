@extends('admin.login.layout')

@section('conteudo')

    <div class="card contain-xs style-transparent">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <img class="img-circle" src="http://www.codecovers.eu/assets/img/modules/materialadmin/avatar1.jpg?1422538623" alt="">
                    <h2>Daniel Johnson</h2>
                    <form class="form" action="http://www.codecovers.eu/materialadmin/dashboards/dashboard" accept-charset="utf-8" method="post">
                        <div class="form-group floating-label">
                            <div class="input-group">
                                <div class="input-group-content">
                                    <input type="password" id="password" class="form-control" name="password">
                                    <label for="password">Password</label>
                                    <p class="help-block"><a href="#">Not Daniel Johnson?</a></p>
                                </div>
                                <div class="input-group-btn">
                                    <button class="btn btn-floating-action btn-primary" type="submit"><i class="fa fa-unlock"></i></button>
                                </div>
                            </div><!--end .input-group -->
                        </div><!--end .form-group -->
                    </form>
                </div><!--end .col -->
            </div><!--end .row -->
        </div><!--end .card-body -->
    </div>

@endsection