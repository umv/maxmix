<!DOCTYPE html>
<html lang="pt-BR">
<head>

    <title>@section('title')@show</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @stack('metas')
            <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    {{--*/ $theme = 1; /*--}}
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/bootstrap.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/materialadmin.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/font-awesome.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/material-design-iconic-font.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/libs/toastr/toastr.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-'.Sentinel::getUser()->theme.'/libs/select2/select2.css')}}" />
    @stack('styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/app.css')}}" />
    <!-- END STYLESHEETS -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="{{asset('assets/admin/js/libs/utils/html5shiv.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/libs/utils/respond.min.js')}}"></script>
    <![endif]-->
</head>
<body style="background: #20252b" class="">

<div class="row">
    <div class="card style-default-dark">
        <div class="card-head">
            <ul class="nav nav-tabs tabs-text-contrast tabs-primary-light" data-toggle="tabs">
                <li class="active"><a href="#filemanager-tab-all">Todos</a></li>
                <li><a href="#filemanager-tab-images">Imagens</a></li>
                <li><a href="#filemanager-tab-documents">Documentos</a></li>
            </ul>
        </div><!--end .card-head -->
        <div class="card-body tab-content">
            <div id="filemanager-tab-all" class="tab-pane">
                <h2>Todos os arquivos</h2>
                @foreach(Storage::allfiles() as $file)
                    {{ public_path($file) }}
                @endforeach
            </div>
            <div id="filemanager-tab-images" class="tab- active">
                <h2>Todas as imagens</h2>
                <div class="row">
                    @foreach(Storage::allfiles('images') as $file)
                        <div class="col-md-1">
                            <div class="holder">
                                <div class="overlay overlay-shade-top">
                                    <strong class="text-default-bright"><small>{{ $file }}</small></strong>
                                </div>
                                <img class="img-responsive" src="{{ asset('media/'.$file) }}"/>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div id="filemanager-tab-documents" class="tab-pane">
                <h2>Todos os documentos</h2>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('assets/admin/js/libs/jquery/jquery-1.11.2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/spin.js/spin.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/autosize/jquery.autosize.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/nanoscroller/jquery.nanoscroller.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/toastr/toastr.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/App.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppNavigation.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppOffcanvas.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppCard.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppForm.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppNavSearch.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppVendor.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/core/demo/Demo.js')}}"></script>
<script>
    toastr.options = {
        "closeButton": true,
        "progressBar": false,
        "debug": false,
        "positionClass": 'toast-bottom-left',
        "showDuration": 330,
        "hideDuration": 330,
        "timeOut":  5000,
        "extendedTimeOut": 1000,
        "showEasing": 'swing',
        "hideEasing": 'swing',
        "showMethod": 'slideDown',
        "hideMethod": 'slideUp',
        "onclick": null
    }
</script>
@stack('scripts')
<script type="application/javascript">
    $( document ).ready(function() {

        $(".select2").select2();

        var mode = '{{ (isset($mode)) ? $mode : ''}}';

        if(mode === 'show')
        {
            $('#content input, #content select, #content button, #content textarea').prop("disabled", true);
        }

        @if(Session::get('info')['save'])
        toastr.success('Salvo com sucesso!');
        @endif

        @stack('scripts_ready')

    });
</script>
</body>
</html>