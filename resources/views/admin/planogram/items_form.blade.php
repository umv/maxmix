@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-sm">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-4">
                    <h4>Dados do item</h4>
                    @if( $mode === 'create' || $mode === 'edit' )
                    <article class="margin-bottom-xxl">
                        <p>
                            Preencha com as informações do item.
                        </p>
                    </article>
                    @endif
                </div>
                <div class="col-lg-offset-1 col-sm-8">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body">
                                <div class="form-group floating-label{{ $errors->first('area_id', ' has-error') }}">
                                    <label for="view">Área</label>
                                    {!! Form::select('area_id', $areas, Input::old('area_id', $item->area_id), ['class' => 'form-control select2', 'id' => 'area_id']) !!}
                                    <span class="help-block">{{ $errors->first('area_id', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('subcategory_id', ' has-error') }}">
                                    <label for="subcategory_id">Subcategoria</label>
                                    {!! Form::select('subcategory_id', $subcategories, Input::old('subcategory_id', $item->subcategory_id), ['class' => 'form-control select2', 'id' => 'subcategory_id']) !!}
                                    <span class="help-block">{{ $errors->first('subcategory_id', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('product_id', ' has-error') }}">
                                    <label for="product_id">Produto</label>
                                    {!! Form::select('product_id', $products, Input::old('product_id', $item->product_id), ['class' => 'form-control select2', 'id' => 'product_id']) !!}
                                    <span class="help-block">{{ $errors->first('product_id', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('position', ' has-error') }}">
                                    <input type="number" min="1" class="form-control" name="position" id="position" value="{{ Input::old('position', $item->position) }}">
                                    <label for="position">Posição</label>
                                    <span class="help-block">{{ $errors->first('position', ':message') }}</span>
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection