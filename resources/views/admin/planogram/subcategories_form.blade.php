@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-md">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group floating-label{{ $errors->first('category_id', ' has-error') }}">
                                            <label for="view">Categorias</label>
                                            {!! Form::select('category_id', $categories, Input::old('category_id', $item->category_id), ['class' => 'form-control select2', 'id' => 'category_id']) !!}
                                            <span class="help-block">{{ $errors->first('category_id', ':message') }}</span>
                                        </div>
                                        <div class="form-group floating-label{{ $errors->first('name', ' has-error') }}">
                                            <input type="text" data-integra="slug" data-target="#slug" class="form-control" name="name" id="name" value="{{ Input::old('name', $item->name) }}">
                                            <label for="name">Nome</label>
                                            <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                        </div>
                                        <div class="form-group floating-label{{ $errors->first('link', ' has-error') }}">
                                            <input type="text" class="form-control" name="link" id="link" value="{{ Input::old('link', $item->link) }}">
                                            <label for="link">Link para compra</label>
                                            <span class="help-block">{{ $errors->first('link', ':message') }}</span>
                                        </div>
                                        <div class="form-group">
                                            <div class="checkbox checkbox-inline checkbox-styled">
                                                <label>
                                                    {!! Form::checkbox('active', true, Input::old('active', $item->active)) !!} <span>Ativo</span>
                                                </label>
                                            </div>
                                            <label for="active">Status</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group floating-label">
                                            <div class="holder" >
                                                <div class="overlay overlay-shade-top">
                                                    <strong class="text-default-bright">DICA</strong>
                                                </div>
                                                <input type="hidden" id="tip-1" class="img-tip-up" data-image="1" name="tip" value="{{ ($item->tip) ? asset(Integra::public_path(Config::get('integra.planogram.tip.path')).$item->tip) : null }}">
                                                <img id="img-tip-up-preview-1" style="max-height: 420px; margin: 0 auto;" class="img-responsive text-center" src="{{ ($item->tip) ? asset(Integra::public_path(Config::get('integra.planogram.tip.path')).$item->tip) : asset('assets/admin/img/image-default.png') }}?{{ date('U') }}">
                                                @if($item->tip)
                                                    <button type="button" data-image="1" id="btn-tip-up-delete-1" class="btn ink-reaction btn-floating-action btn-xs btn-danger stick-top-right btn-tip-up-delete" data-toggle="tooltip" data-placement="top" title="Remover"><i class="md md-delete"></i></button>
                                                @endif
                                            </div>
                                            <input type="file" data-image="1" class="btn btn-sm btn-block btn-accent-dark ink-reaction btn-tip-up-file" placeholder="Enviar Imagem" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection

@push('styles')
<link href="{{ asset('assets/admin/js/bower_components/cropper/dist/cropper.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{ asset('assets/admin/js/bower_components/cropper/dist/cropper.min.js') }}"></script>
<script>

    function loadImageFile(input) {
        var i = $(input).data('image');
        var image = $('#img-tip-up-preview-'+i);
        image.cropper('destroy');
        if (input.files && input.files[0]) {

            var reader = new FileReader(), rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

            reader.onload = function (e) {
                image.attr('src', e.target.result);
                image.cropper({
                    viewMode : 2,
                    autoCropArea: 1,
                    crop: function(e) {
                        $('#tip-'+i).val(image.cropper('getCroppedCanvas').toDataURL());
                    }
                });
            };

            if (!rFilter.test(input.files[0].type)) {
                toastr.error('Você deve selecionar um arquivo de imagem válido!');
                return;
            }

            reader.readAsDataURL(input.files[0]);

            $('#btn-tip-up-delete-'+i).hide();

        } else {
            $('#tip-'+i).val('').trigger('change');;
        }
    }
</script>
@endpush

@push('scripts_ready')

$(".btn-tip-up-file").change(function(){
loadImageFile(this);
});

$(".img-tip-up").on('change', function(){
var i = $(this).data('image');
if(this.value == ''){
$('#img-tip-up-preview-'+i).attr('src', '{{ asset('assets/admin/img/image-default.png') }}');
$('#btn-tip-up-delete-'+i).hide();
}
});

$('.btn-tip-up-delete').on('click', function(){
var i = $(this).data('image');
$('#tip-'+i).val('').trigger('change');
});

@endpush