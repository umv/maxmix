@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-lg">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->code : null }}</small></h2>
                </div>
            </div>
            <div class="card card-bordered form ">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="{{ Integra::public_uri($item->getSrc()) }}" alt="Planograma" class="img-responsive">
                        </div>
                        <div class="col-md-6">
                            <table class="table">
                                <tr>
                                    <td>Subcategoria</td>
                                    <td><h3>{{ $item->subcategory->name }}</h3></td>
                                </tr>
                                <tr>
                                    <td>Estado</td>
                                    <td><h4>{{ $item->state->name }}</h4></td>
                                </tr>
                                <tr>
                                    <td>Tamanho da loja</td>
                                    <td><h5>{{ $item->getStoreSize() }}</h5></td>
                                </tr>
                                <tr>
                                    <td>Usuário</td>
                                    <td><h6>{{ $item->user->name }}</h6></td>
                                </tr>
                                <tr>
                                    <td>Gerado em</td>
                                    <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection