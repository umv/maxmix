@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-sm">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ ($mode == 'create') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_create') : (($mode == 'edit') ? trans('admin/controllers.'.$config->getRouteSlug().'.mode_edit') : trans('admin/controllers.'.$config->getRouteSlug().'.mode_show')) }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->brand->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-4">
                    <h4>Dados da marca</h4>
                    @if( $mode === 'create' || $mode === 'edit' )
                    <article class="margin-bottom-xxl">
                        <p>
                            Preencha com as informações da marca.
                        </p>
                    </article>
                    @endif
                </div>
                <div class="col-lg-offset-1 col-sm-8">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ ($mode == 'create') ? route($config->getRouteStore()) : route($config->getRouteUpdate(), $item->id) }}" autocomplete="off" class="form">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body">
                                <div class="form-group floating-label{{ $errors->first('subcategory_id', ' has-error') }}">
                                    <label for="view">Subcategoria</label>
                                    {!! Form::select('subcategory_id', $subcategories, Input::old('subcategory_id', $item->subcategory_id), ['class' => 'form-control select2', 'id' => 'subcategory_id']) !!}
                                    <span class="help-block">{{ $errors->first('subcategory_id', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label{{ $errors->first('brand_id', ' has-error') }}">
                                    <label for="brand_id">Marca</label>
                                    {!! Form::select('brand_id', $brands, Input::old('brand_id', $item->brand_id), ['class' => 'form-control select2', 'id' => 'brand_id']) !!}
                                    <span class="help-block">{{ $errors->first('brand_id', ':message') }}</span>
                                </div>
                                <div class="form-group floating-label">
                                    <div class="holder" >
                                        <div class="overlay overlay-shade-top">
                                            <strong class="text-default-bright">IMAGEM</strong>
                                        </div>
                                        <input type="hidden" id="image-1" class="img-image-up" data-image="1" name="image" value="{{ ($item->image) ? asset(Integra::public_path(Config::get('integra.planogram.brand.path')).$item->image) : null }}">
                                        <img id="img-image-up-preview-1" style="max-height: 180px; margin: 0 auto;" class="img-responsive text-center" src="{{ ($item->image) ? asset(Integra::public_path(Config::get('integra.planogram.brand.path')).$item->image) : asset('assets/admin/img/image-default.png') }}?{{ date('U') }}">
                                        @if($item->image)
                                            <button type="button" data-image="1" id="btn-image-up-delete-1" class="btn ink-reaction btn-floating-action btn-xs btn-danger stick-top-right btn-image-up-delete" data-toggle="tooltip" data-placement="top" title="Remover"><i class="md md-delete"></i></button>
                                        @endif
                                    </div>
                                    <input type="file" data-image="1" class="btn btn-sm btn-block btn-accent-dark ink-reaction btn-image-up-file" placeholder="Enviar Imagem" />
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">{{ ($mode == 'create') ? trans('admin/system.create') : (($mode == 'edit') ? trans('admin/system.update') : '') }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection

@push('styles')
<link href="{{ asset('assets/admin/js/bower_components/cropper/dist/cropper.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{ asset('assets/admin/js/bower_components/cropper/dist/cropper.min.js') }}"></script>
<script>

    function loadImageFile(input) {
        var i = $(input).data('image');
        var image = $('#img-image-up-preview-'+i);
        image.cropper('destroy');
        if (input.files && input.files[0]) {

            var reader = new FileReader(), rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

            reader.onload = function (e) {
                image.attr('src', e.target.result);
                image.cropper({
                    viewMode : 2,
                    autoCropArea: 1,
                    crop: function(e) {
                        $('#image-'+i).val(image.cropper('getCroppedCanvas').toDataURL());
                    }
                });
            };

            if (!rFilter.test(input.files[0].type)) {
                toastr.error('Você deve selecionar um arquivo de imagem válido!');
                return;
            }

            reader.readAsDataURL(input.files[0]);

            $('#btn-image-up-delete-'+i).hide();

        } else {
            $('#image-'+i).val('').trigger('change');;
        }
    }
</script>
@endpush

@push('scripts_ready')

$(".btn-image-up-file").change(function(){
    loadImageFile(this);
});

$(".img-image-up").on('change', function(){
    var i = $(this).data('image');
    if(this.value == ''){
        $('#img-image-up-preview-'+i).attr('src', '{{ asset('assets/admin/img/image-default.png') }}');
        $('#btn-image-up-delete-'+i).hide();
    }
});

$('.btn-image-up-delete').on('click', function(){
    var i = $(this).data('image');
    $('#image-'+i).val('').trigger('change');
});

@endpush