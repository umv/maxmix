@extends('admin.layouts.master')

@section('content')

    @include('admin.layouts.inc.content_header')

    <section>

        <div class="section-body contain-sm">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary pull-left">{{ trans('admin/controllers.'.$config->getRouteSlug().'.mode_import') }} <small>{{ ($mode === 'edit' || $mode === 'show') ? ' - '.$item->name : null }}</small></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-4">
                    <h4>Dados do Mix</h4>
                    @if( $mode === 'create' || $mode === 'edit' )
                    <article class="margin-bottom-xxl">
                        <p>
                            Selecione o arquivo CSV para importar os itens.
                        </p>
                    </article>
                    @endif
                </div>
                <div class="col-lg-offset-1 col-sm-8">
                    @if( $mode === 'create' || $mode === 'edit' )
                    <form method="post" action="{{ route($config->getRouteStore()) }}" autocomplete="off" class="form" enctype="multipart/form-data">
                    <input name="type" type="hidden" value="IMPORT">
                    @endif
                        @if( $mode !== 'create' )
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        {!! csrf_field() !!}
                        <div class="card @if($mode === 'show') card-bordered form @endif">
                            <div class="card-body">
                                <div class="form-group floating-label{{ $errors->first('area_id', ' has-error') }}">
                                    <label for="view">Área</label>
                                    {!! Form::select('area_id', $areas, Input::old('area_id', $item->area_id), ['class' => 'form-control select2', 'id' => 'area_id']) !!}
                                    <span class="help-block">{{ $errors->first('area_id', ':message') }}</span>
                                </div>
                                <div class="form-group {{ $errors->first('name', ' has-error') }}">
                                    <label for="name">Arquivo</label>
                                    <br>
                                    <input type="file" name="file" class="btn btn-sm btn-block btn-accent-dark ink-reaction btn-image-up-file" placeholder="Enviar arquivo" />
                                    <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                </div>
                            </div>
                            @if( $mode === 'create' || $mode === 'edit' )
                            <div class="card-actionbar style-default-light action-bar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-primary ink-reaction">Enviar</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    @if( $mode === 'create' || $mode === 'edit' )
                    </form>
                    @endif
                </div>
            </div>
        </div>

    </section>

@endsection