@extends($config['view'].'layouts.master')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Planograma</h2>
            </div>
        </div>
        <div class="card card-bordered form ">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <img src="{{ Integra::public_uri($item->getSrc()) }}" alt="Planograma" class="img-responsive">
                    </div>
                    <div class="col-md-6">
                        <table class="table">
                            <tr>
                                <td>Subcategoria</td>
                                <td><h3>{{ $item->subcategory->name }}</h3></td>
                            </tr>
                            <tr>
                                <td>Estado</td>
                                <td><h4>{{ $item->state->name }}</h4></td>
                            </tr>
                            <tr>
                                <td>Tamanho da loja</td>
                                <td><h5>{{ $item->getStoreSize() }}</h5></td>
                            </tr>
                            <tr>
                                <td>Gerado em</td>
                                <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection