@extends($config['view'].'layouts.master')

@section('content')

    <div class="container">

        <h1 class="title pull-left margin-0"><img src="{{ Integra::asset('imagens/icon-planogram-outline.png') }}" alt="Planograma" width="50" class="margin-r-10"> GERADOR DE PLANOGRAMA</h1>
        <a href="{{ url('/') }}" class="btn btn-link btn-xs pull-right hidden-xs"><br><i class="material-icons">arrow_back</i> Voltar</a>
        <div class="clearfix"></div>
        <hr>

        {{--<div class="media">
            <div class="media-left media-middle">
                <img src="{{ Integra::asset('imagens/icon-planogram-outline.png') }}" alt="Planograma" width="60">
            </div>

            <div class="media-body">
                <h1>
                    Planogramas
                </h1>
            </div>
            <div class="media-right">
                <a href="javascript:window.history.back();" class="btn btn-default btn-xs pull-right hidden-xs">
                    <i class="material-icons">arrow_back</i> {{ trans('sites/general.back') }}
                </a>
            </div>
        </div>
        <hr>--}}
    </div>

    <form name="frmPlanograma" id="frmPlanograma" class="frm-planograma" method="post" action="{{ route('sites.planogram.generator') }}">
        <div class="container">
            {!! csrf_field() !!}
            <input type="radio" id="loja_300_600" name="store_size" value="1" checked="checked" />

            {{--<fieldset>
                <section class="loja">
                    <legend>Tamanho da Loja</legend>

                    <div class="row">
                        <div class="col-md-6">
                            <h3>
                                Escolha o Tamanho da Loja
                            </h3>
                        </div>

                        <div class="col-md-6">
                            <div class="ctn-informacao hidden-xs">
                                <div class="img-informacao">i</div>
                                Escolha dentre as opções abaixo, o tamanho aproximado da sua loja.
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-4 text-center box-loja">
                            <div class="ctn-img">
                                <label for="loja_300_600">
                                    <img src="{{ Integra::asset('imagens/loja-entre-300m-600m.png') }}" alt="Entre 300 a 600m²" title="Entre 300 a 600m²" class="img-responsive" />
                                </label>
                            </div>

                            <div class="">
                                <input type="radio" id="loja_300_600" name="store_size" value="1" checked="checked" />
                                <label for="loja_300_600">
                                    <span><span></span></span><br>Entre<br>300 a 600m²
                                </label>
                            </div>
                        </div>

                        <div class="col-xs-4 text-center box-loja">
                            <div class="ctn-img">
                                <label for="loja_600_1000">
                                    <img src="{{ Integra::asset('imagens/loja-entre-600m-1000m.png') }}" alt="Entre 600 a 1.000m²" title="Entre 600 a 1.000m²" class="img-responsive" />
                                </label>
                            </div>

                            <div class="">
                                <input type="radio" id="loja_600_1000" name="store_size" value="2" />
                                <label for="loja_600_1000">
                                    <span><span></span></span><br>Entre<br>600 a 1.000m²
                                </label>
                            </div>
                        </div>

                        <div class="col-xs-4 text-center box-loja">
                            <div class="ctn-img">
                                <label for="loja_acima_1000">
                                    <img src="{{ Integra::asset('imagens/loja-acima-1000m.png') }}" alt="Acima de 1.000m²" title="Acima de 1.000m²" class="img-responsive" />
                                </label>
                            </div>

                            <div class="">
                                <input type="radio" id="loja_acima_1000" name="store_size" value="3" />
                                <label for="loja_acima_1000">
                                    <span><span></span></span><br>Acima de<br>1.000m²
                                </label>
                            </div>
                        </div>
                    </div>
                </section>
                <hr>
            </fieldset>--}}

            <fieldset>
                <section class="categoria">
                    <legend>Categoria</legend>

                    <div class="row">
                        <div class="col-md-6">
                            <h3>
                                Escolha a Categoria dos Produtos
                            </h3>
                        </div>

                        <div class="col-md-6">
                            <div class="ctn-informacao hidden-xs">
                                <div class="img-informacao">i</div>
                                Escolha na lista abaixo, a categoria do planograma que você deseja montar, ou se preferir, digite o nome no quadro ao lado.
                            </div>
                        </div>
                    </div>

                    <div class="ctn-categorias">
                        <div class="ctn-filtro">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control filter-input" data-target="#lista-categorias" placeholder="Filtre pela categoria" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">
                                                <i class="material-icons">search</i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="lista-categorias" class="lista-categorias scrollbar-custom filter-list">
                            <div class="row">
                                @foreach($categories as $category)
                                    <div class="col-md-6 filter-item">
                                        <input type="radio" id="category-{{ $category->id }}" name="category_id" value="{{ $category->id }}" />
                                        <label for="category-{{ $category->id }}">
                                            <span><span></span></span>{{ $category }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row">
                                @foreach($subcategories as $id => $subcategory)
                                    <div class="col-md-6 filter-item">
                                        <input type="radio" id="subcategory-{{ $id }}" name="subcategory_id" value="{{ $id }}" />
                                        <label for="subcategory-{{ $id }}">
                                            <span><span></span></span>{{ $subcategory }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>
                <hr>
            </fieldset>

            <fieldset>
                <section class="estado">
                    <legend>Estado</legend>

                    <div class="row">
                        <div class="col-md-6 ">
                            <h3>
                                Selecione o Estado em que se encontra seu Estabalecimento
                            </h3>
                        </div>

                        <div class="col-md-6">
                            <div class="ctn-informacao">
                                <div class="img-informacao">i</div>
                                Selecione na lista abaixo, o estado em que a sua loja está localizada, ou se preferir, digite o nome no quadro ao lado.
                            </div>
                        </div>
                    </div>

                    <div class="ctn-estados">
                        <div class="ctn-filtro">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control filter-input" data-target="#lista-estados" placeholder="Filtre pelo estado" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">
                                                <i class="material-icons">search</i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="lista-estados" class="lista-estados scrollbar-custom filter-list">
                            <div class="row">
                                @foreach($states as $id => $state)
                                    <div class="col-sm-6 col-md-4 col-lg-3 filter-item">
                                        <input type="radio" id="state-{{ $id }}" name="state_id" value="{{ $id }}" />
                                        <label for="state-{{ $id }}" class="upper">
                                            <span><span></span></span>{{ $state }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>
                <hr>
            </fieldset>

            <fieldset>
                <section class="quantidade-modulos">
                    <legend>Quantidade de módulos</legend>

                    <div class="row">
                        <div class="col-md-6">
                            <h3>
                                Escolha a quantidade de módulos
                            </h3>
                        </div>

                        <div class="col-md-6">
                            <div class="ctn-informacao hidden-xs">
                                <div class="img-informacao">i</div>
                                Escolha a quantidade de módulos que a a sua categoria possui.
                            </div>
                        </div>
                    </div>

                    <div class="ctn-modulos">
                        <div class="row">
                            <div class="col-xs-4 text-center">
                                <div class="box-quantidade-modulos">
                                    <div class="ctn-img">
                                        <label for="modulo_01">
                                            <img src="{{ Integra::asset('imagens/modulos-1.jpg') }}" alt="01 módulo" title="01 módulo" class="img-responsive" />
                                        </label>
                                    </div>

                                    <input type="radio" id="modulo_01" name="quantidade_modulos" value="1" checked="checked" />
                                    <label for="modulo_01">
                                        <span><span></span></span><br>01<br>módulo
                                    </label>
                                </div>

                            </div>

                            <div class="col-xs-4 text-center">
                                <div class="box-quantidade-modulos">
                                    <div class="ctn-img">
                                        <label for="modulo_02">
                                            <img src="{{ Integra::asset('imagens/modulos-2.jpg') }}" alt="02 módulos" title="02 módulos" class="img-responsive" />
                                        </label>
                                    </div>

                                    <input type="radio" id="modulo_02" name="quantidade_modulos" value="2" />
                                    <label for="modulo_02">
                                        <span><span></span></span><br>02<br>módulos
                                    </label>
                                </div>
                            </div>

                            <div class="col-xs-4 text-center">
                                <div class="box-quantidade-modulos">
                                    <div class="ctn-img">
                                        <label for="modulo_03">
                                            <img src="{{ Integra::asset('imagens/modulos-3.jpg') }}" alt="03 módulos" title="03 módulos" class="img-responsive" />
                                        </label>
                                    </div>
                                    <input type="radio" id="modulo_03" name="quantidade_modulos" value="3" />
                                    <label for="modulo_03">
                                        <span><span></span></span><br>03<br>módulos
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <hr>
            </fieldset>

            <fieldset>
                <section class="quantidade-prateleiras">
                    <legend>Qtd. de prateleiras e largura</legend>

                    <div class="row">
                        <div class="col-md-6">
                            <h3>
                                Defina a quantidade de prateleiras e largura do módulo
                            </h3>
                        </div>

                        <div class="col-md-6">
                            <div class="ctn-informacao hidden-xs">
                                <div class="img-informacao">i</div>
                                De acordo com a sua escolha anterior, defina agora quantas prateleiras a categoria que você está montando ocupa em cada módulo, e a seguir qual é a largura dessas prateleiras.
                            </div>
                        </div>
                    </div>

                    <div class="ctn-prateleiras">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="hidden-md hidden-lg">
                                    <h4>Quantidade de plateleiras</h4>
                                </div>

                                <div class="row">
                                    <div class="col-xs-4 text-center">
                                        <div class="box-quantidade-prateleiras prateleira_1">
                                            <div class="ctn-img">
                                                <label for="prateleira_modulo_1">
                                                    <img src="{{ Integra::asset('imagens/gondola-0-prateleiras.jpg') }}" id="img_prateleira_modulo_1" alt="Prateleira" title="" class="img-responsive" />
                                                </label>
                                            </div>

                                            <div class="text-center">
                                                <select name="gondola[1]" id="prateleira_modulo_1" attribute="img_prateleira_modulo_1" class="select-prateleira form-control required">
                                                    <option value="0">-Selecione-</option>
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-xs-4 text-center">
                                        <div class="box-quantidade-prateleiras prateleira_2">
                                            <div class="ctn-img">
                                                <label for="prateleira_modulo_2">
                                                    <img src="{{ Integra::asset('imagens/gondola-0-prateleiras.jpg') }}" id="img_prateleira_modulo_2" alt="Prateleira" title="" class="img-responsive" />
                                                </label>
                                            </div>

                                            <div class="text-center">
                                                <select name="gondola[2]" id="prateleira_modulo_2" attribute="img_prateleira_modulo_2" class="select-prateleira form-control required">
                                                    <option value="0">-Selecione-</option>
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-xs-4 text-center">
                                        <div class="box-quantidade-prateleiras prateleira_3">
                                            <div class="ctn-img">
                                                <label for="prateleira_modulo_3">
                                                    <img src="{{ Integra::asset('imagens/gondola-0-prateleiras.jpg') }}" id="img_prateleira_modulo_3" alt="Prateleira" title="" class="img-responsive" />
                                                </label>
                                            </div>

                                            <div class="text-center">
                                                <select name="gondola[3]" id="prateleira_modulo_3" attribute="img_prateleira_modulo_3" class="select-prateleira form-control required">
                                                    <option value="0">-Selecione-</option>
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 largura-gondula">
                                <div class="hidden-md hidden-lg">
                                    <h4>Dimensão das plateleiras</h4>
                                </div>
                                <div class="col-xs-6 text-center">
                                    <div class="ctn-img">
                                        <label for="largura_gondola_1">
                                            <img src="{{ Integra::asset('imagens/largura-gondola-1.jpg') }}" alt="1,00 m" title="1,00 m" class="img-responsive" />
                                        </label>
                                    </div>
                                    <input type="radio" id="largura_gondola_1" name="shelf_size" value="1" checked="checked" />
                                    <label for="largura_gondola_1">
                                        <span><span></span></span>1,0 m
                                    </label>
                                </div>

                                <div class="col-xs-6 text-center">
                                    <div class="ctn-img">
                                        <label for="largura_gondola_13">
                                            <img src="{{ Integra::asset('imagens/largura-gondola-1.3.jpg') }}" alt="1,30 m" title="1,30 m" class="img-responsive" />
                                        </label>
                                    </div>
                                    <input type="radio" id="largura_gondola_13" name="shelf_size" value="1.3" />
                                    <label for="largura_gondola_13">
                                        <span><span></span></span>1,3 m
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <hr>
            </fieldset>

            <button id="enviar" type="submit" class="btn next" onclick="ga('send', 'event', 'Planograma', 'create', 'Planograma');">
                <span>GERAR PLANOGRAMA</span>  <i class="material-icons">send</i>
            </button>
        </div>
    </form>

@endsection

@push('styles')

<link href="{{ Integra::asset('css/validationEngine.jquery.min.css') }}" rel="stylesheet" media="screen" />

@endpush

@push('scripts')

<script type="text/javascript" src="{{ Integra::asset('js/jquery.meio.mask.min.js') }}"></script>
<script type="text/javascript" src="{{ Integra::asset('js/jquery.formtowizard.js') }}"></script>
<script type="text/javascript" src="{{ Integra::asset('js/jquery.validationEngine.min.js') }}"></script>
<script type="text/javascript" src="{{ Integra::asset('js/jquery.validationEngine-pt_BR.min.js') }}"></script>

<script>
    (function($){
        $(window).load(function(){
            /*$("#lista-categorias").mCustomScrollbar({
                theme:"minimal"
            });*/
        });
    })(jQuery);
</script>

@endpush

@push('scripts_ready')

    /* Início Form Wizard com seus validadores */
    var $frmPlanograma = $( '#frmPlanograma' );

    $frmPlanograma.validationEngine('validate');

    $frmPlanograma.formToWizard({
        submitButton: 'enviar',
        showProgress: true,
        prevBtnName: '<i class="material-icons">arrow_back</i> <span>Anterior</span>',
        nextBtnName: '<span>Próximo</span> <i class="material-icons">arrow_forward</i>',
        showStepNo: false,
        validateBeforeNext: function() {
            return $frmPlanograma.validationEngine( 'validate' );
        }
    });
    /* Fim Form Wizard com seus validadores */

    /* Exibe a quantidade de gondolas exata na tela para escolha das prateleiras */
    $('.box-quantidade-prateleiras').hide(); // Oculta todas as prateleiras por padrão
    $('.prateleira_1').show(); // Exibe apenas uma prateleira por default

    $('input[name="quantidade_modulos"]').click(function(){
        var qtdGondolas = $(this).val();
        $('.box-quantidade-prateleiras').hide();

        for(i=1; i<=qtdGondolas; i++){
            $('.prateleira_'+i).show();
        }
    });

    /* Muda a imagem com a quantidade de prateleira nas gondolas */
    $('.select-prateleira').change(function(){
        var alvo = $(this).attr('attribute');
        $('#'+alvo).attr('src', '{{ Integra::asset('/') }}/imagens/gondola-'+$(this).val()+'-prateleiras.jpg');
    });

    $(".filter-input").keyup(function(){
        var filter = $(this).val();
        var target = $(this).data('target');
        $(target+".filter-list .filter-item").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
            } else {
                $(this).show();
            }
        });
    });

@endpush