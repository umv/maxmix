@extends($config['view'].'layouts.master')

@section('content')

    <section class="conclusao-planograma">
        <div class="container">

            <h1 class="title pull-left margin-0"><img src="{{ Integra::asset('imagens/icon-planogram-outline.png') }}" alt="Planograma" width="50" class="margin-r-10"> GERADOR DE PLANOGRAMA</h1>
            <a href="{{ route('sites.planogram.generator') }}#steps" class="btn btn-link btn-xs pull-right hidden-xs"><br><i class="material-icons">arrow_back</i> Voltar</a>
            <div class="clearfix"></div>
            <hr>
            <h1 class="text-default">
                {{ $planogram->subcategory->name }} <small>- {{ $planogram->state->name }}</small>

            </h1>

            {{--<h2 class="pull-left" style="margin-right: 20px;">
                <img src="{{ Integra::asset('imagens/icon-planogram.png') }}" alt="Planograma" width="40">
                <a href="{{ route('sites.planogram.generator') }}">Planogramas</a>
            </h2>
            <h3 class="pull-left">
                {{ $planogram->subcategory->name }} <small>- {{ $planogram->state->name }} - Loja {{ $planogram->getStoreSize() }}</small>

            </h3>
            <a href="{{ route('sites.planogram.generator') }}" class="btn btn-default btn-xs pull-right hidden-xs"><i class="material-icons">arrow_back</i> Voltar</a>
            <div class="clearfix"></div>
            <hr>--}}
            <div class="row">
                <div class="col-lg-9">
                    <div style="border:1px solid #ddd">
                        <section id="focal">
                            <div class="parent text-center">
                                <div class="panzoom text-center">
                                    <img src="{!! $img->encode('data-url') !!}" class="img-responsive">
                                </div>
                            </div>
                        </section>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-xs-3">
                            <button type="button" onclick="$('#focal').fullScreen(true); ga('send', 'event', 'Planograma', 'zoom', '{{ $planogram->subcategory->name }} - {{ $planogram->state->name }}');" class="btn btn-default btn-block btn-sm" >
                                <i class="material-icons">zoom_out_map</i> <span class="hidden-xs">Zoom</span>
                            </button>
                        </div>
                        <div class="col-xs-3">
                            <a href="{{ Integra::public_uri($planogram->getSrc()) }}" target="_blank" class="btn btn-default btn-block btn-sm" onclick="ga('send', 'event', 'Planograma', 'zoom_click', '{{ $planogram->subcategory->name }} - {{ $planogram->state->name }}');">
                                <i class="material-icons">file_download</i> <span class="hidden-xs">Download</span>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <button type="button" class="btn btn-default btn-block btn-sm" data-toggle="modal" data-target="#planogram-modal-email">
                                <i class="material-icons">email</i> <span class="hidden-xs">Enviar por e-mail</span>
                            </button>
                        </div>
                        <div class="col-xs-3">
                            <button type="button" onclick="$('#focal').print(); ga('send', 'event', 'Planograma', 'print_click', '{{ $planogram->subcategory->name }} - {{ $planogram->state->name }}');" class="btn btn-default btn-block btn-sm">
                                <i class="material-icons">print</i> <span class="hidden-xs">Imprimir</span>
                            </button>
                        </div>
                    </div>

                    <hr>

                </div>
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-sm-12">
                            <a href="{{ ($planogram->subcategory->link) ? $planogram->subcategory->link : 'https://www.martinsatacado.com.br/martins/pt/BRL/search/?text='.$planogram->subcategory->name }}" target="_blank" class="btn btn-primary btn-block btn-lg" onclick="ga('send', 'event', 'Planograma', 'buy_click', '{{ $planogram->subcategory->name }} - {{ $planogram->state->name }}');">
                                <i class="material-icons">shopping_cart</i><br> COMPRAR
                            </a>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-{{ ($planogram->subcategory->tip || $planogram->subcategory->category->tip) ? 6 : 12 }}">
                            <button type="button" class="btn btn-default btn-sm btn-block" data-toggle="modal" data-target="#planogram-modal-mix" onclick="ga('send', 'event', 'Planograma', 'mix_view', '{{ $planogram->subcategory->name }} - {{ $planogram->state->name }}');">
                                <i class="material-icons">format_list_numbered</i> <br> MIX IDEAL
                            </button>
                        </div>
                        @if($planogram->subcategory->tip || $planogram->subcategory->category->tip)
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-default btn-sm btn-block" data-toggle="modal" data-target="#planogram-modal-tip" onclick="ga('send', 'event', 'Planograma', 'tips_click', '{{ $planogram->subcategory->name }} - {{ $planogram->state->name }}');">
                                <i class="material-icons">lightbulb_outline</i> <br> DICAS
                            </button>
                        </div>
                        @endif
                    </div>
                    <hr>
                    <div class="box-570">
                        <table class="table table-condensed" cellspacing="1">
                            <thead>
                            <tr>
                                <th colspan="2">Espaço (em cm) dos produtos na prateleira.</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($brands as $brand)
                                <tr>
                                    <td>{{ $brand['nome'] }}:</td>
                                    <td>{{ number_format($brand['cm'], 0) }} cm</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </section>

    <!-- Modal E-mail -->
    <div class="modal fade" id="planogram-modal-email" tabindex="-1" role="dialog" aria-labelledby="planogram-modal-email-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="planogram-modal-email-label"><i class="material-icons">email</i> ENVIAR PLANOGRAMA POR E-MAIL</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ route('sites.planogram.sendemail', $planogram->code) }}" id="planogram-form-email">
                        <fieldset>
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="email" name="email" value="{{ $user->email }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Mensagem</label>
                                <textarea name="message" rows="3" class="form-control"></textarea>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary btn-planogram-email" onclick="ga('send', 'event', 'Planograma', 'email_send', '{{ $planogram->subcategory->name }} - {{ $planogram->state->name }}');"><i class="material-icons">send</i> Enviar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Tip -->
    <div class="modal fade" id="planogram-modal-tip" tabindex="-1" role="dialog" aria-labelledby="planogram-modal-tip-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="planogram-modal-tip-label"><i class="material-icons">lightbulb_outline</i> DICAS</h4>
                </div>
                <div class="modal-body">
                    <img src="{{ Integra::public_uri( ($planogram->subcategory->tip) ? $planogram->subcategory->getSrcTip() : $planogram->subcategory->category->getSrcTip() ) }}" id="tip" alt="Dica da subtegoria" class="img-responsive">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary" onclick="$('#tip').print()"><i class="material-icons">print</i> Imprimir</button>
                    <button type="button" class="btn btn-primary" onclick="$('#tip').fullScreen(true)"><i class="material-icons">zoom_out_map</i> Zoom</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Mix -->
    <div class="modal fade" id="planogram-modal-mix" tabindex="-1" role="dialog" aria-labelledby="planogram-modal-mix-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="planogram-modal-mix-label"><i class="material-icons">format_list_numbered</i> MIX IDEAL </h4>
                </div>
                <div class="modal-body">
                    <div id="mix">
                        @foreach($planogram->subcategory->getBrands($planogram->state->areas[0]['id']) as $brand)
                            @if(count($planogram->subcategory->getMix($planogram->state->areas[0]['id'], $brand->id)))
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">{{ $brand->name }}</h5>
                                    </div>
                                    <div class="card-body">

                                    </div>
                                    <table class="table-responsive table-striped table-hover table-bordered table-condensed">
                                        <thead>
                                        <tr>
                                            <th width="5%">Posição</th>
                                            <th width="10%">EAN</th>
                                            <th>Item</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($planogram->subcategory->getMix($planogram->state->areas[0]['id'], $brand->id) as $item)
                                            <tr>
                                                <td>{{ $item->position }}</td>
                                                <td>{{ $item->ean }}</td>
                                                <td style="word-break: break-all;">{{ $item->name }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary" onclick="$('#mix').print()"><i class="material-icons">print</i> Imprimir</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script type="text/javascript" src="{{ Integra::asset('js/jquery.panzoom.min.js') }}"></script>
<script type="text/javascript" src="{{ Integra::asset('js/jquery.mousewheel.js') }}"></script>
<script type="text/javascript" src="{{ Integra::asset('js/jquery.fullscreen-min.js') }}"></script>
<script type="text/javascript" src="{{ Integra::asset('js/jquery.print.js') }}"></script>
<script>
    (function() {
        var $section = $('#focal');
        var $panzoom = $section.find('.panzoom').panzoom();
        $panzoom.parent().on('mousewheel.focal', function( e ) {
            e.preventDefault();
            var delta = e.delta || e.originalEvent.wheelDelta;
            var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
            $panzoom.panzoom('zoom', zoomOut, {
                minScale: 1,
                increment: 0.3,
                animate: true,
                focal: e
            });
        });
    })();
</script>
@endpush

@push('scripts_ready')


    $(document).on('click', '.btn-planogram-email', function(){

        $.post(
            "{{ route('sites.planogram.sendemail', $planogram->code) }}",
            $('#planogram-form-email').serialize()
        ).done(function( data ) {
            $('#planogram-modal-email').modal('hide');
            alert('Envio realizado com sucesso');
        });

    });


@endpush