@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.monitoring.inc.header')

    <div class="container">

        @if(isset($tags))

            <div class="jumbotron jumbotron-default">

                <div class="row">
                    <div class="col-lg-12">
                        <h3>Escolha os períodos:</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-2 text-center">
                        <h3>Período 1</h3>
                        <select name="tag_1" id="tag_1" class="form-control input-lg">
                            <option value="">Selecione o período</option>
                            @foreach($tags as $tag)
                                <option value="{{ Hashids::encode($tag->id) }}">{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 text-center">
                        <h3>Período 2</h3>
                        <select name="tag_2" id="tag_2" class="form-control input-lg">
                            <option value="">Selecione o período</option>
                            @foreach($tags as $tag)
                                <option value="{{ Hashids::encode($tag->id) }}">{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <a href="#" id="btn-monitoring" class="btn next" style="float: none;">
                                <span>Acompanhar períodos</span>
                                <i class="material-icons">arrow_forward</i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        @else

            <div class="alert alert-danger">
                <h4>Não existem dados para sortimento</h4>
            </div>

        @endif

    </div>


@endsection

@push('scripts_ready')

    $('#btn-monitoring').on('click', function(){

        var url = "{{ route('sites.assortment.monitoring.init', [Hashids::encode($client->id), 'TAGS']) }}";

        var tag1 = $('#tag_1').val();
        var tag2 = $('#tag_2').val();

        if( tag1 == '' || tag2 == '' ){

            alert('Escolha o Período 1 e o Período 2 para continuar!');
            $('#tag_1').focus();

        } else {

            url = url.replace('TAGS', tag1+'-'+tag2);
            document.location.href = url;

        }

    });

@endpush