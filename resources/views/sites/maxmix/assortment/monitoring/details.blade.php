<div class="row">
    <div class="col-md-12">
        <div class="panel- panel-primary bg-muted">

            @if(isset($table['subcategory']))

                <div class="bg-primary">

                    <div class="well well-sm bg-primary margin-0">
                        <b>Subcategoria: </b> {{ $table['subcategory']->name }}
                    </div>

                </div>

            @endif

            <table class="table table-bordered table-hover" style="margin-bottom: 0">
                <thead>
                    <tr>
                        <th></th>
                        @foreach($tags as $tag)
                            <th class="text-center">{{ $tag->name }}</th>
                        @endforeach
                        <th class="text-center">Evolução</th>
                    </tr>
                </thead>
                <tbody>
                    @if(in_array($type, ['category', 'subcategory']))
                    <tr>
                        <th>GRUPO</th>
                        @foreach($tags as $tag)
                            <td class="text-center">
                                <b class="label bg-class bg-class-{{ $tag->valuation['group'] }}">{{ $tag->valuation['group'] }}</b>
                            </td>
                        @endforeach
                        <th></th>
                    </tr>
                    @endif
                    @if(in_array($type, ['subcategory', 'brand']))
                    <tr>
                        <th>SKU's</th>
                        @foreach($tags as $tag)
                            <td class="text-center">
                                {{ number_format($tag->valuation['sku'], 0, ',', '.') }}
                            </td>
                        @endforeach
                        <td class="text-center text-primary">
                            {{--@if($table['sku'] > 0)
                                {{ number_format($table['sku'], 0) }} itens
                            @endif--}}
                            @if(isset($table['info']['itens']['removed']) && count($table['info']['itens']['removed']) > 0)
                                <p>{{ count($table['info']['itens']['removed']) }} removidos</p>
                            @endif
                            @if(isset($table['info']['itens']['added']) && count($table['info']['itens']['added']) > 0)
                                {{ count($table['info']['itens']['added']) }} adicionados
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>QUANTIDADE</th>
                        @foreach($tags as $tag)
                            <td class="text-center">
                                {{ number_format($tag->valuation['amount'], 0, ',', '.') }}
                            </td>
                        @endforeach
                        <td class="text-center text-primary">

                        </td>
                    </tr>
                    @endif
                    <tr>
                        <th>PARTICIPAÇÃO</th>
                        @foreach($tags as $tag)
                            <td class="text-center">
                                {{ ($tag->valuation['sku'] > 0) ? number_format($tag->valuation['participation'], 2, ',', '.').'%' : '--' }}
                            </td>
                        @endforeach
                        <th class="text-center text-{{ ($table['participation'] > 0) ? 'success' : 'danger' }}">
                            @if($tag->valuation['sku'] > 0)
                                <i class="material-icons">{{ ($table['participation'] > 0) ? 'arrow_upward' : 'arrow_downward' }}</i>
                                {{ number_format($table['participation'], 2, ',', '.') }}%
                            @endif
                        </th>
                    </tr>
                    <tr>
                        <th>RECEITA MÉDIA</th>
                        @foreach($tags as $tag)
                            <td class="text-center">
                                @if($tag->valuation['sku'] > 0)
                                    R$ {{ number_format($tag->valuation['total_revenue'], 2, ',', '.') }}
                                @else
                                    --
                                @endif
                            </td>
                        @endforeach
                        <th class="text-center text-{{ ($table['total_revenue'] > 0) ? 'success' : 'danger' }}">
                            @if($tag->valuation['sku'] > 0)
                                <i class="material-icons">{{ ($table['total_revenue'] > 0) ? 'arrow_upward' : 'arrow_downward' }}</i>
                                {{ number_format($table['total_revenue'], 2, ',', '.') }}%
                            @endif
                        </th>
                    </tr>
                    <tr>
                        <th rowspan="2" style="vertical-align: middle;">MARGEM MÉDIA</th>
                        @foreach($tags as $tag)
                            <td class="text-center">
                                @if($tag->valuation['sku'] > 0)
                                    R$ {{ number_format($tag->valuation['total_gross_margin'], 2, ',', '.') }}
                                @else
                                    --
                                @endif
                            </td>
                        @endforeach
                        <th rowspan="2" style="vertical-align: middle;" class="text-center text-{{ ($table['total_gross_margin'] > 0) ? 'success' : 'danger' }}">
                            @if($tag->valuation['sku'] > 0)
                                <i class="material-icons">{{ ($table['total_gross_margin'] > 0) ? 'arrow_upward' : 'arrow_downward' }}</i>
                                {{ number_format($table['total_gross_margin'], 2, ',', '.') }}%
                            @endif
                        </th>
                    </tr>
                    <tr>
                        @foreach($tags as $tag)
                            <td class="text-center">
                                @if($tag->valuation['sku'] > 0)
                                    {{ number_format($tag->valuation['percentage_gross_margin'], 2, ',', '.') }}%
                                @else
                                    --
                                @endif
                            </td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
            @if(isset($table['info']) && count($table['info']) > 0)
                <div class="panel-body">
                    <p>
                        Informações:
                    </p>
                    <ul>
                        @if(in_array($type, ['brand']))
                            @if(isset($table['info']['brand_status']))
                                <li>
                                    @if($table['info']['brand_status'] == 0)
                                        <b class="text-danger"><i class="material-icons">thumb_down</i> Esta marca estava avaliada para exclusão e não foi removida.</b>
                                    @elseif($table['info']['brand_status'] == 2)
                                        <b class="text-success"><i class="material-icons">thumb_up</i> A marca foi removida.</b>
                                    @elseif($table['info']['brand_status'] == 3)
                                        <b class="text-info">Esta marca foi adicionada.</b>
                                    @else
                                        <b class="text-success"><i class="material-icons">thumb_up</i> A marca foi mantida.</b>
                                    @endif
                                </li>
                            @endif
                            @if(isset($table['info']['margin']) && $table['info']['margin'] != null)
                                <li>
                                    @if($table['info']['margin'] == 0)
                                        <b class="text-danger"><i class="material-icons">thumb_down</i> A margem média continua abaixo da margem média da subcategoria.</b>
                                    @elseif($table['info']['margin'] == 1)
                                        <b class="text-success"><i class="material-icons">thumb_up</i> A margem média passou a ficar acima da margem média da subcategoria.</b>
                                    @elseif($table['info']['margin'] == 2)
                                        <b class="text-success"><i class="material-icons">thumb_up</i> A margem média continua acima da margem média da subcategoria.</b>
                                    @elseif($table['info']['margin'] == 3)
                                        <b class="text-danger"><i class="material-icons">thumb_down</i> A margem média passou a ficar abaixo da margem média da subcategoria.</b>
                                    @endif
                                </li>
                            @endif
                        @endif

                        @if(in_array($type, ['subcategory']))
                            @if(isset($table['info']['itens']['not_removed']) && count($table['info']['itens']['not_removed']) > 0)
                                <li>
                                    <b class="text-danger"><i class="material-icons">thumb_down</i> {{ count($table['info']['itens']['not_removed']) }} itens estavam avaliados para exclusão e não foram removidos.</b>
                                </li>
                            @endif
                            @if(isset($table['info']['itens']['yes_removed'])  && count($table['info']['itens']['yes_removed']) > 0)
                                <li>
                                    <b class="text-success"><i class="material-icons">thumb_up</i> {{ count($table['info']['itens']['yes_removed']) }} itens estavam avaliados para exclusão e foram removidos.</b>
                                </li>
                            @endif
                        @endif
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>