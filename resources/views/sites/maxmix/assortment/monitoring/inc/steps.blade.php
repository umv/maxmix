<div class="steps visible-lg visible-md">
    <div class="container">
        <div class="row">
            <div class="col-md-3 text-center">
                <span class="step {{ ($step == 'init') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.monitoring.init', [Hashids::encode($client->id), $tags_url]) }}#steps">
                        {{ trans('sites/maxmix/assortment.step.init') }}
                    </a>
                </span>
            </div>
            <div class="col-md-3 text-center">
                <span class="step {{ ($step == 'categories') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.monitoring.categories', [Hashids::encode($client->id), $tags_url]) }}#steps">
                        {{ trans('sites/maxmix/assortment.step.categories') }}
                    </a>
                </span>
            </div>
            <div class="col-md-3 text-center">
                <span class="step {{ ($step == 'subcategories') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.monitoring.subcategories', [Hashids::encode($client->id), $tags_url]) }}#steps">
                        {{ trans('sites/maxmix/assortment.step.subcategories') }}
                    </a>
                </span>
            </div>
            <div class="col-md-3 text-center">
                <span class="step {{ ($step == 'brands') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.monitoring.brands', [Hashids::encode($client->id), $tags_url]) }}#steps">
                        {{ trans('sites/maxmix/assortment.step.brands') }}
                    </a>
                </span>
            </div>
        </div>
    </div>
</div>