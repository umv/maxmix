@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.monitoring.inc.header')

    @include($config['view'].'assortment.monitoring.inc.steps', ['step' => 'categories'])

    <div class="container">

        <h3 class="title">Acompanhamento de categorias</h3>

        <br><br>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="title">
                            Margem Média
                        </h4>
                    </div>
                    <div class="panel-body">
                        <p class="text-muted">
                            <small><i class="material-icons">info_outline</i> Clique no nome da categoria para visualizar mais detalhes.</small>
                        </p>
                        <div id="chart" style="width:100%; height:{{ count($chart['categories'])*60 }}px;"></div>
                    </div>
                </div>

            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="navigate">
                    <div class="">
                        <a href="{{ route('sites.assortment.monitoring.init', [Hashids::encode($client->id), $tags_url]) }}#steps" class="btn prev">
                            <i class="material-icons">arrow_back</i>
                            <span>{{ trans('sites/general.previous') }}</span>
                        </a>

                        <a href="{{ route('sites.assortment.monitoring.subcategories', [Hashids::encode($client->id), $tags_url]) }}#steps" class="btn next">
                            <span>{{ trans('sites/general.next') }}</span>
                            <i class="material-icons">arrow_forward</i>
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title title" id="myModalLabel"></h3>
                </div>
                <div class="modal-bod">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('sites/general.close') }}</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://blacklabel.github.io/custom_events/js/customEvents.js"></script>
@endpush

@push('scripts_ready')

    var categories = {!! json_encode($chart['data']['categories'])  !!};

    $('#chart').highcharts({
        chart: {
            type: 'bar',
            style: {
                fontFamily: '"Lato","Helvetica Neue",Helvetica,Arial,sans-serif'
            }
        },
        colors: ['#bbbbbb', '#dc6000'],
        xAxis: {
            categories: {!! json_encode($chart['categories'])  !!},
            title: {
                text: null
            },
            labels: {
                style: {"cursor":"pointer","fontWeight":"bold","fontSize":"14px"},
                useHTML: true,
                formatter: function () {
                    var res = this.value.split(": ");
                    var category = res[0].replace(/\s{1,}/g, '');
                    category = categories[category];
                    return "<span class='btnMonitoringDetails' data-category='"+res[0]+"' data-id='"+category+"'>"+this.value.replace(': ', '<br>')+"</span>";
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Margem média mensal em reais (R$)',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            enabled: false,
            backgroundColor: '#ffffff',
            shared: true,
            useHTML: true,
            headerFormat: '<br><b>{point.key}</b><hr><table>',
            pointFormat: '<tr><td style="color: {series.color}">{series.name}: </td>' +
                    '<td style="text-align: right"><b>R$ {point.y}</b></td></tr>',
            footerFormat: '</table>',
            //valuePrefix: 'R$ '
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return (Intl.NumberFormat()) ? new Intl.NumberFormat().format(this.y) : this.y;
                    }
                }

            },
            series: {
                groupPadding: 0.1
            }
        },
        legend: {
            align: 'center',
            verticalAlign: 'top',
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF')
        },
        credits: {
            enabled: false
        },
        series: {!! json_encode($chart['series']) !!}
    });


$('.btnMonitoringDetails').on('click', function(){

    var link = "{{ route('sites.assortment.monitoring.details', [Hashids::encode($client->id), $tags_url, 'category']) }}?c="+$(this).data('id');

    $('#myModal .modal-title').html("Acompanhamento da Categoria:<br><b>" + $(this).data('category') + "<b>");
    $('#myModal .modal-bod').html("{{ trans('sites/general.loading') }}...");
    $('#myModal').modal('show');

    $.get( link, function( data ) {
        $('#myModal .modal-bod').html( data );
    });

});

@endpush