<div style="padding: 5px">

    <div style="background: #ecf0f1">
        <table class="table table-bordered margin-0" style="background: #cfcfcf; border-radius: 5px;">
            <tr class="">
                <td width="35%">
                    <div class="text-center">
                        <p>
                            <small><b>{{ trans('sites/maxmix/assortment.tag.label.sku') }}</b></small>
                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.sku') }}"><i class="material-icons">info_outline</i></small>
                        </p>
                        <p>
                            {{ number_format($subcategory->sku, 0, ',', '.') }}
                        </p>
                    </div>
                </td>
                <td width="35%">
                    <div class="text-center">
                        <p>
                            <small><b>{{ trans('sites/maxmix/assortment.tag.label.amount') }}</b></small>
                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.amount') }}"><i class="material-icons">info_outline</i></small>
                        </p>
                        <p>
                            {{ number_format($subcategory->amount, 0, ',', '.') }}
                        </p>
                    </div>
                </td>
                <td width="30%">
                    <div class="text-center">
                        <p>
                            <small><b>{{ trans('sites/general.group') }}</b></small>
                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.brand.info.group') }}"><i class="material-icons">info_outline</i></small>
                        </p>
                        <p>
                            <b class="label bg-class bg-class-{{ $classification }}">{{ $classification }}</b>
                        </p>
                    </div>
                </td>
            </tr>
            <tr class="">
                <td width="35%">
                    <div class="text-center">
                        <p>
                            <small><b>{{ trans('sites/maxmix/assortment.tag.label.revenue') }}</b></small>
                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.revenue') }}"><i class="material-icons">info_outline</i></small>
                        </p>
                        <p>
                            R$ {{ number_format($subcategory->total_revenue, 2, ',', '.') }}
                        </p>
                    </div>
                </td>
                <td width="35%">
                    <div class="text-center">
                        <p>
                            <small><b>{{ trans('sites/maxmix/assortment.tag.label.margin') }}</b></small>
                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.margin') }}"><i class="material-icons">info_outline</i></small>
                        </p>
                        <p>
                            R$ {{ number_format($subcategory->total_gross_margin, 2, ',', '.') }}
                        </p>
                    </div>
                </td>
                <td width="30%">
                    <div class="text-center">
                        <p>
                            <small><b>{{ trans('sites/general.margin') }}</b></small>
                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.brand.info.margin') }}"><i class="material-icons">info_outline</i></small>
                        </p>
                        <p>
                            <b class="text-default">{{ number_format($margin, 2, ',', '.') }}%</b>
                        </p>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <hr>

    <div class="panel-group panel-assortment" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-column panel-default">
            <div class="panel-heading" role="tab" id="headingNone">
                <h4 class="panel-title">
                    <table class="table table-condensed">
                        <tr>
                            <td width="15%" class="text-center">
                                {{ trans('sites/general.position') }}
                            </td>
                            <td width="45%">
                                {{ trans('sites/general.brand') }}
                            </td>
                            <td width="20%" class="text-right">
                                {{ trans('sites/general.participation') }}
                            </td>
                            <td width="20%" class="text-right">
                                Info.
                            </td>
                        </tr>
                    </table>
                </h4>
            </div>
        </div>
        @foreach($brands as $key => $item)
            <div class="panel panel-{{ ($item['status'] == 0) ? 'default' : 'primary' }} link-hover-{{ md5($item['brand']) }}" onmouseleave="linkMouseLeave(this)" onmouseenter="linkMouseEnter(this)" data-link-hover="{{ md5($item['brand']) }}">
                <div class="panel-heading" role="tab" id="heading{{ $item['brand_id'] }}" onclick="loadComparisonBrandDetails({{$client['id']}}, '{{$item['brand']}}')" data-client_id="{{$client['id']}}" data-brand_name="{{$item['brand']}}">
                    <h4 class="panel-title">
                        <table class="table">
                            <tr>
                                <td width="15%" class="text-center">
                                    {{ $key+1 }}
                                </td>
                                <td width="45%">
                                    <b>{{ $item['brand'] }}</b>
                                </td>
                                <td width="20%" class="text-right">
                                    {{ number_format($item['participation'], 2, ',', '.') }}%
                                </td>
                                <td width="20%" class="text-right">
                                    <span class="label {{ ($item['status'] == 0) ? 'label-danger' : 'label-success' }}">{{ strtoupper( ($item['status'] == 0) ? trans('sites/general.rate') : trans('sites/general.keep') ) }}</span>
                                </td>
                            </tr>
                        </table>
                    </h4>
                </div>
            </div>
        @endforeach
    </div>
</div>