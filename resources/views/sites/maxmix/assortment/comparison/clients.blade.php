@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.comparison.inc.header')

    @include($config['view'].'assortment.comparison.inc.steps', ['step' => 'client'])

    <form class="frm-planograma" method="post" action="{{ route('sites.assortment.comparison.period') }}">
        {!! csrf_field() !!}
        <div class="container">

            <fieldset>
                <section class="categoria">

                    <div class="row">
                        <div class="col-md-6">
                            <h3>
                                Escolha as lojas
                            </h3>
                        </div>

                        <div class="col-md-6">
                            <div class="ctn-informacao hidden-xs">
                                <div class="img-informacao">i</div>
                                Escolha na lista abaixo, as lojas que deseja fazer a comparação.
                            </div>
                        </div>
                    </div>

                    <div class="ctn-categorias">
                        <div id="lista-categorias" class="lista-categorias scrollbar-custom filter-list">
                            <div class="row">
                                @foreach($clients as $client)
                                    <div class="col-md-6 filter-item">
                                        <input type="checkbox" id="client-{{ $client->id }}" name="clients_id[]" value="{{ $client->id }}" />
                                        <label for="client-{{ $client->id }}">
                                            <span><span></span></span>{{ $client->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>

            </fieldset>

            <hr>

            <div class="navigate-">
                <button type="submit" class="btn next">
                    <span>{{ trans('sites/general.next') }}</span>
                    <i class="material-icons">arrow_forward</i>
                </button>

                <div class="clearfix"></div>
            </div>
        </div>



    </form>


@endsection