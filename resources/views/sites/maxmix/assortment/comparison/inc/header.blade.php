<div class="container">

    <h1 class="title pull-left margin-0"><img src="{{ Integra::asset('imagens/icon-assortment-outline.png') }}" alt="Sortimento" width="50" class="margin-r-10">
        Comparação de Sortimento
    </h1>
    <a href="{{ url('/') }}" class="btn btn-link btn-xs pull-right hidden-xs"><br><i class="material-icons">arrow_back</i> Voltar</a>
    <div class="clearfix"></div>
    <hr>

    <div id="steps"></div>

    @if(isset($clients_selected))
        <p class="text-default text-center">
            <small><b>Lojas: </b> {{ implode(', ', $clients_selected) }}</small>
        </p>
    @endif

    <br>

</div>