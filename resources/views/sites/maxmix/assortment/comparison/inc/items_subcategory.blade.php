<th class="text-center bg-class bg-class-{{$class}}">
    <h2>{{$class}}</h2>
</th>
@foreach($clients_selected as $id => $name)
    <td width="{{ 90/count($clients_selected) }}%">

        <table>
            <tr>
                <th class="text-center">Porcentagem</th>
                <th class="text-center">Qtd. Subcategorias</th>
                <th class="text-center">Qtd. Itens</th>
            </tr>
            <tr>
                <th class="text-center">{{$data[$id]['table'][$class]['percentage']}}%</th>
                <th class="text-center">{{$data[$id]['table'][$class]['subcategory']}}</th>
                <th class="text-center">{{$data[$id]['table'][$class]['sku']}}</th>
            </tr>
        </table>

        <table class="table table-condensed table-striped">
            <caption>Subcategorias</caption>
            <thead>
                <tr>
                    <th width="5%"><small>POSIÇÃO</small></th>
                    <th><small>SUBCATEGORIA</small></th>
                    <th width="10%"><small>PARTICIPAÇÃO</small></th>
                </tr>
            </thead>
            <tbody>
            @foreach($data[$id]['subcategories'] as $key => $item)
                @if($item['group'] == $class)
                    <tr>
                        <td class="text-center">
                            {{ $key+1 }}
                        </td>
                        <td>
                            {{$item['subcategory']}}
                        </td>
                        <td class="text-right">
                            {{ number_format($item['participation'], 2, ',', '.') }}%
                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </td>
@endforeach