<div class="steps visible-lg visible-md">
    <div class="container">
        <div class="row">
            {{--<div class="col-md-3 text-center">
                <span class="step {{ ($step == 'categories') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.comparison.categories', $parameters) }}#steps">
                        Categorias
                    </a>
                </span>
            </div>--}}
            <div class="col-md-4 text-center">
                <span class="step {{ ($step == 'subcategories') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.comparison.subcategories', $parameters) }}#steps">
                        Subcategorias
                    </a>
                </span>
            </div>
            <div class="col-md-4 text-center">
                <span class="step {{ ($step == 'brands') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.comparison.brands', $parameters) }}#steps">
                        Marcas
                    </a>
                </span>
            </div>
            <div class="col-md-4 text-center">
                <span class="step {{ ($step == 'items') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.comparison.items', $parameters) }}#steps">
                        Itens
                    </a>
                </span>
            </div>
        </div>
    </div>
</div>