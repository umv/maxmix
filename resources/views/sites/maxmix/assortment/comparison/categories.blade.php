@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.comparison.inc.header')

    @include($config['view'].'assortment.comparison.inc.steps', ['step' => 'categories'])

    <div class="container">

        <h3 class="title">Comparação de categorias</h3>

        <br><br>

        <div class="table-responsive">
            <table class="table table-bordered">

                <thead>
                <tr>
                    <th width="10%" class="active"></th>
                    @foreach($clients_selected as $id => $name)
                        <th width="{{ 90/count($clients_selected) }}%" class="text-center active">
                            <h4>{{$name}}</h4>
                        </th>
                    @endforeach
                </tr>
                </thead>

                <tbody>
                <tr>
                    @include($config['view'].'assortment.comparison.inc.items_category', ['class' => 'A'])
                </tr>
                <tr>
                    @include($config['view'].'assortment.comparison.inc.items_category', ['class' => 'B'])
                </tr>
                <tr>
                    @include($config['view'].'assortment.comparison.inc.items_category', ['class' => 'C'])
                </tr>
                <tr>
                    @include($config['view'].'assortment.comparison.inc.items_category', ['class' => 'D'])
                </tr>
                </tbody>

            </table>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="navigate">
                    <div class="">

                        <a href="{{ route('sites.assortment.comparison.subcategories', $parameters) }}#steps" class="btn next">
                            <span>{{ trans('sites/general.next') }}</span>
                            <i class="material-icons">arrow_forward</i>
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection