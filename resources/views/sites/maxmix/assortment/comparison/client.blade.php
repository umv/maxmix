@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.comparison.inc.header')

    <div class="container">

        @if(isset($clients))

            <h3>Escolha as lojas:</h3>

            <form class="frm-planograma" method="post" action="{{ route('sites.assortment.comparison.period') }}">
                {!! csrf_field() !!}
                <div class="jumbotron jumbotron-default">
                    <div class="ctn-categorias">
                        <div id="lista-categorias" class="lista-categorias scrollbar-custom filter-list">
                            <div class="row">

                                @foreach($clients as $client)
                                    <div class="col-md-6 filter-item">
                                        <input type="checkbox" id="client-{{ $client->id }}" name="clients_id[]" value="{{ $client->id }}" />
                                        <label for="client-{{ $client->id }}">
                                            <span><span></span></span>{{ $client->name }}
                                        </label>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <button type="submit" id="btn-monitoring" class="btn next" style="float: none;">
                                    <span>Escolher período</span>
                                    <i class="material-icons">arrow_forward</i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>

        @else

            <div class="alert alert-danger">
                <h4>Não existem dados para sortimento</h4>
            </div>

        @endif

    </div>


@endsection