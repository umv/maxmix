@extends($config['view'].'layouts.master')

@section('styles')

    <style>
        .link-hover-active{
            opacity: 0.5;
            cursor: pointer;
        }
        .table .table {
            background-color: initial;
        }
    </style>

@endsection

@section('content')

    @include($config['view'].'assortment.comparison.inc.header')

    @include($config['view'].'assortment.comparison.inc.steps', ['step' => 'itens'])

    <div class="container">

        <h3 class="title">Comparação de itens</h3>

        <br><br>

        <div class="row">
            <div class="col-md-6 @if(!$subcategory_name) col-md-offset-3 @endif">
                <div class="form-group has-success">
                    <label class="upper">{{ trans('sites/general.subcategory') }}</label>
                    {!! Form::select( 'subcategory', $subcategories_grouped, Input::old( 'subcategory', $subcategory_name ), ['id' => 'subcategory', 'class' => 'form-control input-lg input-primary' ,'placeholder' => trans('sites/general.select_subcategory') ]) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                @if($subcategory_name)
                    <div class="table-responsive">
                        <table class="table margin-0">
                            <tr>
                                @foreach($clients_selected as $id => $name)
                                    <th width="{{ 100/count($clients_selected) }}%" class="text-center" style="padding: 0; white-space: nowrap;">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading" style="background-color: #f48129;">
                                                <b class="title">
                                                    {{$name}}
                                                </b>
                                            </div>
                                            <div id="comparison_box_{{$id}}">
                                                <img src="{{ Integra::asset('imagens/bx_loader.gif') }}">
                                            </div>
                                        </div>
                                    </th>
                                @endforeach
                            </tr>
                        </table>
                    </div>
                @else

                    <div class="text-center">
                        <h1>
                            <big><i class="material-icons text-muted">arrow_upward</i></big>
                            <br>
                            <small>{{ trans('sites/maxmix/assortment.select_category') }}</small>
                        </h1>
                    </div>

                @endif
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="navigate">
                    <div class="">

                        <a href="{{ route('sites.assortment.comparison.brands', $parameters) }}{{isset($subcategory_name)?'?s='.urlencode($subcategory_name):''}}#steps" class="btn prev">
                            <i class="material-icons">arrow_back</i>
                            <span>{{ trans('sites/general.previous') }}</span>
                        </a>

                        <a href="{{ route('sites.assortment.comparison.client', $parameters) }}#steps" class="btn next">
                            <span>{{ trans('sites/general.conclude') }}</span>
                            <i class="material-icons">arrow_forward</i>
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @if(isset($subcategory_name))

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title title" id="myModalLabel"></h3>
                    </div>
                    <div class="modal-body">

                    </div>
                </div>
            </div>
        </div>

    @endif

@endsection

@push('scripts')

<script>

    @if(isset($subcategory_name))

        function loadComparisonItem(client_id){

            var link = "{{ route('sites.assortment.comparison.items.client', $parameters) }}?s={{$subcategory_name}}&c="+client_id;

            $( "#comparison_box_"+client_id ).load( encodeURI(link), function(){
                $('[data-toggle="tooltip"]').tooltip();
                $('[data-toggle="popover"]').popover();
            });

        }

        function loadComparisonItemDetails(client_id, item_name, ean){

            var link = "{{ route('sites.assortment.comparison.items.details', $parameters) }}?s={{$subcategory_name}}&c="+client_id+'&i='+ean;

            $( "#myModal .modal-body" ).load( encodeURI(link) );

            $('#myModal .modal-title').html("Comparativo do item: <br> <b>"+item_name+"</b>");
            $('#myModal .modal-body').html('<div class="text-center"><img src="{{ Integra::asset('imagens/bx_loader.gif') }}"></div>');
            $('#myModal').modal('show');

        }

        function linkMouseEnter(obj){

            var name = $(obj).data('link-hover');

            $('.link-hover-'+name).addClass('link-hover-active');

        }

        function linkMouseLeave(obj){

            var name = $(obj).data('link-hover');

            $('.link-hover-'+name).removeClass('link-hover-active');

        }

    @endif

</script>

@endpush

@push('scripts_ready')

    $('#subcategory').on('change', function(){

        var s = this.value;
        var url = "{{ route('sites.assortment.comparison.items', $parameters) }}?s=";

        document.location.href = url+s+"#steps";

    });

    @if(isset($subcategory_name))

        $('.btnComparisonItemDetails').on('click', function(){

            var client_id = $(this).data('client_id');
            var item_name = $(this).data('item_name');

            $('#myModal .modal-title').html("Comparativo do item: <br> <b>"+item_name+"</b>");
            $('#myModal .modal-body').html("{{ trans('sites/general.loading') }}...");
            $('#myModal').modal('show');

            loadComparisonItemDetails(client_id, item_name);

        });

        @foreach($clients_selected as $client_id => $client_name)
            loadComparisonItem({{$client_id}});
        @endforeach

    @endif

@endpush