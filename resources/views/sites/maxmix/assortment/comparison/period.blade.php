@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.comparison.inc.header')

    {{--@include($config['view'].'assortment.comparison.inc.steps', ['step' => 'period'])--}}

    <div class="container">

    @if($dates)

            <h3>Escolha a período:</h3>

        <form class="frm-planograma" method="post" action="{{ route('sites.assortment.comparison.process') }}">
            {!! csrf_field() !!}
            @foreach($clients_selected as $id => $name)
            <input type="hidden" name="clients_id[]" value="{{ $id }}">
            @endforeach
            <div class="jumbotron jumbotron-default">
                <div class="ctn-categorias">
                    <div id="lista-categorias" class="lista-categorias scrollbar-custom filter-list">
                        <div class="row">
                            @foreach($dates as $year => $period)
                                <div class="col-md-12 text-center">
                                    <h4><i class="material-icons">date_range</i> {{$year}}</h4>
                                    <br>
                                </div>
                                @foreach($period as $date)
                                    <div class="col-md-3">
                                        <input type="checkbox" id="date-{{ $date }}" name="dates[]" value="{{ $date }}" />
                                        <label for="date-{{ $date }}">
                                            <span><span></span></span>{{ IntegraMonthToString(date('m', strtotime($date))) }}
                                        </label>
                                    </div>
                                @endforeach
                                <div class="col-md-12">
                                    <hr>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <hr>

                <div class="text-center">

                    <a href="javascript:window.history.back();" class="btn prev" style="float: none">
                        <i class="material-icons">arrow_back</i>
                        <span>Alterar lojas</span>
                    </a>

                    <button type="submit" class="btn next" style="float: none">
                        <span>Iniciar comparação</span>
                        <i class="material-icons">arrow_forward</i>
                    </button>

                    <div class="clearfix"></div>

                </div>

            </div>

        </form>

    @else
        <div class="well">

            <h4 class="text-center">

                Não existem períodos compatíveis <br> para fazer comparação com estes clientes

            </h4>

            <hr>

            <div class="text-center">

                <a href="javascript:window.history.back();" class="btn prev" style="float: none">
                    <i class="material-icons">arrow_back</i>
                    <span>Alterar lojas</span>
                </a>

                <div class="clearfix"></div>

            </div>

        </div>


    @endif

    </div>


@endsection