<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Solicitação de Sortimento</title>
    <style type="text/css">
        body {
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            margin:0 !important;
            width: 100% !important;
            -webkit-text-size-adjust: 100% !important;
            -ms-text-size-adjust: 100% !important;
            -webkit-font-smoothing: antialiased !important;
        }
        .tableContent img {
            border: 0 !important;
            display: block !important;
            outline: none !important;
        }
        a{
            color:#382F2E;
        }

        p, h1,h2,ul,ol,li,div{
            margin:0;
            padding:0;
        }

        h1,h2{
            font-weight: normal;
            background:transparent !important;
            border:none !important;
        }

        .contentEditable h2.big,.contentEditable h1.big{
            font-size: 26px !important;
        }

        .contentEditable h2,.contentEditable h1{
            font-size: 37px !important;
        }

        td,table{
            vertical-align: top;
        }
        td{
            vertical-align: middle;
        }

        a.link1{
            font-size:13px;
            color:white;
            line-height: 24px;
            text-decoration:none;
        }
        a{
            text-decoration: none;
        }

        .link3{
            color:white;
            border:1px solid #18bc9c;
            padding:10px 18px;
            border-radius:3px;
            -moz-border-radius:3px;
            -webkit-border-radius:3px;
            background:#18bc9c;
        }

        h2,h1{
            line-height: 20px;
        }
        p{
            font-size: 14px;
            line-height: 21px;
            color:#AAAAAA;
        }

        .contentEditable li{

        }

        .appart p{

        }
        .bgItem{
            background: #ffffff;
        }
        .bgBody{
            background: #ffffff;
        }

        img {
            outline:none;
            text-decoration:none;
            -ms-interpolation-mode: bicubic;
            width: auto;
            max-width: 100%;
            clear: both;
            display: block;
            float: none;
        }

    </style>


    <script type="colorScheme" class="swatch active">
{
    "name":"Default",
    "bgBody":"ffffff",
    "link":"27A1E5",
    "color":"AAAAAA",
    "bgItem":"ffffff",
    "title":"444444"
}
</script>


</head>
<body paddingwidth="0" paddingheight="0" bgcolor="#d1d3d4"  style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"  style='font-family:Helvetica, sans-serif;'>
    <!-- =============== START HEADER =============== -->

    <tr>
        <td align='center'>
            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" >
                <tr>
                    <td class="bgItem" align="center">
                        <table width="580" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td class='movableContentContainer' align="center">

                                    <div class='movableContent'>
                                        <table width="580" border="0" cellspacing="0" cellpadding="10" align="center">
                                            <tr><td height='15'></td></tr>
                                            <tr>
                                                <td bgcolor="#313a45">
                                                    <table width="580" border="0" cellspacing="0" cellpadding="0" align="center">
                                                        <tr>
                                                            <td width='130'>
                                                                <div class='contentEditableContainer contentImageEditable'>
                                                                    <div class='contentEditable'>
                                                                        <a target='_blank' href="{{ url('/') }}"><img src="{{ Integra::asset('imagens/logo-umv.png') }}" alt="UMV" height='60' data-default="placeholder" data-max-width="200"></a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td width='10'></td>
                                                            <td valign="middle" style='vertical-align: middle;'>
                                                                <div class='contentEditableContainer contentTextEditable'>
                                                                    <div class='contentEditable' style='text-align: left;font-weight: light; color:#555555;font-size:26;line-height: 39px;font-family: Helvetica Neue;'>
                                                                        <h1 class='big'><a target='_blank' href="{{ url('/') }}"><img src="{{ Integra::asset('imagens/logo-maxmix.png') }}" alt="MaxMix" height='60' data-default="placeholder" data-max-width="200"></a></h1>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td width='10'></td>
                                                            <td valign="middle" style='vertical-align: middle;' width='150'>
                                                                <div class='contentEditableContainer contentTextEditable'>
                                                                    <div class='contentEditable' style='text-align: right;'>

                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <!-- =============== END HEADER =============== -->
                                    <!-- =============== START BODY =============== -->
                                    <div class='movableContent'>
                                        <table width="580" border="0" cellspacing="0" cellpadding="0" align="center">
                                            <tr><td height='40'></td></tr>
                                            <tr>
                                                <td>
                                                    <div class='contentEditableContainer contentImageEditable'>
                                                        <p>
                                                            Um cliente solicitou contato para usar o módulo de Sortimento.
                                                            <br>
                                                            <b>O contato foi enviado pelo site: {{ strtoupper($site->name) }} ({{ $site->url }})</b>
                                                        </p>
                                                        <br><br>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class='movableContent'>
                                        <table width="580" border="0" cellspacing="0" cellpadding="0" align="center">
                                            <tr><td height='40' colspan="3"></td></tr>
                                            <tr>
                                                <td>
                                                    <table width="410" cellpadding="6" cellspacing="2" align="center">
                                                        <thead>
                                                        <tr>
                                                            <th colspan="2">Dados do cliente</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>CNPJ:</td>
                                                                <td>{{ $request->input('cnpj') }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Razão Social:</td>
                                                                <td>{{ $request->input('name') }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>E-mail:</td>
                                                                <td>{{ $request->input('email') }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Telefone:</td>
                                                                <td>{{ $request->input('telefone') }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr><td height='40' colspan="3"></td></tr>
                                            <tr><td colspan="3"><hr style='height:1px;background:#DDDDDD;border:none;' ></td></tr>
                                        </table>
                                    </div>


                                    <!-- =============== END BODY =============== -->
                                    <!-- =============== START FOOTER =============== -->
                                    <div class='movableContent'>
                                        <table width="580" border="0" cellspacing="0" cellpadding="0" align="center">
                                            <tr><td colspan="3" height='48'></td></tr>
                                            <tr>
                                                <td width='90'></td>
                                                <td width='400'align='center' style='text-align: center;'>
                                                    <table width='400' cellpadding="0" cellspacing="0" align="center">
                                                        <tr>
                                                            <td>
                                                                <div class='contentEditableContainer contentTextEditable'>
                                                                    <div class='contentEditable' style='text-align: center;color:#AAAAAA;'>
                                                                        <p>
                                                                            Enviado por {{ $user->name }} <br/>
                                                                            {{ $user->email }} <br/>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width='90'></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- =============== END FOOTER =============== -->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>
