

<div class="row">
    <div class="col-md-5">
        <div class="panel-group panel-assortment" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-column panel-default">
                <div class="panel-heading" role="tab" id="headingNone">
                    <h4 class="panel-title">
                            <table class="table table-condensed">
                                <tr>
                                    <td width="55%">
                                        Subcategoria
                                    </td>
                                    @if(!$category->perishable)
                                    <td width="15%" class="text-right">
                                        Marcas
                                    </td>
                                    @endif
                                    <td width="20%" class="text-right">
                                        Participação
                                    </td>
                                    <td width="10%" class="text-right">
                                        Grupo
                                    </td>
                                </tr>
                            </table>

                    </h4>
                </div>
            </div>
            @foreach($subcategories as $key => $item)
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading{{ $item['subcategory_id'] }}">
                        <h4 class="panel-title">
                            <table class="table">
                                    <tr>
                                        <td width="55%">
                                            <b>{{ $item['subcategory'] }}</b>
                                        </td>
                                        @if(!$category->perishable)
                                        <td width="15%" class="text-right">
                                            {{ count($item['brands']) }}
                                        </td>
                                        @endif
                                        <td width="20%" class="text-right">
                                            <b>{{ number_format($item['percentage'], 2, ',', '.') }}%</b>
                                        </td>
                                        <td width="10%" class="text-right">
                                            <b class="label bg-class bg-class-{{ $item['classification'] }}">{{ $item['classification'] }}</b>
                                        </td>
                                    </tr>
                                </table>

                        </h4>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="col-md-7">

        <div class="row text-center">
            <div class="col-xs-3">
                <h3><b class="label bg-class bg-class-A">A</b></h3> <b>{{$table['A']['percentage']}}%</b> <br> {{$table['A']['sku']}} itens.
            </div>
            <div class="col-xs-3">
                <h3><b class="label bg-class bg-class-B">B</b></h3> <b>{{$table['B']['percentage']}}%</b> <br> {{$table['B']['sku']}} itens.
            </div>
            <div class="col-xs-3">
                <h3><b class="label bg-class bg-class-C">C</b></h3> <b>{{$table['C']['percentage']}}%</b> <br> {{$table['C']['sku']}} itens.
            </div>
            <div class="col-xs-3">
                <h3><b class="label bg-class bg-class-D">D</b></h3> <b>{{$table['D']['percentage']}}%</b> <br> {{$table['D']['sku']}} itens.
            </div>
        </div>

        <hr>

        <div class="well">

            <p class="text-center">
                Essa ferramenta permite avaliarmos a importância da subcategoria na loja e o seu peso dentro da categoria.
            </p>

        </div>

        <p class="text-center">
            <img src="https://chart.googleapis.com/chart?cht=p&chs=500x300&chp=4.71&chd=t:@foreach($subcategories as $key => $item){{($key!=0)?',':''}}{{ number_format($item['percentage'], 0, '.', '') }}@endforeach&chdl=@foreach($subcategories as $key => $item){{($key!=0)?'|':''}}{{number_format($item['percentage'], 2, ',', '.')}}% - {{$item['subcategory']}}@endforeach&chco=dc6000" class="img-responsive">
        </p>

    </div>
</div>
