@extends($config['view'].'layouts.master')

@section('content')

    <div class="container">
        <h2>
            <img src="{{ Integra::asset('imagens/icon-assortment.png') }}" alt="Sortimento" width="40">
            <a href="{{ route('sites.assortment.valuation.client', Hashids::encode($client->id)) }}">
                Sortimento [{{ $client->name }}] <small>- {{$tag->name}}</small>
            </a>
        </h2>
        <hr>
    </div>

    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <label for="">Escolha a categoria:</label>
                <select class="form-control" name="category" id="category">
                    <option value="">Selecione a categoria</option>
                    @foreach($categories as $id => $category)
                        <option value="{{ $id }}" @if($id == $category_id) selected="selected"@endif>{{ $category }}</option>
                    @endforeach
                </select>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <h3>COMPOSIÇÃO DA CATEGORIA</h3>
            </div>
        </div>

        <div class="card card-bordered form ">
            <div class="card-body">

                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>SUBCATEGORIAS</th>
                            <th width="10%">MARCAS</th>
                            <th width="10%">PARTICIPAÇÃO</th>
                            <th width="5">CLASSIFICAÇÃO</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subcategories as $key => $subcategory)
                        <tr>
                            <td>
                                <a href="{{ route('sites.assortment.valuation.brands', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}?s={{$subcategory['subcategory_id']}}">
                                    {{ $subcategory['subcategory'] }}
                                </a>
                            </td>
                            <td>{{ count($subcategory['brands']) }}</td>
                            <td>{{ number_format($subcategory['percentage'], 0, ',', '.') }}</td>
                            <td class="bg-class bg-class-{{ $subcategory['classification'] }}">{{ $subcategory['classification'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endsection

@push('scripts_ready')

$('#category').on('change', function(){

    var c = this.value;
    var url = "{{ route('sites.assortment.valuation.categories.composition', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}?c=";

    document.location.href = url+c;

});

@endpush