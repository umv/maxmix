@extends($config['view'].'layouts.master')

@section('content')
<div class="container">
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> Encontramos algums problemas com seus dados. Certifique de ter confirmado seu cadastro no e-mail enviado.<br><br>
		</div>
	@endif
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4">
			<div class="box-info">
				<h2>Faça seu login</h2>
				<form role="form" method="POST" action="{{ route('sites.auth.login') }}">
					{!! csrf_field() !!}

					<div class="form-group {{ $errors->first('email', ' has-error') }}">
						<label class="control-label">Endereço de e-mail</label>
						<input type="email" class="form-control" name="email" value="{{ old('email') }}">
						<span class="help-block">{{ $errors->first('email', ':message') }}</span>
					</div>

					<div class="form-group {{ $errors->first('password', ' has-error') }}">
						<label class="control-label">Senha</label>
						<input type="password" class="form-control" name="password">
						<span class="help-block">{{ $errors->first('password', ':message') }}</span>
					</div>

					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="remember"> Lembrar de mim
							</label>
						</div>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">
							ENTRAR
						</button>
					</div>

					<div class="text-center">
						<a class="btn btn-link btn-sm" href="{{ route('sites.auth.password.email') }}">Esqueceu sua senha?</a>
						<div class="clearfix"></div>
						<a class="btn btn-link btn-sm" href="{{ route('sites.auth.register') }}">Cadastrar</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
