@extends($config['view'].'layouts.master')

@section('content')
<div class="container">
	<h1 class="title"><i class="material-icons">account_box</i> Conta</h1>
	<hr>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> Encontramos algums problemas com seus dados.<br><br>
				</div>
			@endif
			<form role="form" method="POST" action="{{ route('sites.auth.account') }}">
				<fieldset>
					<legend>Atualize suas informações</legend>
					{!! csrf_field() !!}
					<div class="form-group {{ $errors->first('name', ' has-error') }}">
						<label class="control-label">Nome</label>
						<input type="text" class="form-control" name="name" value="{{ old('name', $user->name) }}">
						<span class="help-block">{{ $errors->first('name', ':message') }}</span>
					</div>

					<div class="form-group {{ $errors->first('email', ' has-error') }}">
						<label class="control-label">Endereço de e-mail</label>
						<input type="email" class="form-control" name="email" value="{{ old('email', $user->email) }}">
						<span class="help-block">{{ $errors->first('email', ':message') }}</span>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group {{ $errors->first('password', ' has-error') }}">
								<label class="control-label">Senha</label>
								<input type="password" class="form-control" name="password">
								<span class="help-block">{{ $errors->first('password', ':message') }}</span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group {{ $errors->first('password_confirmation', ' has-error') }}">
								<label class="control-label">Confirmação de senha</label>
								<input type="password" class="form-control" name="password_confirmation">
								<span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary pull-right">
							Atualizar
						</button>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
@endsection
