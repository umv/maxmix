<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Confirmação de cadastro</title>
</head>
<body>
<h1>Obrigado pelo cadastro!</h1>

<p>
    Nós só precisamos que você <a href="{{ route('sites.auth.register.confirm', $user->token) }}">confirme seu endereço de e-mail</a>, é rápido!
</p>
</body>
</html>