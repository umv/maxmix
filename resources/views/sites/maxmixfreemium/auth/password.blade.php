@extends($config['view'].'layouts.master')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="box-info">
				<h2>Esqueceu sua senha?</h2>
				@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
				@endif
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> Encontramos algums problemas com seus dados.<br><br>
					</div>
				@endif
				<form role="form" method="POST" action="{{ route('sites.auth.password.email') }}">
					{!! csrf_field() !!}

					<div class="form-group {{ $errors->first('email', ' has-error') }}">
						<label class="control-label">Endereço de e-mail</label>
						<input type="email" class="form-control" name="email" value="{{ old('email') }}">
						<span class="help-block">{{ $errors->first('email', ':message') }}</span>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-lg btn-block">
							ENVIAR LINK DE ALTERAÇÃO DE SENHA
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection
