@extends($config['view'].'layouts.master')

@section('content')

    <div class="container">
        <h1 class="title pull-left margin-0"><img src="{{ Integra::asset('imagens/icon-planogram-outline.png') }}" alt="Planograma" width="50" class="margin-r-10"> <a href="{{ route('sites.planogram.resource.index') }}">Planogramas gerados</a></h1>
        <a href="{{ route('sites.planogram.generator') }}#steps" class="btn btn-primary pull-right"><i class="material-icons">add_circle_outline</i> Novo planograma</a>
        <div class="clearfix"></div>
        <hr>
    </div>

    @include($config['view'].'datatables.index')
@endsection