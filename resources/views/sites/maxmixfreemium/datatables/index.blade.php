
@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/t/bs/dt-1.10.11,r-2.0.2,rr-1.1.1,se-1.1.2/datatables.min.css"/>
@endpush
    <section class="container">

        <div >

            <div id="integra-datatables-toolbar">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                {{ trans('list.options') }} <i class="glyphicon glyphicon-triangle-bottom"></i>
                            </button>
                            <ul class="dropdown-menu animation-expand" role="menu">
                                {{--<li><a href="#">{{ trans('list.import') }}</a></li>
                                <li><a href="#">{{ trans('list.export') }}</a></li>
                                <li class="divider"></li>--}}
                                <li><a href="javascript:void(0)" data-integra="datatables-delete-selected"><i class="glyphicon glyphicon-trash text-danger"></i> {{ trans('list.delete') }}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-8">
                        {{--<input type="text" data-integra="datatables-search" class="form-control" placeholder="Faça sua pesquisa">--}}
                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-integra="datatables-page-go" data-container="body" data-toggle="popover" data-placement="bottom" data-content='<div data-integra="datatables-page-go-content" class="row" style="width: 250px;">
                                        <div class="col-lg-4">
                                            <input type="number" class="form-control input-sm" name="page" value="" min="1" max="10">
                                        </div>
                                        <div class="col-lg-4">
                                            de <span name="pages"><span>
                                        </div>
                                        <div class="col-lg-4">
                                            <button type="button" name="go" class="btn btn-default btn-sm">{{ trans('list.go') }}</button>
                                        </div>
                                    </div>' data-html="true" data-original-title="{{ trans('list.go_title') }}" title="{{ trans('list.go_title') }}">
                                    <i class="glyphicon glyphicon-file"></i>
                                </button>
                            </div>
                            <div class="btn-group">
                                <button type="button" data-integra="datatables-previous" class="btn btn-sm ink-reaction btn-default-light">
                                    <i class="glyphicon glyphicon-chevron-left"></i>
                                </button>
                                <button type="button" data-integra="datatables-next" class="btn btn-sm ink-reaction btn-default-light">
                                    <i class="glyphicon glyphicon-chevron-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div id="integra-datatables-content">
                        {!! $datatables->table(array('class'=>'table table-hover table-striped table-bordered table-app', 'style' => '')) !!}
                    </div>
                </div>
            </div>
        </div>

    </section>



@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/t/bs/dt-1.10.11,r-2.0.2,rr-1.1.1,se-1.1.2/datatables.min.js"></script>
<script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script>
    function datatablesDeleteItens(data){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.post( "{{route($resource->getRouteStore())}}", { type: 'DELETE', data: data }, function(){LaravelDataTables.dataTableBuilder.draw();} );
    }
    function datatablesReorderItens(data){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.post( "{{route($resource->getRouteStore())}}", { type: 'REORDER', data :data}, function(){LaravelDataTables.dataTableBuilder.draw();} );
    }
    function datatablesFilterGlobal(){
        $('#dataTableBuilder').DataTable().search(
                $('[data-integra="datatables-search"]').val()
        ).draw();
    }
    function datatablesPagination(){
        var pages = LaravelDataTables.dataTableBuilder.page.info().pages;
        var page = LaravelDataTables.dataTableBuilder.page();
        (page == 0) ? $('[data-integra="datatables-previous"]').attr('disabled', true) : $('[data-integra="datatables-previous"]').attr('disabled', false);
        ((page+1) == pages) ? $('[data-integra="datatables-next"]').attr('disabled', true) : $('[data-integra="datatables-next"]').attr('disabled', false);
        (pages == 1) ? $('[data-integra="datatables-page-go"]').attr('disabled', true) : $('[data-integra="datatables-page-go"]').attr('disabled', false);
    }
    function datatablesFormat(){
        datatablesFormatCheckbox();
        datatablesFormatActions();
        datatablesPagination();
        $('[data-toggle="tooltip"]').tooltip();
    }
    function datatablesMakeCheckbox(data, type, full, meta){
        return '<input type="checkbox" value="'+data+'" data-integra="datatables-delete-checkbox">';
    }
    function datatablesMakeActions( data, type, row, meta ) {
        return '<div class="text-center">'
                + '<a class="btn btn-icon-toggle btn-xs btn-info" data-integra="datatables-read" data-id="'+data+'" href="{{ route($resource->getRouteShow(), '#1') }}" data-toggle="tooltip" data-placement="top" title="Visualizar" style="margin-right: 5px;"><i class="glyphicon glyphicon-th-list"></i></a>'
                + '<a class="btn btn-icon-toggle btn-xs btn-warning" data-integra="datatables-update" data-id="'+data+'" href="{{ route($resource->getRouteEdit(), '#1') }}" data-toggle="tooltip" data-placement="top" title="Editar" style="margin-right: 5px;"><i class="glyphicon glyphicon-pencil"></i></a>'
                + '<a class="btn btn-icon-toggle btn-xs btn-danger" data-integra="datatables-delete" data-id="'+data+'" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Remover"><i class="glyphicon glyphicon-trash"></i></a>'
                + '</div>'
    }
    function datatablesMakeActionRead( data, type, row, meta ) {
        return '<div class="text-center">'
                + '<a class="btn btn-icon-toggle btn-xs btn-info" data-integra="datatables-read" data-id="'+data+'" href="{{ route($resource->getRouteShow(), '#1') }}" data-toggle="tooltip" data-placement="top" title="Visualizar"><i class="glyphicon glyphicon-th-list"></i></a>'
                + '</div>'
    }
    function datatablesMakeActionReadDelete( data, type, row, meta ) {
        return '<div class="text-center">'
                + '<a class="btn btn-icon-toggle btn-xs btn-info" data-integra="datatables-read" data-id="'+data+'" href="{{ route($resource->getRouteShow(), '#1') }}" data-toggle="tooltip" data-placement="top" title="Visualizar" style="margin-right: 5px;"><i class="glyphicon glyphicon-th-list"></i></a>'
                + '<a class="btn btn-icon-toggle btn-xs btn-danger" data-integra="datatables-delete" data-id="'+data+'" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Remover"><i class="glyphicon glyphicon-trash"></i></a>'
                + '</div>'
    }
    function datatablesFormatCheckbox(){
        $('[data-integra="datatables-delete-checkbox"]').on('click', function(){
            ($(this).attr('checked')) ? $(this).parent().parent().addClass('warning') : $(this).parent().parent().removeClass('warning');
        });
    }
    function datatablesFormatActions(){
        $('[data-integra="datatables-read"], [data-integra="datatables-update"]').each( function(){
            $(this).attr('href', $(this).attr('href').replace('#1', $(this).data('id')));
        });
        $('[data-integra="datatables-delete"]').on('click', function(){
            if(confirm("Deseja realmente excluir este item?")){
                var data = [$(this).data('id')];
                datatablesDeleteItens(data);
                //window.location.href = $(this).data('href').replace('#1', $(this).data('id'));
            }
        });
    }
    function datatablesFormatImage(data){
        var asset = '{{ asset('/') }}';
        return (data) ? '<img src="'+asset+data+'?{{date('U')}}" height="50">' : '<img src="{{ asset('assets/admin/img/image-default-datatables.png') }}" height="50">';
    }
    function datatablesFormatActive(data, type, full, meta){
        //console.log(full);
        return (data) ? '<a href="javascript:void(0);" data-id="'+full['id']+'"><i class="md md-lens text-success" data-toggle="tooltip" data-placement="top" title="Ativo"></i></a>' : '<i class="md md-lens text-danger" data-toggle="tooltip" data-placement="top" title="Inativo"></i>';
    }
    function datatablesFormatMoment(data){
        return (data) ? moment(data, 'YYYY-MM-DD HH:mm:ss').fromNow(): '';
    }
    function datatablesFormatDate(data){
        return (data) ? moment(data, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY') : '';
    }
    function datatablesFormatDatetime(data) {
        return (data) ? moment(data, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY HH:m:s') : '';
    }
</script>

{!! $datatables->scripts() !!}
@endpush

@push('scripts_ready')

    moment.locale('pt_br');

    // Busca global
    $('[data-integra="datatables-search"]').on( 'keyup click', function () {
        datatablesFilterGlobal();
    } );

    // Filtros individuais
    $("#dataTableBuilder tfoot tr").appendTo($("#dataTableBuilder thead"));

    LaravelDataTables.dataTableBuilder.columns().every(function(){
        var column = this;
        var input = document.createElement("input");
        input.setAttribute("class","form-control input-sm");
        input.setAttribute("style","width:100%");
        $(input).appendTo($(column.footer()).empty()).on('change', function () {
            column.search($(this).val()).draw();
        });
    });

    $("#dataTableBuilder thead tr:nth-child(2) th.datatables-column-checkbox, #dataTableBuilder thead tr:nth-child(2) th.datatables-column-action").empty();

    // Paginacao
    $('[data-integra="datatables-next"]').on('click', function(){
        LaravelDataTables.dataTableBuilder.page( 'next' ).draw( 'page' );
    });

    $('[data-integra="datatables-previous"]').on('click', function(){
        LaravelDataTables.dataTableBuilder.page( 'previous' ).draw( 'page' );
    });

    $('[data-integra="datatables-page-go"]').on('shown.bs.popover', function () {
        var info = LaravelDataTables.dataTableBuilder.page.info();
        $('[data-integra="datatables-page-go-content"] input[name="page"]').val(info.page+1).attr('max', info.pages);
        $('[data-integra="datatables-page-go-content"] span[name="pages"]').text(info.pages);
        $('[data-integra="datatables-page-go-content"] button[name="go"]').on( 'click', function () {
            var page = $('[data-integra="datatables-page-go-content"] input[name="page"]').val();
            (page > info.pages) ? alert("Página não existe!") : LaravelDataTables.dataTableBuilder.page(page-1).draw( 'page' );
        });
    })

    //Checkbox
    $('[data-integra="datatables-delete-selected"]').on('click', function(){

        var data = [];

        $('[data-integra="datatables-delete-checkbox"]').each( function(){
console.log($(this).val());
            ($(this).is(':checked')) ? data.push($(this).val()) : '';
        });

console.log(data);

        if(data.length < 1){

            alert("Selecione no mínimo um item para excluir");

        } else {

            if(confirm("Deseja realmente excluir os itens selecionados?")){

                datatablesDeleteItens(data);

            }

        }

    });

    // Row Reorder
    LaravelDataTables.dataTableBuilder.on( 'row-reorder', function ( e, diff, edit ) {

        var data = [];

        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            data[i] = {id: LaravelDataTables.dataTableBuilder.row( diff[i].node ).data()['id'], old:(diff[i].oldPosition+1), new:(diff[i].newPosition+1)};

        }
        datatablesReorderItens(data);

    } );

    //Actions
    $('#dataTableBuilder')
            .on('draw.dt', function(){
                datatablesFormat();
            })
            .dataTable();

@endpush

