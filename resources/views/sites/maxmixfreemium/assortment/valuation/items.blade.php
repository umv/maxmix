@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.valuation.inc.header')

    @include($config['view'].'assortment.valuation.inc.steps', ['step' => 'itens'])

    <div class="container">

        <div class="row">
            <div class="col-sm-8">
                <h3 class="title">{{ trans('sites/maxmix/assortment.title.itens') }}</h3>
            </div>
            <div class="col-sm-4">
                @if($subcategory_id)
                <a href="{{ route('sites.assortment.valuation.items', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}?s={{$subcategory_id}}&print=1" target="_blank" class="btn btn-primary btn-sm pull-right"><i class="material-icons">print</i> Imprimir</a>
                @endif
            </div>
        </div>

        <br><br>

        <div class="row">
            <div class="col-md-6 @if(!isset($itens)) col-md-offset-3 @endif">
                <div class="form-group has-success-">
                    <label class="upper">{{ trans('sites/general.subcategory') }}</label>
                    {!! Form::select( 'subcategory', $subcategories_grouped, Input::old( 'subcategory', $subcategory_id ), ['id' => 'subcategory', 'class' => 'form-control input-lg input-primary' ,'placeholder' => trans('sites/general.select_subcategory') ]) !!}
                </div>
            </div>
            @if(isset($itens))
                <div class="col-md-6">

                    <div class="row">
                        <div class="col-xs-4 text-center">
                            <label class="upper">{{ trans('sites/general.position') }}</label>
                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.item.info.positon') }}"><i class="material-icons">info_outline</i></small>
                            <br>
                            <h1><b class="text-default">{{ $position }}</b></h1>
                        </div>
                        <div class="col-xs-4 text-center">
                            <label class="upper">{{ trans('sites/general.margin') }}</label>
                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.item.info.margin') }}"><i class="material-icons">info_outline</i></small>
                            <br>
                            <h1><b class="text-default">{{ number_format($margin, 2, ',', '.') }}%</b></h1>
                        </div>
                        <div class="col-xs-4 text-center">
                            <label class="upper">{{ trans('sites/general.group') }}</label>
                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.item.info.group') }}"><i class="material-icons">info_outline</i></small>
                            <br>
                            <h1><b class="label bg-class bg-class-{{ $classification }}">{{ $classification }}</b></h1>
                        </div>
                    </div>

                </div>
            @endif

        </div>

        <hr>

        @if(isset($itens))

            <div class="row">
                <div class="col-md-6">
                    <div class="panel-group panel-assortment" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-column panel-default">
                            <div class="panel-heading" role="tab" id="headingNone">
                                <h4 class="panel-title">
                                    <a role="button" class="btn-block" href="#">
                                        <table class="table table-condensed">
                                            <tr>
                                                <td width="50%">
                                                    Item
                                                </td>
                                                <td width="20%" class="text-center">
                                                    {{ trans('sites/general.brand') }}
                                                </td>
                                                <td width="10%" class="text-right">
                                                    {{ trans('sites/general.participation') }}
                                                </td>
                                                <td width="15%" class="text-right">
                                                    {{ trans('sites/general.margin') }}
                                                </td>
                                                <td width="5%" class="text-right">
                                                    Class.
                                                </td>
                                            </tr>
                                        </table>
                                    </a>
                                </h4>
                            </div>
                        </div>
                        @foreach($itens as $key => $item)
                            <div class="panel panel-{{ ($item['recomendation']['W'] != 0) ? 'primary' : 'default' }}">
                                <div class="panel-heading" role="tab" id="heading{{ $item['product_id'] }}">
                                    <h4 class="panel-title">
                                        <a role="button" class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $item['product_id'] }}" aria-expanded="true" aria-controls="collapse{{ $item['product_id'] }}">
                                            <table class="table">
                                                <tr>
                                                    <td width="50%">
                                                        <b>{{ $item['product'] }}</b>
                                                        <br><small>EAN: {{ $item['ean'] }}</small>
                                                    </td>
                                                    <td width="20%">
                                                        <span class="{{ (!$item['brand_valuation']['status']) ? 'text-danger' : 'text-success-' }}">
                                                            {{ $item['brand'] }}
                                                        </span>
                                                    </td>
                                                    <td width="10%" class="text-right">
                                                        {{ number_format($item['participation'], 2, ',', '.') }}%
                                                    </td>
                                                    <td width="15%" class="text-right">
                                                        <span class="{{ ($item['percentage'] < $margin) ? 'text-danger' : 'text-success-' }}">
                                                            <b>{{ number_format($item['percentage'], 2, ',', '.') }}%</b>
                                                        </span>
                                                    </td>
                                                    <td width="5%" class="text-right">
                                                        <b class="label bg-class bg-class-{{ $item['classification'] }}">{{ $item['classification'] }}</b>
                                                    </td>
                                                </tr>
                                            </table>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse{{ $item['product_id'] }}" class="panel-collapse collapse @if($key == 0) in @endif" role="tabpanel" aria-labelledby="heading{{ $item['product_id'] }}">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td width="15%">
                                                <span class="text-center">
                                                    <p><small><b>{{ trans('sites/maxmix/assortment.item.label.amount') }}</b></small></p>
                                                    <p>
                                                        {{ number_format($item['amount'], 0, ',', '.') }}
                                                        @if($key == 0)
                                                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.item.tip.amount') }}"><i class="material-icons">info_outline</i></small>
                                                        @endif
                                                    </p>
                                                </span>
                                            </td>
                                            <td width="25%">
                                                <span class="text-center">
                                                    <p><small><b>{{ trans('sites/maxmix/assortment.item.label.price') }}</b></small></p>
                                                    <p>
                                                        R$ {{ number_format($item['unit_price'], 2, ',', '.') }}
                                                        @if($key == 0)
                                                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.item.tip.price') }}"><i class="material-icons">info_outline</i></small>
                                                        @endif
                                                    </p>
                                                </span>
                                            </td>
                                            <td width="30%">
                                                <span class="text-center">
                                                    <p><small><b class="upper">{{ trans('sites/maxmix/assortment.average_revenue') }}</b></small></p>
                                                    <p>
                                                        R$ {{ number_format($item['total_revenue'], 2, ',', '.') }}
                                                        @if($key == 0)
                                                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.item.tip.revenue') }}"><i class="material-icons">info_outline</i></small>
                                                        @endif
                                                    </p>
                                                </span>
                                            </td>
                                            <td width="30%">
                                                <span class="text-center">
                                                    <p><small><b class="upper">{{ trans('sites/maxmix/assortment.average_margin') }}</b></small></p>
                                                    <p>
                                                        R$ {{ number_format($item['total_gross_margin'], 2, ',', '.') }}
                                                        @if($key == 0)
                                                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.item.tip.margin') }}"><i class="material-icons">info_outline</i></small>
                                                        @endif
                                                    </p>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="text-center active">
                                                <div class="row">
                                                    <div class="col-sm-6 text-center">
                                                        <p><small class="upper text-muted">RECOMENDAÇÃO FINANCEIRA</small></p>
                                                        <span class="upper">
                                                            <b>
                                                                @if($item['recomendation']['status'])
                                                                    <i class="material-icons text-success">thumb_up</i>
                                                                @else
                                                                    <i class="material-icons text-warning">warning</i>
                                                                @endif
                                                                {{ $item['recomendation']['info'] }}
                                                            </b>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 text-center">
                                                        <p><small class="upper text-muted">IMPORTÂNCIA DO ITEM</small></p>
                                                        <span class=" upper">
                                                                <b>{{ $matriz[$classification.$item['classification']] }}</b>
                                                            </span>
                                                        @if($key == 0)
                                                            <a tabindex="0" class="popover-open text-muted" role="button" data-toggle="popover" data-container="body" data-placement="bottom" data-trigger="hover focus" title="IMPORTÂNCIA" data-content="{{ trans('sites/maxmix/assortment.item.popover.importance') }}"><small><i class="material-icons">info_outline</i></small></a>
                                                        @endif
                                                    </div>
                                                </div>
                                                {{--<pre>
                                                    {{ print_r($item['recomendation']) }}
                                                </pre>
                                                <span class="text-{{ $matriz[$classification.$item['classification']]{0} }} upper">
                                                    <b>{{ $matriz[$classification.$item['classification']] }}</b>
                                                </span>
                                                @if($key == 0)
                                                    <a tabindex="0" class="popover-open text-muted" role="button" data-toggle="popover" data-container="body" data-placement="bottom" data-trigger="hover focus" title="IMPORTÂNCIA" data-content="{{ trans('sites/maxmix/assortment.item.popover.importance') }}"><small><i class="material-icons">info_outline</i></small></a>
                                                @endif

                                                {!! ($item['percentage'] < $margin) ? '<br><span class="text-danger">'.trans('sites/maxmix/assortment.item.alert.margin').'</span>' : '' !!}

                                                --}}

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-6">
                    @if($working_capital > 0)
                    <div class="text-center">
                        <h3>Capital de giro adicional: <br> <b>R$ {{ number_format($working_capital, 2, ',', '.') }}</b></h3>
                        <p class="text-muted">Este é valor do capital de giro adicional com a remoção dos itens avaliados para exclusão</p>
                    </div>
                    @endif
                    <div class="jumbotron jumbotron-default">
                        <p class="text-center">
                            {{ trans('sites/maxmix/assortment.item.phrase_1') }}
                        </p>

                        <div class="row">
                            <div class="media">
                                <div class="media-left media-middle">
                                    <h2><i class="material-icons text-muted">looks_one</i></h2>
                                </div>
                                <div class="media-body">
                                    <h4>{{ trans('sites/maxmix/assortment.item.phrase_2') }}</h4>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="media">
                                <div class="media-left media-middle">
                                    <h2><i class="material-icons text-muted">looks_two</i></h2>
                                </div>
                                <div class="media-body">
                                    <h4>{{ trans('sites/maxmix/assortment.item.phrase_3') }}</h4>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="text-center">
                            <img src="{{ Integra::asset('imagens/assortment/classificacao.png') }}" class="img-responsive center-block" width="260" alt="Sortimento">
                        </div>
                    </div>
                </div>
            </div>

        @else

            <div class="text-center">
                <h1>
                    <big><i class="material-icons text-muted">arrow_upward</i></big>
                    <br>
                    <small>{{ trans('sites/maxmix/assortment.select_category') }}</small>
                </h1>
            </div>

        @endif

        <hr>
        <div class="navbar-fixed-bottom">

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="navigate text-center">

                    @if(isset($itens))
                    <a href="{{ route('sites.assortment.valuation.items', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}?s={{$subcategory_id}}&print=1&mix=1" target="_blank" class="btn btn-primary">
                        <b>MIX IDEAL</b>
                    </a>
                    @endif

                    <a href="{{ route('sites.assortment.valuation.brands', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}?s={{$subcategory_id}}#steps" class="btn prev">
                        <i class="material-icons">arrow_back</i>
                        <span>{{ trans('sites/general.previous') }}</span>
                    </a>

                    <a href="{{ route('sites.assortment.valuation.client', Hashids::encode($client->id)) }}" class="btn next">
                        <span>{{ trans('sites/general.conclude') }}</span>
                        <i class="material-icons">arrow_forward</i>
                    </a>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts_ready')

$('#subcategory').on('change', function(){

    var s = this.value;
    var url = "{{ route('sites.assortment.valuation.items', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}?s=";

    document.location.href = url+s+"#steps";

});

$('.popover-open').popover('show');
setTimeout(function() {$('.popover-open').popover('hide')},3000);

@endpush