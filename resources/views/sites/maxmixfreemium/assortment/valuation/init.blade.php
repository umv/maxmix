@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.valuation.inc.header')

    @include($config['view'].'assortment.valuation.inc.steps', ['step' => 'init'])

    <div class="container">

        <h3 class="title">{{ trans('sites/maxmix/assortment.title.init') }}</h3>

        <br><br>

        <div class="jumbotron jumbotron-default">

            <p class="text-center">
                {{ trans('sites/maxmix/assortment.init.phrase_1') }}:
            </p>

            <div class="row text-center">
                <div class="col-md-3">
                    <h2><b><i class="material-icons text-muted">looks_one</i></b> <br> {{ trans('sites/maxmix/assortment.init.step_1') }}</h2>
                </div>
                <div class="col-md-3">
                    <h2><b><i class="material-icons  text-muted">looks_two</i></b> <br> {{ trans('sites/maxmix/assortment.init.step_2') }}</h2>
                </div>
                <div class="col-md-3">
                    <h2><b><i class="material-icons  text-muted">looks_3</i></b> <br> {{ trans('sites/maxmix/assortment.init.step_3') }}</h2>
                </div>
                <div class="col-md-3">
                    <h2><b><i class="material-icons  text-muted">looks_4</i></b> <br> {{ trans('sites/maxmix/assortment.init.step_4') }}</h2>
                </div>
            </div>

            <br>

            <div class="text-center alert alert-default">
                ...
                <br><br>
                <p class="text-default">
                    {{ trans('sites/maxmix/assortment.init.phrase_2') }}
                </p>
            </div>

            <hr>

            <div class="row">
                <div class="col-md-6 text-center">
                    <img src="{{ Integra::asset('imagens/assortment/grupo.png') }}" class="img-responsive" width="250" alt="Sortimento">
                </div>
                <div class="col-md-6">
                    <p class="text-center">
                        <br><br><br>
                        {{ trans('sites/maxmix/assortment.init.phrase_3') }}
                    </p>
                </div>
            </div>

        </div>

        <hr>

        <div class="navigate">
            <a href="{{ route('sites.assortment.valuation.cockpit', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps" class="btn next">
                <span>{{ trans('sites/general.next') }}</span>
                <i class="material-icons">arrow_forward</i>
            </a>

            <div class="clearfix"></div>
        </div>

    </div>


@endsection