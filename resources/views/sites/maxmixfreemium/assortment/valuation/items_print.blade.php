<html>

    <head>

        <link rel="stylesheet" href="{{ Integra::asset('css/bootstrap.min.css') }}">

        <style>
            *{
                font-size: 12px !important;
            }
        </style>
    </head>

    <body>

        <div class="text-center hidden-print">
            <br>
            <button class="btn btn-primary " onclick="window.print()">IMPRIMIR</button>
        </div>

        <div class="container">

            <h2>
                @if(isset($client) && isset($tag))
                        {{ trans('sites/general.assortment') }} [{{ $client->name }}] <small>- {{$tag->name}}</small>
                @elseif(isset($client))
                        {{ trans('sites/general.assortment') }} [{{ $client->name }}]
                @else
                        {{ trans('sites/general.assortment') }}
                @endif
            </h2>

            <h3>{{ ($mix) ? 'Mix ideal' : 'Avaliação de itens' }} da subcategoria: <b>{{ $subcategory->name }}</b></h3>

            <hr>

            @if(!$mix)
            <div class="row">

                <div class="col-xs-6">
                    <p>
                        <strong>{{ trans('sites/general.position') }}:</strong>
                        <big>{{ $position }}</big>

                        - <strong>{{ trans('sites/general.margin') }}:</strong>
                        <big>{{ number_format($margin, 2, ',', '.') }}%</big>

                        - <strong>{{ trans('sites/general.group') }}:</strong>
                        <big>{{ $classification }}</big>
                    </p>
                </div>

            </div>
            @endif

            <table class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>EAN</th>
                        @if($mix)
                        <th>Item</th>
                        @endif
                        <th>{{ trans('sites/general.brand') }}</th>
                        @if(!$mix)
                        <th>{{ trans('sites/maxmix/assortment.cockipt.label.amount') }}</th>
                        <th>{{ trans('sites/maxmix/assortment.cockipt.label.price') }} (R$)</th>
                        <th>{{ trans('sites/maxmix/assortment.average_revenue') }} (R$)</th>
                        <th>{{ trans('sites/maxmix/assortment.average_margin') }} (R$)</th>
                        <th>{{ trans('sites/general.margin') }} (%)</th>
                        <th>{{ trans('sites/general.participation') }} (%)</th>
                        <th>Class.</th>
                        @elseif($request->brand_size)
                        <th>Tamanho</th>
                        @endif
                        <th>Recomendação</th>
                        @if(!$mix)
                        <th>Importância</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($itens as $key => $item)
                        @if(!$mix || ($mix && $item['brand_valuation']['status'] && $item['recomendation']['W'] != 0))
                            <tr>
                                @if($mix)
                                <td>{{ $item['ean'] }}</td>
                                @endif
                                <td>
                                    <b>{{ $item['product'] }}</b>
                                    @if(!$mix)
                                    <br><small>EAN: {{ $item['ean'] }}</small>
                                    @endif
                                </td>
                                <td>
                                    {{ $item['brand'] }}
                                </td>
                                @if(!$mix)
                                <td class="text-right">
                                    {{ number_format($item['amount'], 0, ',', '.') }}
                                </td>
                                <td class="text-right">
                                    {{ number_format($item['unit_price'], 2, ',', '.') }}
                                </td>
                                <td class="text-right">
                                    {{ number_format($item['total_revenue'], 2, ',', '.') }}
                                </td>
                                <td class="text-right">
                                    <b>{{ number_format($item['total_gross_margin'], 2, ',', '.') }}</b>
                                </td>
                                <td class="text-right">
                                    {{ number_format($item['percentage'], 2, ',', '.') }}
                                </td>
                                <td class="text-right">
                                    {{ number_format($item['participation'], 2, ',', '.') }}
                                </td>
                                <td class="text-center">
                                    {{ $item['classification'] }}
                                </td>
                                @elseif($request->brand_size)
                                <td class="text-right">{{ number_format($item['length'], 0, ',', '.') }} cm</td>
                                @endif
                                <td>
                                    {{ $item['recomendation']['info'] }}
                                </td>
                                @if(!$mix)
                                <td>
                                    {{ $matriz[$classification.$item['classification']] }}
                                </td>
                                @endif
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>

        </div>

    </body>

</html>




