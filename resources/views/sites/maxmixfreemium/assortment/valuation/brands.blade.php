@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.valuation.inc.header')

    @include($config['view'].'assortment.valuation.inc.steps', ['step' => 'brands'])

    <div class="container">

        <h3 class="title">{{ trans('sites/maxmix/assortment.title.brands') }}</h3>

        <br><br>

        <div class="row">
            <div class="col-md-6 @if(!isset($brands)) col-md-offset-3 @endif">
                <div class="form-group has-success-">
                    <label class="upper">{{ trans('sites/general.subcategory') }}</label>
                    {!! Form::select( 'subcategory', $subcategories_grouped, Input::old( 'subcategory', $subcategory_id ), ['id' => 'subcategory', 'class' => 'form-control input-lg input-primary' ,'placeholder' => trans('sites/general.select_subcategory') ]) !!}
                </div>
            </div>
            @if(isset($brands))
            <div class="col-md-6">

                <div class="row">
                    <div class="col-xs-6 text-center">
                        <label class="upper">{{ trans('sites/general.margin') }}</label>
                        <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.brand.info.margin') }}"><i class="material-icons">info_outline</i></small>
                        <br>
                        <h1><b class="text-default">{{ number_format($margin, 2, ',', '.') }}%</b></h1>
                    </div>
                    <div class="col-xs-6 text-center">
                        <label class="upper">{{ trans('sites/general.group') }}</label>
                        <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.brand.info.group') }}"><i class="material-icons">info_outline</i></small>
                        <br>
                        <h1><b class="label bg-class bg-class-{{ $classification }}">{{ $classification }}</b></h1>
                    </div>
                </div>

            </div>
            @endif

        </div>

        <hr>

        @if(isset($brands))

        <div class="row">
            <div class="col-md-6">
                <div class="panel-group panel-assortment" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-column panel-default">
                        <div class="panel-heading" role="tab" id="headingNone">
                            <h4 class="panel-title">
                                <a role="button" class="btn-block" href="#">
                                    <table class="table table-condensed">
                                        <tr>
                                            <td width="45%">
                                                {{ trans('sites/general.brand') }}
                                            </td>
                                            <td width="20%" class="text-right">
                                                {{ trans('sites/general.participation') }}
                                            </td>
                                            <td width="15%" class="text-right">
                                                <a tabindex="0" class="popover-open" role="button" data-toggle="popover" data-container="body" data-placement="top" data-trigger="hover focus" title="{{ strtoupper(trans('sites/general.margin')) }}" data-content="{{ trans('sites/maxmix/assortment.brand.popover.margin') }}">
                                                    {{ trans('sites/general.margin') }}
                                                </a>
                                            </td>
                                            <td width="20%" class="text-right">
                                                Info.
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                            </h4>
                        </div>
                    </div>
                    @foreach($brands as $key => $item)
                        <div class="panel panel-{{ ($item['status'] == 0) ? 'default' : 'primary' }}">
                            <div class="panel-heading" role="tab" id="heading{{ $item['brand_id'] }}">
                                <h4 class="panel-title">
                                    <a role="button" class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $item['brand_id'] }}" aria-expanded="true" aria-controls="collapse{{ $item['brand_id'] }}">
                                        <table class="table">
                                            <tr>
                                                <td width="45%">
                                                    <b>{{ $item['brand'] }}</b>
                                                </td>
                                                <td width="20%" class="text-right">
                                                    {{ number_format($item['participation'], 2, ',', '.') }}%
                                                </td>
                                                <td width="15%" class="text-right">
                                                    <span class="{{ ($item['percentage_gross_margin'] < $margin) ? 'text-danger' : 'text-success-' }}">
                                                        <b>{{ number_format($item['percentage_gross_margin'], 2, ',', '.') }}%</b>
                                                    </span>
                                                </td>
                                                <td width="20%" class="text-right">
                                                    <span class="label {{ ($item['status'] == 0) ? 'label-danger' : 'label-success' }}">{{ strtoupper( ($item['status'] == 0) ? trans('sites/general.rate') : trans('sites/general.keep') ) }}</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{ $item['brand_id'] }}" class="panel-collapse collapse @if($key == 0) in @endif" role="tabpanel" aria-labelledby="heading{{ $item['brand_id'] }}">
                                <table class="table table-bordered">
                                    <tr>
                                        <td width="15%">
                                        <span class="text-center">
                                            <p><small><b>SKU's</b></small></p>
                                            <p>
                                                {{ $item['sku'] }}
                                                @if($key == 0)
                                                    <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.brand.tip.sku') }}"><i class="material-icons">info_outline</i></small>
                                                @endif
                                            </p>
                                        </span>
                                        </td>
                                        <td width="42.5%">
                                        <span class="text-center">
                                            <p><small><b>{{ trans('sites/maxmix/assortment.average_revenue') }}</b></small></p>
                                            <p>
                                                R$ {{ number_format($item['total_revenue'], 2, ',', '.') }}
                                                @if($key == 0)
                                                    <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.category.tip.revenue') }}"><i class="material-icons">info_outline</i></small>
                                                @endif
                                            </p>
                                        </span>
                                        </td>
                                        <td width="42.5%">
                                        <span class="text-center">
                                            <p><small><b>{{ trans('sites/maxmix/assortment.average_margin') }}</b></small></p>
                                            <p>
                                                R$ {{ number_format($item['total_gross_margin'], 2, ',', '.') }}
                                                @if($key == 0)
                                                    <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.category.tip.margin') }}"><i class="material-icons">info_outline</i></small>
                                                @endif
                                            </p>
                                        </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="text-center active">
                                            {!! ($item['status'] == 0) ? '<b><i class="material-icons text-danger">thumb_down</i> '.trans('sites/maxmix/assortment.brand.status.0').'</b>' : '<b><i class="material-icons text-success">thumb_up</i> '.trans('sites/maxmix/assortment.brand.status.1').'</b>' !!}
                                            <br>
                                            {!! ($item['percentage_gross_margin'] < $margin) ? '<span class="text-danger">'.trans('sites/maxmix/assortment.brand.alert.margin').'</span>' : '' !!}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-6">
                @if($working_capital > 0)
                <div class="text-center">
                    <h3>Capital de giro adicional: <br> <b>R$ {{ number_format($working_capital, 2, ',', '.') }}</b></h3>
                    <p class="text-muted">Este é valor do capital de giro adicional com a remoção das marcas avaliadas para exclusão</p>
                </div>
                @endif
                @if(count($brands) > 1)
                {{--<div id="chart-table" style="height: 300px;"></div>
                @areachart('ChartTable', 'chart-table')--}}
                @endif
                <div class="jumbotron jumbotron-default text-center">
                    <p>
                        {{ trans('sites/maxmix/assortment.brand.phrase_1') }}
                    </p>
                    <hr>
                    <small>
                        {{ trans('sites/maxmix/assortment.brand.phrase_2') }}
                    </small>
                </div>

            </div>
        </div>

        @else

            <div class="text-center">
                <h1>
                    <big><i class="material-icons text-muted">arrow_upward</i></big>
                    <br>
                    <small>{{ trans('sites/maxmix/assortment.select_category') }}</small>
                </h1>
            </div>

        @endif

        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="navigate text-center">

                    @if(isset($brands))

                    <button type="button" class="btn btn-primary btnGeneratePlanogram">
                        <b>GERAR PLANOGRAMA</b>
                    </button>

                    @endif

                    <a href="{{ route('sites.assortment.valuation.subcategories', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps" class="btn prev">
                        <i class="material-icons">arrow_back</i>
                        <span>{{ trans('sites/general.previous') }}</span>
                    </a>

                    <a href="{{ route('sites.assortment.valuation.items', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}?s={{$subcategory_id}}#steps" class="btn next">
                        <span>{{ trans('sites/general.next') }}</span>
                        <i class="material-icons">arrow_forward</i>
                    </a>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    </div>

    @if(isset($brands))

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title title" id="myModalLabel">Gerar planograma para a subcategoria: <br> <b>{{ $subcategory->name }}</b></h3>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    @endif

@endsection

@push('scripts')

<script>

    @if(isset($brands))

    function loadGeneratorPlanogram(){

        var link = "{{ route('sites.assortment.planogram.brands', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}?s={{$subcategory_id}}";

        $( "#myModal .modal-body" ).load( link );

    }

    @endif

</script>

@endpush

@push('scripts_ready')

$('#subcategory').on('change', function(){

    var s = this.value;
    var url = "{{ route('sites.assortment.valuation.brands', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}?s=";

    document.location.href = url+s+"#steps";

});

$('.popover-open').popover('show');
setTimeout(function() {$('.popover-open').popover('hide')},3000);

@if(isset($brands))

$('.btnGeneratePlanogram').on('click', function(){

    $('#myModal .modal-body').html("{{ trans('sites/general.loading') }}...");
    $('#myModal').modal('show');

    loadGeneratorPlanogram();

});

@endif

@endpush