@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.valuation.inc.header')

    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <h3>{{ trans('sites/maxmix/assortment.choice_period') }}:</h3>
            </div>
        </div>

        <div class="panel-group panel-assortment" id="accordion" role="tablist" aria-multiselectable="true">
            @forelse($tags as $key => $tag)
                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="heading{{ $tag->id }}">
                        <h4 class="panel-title">
                            <a role="button" class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $tag->id }}" aria-expanded="true" aria-controls="collapse{{ $tag->id }}">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <h4> <i class="material-icons">subdirectory_arrow_right</i> {{ $tag->name }}</h4>
                                            {{ trans('sites/general.months') }}:
                                            @foreach($tag->getDate() as $date)
                                                <span class="label label-default">{{ date('M/Y', strtotime($date)) }}</span>
                                            @endforeach
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse{{ $tag->id }}" class="panel-collapse  collapse @if($key == 0) in @endif" role="tabpanel" aria-labelledby="heading{{ $tag->id }}">

                        <table class="table table-bordered-" style="background: #cfcfcf;">
                            <tr class="">
                                <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>{{ trans('sites/maxmix/assortment.tag.label.sku') }}</b></small></p>
                                        <p>
                                            {{ number_format($tag->total['sku'], 0, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.sku') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                </td>
                                <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>{{ trans('sites/maxmix/assortment.tag.label.amount') }}</b></small></p>
                                        <p>
                                            {{ number_format($tag->total['amount'], 0, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.amount') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                </td>
                                <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>{{ trans('sites/maxmix/assortment.tag.label.revenue') }}</b></small></p>
                                        <p>
                                            R$ {{ number_format($tag->total['total_revenue'], 2, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.revenue') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                </td>
                                <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>{{ trans('sites/maxmix/assortment.tag.label.margin') }}</b></small></p>
                                        <p>
                                            R$ {{ number_format($tag->total['total_gross_margin'], 2, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.margin') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                </td>
                            </tr>
                        </table>

                        {{--<table class="table table-bordered" style=" background: #F5F5F5;">
                            <thead style=" background: #efefef;">
                                <tr class="">
                                    <td colspan="2" class="text-center"><small><b>MARCAS</b> avaliadas para exclusão</small></td>
                                    <td colspan="2" class="text-center"><small><b>ITENS</b> avaliadas para exclusão</small></td>
                                </tr>
                            </thead>
                            <tr class="">
                                <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>QUANTIDADE</b></small></p>
                                        <p>
                                            {{ number_format($tag->total['sku'], 0, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.sku') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                </td>
                                <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>GANHO DE CAPITAL DE GIRO</b></small></p>
                                        <p>
                                            R$ {{ number_format($tag->total['total_revenue'], 2, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.revenue') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                </td>
                                <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>QUANTIDADE</b></small></p>
                                        <p>
                                            {{ number_format($tag->total['amount'], 0, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.amount') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                </td>
                                <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>GANHO DE CAPITAL DE GIRO</b></small></p>
                                        <p>
                                            R$ {{ number_format($tag->total['total_gross_margin'], 2, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.margin') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                </td>
                            </tr>
                        </table>--}}

                        {{--<table class="table table-bordered">
                            <tr class="active">
                                <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>{{ trans('sites/maxmix/assortment.tag.label.sku') }}</b></small></p>
                                        <p>
                                            {{ number_format($tag->total['sku'], 0, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.sku') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                </td>
                                <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>{{ trans('sites/maxmix/assortment.tag.label.amount') }}</b></small></p>
                                        <p>
                                            {{ number_format($tag->total['amount'], 0, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.amount') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                </td>
                                <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>{{ trans('sites/maxmix/assortment.tag.label.revenue') }}</b></small></p>
                                        <p>
                                            R$ {{ number_format($tag->total['total_revenue'], 2, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.revenue') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                </td>
                                <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>{{ trans('sites/maxmix/assortment.tag.label.margin') }}</b></small></p>
                                        <p>
                                            R$ {{ number_format($tag->total['total_gross_margin'], 2, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.margin') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                </td>
                            </tr>
                        </table>--}}

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-8">
                                    <small class="text-muted">Confira a avaliação completa deste período:</small>
                                    <div class="row">
                                        <div class="col-sm-2 col-xs-6">
                                            <a href="{{ route('sites.assortment.valuation.cockpit', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}" class="btn btn-link btn-xs btn-block upper">
                                                <i class="material-icons">view_quilt</i> {{ trans('sites/maxmix/assortment.step.cockpit') }}
                                            </a>
                                        </div>
                                        <div class="col-sm-3 col-xs-6">
                                            <a href="{{ route('sites.assortment.valuation.categories', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}" class="btn btn-link btn-xs btn-block upper">
                                                <i class="material-icons">pie_chart</i> {{ trans('sites/maxmix/assortment.step.categories') }}
                                            </a>
                                        </div>
                                        <div class="col-sm-3 col-xs-6">
                                            <a href="{{ route('sites.assortment.valuation.subcategories', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}" class="btn btn-link btn-xs btn-block upper">
                                                <i class="material-icons">pie_chart</i> {{ trans('sites/maxmix/assortment.step.subcategories') }}
                                            </a>
                                        </div>
                                        <div class="col-sm-2 col-xs-6">
                                            <a href="{{ route('sites.assortment.valuation.brands', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}" class="btn btn-link btn-xs btn-block upper">
                                                <i class="material-icons">exposure</i> {{ trans('sites/maxmix/assortment.step.brands') }}
                                            </a>
                                        </div>
                                        <div class="col-sm-2 col-xs-6">
                                            <a href="{{ route('sites.assortment.valuation.items', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}" class="btn btn-link btn-xs btn-block upper">
                                                <i class="material-icons">format_line_spacing</i> {{ trans('sites/maxmix/assortment.step.itens') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <a href="{{ route('sites.assortment.valuation.init', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps" class="btn next pull-right">
                                        <span>{{ trans('sites/maxmix/assortment.valuation_categories') }}</span>
                                        <i class="material-icons">arrow_forward</i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty

                <div class="alert alert-danger">
                    <h4>Não existem dados para sortimento</h4>
                </div>

            @endforelse
        </div>
    </div>


@endsection