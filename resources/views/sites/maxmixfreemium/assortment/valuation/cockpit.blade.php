@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.valuation.inc.header')

    @include($config['view'].'assortment.valuation.inc.steps', ['step' => 'cockpit'])

    <div class="container">

        <h3 class="title">{{ trans('sites/maxmix/assortment.title.cockpit') }}</h3>

        <br><br>

        <div class="row">
            <div class="col-md-6">
                <h4 class="upper"><b>{{ trans('sites/general.matrix') }}:</b> {{ trans('sites/general.subcategory') }} <small><b>X</b></small> {{ trans_choice('sites/general.product', 0) }}</h4>

                <div class="well well-sm">

                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2" rowspan="2" class="active"></th>
                            <th colspan="4" class="text-center active upper">{{ trans_choice('sites/general.product', 1) }}</th>
                        </tr>
                        <tr>
                            <th class="text-center bg-class bg-class-A">
                                <h2>A</h2>
                            </th>
                            <th class="text-center bg-class bg-class-B">
                                <h2>B</h2>
                            </th>
                            <th class="text-center bg-class bg-class-C">
                                <h2>C</h2>
                            </th>
                            <th class="text-center bg-class bg-class-D">
                                <h2>D</h2>
                            </th>
                        </tr>
                        <tr>
                            <th rowspan="4" width="50px" class="active text-center">
                                {!! trans('sites/maxmix/assortment.cockipt.column_subcategory') !!}
                            </th>
                            <th class="text-center bg-class bg-class-A">
                                <h2>A</h2>
                            </th>
                            <td class="bg-class bg-class-A">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.1') }}" data-link="1" data-l="A/A">
                                    1
                                </a>
                            </td>
                            <td class="bg-class bg-class-B">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.2') }}" data-link="2" data-l="A/B">
                                    2
                                </a>
                            </td>
                            <td class="bg-class bg-class-3">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.3') }}" data-link="3" data-l="A/C">
                                    3
                                </a>
                            </td>
                            <td class="bg-class bg-class-C">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.4') }}" data-link="4" data-l="A/D">
                                    4
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center bg-class bg-class-B">
                                <h2>B</h2>
                            </th>
                            <td class="bg-class bg-class-B">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.2') }}" data-link="2" data-l="B/A">
                                    2
                                </a>
                            </td>
                            <td class="bg-class bg-class-3">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.3') }}" data-link="3" data-l="B/B">
                                    3
                                </a>
                            </td>
                            <td class="bg-class bg-class-C">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.4') }}" data-link="4" data-l="B/C">
                                    4
                                </a>
                            </td>
                            <td class="bg-class bg-class-5">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.5') }}" data-link="5" data-l="B/D">
                                    5
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center bg-class bg-class-C">
                                <h2>C</h2>
                            </th>
                            <td class="bg-class bg-class-3">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.3') }}" data-link="3" data-l="C/A">
                                    3
                                </a>
                            </td>
                            <td class="bg-class bg-class-C">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.4') }}" data-link="4" data-l="C/B">
                                    4
                                </a>
                            </td>
                            <td class="bg-class bg-class-5">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.5') }}" data-link="5" data-l="C/C">
                                    5
                                </a>
                            </td>
                            <td class="bg-class bg-class-6">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.6') }}" data-link="6" data-l="C/D">
                                    6
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center bg-class bg-class-D">
                                <h2>D</h2>
                            </th>
                            <td class="bg-class bg-class-C">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.4') }}" data-link="4" data-l="D/A">
                                    4
                                </a>
                            </td>
                            <td class="bg-class bg-class-5">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.5') }}" data-link="5" data-l="D/B">
                                    5
                                </a>
                            </td>
                            <td class="bg-class bg-class-6">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.6') }}" data-link="6" data-l="D/C">
                                    6
                                </a>
                            </td>
                            <td class="bg-class bg-class-D">
                                <a class="btn-cockpit btnCockpitItens" href="javascript:void(0)" data-important="{{ trans('sites/maxmix/assortment.cockipt.importance.7') }}" data-link="7" data-l="D/D">
                                    7
                                </a>
                            </td>
                        </tr>

                    </table>

                    <div class="text-right">
                        <small> {{ trans('sites/maxmix/assortment.cockipt.info.click') }}<small><i class="material-icons">info_outline</i></small></small>
                    </div>

                </div>

            </div>
            <div class="col-md-6">

                <div class="hidden-sm hiddem-xs">
                    <br>
                    <h2 class="text-primary">
                        {{ trans('sites/maxmix/assortment.cockipt.info.select_importance') }}
                    </h2>
                </div>

                <ul>
                    <li><h3>Legenda:</h3></li>
                    <li>
                        <b class="label bg-class bg-class-A">1</b> - {{ trans('sites/maxmix/assortment.cockipt.importance.1') }}
                    </li>
                    <li>
                        <b class="label bg-class bg-class-B">2</b> - {{ trans('sites/maxmix/assortment.cockipt.importance.2') }}
                    </li>
                    <li>
                        <b class="label bg-class bg-class-3">3</b> - {{ trans('sites/maxmix/assortment.cockipt.importance.3') }}
                    </li>
                    <li>
                        <b class="label bg-class bg-class-C">4</b> - {{ trans('sites/maxmix/assortment.cockipt.importance.4') }}
                    </li>
                    <li>
                        <b class="label bg-class bg-class-5">5</b> - {{ trans('sites/maxmix/assortment.cockipt.importance.5') }}
                    </li>
                    <li>
                        <b class="label bg-class bg-class-6">6</b> - {{ trans('sites/maxmix/assortment.cockipt.importance.6') }}
                    </li>
                    <li>
                        <b class="label bg-class bg-class-D">7</b> - {{ trans('sites/maxmix/assortment.cockipt.importance.7') }}
                    </li>
                </ul>
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="navigate">
                    <div class="">
                        <a href="{{ route('sites.assortment.valuation.init', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps" class="btn prev">
                            <i class="material-icons">arrow_back</i>
                            <span>{{ trans('sites/general.previous') }}</span>
                        </a>

                        <a href="{{ route('sites.assortment.valuation.categories', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps" class="btn next">
                            <span>{{ trans('sites/general.next') }}</span>
                            <i class="material-icons">arrow_forward</i>
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title title upper" id="myModalLabel"></h3>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <a href="#" target="_blank" class="btn btn-primary btnPrint"><i class="material-icons">print</i> Imprimir</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('sites/general.close') }}</button>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts_ready')

$('.btnCockpitItens').on('click', function(){

    ga('send', 'event', 'Sortimento - Avaliação', 'cockipt_important_click', 'Itens pela importância', $(this).data('link'));

    $('#myModal .modal-title').html("Importância: <b>" + $(this).data('important') + "<b>");
    $('#myModal .modal-body').html("{{ trans('sites/general.loading') }}...");
    $('#myModal').modal('show');

    var link = "{{ route('sites.assortment.cockpit.items', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}/"+$(this).data('link');

    $('#myModal .modal-footer a.btnPrint').attr('href', link);

    $.get( link+"?a=1", function( data ) {
        $('#myModal .modal-body').html( data );
        $('[data-toggle="tooltip"]').tooltip();
    });

});

@endpush