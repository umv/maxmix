@extends($config['view'].'layouts.master')

@push('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.2.0/ekko-lightbox.min.css" rel="stylesheet" />
@endpush

@section('content')

    <div class="container">

        <div class="text-center">

            <h1>Faça uma análise completa do sortimento da sua loja</h1>

            <h4>O gerenciamento de categorias e sortimento adequado para seu padrão de loja agora ficou fácil e rápido</h4>

            <br>

        </div>

        <br>

        <div class="row">

            <div class="col-md-6">
                <iframe src="https://player.vimeo.com/video/231598104?color=ff9933&title=0&byline=0&portrait=0" width="100%" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>

            <div class="col-md-6">

                <div class="media">
                    <div class="media-left">
                        <h2><i class="material-icons text-muted">dns</i></h2>
                    </div>
                    <div class="media-body">
                        <h4>AVALIAÇÃO DE SORTIMENTO</h4>
                        <p>Faça uma avaliação completa de categorias, subcategorias, marcas e itens do sortimento da sua loja.</p>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="media">
                    <div class="media-left">
                        <h2><i class="material-icons text-muted">timeline</i></h2>
                    </div>
                    <div class="media-body">
                        <h4>ACOMPANHAMENTO DE PERÍODOS</h4>
                        <p>Visualize o desempenho das categorias, subcategorias e marcas do seu sortimento entre períodos.</p>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="media">
                    <div class="media-left">
                        <h2><i class="material-icons text-muted">compare_arrows</i></h2>
                    </div>
                    <div class="media-body">
                        <h4>COMPARAÇÃO DE LOJAS</h4>
                        <p>Faça um comparativo do sortimento entre lojas da sua rede.</p>
                    </div>
                </div>
                <div class="clearfix"></div>

            </div>

        </div>

        <br><br>

        <div class="text-center">

            <a href="#formAssortment" class="btn btn-primary btn-lg">
                SOLICITAR SORTIMENTO
            </a>

        </div>

        <br><br>
        <hr>
        <br><br>

        <div class="row">

            <div class="col-md-6">

                <div class="media">
                    <div class="media-left">
                        <h3><i class="material-icons text-muted">dns</i></h3>
                    </div>
                    <div class="media-body">
                        <h3>AVALIAÇÃO DE SORTIMENTO</h3>
                        <br>
                        <h4>Receba indicativos de quais produtos precisa ter na sua loja</h4>
                        <h4>Quais marcas sao relevantes nas categorias que tem em exposição</h4>
                        <h4>Os espaços que as marcas e produtos devem ocupar nas gôndolas</h4>
                        <br>
                        <a href="#formAssortment" class="btn btn-primary">
                            SOLICITAR AGORA
                        </a>
                    </div>
                </div>

            </div>

            <div class="col-md-6">

                <a href="{{ Integra::asset('imagens/lp_assortment/valuation.png') }}" data-toggle="lightbox" data-title="AVALIAÇÃO DE SORTIMENTO">
                    <img src="{{ Integra::asset('imagens/lp_assortment/valuation.png') }}" width="100%" class="img-responsive">
                </a>

            </div>

        </div>

        <br><br>
        <hr>
        <br><br>

        <div class="row">

            <div class="col-md-6">

                <a href="{{ Integra::asset('imagens/lp_assortment/monitoring.png') }}" data-toggle="lightbox" data-title="ACOMPANHAMENTO DE PERÍODOS">
                    <img src="{{ Integra::asset('imagens/lp_assortment/monitoring.png') }}" width="100%" class="img-responsive">
                </a>

            </div>

            <div class="col-md-6">

                <div class="media">
                    <div class="media-left">
                        <h3><i class="material-icons text-muted">timeline</i></h3>
                    </div>
                    <div class="media-body">
                        <h3>ACOMPANHAMENTO DE PERÍODOS</h3>
                        <br>
                        <h4>Com o acompanhamento você pode ver a evolução das suas ações dentro do seu mix</h4>
                        <h4>Faça o camparativo de venda e lucro entre períodos divividos em categorias, subcategorias e marcas</h4>
                        <h4>Conheça o impacto quando marcas e itens são removidos ou adicionados</h4>
                        <br>
                        <a href="#formAssortment" class="btn btn-primary">
                            SOLICITAR AGORA
                        </a>
                    </div>
                </div>

            </div>

        </div>

        <br><br>
        <hr>
        <br><br>

        <div class="row">

            <div class="col-md-6">

                <div class="media">
                    <div class="media-left">
                        <h3><i class="material-icons text-muted">compare_arrows</i></h3>
                    </div>
                    <div class="media-body">
                        <h3>COMPARAÇÃO DE LOJAS</h3>
                        <br>
                        <h4>Com o comparativo, você pode ver o desempenho de cada loja dentro da sua rede</h4>
                        <h4>Conheça as lojas com melhor e pior desempenho, dividos em subcategorias, marcas e itens</h4>
                        <h4>Visualize a falta de itens no mix de cada loja</h4>
                        <br>
                        <a href="#formAssortment" class="btn btn-primary">
                            SOLICITAR AGORA
                        </a>
                    </div>
                </div>

            </div>

            <div class="col-md-6">

                <a href="{{ Integra::asset('imagens/lp_assortment/comparison.png') }}" data-toggle="lightbox" data-title="COMPARAÇÃO DE LOJAS">
                    <img src="{{ Integra::asset('imagens/lp_assortment/comparison.png') }}" width="100%" class="img-responsive">
                </a>

            </div>

        </div>

        <br><br>
        <hr>
        <br><br>

        <div class="text-center">
            <h2>
                Conheça o sortimento ideal para sua loja, com indicação de produtos e marcas com base no compartamento do seu cliente
            </h2>
        </div>

        <br><br>
        <hr>
        <br><br>

        <div class="row">

            <div id="formAssortment" class="col-md-12">

                <div class="text-center">
                    <h1>SOLICITE E FAÇA O SORTIMENTO DA SUA LOJA</h1>
                </div>

                <br>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="well">

                            <form action="{{ route('sites.assortment.sendrequest') }}" id="assortment-form-request">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <label>CNPJ</label>
                                    <input value="{{ $user->cnpj }}" required name="cnpj" type="text" class="form-control" placeholder="CNPJ da sua loja">
                                </div>
                                <div class="form-group">
                                    <label>Razão Social</label>
                                    <input value="{{ ($user->email != 'maxmix@maxmix.net.br') ? $user->name : '' }}" required name="name" type="text" class="form-control" placeholder="Razão social da sua loja">
                                </div>
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input value="{{ ($user->email != 'maxmix@maxmix.net.br') ? $user->email : '' }}" required name="email" type="email" class="form-control" placeholder="E-mail para contato">
                                </div>
                                <div class="form-group">
                                    <label>Telefone</label>
                                    <input type="text" name="telefone" required class="form-control" placeholder="Telefone para contato">
                                </div>

                                <button type="button" class="btn btn-primary btn-lg btn-block btn-assortment-request">ENVIAR</button>

                            </form>

                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection

@push('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.2.0/ekko-lightbox.min.js"></script>
@endpush

@push('scripts_ready')

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
            $(this).ekkoLightbox({
            alwaysShowClose: true
        });
    });

    $(document).on('click', '.btn-assortment-request', function(){

        $.post(
            "{{ route('sites.assortment.sendrequest') }}",
            $('#assortment-form-request').serialize()
        ).done(function( data ) {
            $('#assortment-form-request')[0].reset();
            alert('Sua solicitação foi enviada com sucesso! Entraremos em contato em breve.');
        });

    });

@endpush