@extends($config['view'].'layouts.master')

@section('title'){{ $page->title }}@endsection

@section('content')

    {!! $page->content !!}

@endsection