@extends($config['view'].'layouts.master')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="box-info">
				<h2>Faça seu cadastro</h2>
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> Encontramos algums problemas com seus dados.<br><br>
					</div>
				@endif
				<form role="form" method="POST" action="{{ route('sites.auth.register') }}">
					{!! csrf_field() !!}

					<div class="form-group {{ $errors->first('name', ' has-error') }}">
						<label class="control-label">Nome</label>
						<input type="text" class="form-control" name="name" value="{{ old('name') }}">
						<span class="help-block">{{ $errors->first('name', ':message') }}</span>
					</div>

					<div class="form-group {{ $errors->first('email', ' has-error') }}">
						<label class="control-label">Endereço de e-mail</label>
						<input type="email" class="form-control" name="email" value="{{ old('email') }}">
						<span class="help-block">{{ $errors->first('email', ':message') }}</span>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group {{ $errors->first('password', ' has-error') }}">
								<label class="control-label">Senha</label>
								<input type="password" class="form-control" name="password">
								<span class="help-block">{{ $errors->first('password', ':message') }}</span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group {{ $errors->first('password_confirmation', ' has-error') }}">
								<label class="control-label">Confirmação de senha</label>
								<input type="password" class="form-control" name="password_confirmation">
								<span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-lg btn-block">
							CADASTRAR
						</button>
					</div>

					<div class="text-center">
						<a class="btn btn-link" href="{{ route('sites.auth.login') }}">Já tem cadastro?</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
