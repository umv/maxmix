<html>
	<head>
		<title>Site demonstração</title>
		<link rel="stylesheet" href="{{ Integra::asset('css/bootstrap.min.css') }}">
	</head>
	<body>
		<div class="container">
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> Encontramos algums problemas com seus dados. Certifique de ter confirmado seu cadastro no e-mail enviado.<br><br>
				</div>
			@endif
			<form role="form" method="POST" id="loginAuthAutomatic" action="{{ route('sites.auth.login') }}" style="display: none;">
				{!! csrf_field() !!}
				<input type="email" class="form-control" name="email" value="maxmix@maxmix.net.br">
				<input type="password" class="form-control" name="password" value="123456">
				<input type="checkbox" name="remember" checked="checked">
				<button type="submit" class="btn btn-primary btn-block">
					ENTRAR
				</button>
			</form>
			<div class="row">
				<div class="col-md-12">
					<hr>
					<div class="jumbotron text-center">
						<h2>Carregando demonstração...</h2>
						<img src="{{ Integra::asset('imagens/bx_loader.gif') }}">
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="{{ Integra::asset('js/jquery.min.js') }}"></script>
		<script type="text/javascript">

			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$(document).ready(function(){

				$('#loginAuthAutomatic').submit();

			});
		</script>
	</body>
</html>

