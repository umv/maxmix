@extends($config['view'].'layouts.master')

@push('styles')

<style>
    [data-player]{
        margin: 0 auto !important;
    }
</style>

@endpush

@section('content')

    @if(Auth::check())

        <section class="dashboard">

            <div class="container">

                <br><br>

                <div class="row">
                    <div class="col-md-6">
                        <div class="jumbotron">
                            <div class="media">
                                <div class="media-left media-middle hidden-xs">
                                    <img src="{{ Integra::asset('imagens/icon-planogram.png') }}" alt="Planograma" width="140">
                                </div>
                                <div class="media-body">
                                    <div class="media-heading">
                                        <a href="{{ route('sites.planogram.generator') }}">
                                            <h2>Planogramas</h2>
                                        </a>
                                    </div>
                                    <p class="text-justify">
                                        Crie planogramas sob medida, com informações sobre exposição e o mix ideal de cada categoria.
                                    </p>
                                </div>
                            </div>
                            <a href="{{ route('sites.planogram.resource.index') }}" class="btn btn-default btn-block margin-t-30">Listar planogramas gerados</a>
                            <a href="{{ route('sites.planogram.generator') }}#steps" class="btn btn-primary btn-block margin-t-30"><i class="material-icons">add_circle_outline</i> Novo</a>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="jumbotron">

                            <div class="media">
                                <div class="media-left media-middle hidden-xs">
                                    <img src="{{ Integra::asset('imagens/icon-assortment.png') }}" alt="Sortimento" width="140">
                                </div>
                                <div class="media-body">
                                    <div class="media-heading">
                                        <a href="{{ route('sites.assortment.clients', 'valuation') }}">
                                            <h2>Sortimento</h2>
                                        </a>
                                    </div>
                                    <p class="text-justify">
                                        Faça uma análise completa do sortimento da sua loja. <br><br>
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <a href="{{ route('sites.assortment.clients', 'monitoring') }}" class="btn btn-default btn-block margin-t-30">
                                        <i class="material-icons">timeline</i> <small>Acompanhamento</small>
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('sites.assortment.comparison.client') }}" class="btn btn-default btn-block margin-t-30">
                                        <i class="material-icons">compare_arrows</i> <small>Comparação</small>
                                    </a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('sites.assortment.clients', 'valuation') }}" class="btn btn-primary btn-block margin-t-30">
                                        <i class="material-icons">dns</i> Avaliação
                                    </a>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <br>
            </div>
        </section>
    @else

    <section class="home§">
        <div class="container">

            <div class="row">
                <div class="col-md-6">
                    <h1>O QUE É O MAXMIX?</h1>
                    <p>Maxmix é uma ferramenta feita especialmente para auxiliar na gestão de categorias da loja, na medida em que entrega o planograma mais efetivo e o sortimento mais ajustado, proporcionando assim um aumento de vendas e lucratividade.</p>
                    <div class="row">
                        <div class="col-md-6">
                            <br>
                            <a href="{{ route('sites.auth.login') }}" class="btn btn-primary btn-block">ACESSE AQUI</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <iframe src="https://player.vimeo.com/video/231598104?color=ff9933&title=0&byline=0&portrait=0" width="100%" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    {{--<div id="player-home"></div>--}}
                </div>
            </div>

            {{--<div class="row">
                <div class="hidden-xs hidden-sm col-md-4 col-lg-5">
                    <div class="box-info text-justify">
                        <h2>O que é o MAXMIX?</h2>
                        <p>Maxmix é uma ferramenta feita especialmente para auxiliar na gestão de categorias da loja, na medida em que entrega o planograma mais efetivo e o sortimento mais ajustado, proporcionando assim um aumento de vendas e lucratividade.</p>
                        --}}{{--<a href="#" class="btn btn-link pull-right">Saiba Mais</a>--}}{{--
                    </div>
                </div>

                <div class="col-md-4 col-lg-4">
                    <div class="box-info">
                        <h2>Faça seu Login</h2>
                        <form name="frm-login" id="frm-login" class="frm-login" method="post" action="{{ route('sites.auth.login') }}">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="sr-only">E-mail</label>
                                <div class="input-group {{ $errors->first('email', ' has-error') }}">
                                    <div class="input-group-addon"><i class="material-icons">email</i></div>
                                    <input type="email" class="form-control required" name="email" value="{{ old('email') }}">
                                </div>
                                <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                            </div>

                            <div class="form-group {{ $errors->first('password', ' has-error') }}">
                                <label class="sr-only">Senha</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="material-icons">lock</i></div>
                                    <input type="password" class="form-control required" name="password">
                                </div>
                                <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">ENTRAR</button>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="{{ route('sites.auth.password.email') }}" class="btn btn-link pull-left">
                                        Esqueceu sua senha?
                                    </a>
                                </div>

                                <div class="col-sm-6">
                                    <a href="{{ route('sites.auth.register') }}" class="btn btn-link pull-right">
                                        Cadastrar
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

                <div class="hidden-xs hidden-sm col-md-4 col-lg-3">
                    <div class="box-info box-info-person">
                        <img src="{{ Integra::asset('imagens/img-silhueta-funcionario.png') }}" class="img-responsive" alt="Conheça o que é MAXMIX" title="" />
                    </div>
                </div>
            </div>--}}
        </div>

        {{--<div class="ilustration hidden-xs hidden-sm"></div>

        <div class="container hidden-xs hidden-sm">
            <div class="padding-tb-10">
                <div class="row">
                    <div class="col-sm-4 text-center">
                        <a href="#">
                            <img src="{{ Integra::asset('imagens/banner-publicitario.png') }}" class="img-responsive padding-tb-10" alt="Banner Publicitário" title="Banner Publicitário" />
                        </a>
                    </div>

                    <div class="col-sm-4 text-center">
                        <a href="#">
                            <img src="{{ Integra::asset('imagens/banner-publicitario.png') }}" class="img-responsive padding-tb-10" alt="Banner Publicitário" title="Banner Publicitário" />
                        </a>
                    </div>

                    <div class="col-sm-4 text-center">
                        <a href="#">
                            <img src="{{ Integra::asset('imagens/banner-publicitario.png') }}" class="img-responsive padding-tb-10" alt="Banner Publicitário" title="Banner Publicitário" />
                        </a>
                    </div>
                </div>
            </div>
        </div>--}}
    </section>

    @endif

@endsection

@push('scripts')

@if(!Auth::check())
{{--<script type="text/javascript" charset="utf-8" src="{{ Integra::asset('js/clappr.min.js') }}"></script>
<script type="text/javascript" charset="utf-8" src="{{ Integra::asset('js/clappr-youtube-playback.js') }}"></script>--}}
@endif

@endpush

@push('scripts_ready')

@if(!Auth::check())

        {{--var player = new Clappr.Player({
            parentId: "#player-home",
            sources: ['Yi0vsfFak7I'],
            //source: "{{ Integra::media('videos/maxmix_flixdovarejo.mp4') }}",
            poster: "{{ Integra::asset('imagens/poster.jpg') }}",
            mediacontrol: {seekbar: "#fd9c01", buttons: "#ffffff"},
            hideVolumeBar: true,
            width: 500,
            height: 281,
            plugins: {
                playback: [YoutubePlayback]
            }
        });--}}

@endif

@endpush