<!DOCTYPE html>
<html lang="pt_BR">
<head>
    <title>MAXMIX - Universidade Martins do Varejo</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ Integra::asset('imagens/favicon.ico') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="theme-color" content="#073663">
    {{--<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400italic,700' rel='stylesheet' type='text/css'>--}}
    {{--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ Integra::asset('css/bootstrap.min.css') }}">
    <link href="{{ Integra::asset('css/app.css') }}?{{date('U')}}" type="text/css" rel="stylesheet" media="all" />
    <link href="{{ Integra::asset('css/app-mod.css') }}?{{date('U')}}" type="text/css" rel="stylesheet" media="all" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    @stack('styles')

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        @if($user)
        ga('create', 'UA-102039631-2', { 'userId':  '{{ ($user->cpf) ? $user->cpf : $user->email }}' });
        @else
        ga('create', 'UA-102039631-2', 'auto');
        @endif
        ga('send', 'pageview');

    </script>
</head>
<body>
<header>
    {{--<div class="bg-primary">
        <div class="container text-left">
            <img src="{{ Integra::asset('imagens/flix-topo.png') }}" alt="Flix Barra" class="img-responsive">
        </div>
    </div>--}}
    <div id="bar-top">
        @if(Auth::check())
            <nav class="navbar navbar-inverse navbar-custom">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Menu</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ url('/') }}">
                            {{--<img src="{{ Integra::asset('imagens/logo-umv.png') }}" alt="UMV - Universidade Martins do Varejo" class="brand-umv pull-left img-responsive" />--}}
                            <img alt="Brand" src="{{ Integra::asset('imagens/logo-maxmix.png') }}" alt="MAXMIX" class="img-responsive pull-left" >
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="{{ (Request::is('/') ? 'active' : '') }}"><a href="{{ url('/') }}">Painel</a></li>
                            <li class="dropdown {{ (Request::is('planogram/generator*') || Request::is('planogram/resource') || Request::is('assortment/clients/valuation*') || Request::is('assortment/clients/monitoring*') || Request::is('assortment/clients/comparation*')) ? 'active' : '' }}">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{--<i class="material-icons">apps</i> --}}Ferramentas <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-header text-default">
                                        <img src="{{ Integra::asset('imagens/icon-planogram-outline.png') }}" alt="Planograma" width="40" class="margin-r-10">
                                        <b>PLANOGRAMAS</b>
                                        <hr style="margin-top: 10px;">
                                    </li>
                                    <li class="{{ (Request::is('planogram/gerador*') ? 'active' : '') }}">
                                        <a href="{{ route('sites.planogram.generator') }}#steps">
                                            <i class="material-icons text-muted">add_circle_outline</i> Novo Planograma
                                        </a>
                                    </li>
                                    <li class="{{ (Request::is('planogram/resource*') ? 'active' : '') }}">
                                        <a href="{{ route('sites.planogram.resource.index') }}">
                                            <i class="material-icons text-muted">view_list</i> Listar planogramas gerados
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li class="dropdown-header text-default"><img src="{{ Integra::asset('imagens/icon-assortment-outline.png') }}" alt="Sortimento" width="40" class="margin-r-10"><b>SORTIMENTO</b>
                                        <hr style="margin-top: 10px;"></li>
                                    <li class="{{ (Request::is('assortment/clients/valuation*') ? 'active' : '') }}">
                                        <a href="{{ route('sites.assortment.clients', 'valuation') }}">
                                            <i class="material-icons text-muted">dns</i> Avaliação
                                        </a>
                                    </li>
                                    <li class="{{ (Request::is('assortment/clients/monitoring*') ? 'active' : '') }}">
                                        <a href="{{ route('sites.assortment.clients', 'monitoring') }}">
                                            <i class="material-icons text-muted">timeline</i> Acompanhamento
                                        </a>
                                    </li>
                                    <li class="{{ (Request::is('assortment/comparison*') ? 'active' : '') }}">
                                        <a href="{{ route('sites.assortment.comparison.client') }}">
                                            <i class="material-icons text-muted">compare_arrows</i> Comparação
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown {{ (Request::is('auth/account*') ? 'active' : '') }}">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{--<i class="material-icons">account_box</i>--}} {{--{{ Auth::user()->name }}--}} Meus dados <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="{{ (Request::is('auth/account*') ? 'active' : '') }}"><a href="{{ route('sites.auth.account') }}">Conta</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{ route('sites.auth.logout') }}">Sair</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        @else
            <a href="{{ url('/') }}">
                <div class="maxmix-cover">
                </div>
            </a>
            {{--<div class="bg-primary padding-tb-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <a href="{{ url('/') }}" title="UMV - Universidade Martins do Varejo" class="">
                                <img src="{{ Integra::asset('imagens/logo-umv.png') }}" alt="UMV - Universidade Martins do Varejo" class="img-responsive brand-umv" />
                            </a>
                            <a href="{{ url('/') }}" title="MAXMIX">
                                <h1 class="sr-only">MaxMix - Universidade Martins do Varejo</h1>
                                <img src="{{ Integra::asset('imagens/logo-maxmix.png') }}" alt="MAXMIX" class="img-responsive" />
                            </a>
                        </div>
                        <div class="col-md-4 hidden-xs hidden-sm">
                            <a href="http://www.martins.com.br" target="_blank" title="Martins Atacadista" class="pull-right">
                                <img src="{{ Integra::asset('imagens/logo-martins-atacadista.png') }}" alt="Martins Atacadista" class="img-responsive" height="58" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>--}}
        @endif
    </div>
</header>

<section class="conteudo content">

    @if (session()->has('message'))
        <div class="container">
            <div class="alert alert-info">{{ session('message') }}</div>
        </div>
    @endif

    @yield('content')
</section>

{{--<footer>
    <div class="parceiros visible-lg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <ul class="list-inline margin-0 padding-tb-10">
                        <li>
                            <a href="http://www.martins.com.br" target="_blank" class="sistema-martins"><small><strong>SISTEMA INTEGRADO MARTINS</strong></small></a>
                        </li>
                        <li>
                            <a href="http://portal.martins.com.br/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_martins.png') }}" alt="Martins" title="Martins" />
                            </a>
                        </li>
                        <li>
                            <a href="http://www.efacil.com.br/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_efacil.png') }}" alt="eFacil" title="eFacil" />
                            </a>
                        </li>
                        <li>
                            <a href="http://www.tribanco.com.br/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_tribanco.png') }}" alt="TRIBANCO" title="TRIBANCO" />
                            </a>
                        </li>
                        <li>
                            <a href="http://www.tribancoseguros.com.br/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_tribanco_seguros.png') }}" alt="TRIBANCO Seguros" title="TRIBANCO Seguros" />
                            </a>
                        </li>
                        <li>
                            <a href="http://www.tricard.com.br/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_tricard.png') }}" alt="TRICARD" title="TRICARD" />
                            </a>
                        </li>
                        <li>
                            <a href="http://b.martins.com.br/umv.aspx" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_umv.png') }}" alt="Universidade Martins do Varejo" title="Universidade Martins do Varejo" />
                            </a>
                        </li>
                        <li>
                            <a href="http://www.redesmart.com.br/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_smart.png') }}" alt="SMART" title="SMART" />
                            </a>
                        </li>
                        <li>
                            <a href="http://formar.martins.com.br/hotsite/default1.asp?idttipo=C&idtcliente=N" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_formar.png') }}" alt="FORM@R" title="FORM@R" />
                            </a>
                        </li>
                        <li>
                            <a href="http://www.iamar.org.br/site/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_iamar.png') }}" alt="IAMAR" title="IAMAR" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container visible-lg">
        <div class="row">
            <div class="col-sm-3 text-center">
                <div class="row">
                    <div class="col-md-12">
                        <a class="go-top" href="javascript:void(0)" title="Clique para voltar ao topo">
                            <img src="{{ Integra::asset('imagens/logo-martins-footer.png') }}" alt="Martins Atacadista" class="img-responsive" />
                        </a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <ul class="social">
                            <li>
                                <a href="https://twitter.com/Siga_Martins" target="_blank" title="Siga-nos no Twitter">
                                    <i class="sprite-footer icon-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.facebook.com/MartinsAtacadista" target="_blank" title="Curta nossa Fan Page">
                                    <i class="sprite-footer icon-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.youtube.com/user/AtacadoOnline" target="_blank" title="Acesso nosso canal no Youtube">
                                    <i class="sprite-footer icon-youtube"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <p>
                            Destacamos que os pre&ccedil;os previstos no site prevalecem aos demais anunciados em outros meios de comunica&ccedil;&atilde;o e sites de buscas.
                        </p>
                        <p>
                            <a href="https://www.certisign.com.br/" target="_blank" title="Certifica&ccedil;&atilde;o Digital no Brasil.">
                                <img src="{{ Integra::asset('imagens/img_certisign.jpg') }}" alt="Certisign" class="img-responsive" />
                            </a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-9 ctnTodosDepartamentos">
                <h3>Todos os departamentos:</h3>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="row">
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=agroveterinarios&CodCat=7" title="Agroveterin&aacute;rios" target="_blank">
                                    <i class="sprite-footer icon-agroveterinarios"></i>
                                    Agroveterin&aacute;rios
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=pack" title="Alimentos" target="_blank">
                                    <i class="sprite-footer icon-alimentos"></i>
                                    Alimentos
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=bebidas" title="Bebidas" target="_blank">
                                    <i class="sprite-footer icon-bebidas"></i>
                                    Bebidas
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=Bomboniere" title="Bomboniere" target="_blank">
                                    <i class="sprite-footer icon-bomboniere"></i>
                                    Bomboniere
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=Cal%C3%A7ados&f_marca=Havaianas&page=1" title="Cal&ccedil;ados" target="_blank">
                                    <i class="sprite-footer icon-calcados"></i>
                                    Cal&ccedil;ados
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=cine/foto&CodCat=24" title="Cine / Foto" target="_blank">
                                    <i class="sprite-footer icon-cine-foto"></i>
                                    Cine / Foto
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=eletro&CodCat=22" title="Eletro" target="_blank">
                                    <i class="sprite-footer icon-eletro"></i>
                                    Eletro
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=escolares/escritorio&CodCat=8&page=20" title="Escolares / Escrit&oacute;rio" target="_blank">
                                    <i class="sprite-footer icon-escolares-escritorios"></i>
                                    Escolares / Escrit&oacute;rio
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=Ferramentas" title="Ferramentas" target="_blank">
                                    <i class="sprite-footer icon-ferramentas"></i>
                                    Ferramentas
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=higiene&CodCat=2" title="Higiene" target="_blank">
                                    <i class="sprite-footer icon-higiene"></i>
                                    Higiene
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=Implementos%20Agropecu%C3%A1rios&page=1" title="Implementos Agropecu&aacute;rios" target="_blank">
                                    <i class="sprite-footer icon-implementos-agropecuarios"></i>
                                    Implementos Agropecu&aacute;rios
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=informatica&CodCat=23" title="Inform&aacute;tica" target="_blank">
                                    <i class="sprite-footer icon-informatica"></i>
                                    Inform&aacute;tica
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=limpeza&CodCat=17" title="Limpeza" target="_blank">
                                    <i class="sprite-footer icon-limpeza"></i>
                                    Limpeza
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=Mat.%20Constru%C3%A7%C3%A3o&page=14" title="Mat. Constru&ccedil;&atilde;o" target="_blank">
                                    <i class="sprite-footer icon-material-construcao"></i>
                                    Mat. Constru&ccedil;&atilde;o
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=Telecomunica%C3%A7%C3%B5es&page=19" title="Telecomunica&ccedil;&otilde;es" target="_blank">
                                    <i class="sprite-footer icon-telecomunicacoes"></i>
                                    Telecomunica&ccedil;&otilde;es
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=Utens%C3%ADlios%20Dom%C3%A9sticos" title="Utens&iacute;lios Dom&eacute;sticos" target="_blank">
                                    <i class="sprite-footer icon-utensilios-domesticos"></i>
                                    Utens&iacute;lios Dom&eacute;sticos
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=Equipamentos%20Institucionais" title="Equipamentos Institucionais" target="_blank">
                                    <i class="sprite-footer icon-equipamentos-institucionais"></i>
                                    Equipamentos Institucionais
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=camping/lazer&CodCat=11" title="Camping / Lazer" target="_blank">
                                    <i class="sprite-footer icon-camping-lazer"></i>
                                    Camping / Lazer
                                </a>
                            </li>
                            <li class="col-md-4">
                                <a href="https://www.martinsatacado.com.br/martins/pt/BRL/search/?text=puericultura&CodCat=85" title="Bêbes / Crian&ccedil;as" target="_blank">
                                    <i class="sprite-footer icon-bebes-criancas"></i>
                                    B&ecirc;bes / Crian&ccedil;as
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="contato-telefone">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <ul class="ulNivel2">
                        <li class="telefone"><i class="sprite-footer icon-telefone"></i> SAC: (34) 3218-1310 </li>
                        <li>Martins Com&eacute;rcio e Servi&ccedil;o de Distribui&ccedil;&atilde;o - S/A</li>
                        <li>Rua Jata&iacute;, 1150 - B. Aparecida - Uberl&acirc;ndia - MG</li>
                        <li>Email: <a href="mailto:contato@martins.com.br">contato@martins.com.br</a></li>
                    </ul>
                </div>

                <div class="col-sm-6">
                    <div class="desenvolvido text-right">
                        <a href="http://www.wingmidia.com.br" target="_blank" class="developed" title="Conhe&ccedil;a a Wing M&iacute;dia Integrada.">
                            <span>Desenvolvido por</span>
                            <img src="{{ Integra::asset('imagens/logo-wing-midia.png') }}" alt="Desenvolvido por Wing M&iacute;dia Integrada" title="Wing M&iacute;dia Integrada" class="img-logo-wing" />
                            <span class="wing">wingmidia</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="text-center">
                {{ date('Y') }} &copy; Martins - Todos os direitos reservados.
            </div>
        </div>
    </div>
</footer>--}}
<footer class="bg-dark">
    <div class="parceiros- visible-lg bg-primary" style="height: 20px; margin: 30px 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    {{--<ul class="list-inline margin-0 padding-tb-10">
                        <li>
                            <a href="http://www.martins.com.br" target="_blank" class="sistema-martins"><small><strong>SISTEMA INTEGRADO MARTINS</strong></small></a>
                        </li>
                        <li>
                            <a href="http://portal.martins.com.br/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_martins.png') }}" alt="Martins" title="Martins" />
                            </a>
                        </li>
                        <li>
                            <a href="http://www.efacil.com.br/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_efacil.png') }}" alt="eFacil" title="eFacil" />
                            </a>
                        </li>
                        <li>
                            <a href="http://www.tribanco.com.br/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_tribanco.png') }}" alt="TRIBANCO" title="TRIBANCO" />
                            </a>
                        </li>
                        <li>
                            <a href="http://www.tribancoseguros.com.br/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_tribanco_seguros.png') }}" alt="TRIBANCO Seguros" title="TRIBANCO Seguros" />
                            </a>
                        </li>
                        <li>
                            <a href="http://www.tricard.com.br/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_tricard.png') }}" alt="TRICARD" title="TRICARD" />
                            </a>
                        </li>
                        <li>
                            <a href="http://b.martins.com.br/umv.aspx" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_umv.png') }}" alt="Universidade Martins do Varejo" title="Universidade Martins do Varejo" />
                            </a>
                        </li>
                        <li>
                            <a href="http://www.redesmart.com.br/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_smart.png') }}" alt="SMART" title="SMART" />
                            </a>
                        </li>
                        <li>
                            <a href="http://formar.martins.com.br/hotsite/default1.asp?idttipo=C&idtcliente=N" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_formar.png') }}" alt="FORM@R" title="FORM@R" />
                            </a>
                        </li>
                        <li>
                            <a href="http://www.iamar.org.br/site/" target="_blank">
                                <img src="{{ Integra::asset('imagens/img_logo_cabecalho_iamar.png') }}" alt="IAMAR" title="IAMAR" />
                            </a>
                        </li>
                    </ul>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="contato-telefone">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <ul class="ulNivel2">
                        <li class="telefone"><i class="sprite-footer icon-telefone"></i> SAC: (34) 3218-1310 </li>
                        <li>Martins Com&eacute;rcio e Servi&ccedil;o de Distribui&ccedil;&atilde;o - S/A</li>
                        <li>Rua Jata&iacute;, 1150 - B. Aparecida - Uberl&acirc;ndia - MG</li>
                        <li>Email: <a href="mailto:contato@martins.com.br" class="text-primary">contato@martins.com.br</a></li>
                        <li>{{ date('Y') }} &copy; Martins - Todos os direitos reservados.</li>
                    </ul>
                </div>

                <div class="col-sm-6">
                    <div class="desenvolvido text-right">
                        <a href="http://www.wingmidia.com.br" target="_blank" class="developed" title="Conhe&ccedil;a a Wing M&iacute;dia Integrada.">
                            <span class="text-primary">Desenvolvido por</span>
                            <img src="{{ Integra::asset('imagens/logo-wing-midia.png') }}" alt="Desenvolvido por Wing M&iacute;dia Integrada" title="Wing M&iacute;dia Integrada" class="img-logo-wing" />
                            <span class="wing text-primary">wingmidia</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript" src="{{ Integra::asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ Integra::asset('js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@stack('scripts')

<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function(){
        /* Início Validando o formulário de login */
        //$( '#frmPlanograma' ).validationEngine( 'validate' );
        /* Fim Validando o formulário de login */

        $('[data-toggle="popover"]').popover();
        $('[data-toggle="tooltip"]').tooltip();

        $('.select2').select2();

        @stack('scripts_ready')


    });
</script>
</body>
</html>



