@extends($config['view'].'layouts.master')

@section('styles')

    <style>
        .link-hover-active{
            opacity: 0.5;
            cursor: pointer;
        }
        .table .table {
            background-color: initial;
        }
    </style>

@endsection

@section('content')

    @include($config['view'].'assortment.comparison.inc.header')

    @include($config['view'].'assortment.comparison.inc.steps', ['step' => 'subcategories'])

    <div class="container">

        <h3 class="title">Comparação de subcategorias</h3>

        <br><br>

        <div class="row">
            <div class="col-md-12">

                <div class="table-responsive">
                    <table class="table margin-0">
                        <tr>
                            @foreach($clients_selected as $id => $name)
                                <th width="{{ 100/count($clients_selected) }}%" class="text-center" style="padding: 0; white-space: nowrap;">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading" style="background-color: #f48129;">
                                            <b class="title">
                                                {{$name}}
                                            </b>
                                        </div>
                                        <div style="background: white; padding: 5px 5px 0 5px">
                                            <table class="table table-bordered margin-0" style="background: #cfcfcf; border-radius: 5px;">
                                                <tr class="">
                                                    <td width="50%">
                                                        <div class="text-center">
                                                            <p>
                                                                <small><b>{{ trans('sites/maxmix/assortment.tag.label.sku') }}</b></small>
                                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.sku') }}"><i class="material-icons">info_outline</i></small>
                                                            </p>
                                                            <p>
                                                                {{ number_format($data[$id]['tag']['sku'], 0, ',', '.') }}
                                                            </p>
                                                        </div>
                                                    </td>
                                                    <td width="50%">
                                                        <div class="text-center">
                                                            <p>
                                                                <small><b>{{ trans('sites/maxmix/assortment.tag.label.amount') }}</b></small>
                                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.amount') }}"><i class="material-icons">info_outline</i></small>
                                                            </p>
                                                            <p>
                                                                {{ number_format($data[$id]['tag']['amount'], 0, ',', '.') }}
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="">
                                                    <td width="50%">
                                                        <div class="text-center">
                                                            <p>
                                                                <small><b>{{ trans('sites/maxmix/assortment.tag.label.revenue') }}</b></small>
                                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.revenue') }}"><i class="material-icons">info_outline</i></small>
                                                            </p>
                                                            <p>
                                                                R$ {{ number_format($data[$id]['tag']['total_revenue'], 2, ',', '.') }}
                                                            </p>
                                                        </div>
                                                    </td>
                                                    <td width="50%">
                                                        <div class="text-center">
                                                            <p>
                                                                <small><b>{{ trans('sites/maxmix/assortment.tag.label.margin') }}</b></small>
                                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.tag.tip.margin') }}"><i class="material-icons">info_outline</i></small>
                                                            </p>
                                                            <p>
                                                                R$ {{ number_format($data[$id]['tag']['total_gross_margin'], 2, ',', '.') }}
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="comparison_box_{{$id}}">
                                            <img src="{{ Integra::asset('imagens/bx_loader.gif') }}">
                                        </div>
                                    </div>
                                </th>
                            @endforeach
                        </tr>
                    </table>
                </div>

            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="navigate">
                    <div class="">

                        <a href="{{ route('sites.assortment.comparison.categories', $parameters) }}#steps" class="btn prev">
                            <i class="material-icons">arrow_back</i>
                            <span>{{ trans('sites/general.previous') }}</span>
                        </a>

                        <a href="{{ route('sites.assortment.comparison.brands', $parameters) }}#steps" class="btn next">
                            <span>{{ trans('sites/general.next') }}</span>
                            <i class="material-icons">arrow_forward</i>
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title title" id="myModalLabel"></h3>
                </div>
                <div class="modal-body">
                    <div class="text-center"><img src="{{ Integra::asset('imagens/bx_loader.gif') }}"></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

<script>

    function loadComparisonSubcategory(client_id){

        var link = "{{ route('sites.assortment.comparison.subcategories.client', $parameters) }}?c="+client_id;

        $( "#comparison_box_"+client_id ).load( encodeURI(link), function(){
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
        });

    }

    function loadComparisonSubcategoryDetails(client_id, subcategory_name){

        var link = "{{ route('sites.assortment.comparison.subcategories.details', $parameters) }}?c="+client_id+'&s='+subcategory_name;

        $( "#myModal .modal-body" ).load( encodeURI(link) );

        $('#myModal .modal-title').html("Comparativo da subcategoria: <br> <b>"+subcategory_name+"</b>");
        $('#myModal .modal-body').html('<div class="text-center"><img src="{{ Integra::asset('imagens/bx_loader.gif') }}"></div>');
        $('#myModal').modal('show');

    }

    function linkMouseEnter(obj){

        var name = $(obj).data('link-hover');

        $('.link-hover-'+name).addClass('link-hover-active');

    }

    function linkMouseLeave(obj){

        var name = $(obj).data('link-hover');

        $('.link-hover-'+name).removeClass('link-hover-active');

    }

</script>

@endpush

@push('scripts_ready')

    @foreach($clients_selected as $client_id => $client_name)
        loadComparisonSubcategory({{$client_id}});
    @endforeach

@endpush