<div style="padding: 5px">

    <hr>

    <div class="panel-group panel-assortment" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-column panel-default">
            <div class="panel-heading" role="tab" id="headingNone">
                <h4 class="panel-title">
                    <table class="table table-condensed">
                        <tr>
                            <td width="15%" class="text-center">
                                {{ trans('sites/general.position') }}
                            </td>
                            <td width="42.5%">
                                {{ trans('sites/general.subcategory') }}
                            </td>
                            <td width="32.5%" class="text-right">
                                <a tabindex="0" class="popover-open" role="button" data-toggle="popover" data-container="body" data-placement="top" data-trigger="hover focus" title="{{ trans('sites/maxmix/assortment.participation_subcategory') }}" data-content="{{ trans('sites/maxmix/assortment.subcategory.popover.participation') }}">
                                    {{ trans('sites/general.participation') }}
                                </a>
                            </td>
                            <td width="10%" class="text-right">
                                {{ trans('sites/general.group') }}
                            </td>
                        </tr>
                    </table>
                </h4>
            </div>
        </div>
        @foreach($subcategories as $key => $item)
            <div class="panel panel-{{ ($item['perishable']) ? 'perishable' : 'primary' }} link-hover-{{ md5($item['subcategory']) }}" onmouseleave="linkMouseLeave(this)" onmouseenter="linkMouseEnter(this)" data-link-hover="{{ md5($item['subcategory']) }}">
                <div class="panel-heading" role="tab" id="heading{{ $item['subcategory_id'] }}" onclick="loadComparisonSubcategoryDetails({{$client['id']}}, '{{$item['subcategory']}}')" data-client_id="{{$client['id']}}" data-subcategory_name="{{$item['subcategory']}}">
                    <h4 class="panel-title">
                        <table class="table">
                            <tr>
                                <td width="15%" class="text-center">
                                    {{ $key+1 }}
                                </td>
                                <td width="42.5%">
                                    <b>{{ $item['subcategory'] }}</b>
                                </td>
                                <td width="32.5%" class="text-right">
                                    <b>{{ number_format($item['participation'], 2, ',', '.') }}%</b>
                                </td>
                                <td width="10%" class="text-right">
                                    <b class="label bg-class bg-class-{{ $item['group'] }}">{{ $item['group'] }}</b>
                                </td>
                            </tr>
                        </table>
                    </h4>
                </div>
            </div>
        @endforeach
    </div>
</div>