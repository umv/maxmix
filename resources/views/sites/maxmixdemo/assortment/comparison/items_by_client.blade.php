{{--
@foreach($itens as $key => $item)
    <button class="btnComparisonBrandDetails btn btn-block btn-sm btn-{{ ($item['recomendation']['W'] != 0) ? 'success' : 'danger' }}" onclick="loadComparisonItemDetails({{$client['id']}}, '{{$item['product']}}')" data-client_id="{{$client['id']}}" data-item_name="{{$item['product']}}">
        {{ $item['product'] }}
    </button>
    <br>
@endforeach--}}

<div style="padding: 5px">

    <div style="background: #ecf0f1">
        <table class="table table-bordered margin-0" style="background: #cfcfcf; border-radius: 5px;">
            <tr class="">
                <td width="35%">
                    <div class="text-center">
                        <p>
                            <small><b>{{ trans('sites/general.position') }}</b></small>
                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.item.info.positon') }}"><i class="material-icons">info_outline</i></small>
                        </p>
                        <p>
                            {{ $position }}
                        </p>
                    </div>
                </td>
                <td width="35%">
                    <div class="text-center">
                        <p>
                            <small><b>{{ trans('sites/general.margin') }}</b></small>
                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.item.info.margin') }}"><i class="material-icons">info_outline</i></small>
                        </p>
                        <p>
                            {{ number_format($margin, 2, ',', '.') }}%
                        </p>
                    </div>
                </td>
                <td width="30%">
                    <div class="text-center">
                        <p>
                            <small><b>{{ trans('sites/general.group') }}</b></small>
                            <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.item.info.group') }}"><i class="material-icons">info_outline</i></small>
                        </p>
                        <p>
                            <b class="label bg-class bg-class-{{ $classification }}">{{ $classification }}</b>
                        </p>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <hr>

    <div class="panel-group panel-assortment" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-column panel-default">
            <div class="panel-heading" role="tab" id="headingNone">
                <h4 class="panel-title">
                    <table class="table table-condensed">
                        <tr>
                            <td width="60%">
                                Item
                            </td>
                            <td width="25%" class="text-center">
                                {{ trans('sites/general.brand') }}
                            </td>
                            <td width="10%" class="text-right">
                                {{ trans('sites/general.participation') }}
                            </td>
                            <td width="5%" class="text-right">
                                Class.
                            </td>
                        </tr>
                    </table>
                </h4>
            </div>
        </div>
        @foreach($itens as $key => $item)
            <div class="panel panel-{{ ($item['recomendation']['W'] != 0) ? 'primary' : 'default' }} link-hover-{{ md5($item['product']) }}" onmouseleave="linkMouseLeave(this)" onmouseenter="linkMouseEnter(this)" data-link-hover="{{ md5($item['product']) }}">
                <div class="panel-heading" role="tab" id="heading{{ $item['product_id'] }}" onclick="loadComparisonItemDetails({{$client['id']}}, '{{$item['product']}}')" data-client_id="{{$client['id']}}" data-item_name="{{$item['product']}}">
                    <h4 class="panel-title">
                        <table class="table">
                            <tr>
                                <td width="50%">
                                    <b>{{ $item['product'] }}</b>
                                    <br><small>EAN: {{ $item['ean'] }}</small>
                                </td>
                                <td width="20%">
                                    <span class="{{ (!$item['brand_valuation']['status']) ? 'text-danger' : 'text-success-' }}">
                                        {{ $item['brand'] }}
                                    </span>
                                </td>
                                <td width="10%" class="text-right">
                                    {{ number_format($item['participation'], 2, ',', '.') }}%
                                </td>
                                <td width="5%" class="text-right">
                                    <b class="label bg-class bg-class-{{ $item['classification'] }}">{{ $item['classification'] }}</b>
                                </td>
                            </tr>
                        </table>
                    </h4>
                </div>
            </div>
        @endforeach
    </div>
</div>