<div class="panel panel-primary">
    <div class="panel-heading">
        <span class="title">DESEMPENHO NA LOJA: {{$current['client']['name']}}</span>
    </div>
    <table class="table table-bordered margin-0">
        <thead>
            <tr class="active">
                <th class="text-center">SKU's</th>
                <th class="text-center">{{ trans('sites/maxmix/assortment.item.label.amount') }}</th>
                <th class="text-center">Participação</th>
                <th class="text-center">{{ trans('sites/maxmix/assortment.average_revenue') }}</th>
                <th class="text-center">{{ trans('sites/maxmix/assortment.average_margin') }}</th>
                <th class="text-center">{{ trans('sites/maxmix/assortment.average_margin') }}</th>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">{{ number_format($current['sku'],0) }}</td>
                <td class="text-center">{{ number_format($current['amount'], 0) }}</td>
                <td class="text-center">{{ number_format($current['participation'], 2, ',', '.') }}%</td>
                <td class="text-center">R$ {{ number_format($current['total_revenue'], 2, ',', '.') }}</td>
                <td class="text-center">R$ {{ number_format($current['total_gross_margin'], 2, ',', '.') }}</td>
                <td class="text-center">{{ number_format($current['percentage_gross_margin'], 2, ',', '.') }}%</td>

            </tr>
        </tbody>
    </table>
</div>

@if($min['client']['id'] != $max['client']['id'])
<div class="panel panel-primary">
    <div class="panel-heading">
        <span class="title">DESEMPENHO NA REDE</span>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th colspan="2" width="30%" class="active">DESEMPENHO MÉDIO</th>
            <th colspan="2" width="35%" class="bg-class bg-class-D">PIOR DESEMPENHO</th>
            <th colspan="2" width="35%" class="bg-class bg-class-A">MELHOR DESEMPENHO</th>
        </tr>
        <tr>
            <th colspan="2"></th>
            <th colspan="2" class="text-center">{{$min['client']['name']}}</th>
            <th colspan="2" class="text-center">{{$max['client']['name']}}</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>SKU's</td>
            <td class="text-right">{{ number_format($avg['sku'],0) }}</td>
            <td>SKU's</td>
            <td class="text-right">{{ number_format($min['sku'],0) }}</td>
            <td>SKU's</td>
            <td class="text-right">{{ number_format($max['sku'],0) }}</td>
        </tr>
        <tr>
            <td>{{ trans('sites/maxmix/assortment.item.label.amount') }}</td>
            <td class="text-right">{{ number_format($avg['amount'], 0) }}</td>
            <td>{{ trans('sites/maxmix/assortment.item.label.amount') }}</td>
            <td class="text-right">{{ number_format($min['amount'], 0) }}</td>
            <td>{{ trans('sites/maxmix/assortment.item.label.amount') }}</td>
            <td class="text-right">{{ number_format($max['amount'], 0) }}</td>
        </tr>
        <tr>
            <td>Participação</td>
            <td class="text-right">{{ number_format($avg['participation'], 2, ',', '.') }}%</td>
            <td>Participação</td>
            <td class="text-right">{{ number_format($min['participation'], 2, ',', '.') }}%</td>
            <td>Participação</td>
            <td class="text-right">{{ number_format($max['participation'], 2, ',', '.') }}%</td>
        </tr>
        <tr>
            <td>{{ trans('sites/maxmix/assortment.average_revenue') }}</td>
            <td class="text-right">R$ {{ number_format($avg['total_revenue'], 2, ',', '.') }}</td>
            <td>{{ trans('sites/maxmix/assortment.average_revenue') }}</td>
            <td class="text-right">R$ {{ number_format($min['total_revenue'], 2, ',', '.') }}</td>
            <td>{{ trans('sites/maxmix/assortment.average_revenue') }}</td>
            <td class="text-right">R$ {{ number_format($max['total_revenue'], 2, ',', '.') }}</td>
        </tr>
        <tr>
            <td>{{ trans('sites/maxmix/assortment.average_margin') }}</td>
            <td class="text-right">R$ {{ number_format($avg['total_gross_margin'], 2, ',', '.') }}</td>
            <td>{{ trans('sites/maxmix/assortment.average_margin') }}</td>
            <td class="text-right">R$ {{ number_format($min['total_gross_margin'], 2, ',', '.') }}</td>
            <td>{{ trans('sites/maxmix/assortment.average_margin') }}</td>
            <td class="text-right">R$ {{ number_format($max['total_gross_margin'], 2, ',', '.') }}</td>
        </tr>
        <tr>
            <td>{{ trans('sites/maxmix/assortment.average_margin') }}</td>
            <td class="text-right">{{ number_format($avg['percentage_gross_margin'], 2, ',', '.') }}%</td>
            <td>{{ trans('sites/maxmix/assortment.average_margin') }}</td>
            <td class="text-right">{{ number_format($min['percentage_gross_margin'], 2, ',', '.') }}%</td>
            <td>{{ trans('sites/maxmix/assortment.average_margin') }}</td>
            <td class="text-right">{{ number_format($max['percentage_gross_margin'], 2, ',', '.') }}%</td>
        </tr>
        </tbody>
    </table>
</div>
@endif

@if($presence_percentage <= 100)
<div class="panel panel-primary">
    <div class="panel-heading">
        <span class="title">PRESENÇA DA SUBCATEGORIA</span>
    </div>
    <div class="panel-body">
        @if($presence_percentage < 100)
        <h4 class="text-center">Encontrada em {{$presence_count}} {{($presence_count > 1) ? 'lojas' : 'loja'}}</h4>
        @endif
        <h4 class="text-center">Presença em {{ number_format($presence_percentage, 0) }}% da rede.</h4>
    </div>
</div>
@endif

