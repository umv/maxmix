@extends($config['view'].'layouts.master')

@section('content')

    <div class="container">

        <h1 class="title pull-left margin-0"><img src="{{ Integra::asset('imagens/icon-assortment-outline.png') }}" alt="Planograma" width="50" class="margin-r-10">
            @if($option == 'valuation')
                {{ trans('sites/maxmix/assortment.valuation_assortment') }}
            @elseif($option == 'monitoring')
                Acompanhamento de Sortimento
            @elseif($option == 'comparison')
                Comparação de Sortimento
            @endif
        </h1>
        <a href="javascript:window.history.back();" class="btn btn-link btn-xs pull-right hidden-xs"><br><i class="material-icons">arrow_back</i> Voltar</a>
        <div class="clearfix"></div>
        <hr>

        {{--<div class="media">
            <div class="media-left media-middle">
                <img src="{{ Integra::asset('imagens/icon-assortment.png') }}" alt="{{ trans('sites/general.assortment') }}" width="40">
            </div>

            <div class="media-body">
                <h2 class="text-success">
                    @if($option == 'valuation')
                        {{ trans('sites/maxmix/assortment.valuation_assortment') }}
                    @elseif($option == 'monitoring')
                        Acompanhamento de Sortimento
                    @endif
                </h2>
            </div>
            <div class="media-right">
                <a href="javascript:window.history.back();" class="btn btn-default btn-xs pull-right hidden-xs">
                    <i class="material-icons">arrow_back</i> {{ trans('sites/general.back') }}
                </a>
            </div>
        </div>
        <hr>--}}
    </div>

    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <h3>{{ trans('sites/maxmix/assortment.choice_client') }}:</h3>
                <br>
            </div>
        </div>

        @forelse($clients as $key => $client)
            <div class="col-md-3">
                <a href="{{ route($route, Hashids::encode($client->id)) }}">
                    <div class="box-info text-center margin-t-30">
                        <h1><big><i class="material-icons text-primary">person</i></big></h1>
                        <h4>
                            <b> {{ $client->name }}</b>
                            <br>
                            <small><b>CNPJ: </b> {{ $client->cnpj }} <br> <b>{{ trans('sites/general.code') }}: </b> {{ $client->code }} </small>
                        </h4>
                    </div>
                </a>
            </div>
        @empty

            <div class="alert alert-danger">
                <h4>Não existem clientes para sortimento</h4>
            </div>

        @endforelse


@endsection