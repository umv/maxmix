<div class="row">
    <div class="col-sm-8">
        <div class="row">
            <div class="col-xs-4">
                <button type="button" onclick="$('#focal').fullScreen(true)" class="btn btn-default btn-block btn-sm" >
                    <i class="material-icons">zoom_out_map</i> <span class="hidden-xs">Zoom</span>
                </button>
            </div>
            <div class="col-xs-4">
                <a href="{!! $planogram['img']->encode('data-url') !!}" target="_blank" class="btn btn-default btn-block btn-sm">
                    <i class="material-icons">file_download</i> <span class="hidden-xs">Download</span>
                </a>
            </div>
            <div class="col-xs-4">
                <button type="button" onclick="$('#focal').print()" class="btn btn-default btn-block btn-sm">
                    <i class="material-icons">print</i> <span class="hidden-xs">Imprimir</span>
                </button>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <a href="{{ route('sites.assortment.valuation.items', [Hashids::encode($client_id), Hashids::encode($tag_id)]) }}?s={{$subcategory_id}}&print=1&mix=1&brand_size={{serialize($brand_size)}}" target="_blank" class="btn btn-primary btn-block">
            <i class="material-icons">format_list_numbered</i> MIX IDEAL
        </a>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-8">
        <div style="border:1px solid #ddd">
            <section id="focal">
                <div class="parent text-center">
                    <div class="panzoom text-center">
                        <img src="{!! $planogram['img']->encode('data-url') !!}" class="img-responsive">
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box-570">
            <table class="table table-bordered table-hover table-striped table-condensed" cellspacing="1">
                <thead>
                <tr>
                    <th colspan="2">Espaço (em cm) dos produtos na prateleira.</th>
                </tr>
                </thead>
                <tbody>
                @foreach($planogram['items_original'] as $item)
                    <tr>
                        <td>{{ $item['name'] }}:</td>
                        <td class="text-right">{{ number_format($item['length'], 0, ',', '.') }} cm</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<hr>

<div class="text-right">
    <button type="button" onclick="loadGeneratorPlanogram()" class="btn btn-default btnPrint">Refazer</button>
    <button type="button" class="btn btn-primary" data-dismiss="modal">{{ trans('sites/general.close') }}</button>
</div>

<script type="text/javascript" src="{{ Integra::asset('js/jquery.panzoom.min.js') }}"></script>
<script type="text/javascript" src="{{ Integra::asset('js/jquery.mousewheel.js') }}"></script>
<script type="text/javascript" src="{{ Integra::asset('js/jquery.fullscreen-min.js') }}"></script>
<script type="text/javascript" src="{{ Integra::asset('js/jquery.print.js') }}"></script>
<script>
    (function() {
        var $section = $('#focal');
        var $panzoom = $section.find('.panzoom').panzoom();
        $panzoom.parent().on('mousewheel.focal', function( e ) {
            e.preventDefault();
            var delta = e.delta || e.originalEvent.wheelDelta;
            var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
            $panzoom.panzoom('zoom', zoomOut, {
                minScale: 1,
                increment: 0.3,
                animate: true,
                focal: e
            });
        });
    })();
</script>