<link href="{{ Integra::asset('css/validationEngine.jquery.min.css') }}" rel="stylesheet" media="screen" />

<form name="frmPlanograma" id="frmPlanograma" class="frm-planograma" method="post" action="{{ route('sites.assortment.planogram.brands', [$client_id, $tag_id]) }}">
    <div class="">
        {!! csrf_field() !!}

        <input type="hidden" name="s" value="{{ $subcategory_id }}">

        <fieldset>
            <section class="quantidade-modulos">

                <h4>
                    Escolha a quantidade de módulos:
                </h4>

                <div class="ctn-modulos">
                    <div class="row">
                        <div class="col-xs-4 text-center">
                            <div class="box-quantidade-modulos">
                                <div class="ctn-img">
                                    <label for="modulo_01">
                                        <img src="{{ Integra::asset('imagens/modulos-1.jpg') }}" alt="01 módulo" title="01 módulo" class="img-responsive" />
                                    </label>
                                </div>

                                <input type="radio" id="modulo_01" name="quantidade_modulos" value="1" checked="checked" />
                                <label for="modulo_01">
                                    <span><span></span></span><br>01<br>módulo
                                </label>
                            </div>

                        </div>

                        <div class="col-xs-4 text-center">
                            <div class="box-quantidade-modulos">
                                <div class="ctn-img">
                                    <label for="modulo_02">
                                        <img src="{{ Integra::asset('imagens/modulos-2.jpg') }}" alt="02 módulos" title="02 módulos" class="img-responsive" />
                                    </label>
                                </div>

                                <input type="radio" id="modulo_02" name="quantidade_modulos" value="2" />
                                <label for="modulo_02">
                                    <span><span></span></span><br>02<br>módulos
                                </label>
                            </div>
                        </div>

                        <div class="col-xs-4 text-center">
                            <div class="box-quantidade-modulos">
                                <div class="ctn-img">
                                    <label for="modulo_03">
                                        <img src="{{ Integra::asset('imagens/modulos-3.jpg') }}" alt="03 módulos" title="03 módulos" class="img-responsive" />
                                    </label>
                                </div>
                                <input type="radio" id="modulo_03" name="quantidade_modulos" value="3" />
                                <label for="modulo_03">
                                    <span><span></span></span><br>03<br>módulos
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <hr>
        </fieldset>

        <fieldset>
            <section class="quantidade-prateleiras">

                <h4>
                    Defina a quantidade de prateleiras e largura do módulo:
                </h4>

                <div class="ctn-prateleiras">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="hidden-md hidden-lg">
                                <h4>Quantidade de plateleiras</h4>
                            </div>

                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <div class="box-quantidade-prateleiras prateleira_1">
                                        <div class="ctn-img">
                                            <label for="prateleira_modulo_1">
                                                <img src="{{ Integra::asset('imagens/gondola-0-prateleiras.jpg') }}" id="img_prateleira_modulo_1" alt="Prateleira" title="" class="img-responsive" />
                                            </label>
                                        </div>

                                        <div class="text-center">
                                            <select name="gondola[1]" id="prateleira_modulo_1" attribute="img_prateleira_modulo_1" class="select-prateleira form-control required">
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-xs-4 text-center">
                                    <div class="box-quantidade-prateleiras prateleira_2">
                                        <div class="ctn-img">
                                            <label for="prateleira_modulo_2">
                                                <img src="{{ Integra::asset('imagens/gondola-0-prateleiras.jpg') }}" id="img_prateleira_modulo_2" alt="Prateleira" title="" class="img-responsive" />
                                            </label>
                                        </div>

                                        <div class="text-center">
                                            <select name="gondola[2]" id="prateleira_modulo_2" attribute="img_prateleira_modulo_2" class="select-prateleira form-control required">
                                                <option value="0">-Selecione-</option>
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-xs-4 text-center">
                                    <div class="box-quantidade-prateleiras prateleira_3">
                                        <div class="ctn-img">
                                            <label for="prateleira_modulo_3">
                                                <img src="{{ Integra::asset('imagens/gondola-0-prateleiras.jpg') }}" id="img_prateleira_modulo_3" alt="Prateleira" title="" class="img-responsive" />
                                            </label>
                                        </div>

                                        <div class="text-center">
                                            <select name="gondola[3]" id="prateleira_modulo_3" attribute="img_prateleira_modulo_3" class="select-prateleira form-control required">
                                                <option value="0">-Selecione-</option>
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 largura-gondula">
                            <div class="hidden-md hidden-lg">
                                <h4>Dimensão das plateleiras</h4>
                            </div>
                            <div class="col-xs-6 text-center">
                                <div class="ctn-img">
                                    <label for="largura_gondola_1">
                                        <img src="{{ Integra::asset('imagens/largura-gondola-1.jpg') }}" alt="1,00 m" title="1,00 m" class="img-responsive" />
                                    </label>
                                </div>
                                <input type="radio" id="largura_gondola_1" name="shelf_size" value="1" checked="checked" />
                                <label for="largura_gondola_1">
                                    <span><span></span></span>1,0 m
                                </label>
                            </div>

                            <div class="col-xs-6 text-center">
                                <div class="ctn-img">
                                    <label for="largura_gondola_13">
                                        <img src="{{ Integra::asset('imagens/largura-gondola-1.3.jpg') }}" alt="1,30 m" title="1,30 m" class="img-responsive" />
                                    </label>
                                </div>
                                <input type="radio" id="largura_gondola_13" name="shelf_size" value="1.3" />
                                <label for="largura_gondola_13">
                                    <span><span></span></span>1,3 m
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <hr>
        </fieldset>

        <button id="enviar" type="submit" class="btn next">
            <span>Gerar Planograma</span>  <i class="material-icons">send</i>
        </button>
    </div>
</form>

<div id="boxPlanogram" style="display: none">

</div>


<script type="text/javascript" src="{{ Integra::asset('js/jquery.meio.mask.min.js') }}"></script>
<script type="text/javascript" src="{{ Integra::asset('js/jquery.formtowizard.js') }}"></script>
<script type="text/javascript" src="{{ Integra::asset('js/jquery.validationEngine.min.js') }}"></script>
<script type="text/javascript" src="{{ Integra::asset('js/jquery.validationEngine-pt_BR.min.js') }}"></script>

<script>

    /* Início Form Wizard com seus validadores */
    var $frmPlanograma = $('#frmPlanograma');

    $frmPlanograma.validationEngine('validate');

    $frmPlanograma.formToWizard({
        submitButton: 'enviar',
        showProgress: false,
        prevBtnName: '<i class="material-icons">arrow_back</i> <span>Anterior</span>',
        nextBtnName: '<span>Próximo</span> <i class="material-icons">arrow_forward</i>',
        showStepNo: true,
        validateBeforeNext: function () {
            return $frmPlanograma.validationEngine('validate');
        }
    });
    /* Fim Form Wizard com seus validadores */

    /* Exibe a quantidade de gondolas exata na tela para escolha das prateleiras */
    $('.box-quantidade-prateleiras').hide(); // Oculta todas as prateleiras por padrão
    $('.prateleira_1').show(); // Exibe apenas uma prateleira por default

    $('input[name="quantidade_modulos"]').click(function () {
        var qtdGondolas = $(this).val();
        $('.box-quantidade-prateleiras').hide();

        for (i = 1; i <= qtdGondolas; i++) {
            $('.prateleira_' + i).show();
        }
    });

    /* Muda a imagem com a quantidade de prateleira nas gondolas */
    $('.select-prateleira').change(function () {
        var alvo = $(this).attr('attribute');
        $('#' + alvo).attr('src', '{{ Integra::asset('/') }}/imagens/gondola-' + $(this).val() + '-prateleiras.jpg');
    });

    $(".filter-input").keyup(function () {
        var filter = $(this).val();
        var target = $(this).data('target');
        $(target + ".filter-list .filter-item").each(function () {
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
            } else {
                $(this).show();
            }
        });
    });

    // Attach a submit handler to the form
    $frmPlanograma.submit(function( event ) {

        // Stop form from submitting normally
        event.preventDefault();

        $('#enviar').html('<span>Carregando...</span> <i class="material-icons">watch_later</i>').attr('disable', true).addClass('btn-wait');

        // Get some values from elements on the page:
        var $form = $( this ),
                term = $form.find( "input[name='s']" ).val(),
                d = $form.serialize(),
                url = $form.attr( "action" );

        // Send the data using post
        var posting = $.post( url, d );

        // Put the results in a div
        posting.done(function( data ) {

            $form.toggle();
            $('#boxPlanogram').toggle().html(data);

            //console.log(data);

            //var content = $( data ).find( "#content" );
            //$( "#result" ).empty().append( content );
        });
    });

</script>