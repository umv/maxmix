@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.monitoring.inc.header')

    @include($config['view'].'assortment.monitoring.inc.steps', ['step' => 'init'])

    <div class="container">

        <h3 class="title">Avaliação mensal</h3>

        <br><br>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title upper">
                    Venda Mensal
                </h4>
            </div>
            <div class="panel-body">

                <div class="table-responsive">

                    <table class="table">
                        <thead>
                            <tr>
                                <th width="20%"></th>
                                <th width="30%" class="text-center">RECEITA</th>
                                <th width="30%" class="text-center">MARGEM EM R$</th>
                                <th width="20%" class="text-center">MARGEM EM %</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>
                                    <h4><b>{{ $tags[0]->name }}</b></h4>
                                </th>
                                <td class="text-center">
                                    <h4>
                                        R$ {{ number_format($tags[0]->total->total_revenue, 2, ',', '.') }}
                                    </h4>
                                </td>
                                <td class="text-center">
                                    <h4>
                                        R$ {{ number_format($tags[0]->total->total_gross_margin, 2, ',', '.') }}
                                    </h4>
                                </td>
                                <td class="text-center">
                                    <h4>
                                        {{ number_format($tags[0]->total->percentage_gross_margin, 2, ',', '.') }}%
                                    </h4>
                                </td>
                            </tr>
                            <tr>
                                <th>

                                </th>
                                <th class="text-center">
                                    @if($tags[0]->total->total_revenue < $tags[1]->total->total_revenue)
                                        <h1><i class="material-icons text-success">trending_up</i></h1>
                                    @elseif($tags[0]->total->total_revenue > $tags[1]->total->total_revenue)
                                        <h1><i class="material-icons text-danger">trending_down</i></h1>
                                    @else
                                        <h1><i class="material-icons text-muted">drag_handle</i></h1>
                                    @endif
                                </th>
                                <th class="text-center">
                                    @if($tags[0]->total->total_gross_margin < $tags[1]->total->total_gross_margin)
                                        <h1><i class="material-icons text-success">trending_up</i></h1>
                                    @elseif($tags[0]->total->total_gross_margin > $tags[1]->total->total_gross_margin)
                                        <h1><i class="material-icons text-danger">trending_down</i></h1>
                                    @else
                                        <h1><i class="material-icons text-muted">drag_handle</i></h1>
                                    @endif
                                </th>
                                <th class="text-center">
                                    @if($tags[0]->total->percentage_gross_margin < $tags[1]->total->percentage_gross_margin)
                                        <h1><i class="material-icons text-success">trending_up</i></h1>
                                    @elseif($tags[0]->total->percentage_gross_margin > $tags[1]->total->percentage_gross_margin)
                                        <h1><i class="material-icons text-danger">trending_down</i></h1>
                                    @else
                                        <h1><i class="material-icons text-muted">drag_handle</i></h1>
                                    @endif
                                </th>
                            </tr>
                            <tr>
                                <th>
                                    <h4><b>{{ $tags[1]->name }}</b></h4>
                                </th>
                                <td class="text-center">
                                    <h4>
                                        R$ {{ number_format($tags[1]->total->total_revenue, 2, ',', '.') }}
                                    </h4>
                                </td>
                                <td class="text-center">
                                    <h4>
                                        R$ {{ number_format($tags[1]->total->total_gross_margin, 2, ',', '.') }}
                                    </h4>
                                </td>
                                <td class="text-center">
                                    <h4>
                                        {{ number_format($tags[1]->total->percentage_gross_margin, 2, ',', '.') }}%
                                    </h4>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>

        <hr>

        <div class="navigate">
            <a href="{{ route('sites.assortment.monitoring.categories', [Hashids::encode($client->id), $tags_url]) }}#steps" class="btn next">
                <span>{{ trans('sites/general.next') }}</span>
                <i class="material-icons">arrow_forward</i>
            </a>

            <div class="clearfix"></div>
        </div>

    </div>


@endsection