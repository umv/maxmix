@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.monitoring.inc.header')

    @include($config['view'].'assortment.monitoring.inc.steps', ['step' => 'brands'])

    <div class="container">

        <h3 class="title">Acompanhamento de marcas</h3>

        <br><br>

        <div class="row">
            <div class="col-md-6 @if(!$subcategory_id) col-md-offset-3 @endif">
                <div class="form-group has-success">
                    <label class="upper">{{ trans('sites/general.subcategory') }}</label>
                    {!! Form::select( 'subcategory', $subcategories_grouped, Input::old( 'subcategory', $subcategory_id ), ['id' => 'subcategory', 'class' => 'form-control input-lg input-primary' ,'placeholder' => trans('sites/general.select_subcategory') ]) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @if($subcategory_id)
                    @if(!empty($chart['categories']))
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="title">
                                Margem Média
                            </h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                <small><i class="material-icons">info_outline</i> Clique no nome da marca para visualizar mais detalhes.</small>
                            </p>
                            <div id="chart" style="width:100%; height:{{ count($chart['categories'])*60 }}px; min-height: 400px"></div>
                        </div>
                    </div>
                    @else

                        <div class="alert alert-warning">
                            <h4>Não existem marcas para acompanhamento. Selecione outra subcategoria.</h4>
                        </div>

                    @endif
                @else

                    <div class="text-center">
                        <h1>
                            <big><i class="material-icons text-muted">arrow_upward</i></big>
                            <br>
                            <small>{{ trans('sites/maxmix/assortment.select_category') }}</small>
                        </h1>
                    </div>

                @endif
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="navigate">
                    <div class="">
                        <a href="{{ route('sites.assortment.monitoring.subcategories', [Hashids::encode($client->id), $tags_url]) }}#steps" class="btn prev">
                            <i class="material-icons">arrow_back</i>
                            <span>{{ trans('sites/general.previous') }}</span>
                        </a>

                        <a href="{{ route('sites.assortment.monitoring.client', Hashids::encode($client->id)) }}" class="btn next">
                            <span>{{ trans('sites/general.conclude') }}</span>
                            <i class="material-icons">arrow_forward</i>
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title title" id="myModalLabel"></h3>
                </div>
                <div class="modal-bod">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('sites/general.close') }}</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    @if($subcategory_id && !empty($chart['categories']))
        <script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>
        <script src="http://blacklabel.github.io/custom_events/js/customEvents.js"></script>
    @endif
@endpush

@push('scripts_ready')

    $('#subcategory').on('change', function(){

        var s = this.value;
        var url = "{{ route('sites.assortment.monitoring.brands', [Hashids::encode($client->id), $tags_url]) }}?s=";

        document.location.href = url+s+"#steps";

    });

    @if($subcategory_id && !empty($chart['categories']))

        {{--var brands = {!! json_encode($chart['data']['brands'])  !!};--}}

        $('#chart').highcharts({
            chart: {
                type: 'bar',
                style: {
                    fontFamily: '"Lato","Helvetica Neue",Helvetica,Arial,sans-serif'
                }
            },
            colors: ['#bbbbbb', '#dc6000'],
            xAxis: {
                categories: {!! json_encode($chart['categories'])  !!},
                title: {
                    text: null
                },
                labels: {
                    style: {"cursor":"pointer","fontWeight":"bold","fontSize":"14px"},
                    useHTML: true,
                    formatter: function () {
                        var res = this.value.split(": ");
                        var id = res[1].replace(/\s{1,}/g, '');
                        //brand = brands[brand];
                        return "<span class='btnMonitoringDetails' data-brand='"+res[0]+"' data-id='"+id+"'>"+res[0]+'<br>'+res[2]+"</span>";
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Margem média mensal em reais (R$)',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                enabled: false,
                backgroundColor: '#ffffff',
                shared: true,
                useHTML: true,
                headerFormat: '<br><b>{point.key}</b><hr><table>',
                pointFormat: '<tr><td style="color: {series.color}">{series.name}: </td>' +
                        '<td style="text-align: right"><b>R$ {point.y}</b></td></tr>',
                footerFormat: '</table>',
                //valuePrefix: 'R$ '
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return (Intl.NumberFormat()) ? new Intl.NumberFormat().format(this.y) : this.y;
                        }
                    }

                },
                series: {
                    groupPadding: 0.1
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'top',
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF')
            },
            credits: {
                enabled: false
            },
            series: {!! json_encode($chart['series']) !!}
        });


        $('.btnMonitoringDetails').on('click', function(){

            var link = "{{ route('sites.assortment.monitoring.details', [Hashids::encode($client->id), $tags_url, 'brand']) }}?s={{$subcategory_id}}&b="+$(this).data('id');

            $('#myModal .modal-title').html("Acompanhamento da marca:<br><b>" + $(this).data('brand') + "<b>");
            $('#myModal .modal-bod').html("{{ trans('sites/general.loading') }}...");
            $('#myModal').modal('show');

            $.get( link, function( data ) {
                $('#myModal .modal-bod').html( data );
            });

        });

    @endif

@endpush