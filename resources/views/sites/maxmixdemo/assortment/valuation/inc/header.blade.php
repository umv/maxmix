<div class="container">
    <h1 class="title pull-left margin-0"><img src="{{ Integra::asset('imagens/icon-assortment-outline.png') }}" alt="Planograma" width="50" class="margin-r-10">
        {{ trans('sites/maxmix/assortment.valuation_assortment') }}
    </h1>
    <a href="@if(isset($client) && isset($tag)){{ route('sites.assortment.valuation.client', Hashids::encode($client->id)) }}@else{{ 'javascript:window.history.back();' }}@endif" class="btn btn-link btn-xs pull-right hidden-xs"><br><i class="material-icons">arrow_back</i> Voltar</a>
    <div class="clearfix"></div>
    <hr>

    <div id="steps"></div>

    @if(isset($client))
        <p class="text-default text-center">
            <small><b>CLIENTE: </b> {{ $client->name }} @if(isset($tag))- <b>PERÍODO: </b> {{$tag->name}}@endif</small>
        </p>
    @endif

    <br>

    {{--<div class="media">
        <div class="media-left media-middle">
            <img src="{{ Integra::asset('imagens/icon-assortment.png') }}" alt="{{ trans('sites/general.assortment') }}" width="80">
        </div>

        <div class="media-body">
            <h2 class="text-primary">
                {{ trans('sites/maxmix/assortment.valuation_assortment') }}
            </h2>
            @if(isset($client))
            <p class="text-primary"><b>Cliente: </b> {{ $client->name }} @if(isset($tag))- <b>Período: </b> {{$tag->name}}@endif</p>
            @endif
        </div>
        <div class="media-right">
            <a href=" @if(isset($client) && isset($tag)){{ route('sites.assortment.valuation.client', Hashids::encode($client->id)) }}@else{{ 'javascript:window.history.back();' }}@endif" class="btn btn-default btn-xs pull-right hidden-xs">
                <i class="material-icons">arrow_back</i> {{ trans('sites/general.back') }}
            </a>
        </div>
    </div>
    <hr>--}}
</div>