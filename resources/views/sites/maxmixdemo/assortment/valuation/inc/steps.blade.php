<div class="steps visible-lg visible-md">
    <div class="container">
        <div class="row">
            <div class="col-md-2 text-center">
                <span class="step {{ ($step == 'init') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.valuation.init', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps">
                        {{ trans('sites/maxmix/assortment.step.init') }}
                    </a>
                </span>
            </div>
            <div class="col-md-2 text-center">
                <span class="step {{ ($step == 'cockpit') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.valuation.cockpit', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps">
                        {{ trans('sites/maxmix/assortment.step.cockpit') }}
                    </a>
                </span>
            </div>
            <div class="col-md-2 text-center">
                <span class="step {{ ($step == 'categories') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.valuation.categories', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps">
                        {{ trans('sites/maxmix/assortment.step.categories') }}
                    </a>
                </span>
            </div>
            <div class="col-md-2 text-center">
                <span class="step {{ ($step == 'subcategories') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.valuation.subcategories', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps">
                        {{ trans('sites/maxmix/assortment.step.subcategories') }}
                    </a>
                </span>
            </div>
            <div class="col-md-2 text-center">
                <span class="step {{ ($step == 'brands') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.valuation.brands', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps">
                        {{ trans('sites/maxmix/assortment.step.brands') }}
                    </a>
                </span>
            </div>
            <div class="col-md-2 text-center">
                <span class="step {{ ($step == 'itens') ? 'active' : '' }}">
                    <a href="{{ route('sites.assortment.valuation.items', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps">
                        {{ trans('sites/maxmix/assortment.step.itens') }}
                    </a>
                </span>
            </div>
        </div>
    </div>
</div>