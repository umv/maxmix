@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.valuation.inc.header')

    @include($config['view'].'assortment.valuation.inc.steps', ['step' => 'subcategories'])

    <div class="container">

        <h3 class="title">Posicionamento das Subcategorias</h3>

        <br><br>

        <div class="row">
            <div class="col-md-6">

                <div class="panel-group panel-assortment" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-column panel-default">
                        <div class="panel-heading" role="tab" id="headingNone">
                            <h4 class="panel-title">
                                <a role="button" class="btn-block" href="#">
                                    <table class="table table-condensed">
                                        <tr>
                                            <td width="15%" class="text-center">
                                                {{ trans('sites/general.position') }}
                                            </td>
                                            <td width="42.5%">
                                                {{ trans('sites/general.subcategory') }}
                                            </td>
                                            <td width="32.5%" class="text-right">
                                                <a tabindex="0" class="popover-open" role="button" data-toggle="popover" data-container="body" data-placement="top" data-trigger="hover focus" title="{{ trans('sites/maxmix/assortment.participation_subcategory') }}" data-content="{{ trans('sites/maxmix/assortment.subcategory.popover.participation') }}">
                                                    {{ trans('sites/general.participation') }}
                                                </a>
                                            </td>
                                            <td width="10%" class="text-right">
                                                {{ trans('sites/general.group') }}
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                            </h4>
                        </div>
                    </div>

                    @foreach($subcategories as $key => $item)
                        <div class="panel panel-{{ ($item['perishable']) ? 'perishable' : 'primary' }}">
                            <div class="panel-heading" role="tab" id="heading{{ $item['subcategory_id'] }}">
                                <h4 class="panel-title">
                                    <a role="button" class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $item['subcategory_id'] }}" aria-expanded="true" aria-controls="collapse{{ $item['subcategory_id'] }}">
                                        <table class="table">
                                            <tr>
                                                <td width="15%" class="text-center">
                                                    {{ $key+1 }}
                                                </td>
                                                <td width="42.5%">
                                                    <b>{{ $item['subcategory'] }}</b>
                                                </td>
                                                <td width="32.5%" class="text-right">
                                                    <b>{{ number_format($item['participation'], 2, ',', '.') }}%</b>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <b class="label bg-class bg-class-{{ $item['group'] }}">{{ $item['group'] }}</b>
                                                </td>
                                            </tr>
                                        </table>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{ $item['subcategory_id'] }}" class="panel-collapse collapse @if($key == 0) in @endif" role="tabpanel" aria-labelledby="heading{{ $item['subcategory_id'] }}">
                                <table class="table table-bordered">
                                    <tr>
                                        <td width="15%">
                                        <span class="text-center">
                                            <p><small><b>SKU's</b></small></p>
                                            <p>
                                                {{ number_format($item['sku'], 0, ',', '.') }}
                                                @if($key == 0)
                                                    <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.category.tip.sku') }}"><i class="material-icons">info_outline</i></small>
                                                @endif
                                            </p>
                                        </span>
                                        </td>
                                        <td width="42.5%">
                                        <span class="text-center">
                                            <p><small><b class="upper">{{ trans('sites/maxmix/assortment.average_revenue') }}</b></small></p>
                                            <p>
                                                R$ {{ number_format($item['total_revenue'], 2, ',', '.') }}
                                                @if($key == 0)
                                                    <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.category.tip.revenue') }}"><i class="material-icons">info_outline</i></small>
                                                @endif
                                            </p>
                                        </span>
                                        </td>
                                        <td width="42.5%">
                                        <span class="text-center">
                                            <p><small><b class="upper">{{ trans('sites/maxmix/assortment.average_margin') }}</b></small></p>
                                            <p>
                                                R$ {{ number_format($item['total_gross_margin'], 2, ',', '.') }}
                                                @if($key == 0)
                                                    <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.category.tip.margin') }}"><i class="material-icons">info_outline</i></small>
                                                @endif
                                            </p>
                                        </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="text-center active">
                                            @if($item['perishable'])
                                                <span class="text-primary">Esta subcategoria é perecível!</span>
                                            @else
                                                <a class="btn btn-link btn-xs" @if($key == 0) data-toggle="tooltip" data-container="body" data-placement="top" @endif title="{{ trans('sites/maxmix/assortment.category.tip.composition') }}" href="{{ route('sites.assortment.valuation.brands', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}?s={{$item['subcategory_id']}}#steps">
                                                    <i class="material-icons">exposure</i> AVALIAÇÃO DE MARCAS DESTA SUBCATEGORIA
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            <div class="col-md-6">
                <div data-spy="affix" data-offset-top="400" data-offset-bottom="600">
                    <div class="row">
                        <div class="col-md-6">
                            <hr>
                            <table class="table-info pull-left" style="width: 10px">
                                <tr>
                                    <td class="bg-class bg-class-A active" data-group="A">A</td>
                                </tr>
                                <tr>
                                    <td class="bg-class bg-class-B" data-group="B">B</td>
                                </tr>
                                <tr>
                                    <td class="bg-class bg-class-C" data-group="C">C</td>
                                </tr>
                                <tr>
                                    <td class="bg-class bg-class-D" data-group="D">D</td>
                                </tr>
                            </table>
                            <div class="box-table-A active">
                                <h4 class="upper">{{ trans('sites/maxmix/assortment.groups.a.title') }}</h4>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_subcategory') }}:</strong> <big>{{ $table['A']['subcategory'] }}</big></p>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_item') }}:</strong> <big>{{ number_format($table['A']['sku'], 0, ',', '.') }}</big></p>
                                <p>
                                    <i class="material-icons icon text-A">sentiment_very_satisfied</i>
                                    {{ trans('sites/maxmix/assortment.subcategory.groups.a.description') }}
                                </p>
                            </div>
                            <div class="box-table-B">
                                <h4 class="upper">{{ trans('sites/maxmix/assortment.groups.b.title') }}</h4>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_subcategory') }}:</strong> <big>{{ $table['B']['subcategory'] }}</big></p>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_item') }}:</strong> <big>{{ number_format($table['B']['sku'], 0, ',', '.') }}</big></p>
                                <p>
                                    <i class="material-icons icon text-B">sentiment_satisfied</i>
                                    {{ trans('sites/maxmix/assortment.groups.b.description') }}
                                </p>
                            </div>
                            <div class="box-table-C">
                                <h4 class="upper">{{ trans('sites/maxmix/assortment.groups.c.title') }}</h4>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_subcategory') }}:</strong> <big>{{ $table['C']['subcategory'] }}</big></p>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_item') }}:</strong> <big>{{ number_format($table['C']['sku'], 0, ',', '.') }}</big></p>
                                <p>
                                    <i class="material-icons icon text-C">sentiment_neutral</i>
                                    {{ trans('sites/maxmix/assortment.groups.c.description') }}
                                </p>
                            </div>
                            <div class="box-table-D">
                                <h4 class="upper">{{ trans('sites/maxmix/assortment.groups.d.title') }}</h4>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_subcategory') }}:</strong> <big>{{ $table['D']['subcategory'] }}</big></p>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_item') }}:</strong> <big>{{ number_format($table['D']['sku'], 0, ',', '.') }}</big></p>
                                <p>
                                    <i class="material-icons icon text-D">sentiment_dissatisfied</i>
                                    {{ trans('sites/maxmix/assortment.groups.d.description') }}
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="chart-table" style="height: 300px;"></div>
                            @piechart('ChartTable', 'chart-table')
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="navigate">
                                <a href="{{ route('sites.assortment.valuation.categories', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps" class="btn prev">
                                    <i class="material-icons">arrow_back</i>
                                    <span>{{ trans('sites/general.previous') }}</span>
                                </a>
                                <a href="{{ route('sites.assortment.valuation.brands', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps" class="btn next">
                                    <span>{{ trans('sites/general.next') }}</span>
                                    <i class="material-icons">arrow_forward</i>
                                </a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--<div class="card card-bordered form ">
            <div class="card-body">

                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th width="5%">POSIÇÃO</th>
                            <th>SUBCATEGORIAS</th>
                            <th width="10%">MARGEM BRUTA</th>
                            <th width="10%">RECEITA</th>
                            <th width="5%">SKU's</th>
                            <th width="1%">PARTICIPAÇÃO</th>
                            <th width="5">GRUPO</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subcategories as $key => $subcategory)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>
                                <a href="{{ route('sites.assortment.valuation.brands', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}?s={{$subcategory['subcategory_id']}}">
                                    {{ $subcategory['subcategory'] }}
                                </a>
                            </td>
                            <td>R$ {{ number_format($subcategory['gross_margin'], 2, ',', '.') }}</td>
                            <td>R$ {{ number_format($subcategory['total_revenue'], 2, ',', '.') }}</td>
                            <td>{{ number_format(ceil($subcategory['amount']), 0, ',', '.') }}</td>
                            <td>{{ number_format($subcategory['percentage'], 2, ',', '.') }} %</td>
                            <td class="bg-class bg-class-{{ $subcategory['position'] }}">{{ $subcategory['position'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>--}}
    </div>


@endsection


@push('scripts_ready')

$('.table-info td.bg-class').on('mouseover', function(){

    $('.table-info td.bg-class, div[class^="box-table-"]').removeClass('active');
    $(this).addClass('active');
    $('.box-table-'+$(this).data('group')).addClass('active');

});

$('.popover-open').popover('show');
setTimeout(function() {$('.popover-open').popover('hide')},3000);

@endpush