<html>

    <head>

        <link rel="stylesheet" href="{{ Integra::asset('css/bootstrap.min.css') }}">

        <style>
            *{
                font-size: 12px !important;
            }
        </style>
    </head>

    <body>

        <div class="text-center hidden-print">
            <br>
            <button class="btn btn-primary " onclick="window.print()">IMPRIMIR</button>
        </div>

        <div class="container">

            <h2>
                @if(isset($client) && isset($tag))
                        {{ trans('sites/general.assortment') }} [{{ $client->name }}] <small>- {{$tag->name}}</small>
                @elseif(isset($client))
                        {{ trans('sites/general.assortment') }} [{{ $client->name }}]
                @else
                        {{ trans('sites/general.assortment') }}
                @endif
            </h2>

            <h3>Lista de itens: <b>{{ $importance }}</b> {{--- Grupo da subcategoria: <b>{{ $group }}</b> - Classificação do item: <b>{{ $class }}</b>--}} </h3>

            <hr>

            <div class="row">

                <div class="col-xs-6">
                    <p>
                        <strong>{{ trans('sites/maxmix/assortment.average_revenue') }}:</strong>
                        <br>
                        R$ <big>{{ number_format($data['total_revenue'], 2, ',', '.') }}</big>  ( <big><b>{{ number_format($data['percentage_revenue'], 2, ',', '.') }}</b>%</big> )
                    </p>
                    <p>
                        <strong>{{ trans('sites/maxmix/assortment.average_margin') }}:</strong>
                        <br>
                        R$ <big>{{ number_format($data['total_gross_margin'], 2, ',', '.') }}</big> ( <big><b>{{ number_format($data['percentage_gross_margin'], 2, ',', '.') }}</b>%</big> )
                    </p>
                </div>

                <div class="col-xs-6">
                    <p><strong>{{ trans('sites/maxmix/assortment.cockipt.label.category') }}:</strong> <big>{{ number_format(count($data['category']),0,',','.') }}</big></p>
                    <p><strong>{{ trans('sites/maxmix/assortment.cockipt.label.subcategory') }}:</strong> <big>{{ number_format(count($data['subcategory']),0,',','.') }}</big></p>
                    <p><strong>{{ trans('sites/maxmix/assortment.cockipt.label.brand') }}:</strong> <big>{{ number_format(count($data['brand']),0,',','.') }}</big></p>
                    <p><strong>{{ trans('sites/maxmix/assortment.cockipt.label.item') }}:</strong> <big>{{ number_format(count($data['item']),0,',','.') }}</big></p>
                </div>

            </div>

            <table class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>{{ trans('sites/general.category') }}</th>
                        <th>{{ trans('sites/general.subcategory') }}</th>
                        <th>{{ trans('sites/maxmix/assortment.cockipt.label.amount') }}</th>
                        <th>{{ trans('sites/maxmix/assortment.cockipt.label.price') }} (R$)</th>
                        <th>{{ trans('sites/maxmix/assortment.average_revenue') }} (R$)</th>
                        <th>{{ trans('sites/maxmix/assortment.average_margin') }} (R$)</th>
                        <th>{{ trans('sites/general.margin') }} (%)</th>
                        <th>{{ trans('sites/general.participation') }} (%)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($itens as $key => $item)
                        <tr>
                            <td>
                                <b>{{ $item['product'] }}</b>
                                <br><small>EAN: {{ $item['ean'] }}</small>
                            </td>
                            <td>
                                {{ $item['category'] }}
                            </td>
                            <td>
                                {{ $item['subcategory'] }}
                            </td>
                            <td class="text-right">
                                {{ number_format($item['amount'], 0, ',', '.') }}
                            </td>
                            <td class="text-right">
                                {{ number_format($item['unit_price'], 2, ',', '.') }}
                            </td>
                            <td class="text-right">
                                {{ number_format($item['total_revenue'], 2, ',', '.') }}
                            </td>
                            <td class="text-right">
                                <b>{{ number_format($item['total_gross_margin'], 2, ',', '.') }}</b>
                            </td>
                            <td class="text-right">
                                {{ number_format($item['percentage'], 2, ',', '.') }}
                            </td>
                            <td class="text-right">
                                {{ number_format($item['participation'], 2, ',', '.') }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

    </body>

</html>




