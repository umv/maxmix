<div class="row">

    <div class="col-md-8">

        <div class="well well-sm" style="max-height: 350px; overflow: auto">
            <div class="panel-group panel-assortment" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-column panel-default">
                    <div class="panel-heading" role="tab" id="headingNone">
                        <h4 class="panel-title">
                            <table class="table table-condensed">
                                <tr class="active">
                                    <td width="60%">
                                        Item
                                    </td>
                                    <td width="10%" class="text-right">
                                        {{ trans('sites/general.participation') }}
                                    </td>
                                    <td width="30%" class="text-right">
                                        {{ trans('sites/maxmix/assortment.average_margin') }}
                                    </td>
                                </tr>
                            </table>
                        </h4>
                    </div>
                </div>
                @foreach($itens as $key => $item)
                    <div class="panel panel-{{ ($item['perishable']) ? 'perishable' : 'primary' }}">
                        <div class="panel-heading" role="tab" id="heading{{ $item['product_id'] }}">
                            <h4 class="panel-title">
                                <a role="button" class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $item['product_id'] }}" aria-expanded="true" aria-controls="collapse{{ $item['product_id'] }}">
                                    <table class="table">
                                        <tr>
                                            <td width="60%">
                                                <b>{{ $item['product'] }}</b>
                                                <br><small>EAN: {{ $item['ean'] }}</small>
                                            </td>
                                            <td width="10%" class="text-right">
                                                {{ number_format($item['participation'], 2, ',', '.') }}%
                                            </td>
                                            <td width="30%" class="text-right">
                                                <b>R$ {{ number_format($item['total_gross_margin'], 2, ',', '.') }}</b>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse{{ $item['product_id'] }}" class="panel-collapse collapse @if($key == 0) in @endif" role="tabpanel" aria-labelledby="heading{{ $item['product_id'] }}">
                            <table class="table table-bordered">
                                <tr>
                                    <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>{{ trans('sites/maxmix/assortment.cockipt.label.amount') }}</b></small></p>
                                        <p>
                                            {{ number_format($item['amount'], 0, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.cockipt.tip.amount') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                    </td>
                                    <td width="25%">
                                    <span class="text-center">
                                        <p><small><b>{{ trans('sites/maxmix/assortment.cockipt.label.price') }}</b></small></p>
                                        <p>
                                            R$ {{ number_format($item['unit_price'], 2, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.cockipt.tip.price') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                    </td>
                                    <td width="25%">
                                    <span class="text-center">
                                        <p><small><b class="upper">{{ trans('sites/maxmix/assortment.average_revenue') }}</b></small></p>
                                        <p>
                                            R$ {{ number_format($item['total_revenue'], 2, ',', '.') }}
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.cockipt.tip.revenue') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                    </td>
                                    <td width="25%">
                                    <span class="text-center">
                                        <p><small><b class="upper">{{ trans('sites/general.margin') }} %</b></small></p>
                                        <p>
                                            {{ number_format($item['percentage'], 2, ',', '.') }}%
                                            @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.cockipt.tip.margin') }}"><i class="material-icons">info_outline</i></small>
                                            @endif
                                        </p>
                                    </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="active" colspan="4">
                                        <div class="row">
                                            <div class="col-sm-4 text-center">
                                                <p><small><b class="upper">{{ trans('sites/general.category') }}</b></small></p>
                                                <a class="btn btn-link btn-xs btn-block" href="{{ route('sites.assortment.valuation.categories', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#heading{{ $item['category_id'] }}">
                                                    {{ $item['category'] }}
                                                </a>
                                            </div>
                                            <div class="col-sm-4 text-center">
                                                <p><small><b class="upper">{{ trans('sites/general.subcategory') }}</b></small></p>
                                                <a class="btn btn-link btn-xs btn-block" href="{{ route('sites.assortment.valuation.subcategories', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#heading{{ $item['subcategory_id'] }}">
                                                    {{ $item['subcategory'] }}
                                                </a>
                                            </div>
                                            <div class="col-sm-4 text-center">
                                                @if($item['perishable'])
                                                    <p class="text-primary"><b class="upper">Esta item é perecível!</b></p>
                                                @else
                                                    <p><small><b class="upper">{{ trans('sites/general.brand') }}</b></small></p>
                                                    <a class="btn btn-link btn-xs btn-block" href="{{ route('sites.assortment.valuation.brands', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}?s={{ $item['subcategory_id'] }}#heading{{ $item['brand_id'] }}">
                                                        {{ $item['brand'] }}
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="col-md-4">

        <div class="alert alert-default bg-primary">
            <p>
                <strong>{{ trans('sites/maxmix/assortment.average_revenue') }}:</strong>
                <br>
                R$ <big>{{ number_format($data['total_revenue'], 2, ',', '.') }}</big>  ( <big><b>{{ number_format($data['percentage_revenue'], 2, ',', '.') }}</b>%</big> )
            </p>
            <p>
                <strong>{{ trans('sites/maxmix/assortment.average_margin') }}:</strong>
                <br>
                R$ <big>{{ number_format($data['total_gross_margin'], 2, ',', '.') }}</big> ( <big><b>{{ number_format($data['percentage_gross_margin'], 2, ',', '.') }}</b>%</big> )
            </p>
        </div>

        <div class="well">
            <p><strong>{{ trans('sites/maxmix/assortment.cockipt.label.category') }}:</strong> <big>{{ number_format(count($data['category']),0,',','.') }}</big></p>
            <p><strong>{{ trans('sites/maxmix/assortment.cockipt.label.subcategory') }}:</strong> <big>{{ number_format(count($data['subcategory']),0,',','.') }}</big></p>
            <p><strong>{{ trans('sites/maxmix/assortment.cockipt.label.brand') }}:</strong> <big>{{ number_format(count($data['brand']),0,',','.') }}</big></p>
            <p><strong>{{ trans('sites/maxmix/assortment.cockipt.label.item') }}:</strong> <big>{{ number_format(count($data['item']),0,',','.') }}</big></p>
        </div>

    </div>

</div>
@if($data['working_capital'] > 0)
    <div class="text-center">
        <h3>Capital de giro adicional: <br> <b>R$ {{ number_format($data['working_capital'], 2, ',', '.') }}</b></h3>
        <p class="text-muted">Este é valor do capital de giro adicional com a remoção dos itens para exclusão</p>
    </div>
@endif
<hr>
<div class="well-well-sm">
    <h4><i class="material-icons">info</i> Dicas:</h4>
    @if($importance_matrix == 1)
        <ul class="list-inline">
            <li>Esses são os principais produtos da sua loja! Eles devem concentrar o seu maior foco. Todos que trabalham na sua loja devem saber quem são esses produtos.</li>
            <li>Esses itens nunca podem faltar nas prateleiras, seu time de repositores deve saber quem eles são e assegurar que eles tenham o seu espaço em gôndola sempre preenchido.</li>
            <li>Esses produtos devem ter uma excelente exposição.</li>
            <li>O processo de compra para esses itens deve ser acompanhado intensamente, com foco na obtenção da melhor negociação possível, o que não significa menor preço sempre. Prazos de pagamento e entrega eficaz também devem ser observados.</li>
            <li>A precificação desses itens deve seguir o mercado, pois o cliente possui forte percepção de preços desses produtos.</li>
            <li>Ações promocionais trazem excelentes resultados para esse grupo de produtos.</li>
        </ul>
        {{--- Esses são os principais produtos da sua loja! Eles devem concentrar o seu maior foco. Todos que trabalham na sua loja devem saber quem são esses produtos.
        <br>- Esses itens nunca podem faltar nas prateleiras, seu time de repositores deve saber quem eles são e assegurar que eles tenham o seu espaço em gôndola sempre preenchido.
        <br>- Esses produtos devem ter uma excelente exposição.
        <br>- O processo de compra para esses itens deve ser acompanhado intensamente, com foco na obtenção da melhor negociação possível, o que não significa menor preço sempre. Prazos de pagamento e entrega eficaz também devem ser observados.
        <br>- A precificação desses itens deve seguir o mercado, pois o cliente possui forte percepção de preços desses produtos.
        <br>- Ações promocionais trazem excelentes resultados para esse grupo de produtos.--}}
    @elseif($importance_matrix == 2)
        - Esses são itens muito importantes para a sua loja.
        <br>- São produtos com boa performance de categorias importantes para a loja.
        <br>- Esses itens não podem faltar nas gôndolas e devem ter uma exposição muito boa, com grande visibilidade.
        <br>- O posicionamento de preço desses itens deve seguir o mercado, pois o cliente possui boa percepção de preços.
        <br>- Ações promocionais trazem bons resultados para esse grupo de produtos.
    @elseif($importance_matrix == 3)
        - Esses ítens possuem um desempenho razoável em categorias medianas para a loja.
        <br>- Você não deve focar esses itens do mesmo modo que os itens 1 e 2.
        <br>- Esses itens não deveriam faltar em sua gôndola.
        <br>- O cliente possui baixa percepção de preço desses itens , a precificação deve ser focada em rentabilizar os ítens, com boa margem bruta.
        <br>- Ações promocionais trazem resultados medianos para a sua loja.
    @elseif($importance_matrix == 4)
        - Esse itens possuem baixo desempenho ou pertencem a categorias pouco representativas na loja.
        <br>- Seu cliente não vai à loja com o objetivo de comprar esses produtos, logo ele não precisa ter uma reposição impecável.
        <br>- Você não deve investir muito tempo e esforço em negociações deses produtos, foque em itens de grupos superiores.
        <br>- Precificação deve levar em conta a rentabilização do item, com a aplicação de boas margens brutas, pois o cliente não possui percepção de preços sobre esse grupo de produtos.
    @elseif($importance_matrix == 5)
        - Esses ítens  devem ocupar pouquíssimo tempo de seu trabalho.
        <br>- Eles podem faltar em sua loja, logo você não deve ter o mesmo foco de reposição dos grupos anteriores.
        <br>- A precificação deve ser focada em rentabilidade, o seu cliente não possui nenhuma percepção de preço desses itens.
        <br>- Ações promocionais neses grupos geram baixíssimo impacto na venda loja.
    @elseif($importance_matrix == 6)
        - Esses itens possuem um desempenho ruim em categorias com pouca relevância na loja.
        <br>- Deve ser avaliada a sua permanência no sortimento da sua loja pois esses produtos trazem pouca contribuição ao resultado e ocupam espaço que deve ser utilizado para exposição de itens mais rentáveis.
    @elseif($importance_matrix == 7)
        - Aqui temos os produtos com o pior desempenho, das categorias menos relevantes da loja.
        <br>- Esses itens devem ser excluídos imediatamente, liberando espaço para produtos mais rentáveis.
        <br>- Categorias com baixíssima relevância, devem possuir poucas marcas e pouquíssimos ítens.
    @endif
</div>
