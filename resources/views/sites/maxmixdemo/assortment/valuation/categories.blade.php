@extends($config['view'].'layouts.master')

@section('content')

    @include($config['view'].'assortment.valuation.inc.header')

    @include($config['view'].'assortment.valuation.inc.steps', ['step' => 'categories'])

    <div class="container">

        <h3 class="title">{{ trans('sites/maxmix/assortment.title.categories') }}</h3>

        <br><br>

        <div class="row">
            <div class="col-md-6">

                <div class="panel-group panel-assortment" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-column panel-default">
                        <div class="panel-heading" role="tab" id="headingNone">
                            <h4 class="panel-title">
                                <a role="button" class="btn-block" href="#">
                                    <table class="table table-condensed">
                                        <tr>
                                            <td width="15%" class="text-center">
                                                {{ trans('sites/general.position') }}
                                            </td>
                                            <td width="42.5%">
                                                {{ trans('sites/general.category') }}
                                            </td>
                                            <td width="32.5%" class="text-right">
                                                <a tabindex="0" class="popover-open" role="button" data-toggle="popover" data-container="body" data-placement="top" data-trigger="hover focus" title="{{ trans('sites/maxmix/assortment.participation_category') }}" data-content="{{ trans('sites/maxmix/assortment.category.popover.participation') }}">
                                                    {{ trans('sites/general.participation') }}
                                                </a>
                                            </td>
                                            <td width="10%" class="text-right">
                                                {{ trans('sites/general.group') }}
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                            </h4>
                        </div>
                    </div>

                    @foreach($categories as $key => $item)
                    <div class="panel panel-{{ ($item['perishable']) ? 'perishable' : 'primary' }}">
                        <div class="panel-heading" role="tab" id="heading{{ $item['category_id'] }}">
                            <h4 class="panel-title">
                                <a role="button" class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $item['category_id'] }}" aria-expanded="true" aria-controls="collapse{{ $item['category_id'] }}">
                                    <table class="table">
                                        <tr>
                                            <td width="15%" class="text-center">
                                                {{ $key+1 }}
                                            </td>
                                            <td width="42.5%">
                                                <b>{{ $item['category'] }}</b>
                                            </td>
                                            <td width="32.5%" class="text-right">
                                                <b>{{ number_format($item['participation'], 2, ',', '.') }}%</b>
                                            </td>
                                            <td width="10%" class="text-right">
                                                <b class="label bg-class bg-class-{{ $item['group'] }}">{{ $item['group'] }}</b>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse{{ $item['category_id'] }}" class="panel-collapse collapse @if($key == 0) in @endif" role="tabpanel" aria-labelledby="heading{{ $item['category_id'] }}">
                            <table class="table table-bordered">
                                <tr>
                                    <td width="15%">
                                        <span class="text-center">
                                            <p><small><b>SKU's</b></small></p>
                                            <p>
                                                {{ number_format($item['sku'], 0, ',', '.') }}
                                                @if($key == 0)
                                                    <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.category.tip.sku') }}"><i class="material-icons">info_outline</i></small>
                                                @endif
                                            </p>
                                        </span>
                                    </td>
                                    <td width="42.5%">
                                        <span class="text-center">
                                            <p><small><b class="upper">{{ trans('sites/maxmix/assortment.average_revenue') }}</b></small></p>
                                            <p>
                                                R$ {{ number_format($item['total_revenue'], 2, ',', '.') }}
                                                @if($key == 0)
                                                    <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.category.tip.revenue') }}"><i class="material-icons">info_outline</i></small>
                                                @endif
                                            </p>
                                        </span>
                                    </td>
                                    <td width="42.5%">
                                        <span class="text-center">
                                            <p><small><b class="upper">{{ trans('sites/maxmix/assortment.average_margin') }}</b></small></p>
                                            <p>
                                                R$ {{ number_format($item['total_gross_margin'], 2, ',', '.') }}
                                                @if($key == 0)
                                                <small class="text-muted" data-toggle="tooltip" data-container="body" data-placement="top" title="{{ trans('sites/maxmix/assortment.category.tip.margin') }}"><i class="material-icons">info_outline</i></small>
                                                @endif
                                            </p>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-center active">
                                        <a class="btn btn-link btn-xs upper btnComposition" @if($key == 0) data-toggle="tooltip" data-container="body" data-placement="top" @endif title="{{ trans('sites/maxmix/assortment.category.tip.composition') }}" href="javascript:void(0)" data-category="{{ $item['category'] }}" data-link="{{ route('sites.assortment.valuation.categories.composition', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}?c={{$item['category_id']}}&a=1">
                                            <i class="material-icons">pie_chart</i> {{ trans('sites/maxmix/assortment.category.composition') }}
                                        </a>
                                        @if($item['perishable'])
                                            <br>
                                            <span class="text-primary">Esta categoria é perecível!</span>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
            <div class="col-md-6">
                <div data-spy="affix" data-offset-top="400" data-offset-bottom="600">
                    <div class="row">
                        <div class="col-md-6">
                            <hr>
                            <table class="table-info pull-left" style="width: 10px">
                                <tr>
                                    <td class="bg-class bg-class-A active" data-group="A">A</td>
                                </tr>
                                <tr>
                                    <td class="bg-class bg-class-B" data-group="B">B</td>
                                </tr>
                                <tr>
                                    <td class="bg-class bg-class-C" data-group="C">C</td>
                                </tr>
                                <tr>
                                    <td class="bg-class bg-class-D" data-group="D">D</td>
                                </tr>
                            </table>
                            <div class="box-table-A active">
                                <h4 class="upper">{{ trans('sites/maxmix/assortment.groups.a.title') }}</h4>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_category') }}:</strong> <big>{{ $table['A']['category'] }}</big></p>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_item') }}:</strong> <big>{{ number_format($table['A']['sku'], 0, ',', '.') }}</big></p>
                                <p>
                                    <i class="material-icons icon text-A">sentiment_very_satisfied</i>
                                    {{ trans('sites/maxmix/assortment.category.groups.a.description') }}
                                </p>
                            </div>
                            <div class="box-table-B">
                                <h4 class="upper">{{ trans('sites/maxmix/assortment.groups.b.title') }}</h4>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_category') }}:</strong> <big>{{ $table['B']['category'] }}</big></p>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_item') }}:</strong> <big>{{ number_format($table['B']['sku'], 0, ',', '.') }}</big></p>
                                <p>
                                    <i class="material-icons icon text-B">sentiment_satisfied</i>
                                    {{ trans('sites/maxmix/assortment.groups.b.description') }}
                                </p>
                            </div>
                            <div class="box-table-C">
                                <h4 class="upper">{{ trans('sites/maxmix/assortment.groups.c.title') }}</h4>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_category') }}:</strong> <big>{{ $table['C']['category'] }}</big></p>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_item') }}:</strong> <big>{{ number_format($table['C']['sku'], 0, ',', '.') }}</big></p>
                                <p>
                                    <i class="material-icons icon text-C">sentiment_neutral</i>
                                    {{ trans('sites/maxmix/assortment.groups.c.description') }}
                                </p>
                            </div>
                            <div class="box-table-D">
                                <h4 class="upper">{{ trans('sites/maxmix/assortment.groups.d.title') }}</h4>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_category') }}:</strong> <big>{{ $table['D']['category'] }}</big></p>
                                <p><strong>{{ trans('sites/maxmix/assortment.count_item') }}:</strong> <big>{{ number_format($table['D']['sku'], 0, ',', '.') }}</big></p>
                                <p>
                                    <i class="material-icons icon text-D">sentiment_dissatisfied</i>
                                    {{ trans('sites/maxmix/assortment.groups.d.description') }}
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="chart-table" style="height: 300px;"></div>
                            @piechart('ChartTable', 'chart-table')
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="navigate">
                                <div class="">
                                    <a href="{{ route('sites.assortment.valuation.cockpit', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps" class="btn prev">
                                        <i class="material-icons">arrow_back</i>
                                        <span>{{ trans('sites/general.previous') }}</span>
                                    </a>

                                    <a href="{{ route('sites.assortment.valuation.subcategories', [Hashids::encode($client->id), Hashids::encode($tag->id)]) }}#steps" class="btn next">
                                        <span>{{ trans('sites/general.next') }}</span>
                                        <i class="material-icons">arrow_forward</i>
                                    </a>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title title" id="myModalLabel"></h3>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('sites/general.close') }}</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts_ready')

$('.table-info td.bg-class').on('mouseover', function(){

    $('.table-info td.bg-class, div[class^="box-table-"]').removeClass('active');
    $(this).addClass('active');
    $('.box-table-'+$(this).data('group')).addClass('active');

});

$('.btnComposition').on('click', function(){

    $('#myModal .modal-title').html("Composição da Categoria: <b>" + $(this).data('category') + "<b>");
    $('#myModal .modal-body').html("{{ trans('sites/general.loading') }}...");
    $('#myModal').modal('show');

    $.get( $(this).data('link'), function( data ) {
        $('#myModal .modal-body').html( data );
    });

});

$('.popover-open').popover('show');
setTimeout(function() {$('.popover-open').popover('hide')},3000);

@endpush