<?php

namespace App\Providers;

use App\Models\Assortment\Product as AssortmentProduct;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class IntegraServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('integra', function()
        {
            return new \App\Integra\Integra;
        });
    }
}
