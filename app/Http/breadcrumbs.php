<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push(trans('controllers.home.title'), route('home'));
});

// Home > Sites
Breadcrumbs::register('sites', function($breadcrumbs, $mode = null)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('controllers.sites.title'), route('sites'));

    if( $mode == 'create' )
        $breadcrumbs->push(trans('controllers.sites.create'));
    elseif( $mode == 'update' )
        $breadcrumbs->push(trans('controllers.sites.update'));
});

// Home > [Controllers]
Breadcrumbs::register('controllers', function($breadcrumbs, $controller, $mode = null)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('controllers.'.$controller.'.title'), route($controller));

    if( $mode == 'create' )
        $breadcrumbs->push(trans('controllers.'.$controller.'.create'));
    elseif( $mode == 'update' )
        $breadcrumbs->push(trans('controllers.'.$controller.'.update'));
});