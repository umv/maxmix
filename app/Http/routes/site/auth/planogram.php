<?php

// Planogram

Route::group(['prefix' => 'planogram'], function()
{
    Route::get('generator', 'Sites\Planogram\PlanogramsController@getGenerator')->name('sites.planogram.generator');
    Route::post('generator', 'Sites\Planogram\PlanogramsController@postGenerator')->name('sites.planogram.generator');

    Route::resource('/resource', 'Sites\Planogram\PlanogramsController', [
        'only' => [
            'index', 'show', 'edit', 'store', 'destroy'
        ],
        'names' => [
            'index' => 'sites.planogram.resource.index',
            'show' => 'sites.planogram.resource.show',
            'edit' => 'sites.planogram.resource.edit',
            'store' => 'sites.planogram.resource.store',
            'destroy' => 'sites.planogram.resource.destroy'
        ]
    ]);

    Route::post('send-email/{code}', 'Sites\Planogram\PlanogramsController@sendEmail')->name('sites.planogram.sendemail');
});