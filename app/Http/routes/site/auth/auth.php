<?php

// Auth

Route::get('auth/account', 'Sites\Auth\AuthController@getAccount')->name('sites.auth.account');
Route::post('auth/account', 'Sites\Auth\AuthController@setAccount')->name('sites.auth.account');