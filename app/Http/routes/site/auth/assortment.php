<?php

// Sortimento
Route::group(['prefix' => 'assortment'], function()
{

    // Avaliacao
    Route::group(['prefix' => 'valuation'], function()
    {

        // Cliente
        Route::group(['prefix' => '{id}'], function()
        {

            Route::get('/', 'Sites\Assortment\AssortmentsController@clientValuation')->name('sites.assortment.valuation.client');

            // Periodo
            Route::group(['prefix' => '{tag}'], function()
            {

                Route::get('init', 'Sites\Assortment\AssortmentsController@initValuation')->name('sites.assortment.valuation.init');

                Route::get('categories', 'Sites\Assortment\AssortmentsController@categoriesValuation')->name('sites.assortment.valuation.categories');
                Route::get('subcategories', 'Sites\Assortment\AssortmentsController@subcategoriesValuation')->name('sites.assortment.valuation.subcategories');
                Route::get('categories/composition', 'Sites\Assortment\AssortmentsController@compositionCategoriesValuation')->name('sites.assortment.valuation.categories.composition');
                Route::get('brands', 'Sites\Assortment\AssortmentsController@brandsValuation')->name('sites.assortment.valuation.brands');
                Route::get('items', 'Sites\Assortment\AssortmentsController@itemsValuation')->name('sites.assortment.valuation.items');
                Route::get('cockpit', 'Sites\Assortment\AssortmentsController@cockpitValuation')->name('sites.assortment.valuation.cockpit');
                Route::get('cockpit/items/{importance}', 'Sites\Assortment\AssortmentsController@itemsByImportanceValuation')->name('sites.assortment.cockpit.items');

            });

        });

    });

    // Acompanhamento
    Route::group(['prefix' => 'monitoring'], function()
    {

        // Cliente
        Route::group(['prefix' => '{id}'], function()
        {

            Route::get('/', 'Sites\Assortment\AssortmentsController@clientMonitoring')->name('sites.assortment.monitoring.client');

            // Periodo
            Route::group(['prefix' => '{tags}'], function()
            {

                Route::get('init', 'Sites\Assortment\AssortmentsController@initMonitoring')->name('sites.assortment.monitoring.init');

                Route::get('categories', 'Sites\Assortment\AssortmentsController@categoriesMonitoring')->name('sites.assortment.monitoring.categories');
                Route::get('subcategories', 'Sites\Assortment\AssortmentsController@subcategoriesMonitoring')->name('sites.assortment.monitoring.subcategories');
                Route::get('brands', 'Sites\Assortment\AssortmentsController@brandsMonitoring')->name('sites.assortment.monitoring.brands');

                Route::get('{type}/details', 'Sites\Assortment\AssortmentsController@detailsMonitoring')->name('sites.assortment.monitoring.details');

            });

        });

    });

    // Comparação
    Route::group(['prefix' => 'comparison'], function()
    {

        Route::get('/', 'Sites\Assortment\AssortmentsController@clientComparison')->name('sites.assortment.comparison.client');
        Route::post('/', 'Sites\Assortment\AssortmentsController@periodComparison')->name('sites.assortment.comparison.period');

        Route::post('/process', 'Sites\Assortment\AssortmentsController@processComparison')->name('sites.assortment.comparison.process');

        // Cliente
        Route::group(['prefix' => '{clients}'], function()
        {
            // Periodo
            Route::group(['prefix' => '{dates}'], function()
            {

                Route::get('categories', 'Sites\Assortment\AssortmentsController@categoriesComparison')->name('sites.assortment.comparison.categories');
                Route::get('subcategories', 'Sites\Assortment\AssortmentsController@subcategoriesComparison')->name('sites.assortment.comparison.subcategories');
                Route::get('subcategories/client', 'Sites\Assortment\AssortmentsController@subcategoriesByClientComparison')->name('sites.assortment.comparison.subcategories.client');
                Route::get('subcategories/details', 'Sites\Assortment\AssortmentsController@subcategoriesDetailsComparison')->name('sites.assortment.comparison.subcategories.details');
                Route::get('brands', 'Sites\Assortment\AssortmentsController@brandsComparison')->name('sites.assortment.comparison.brands');
                Route::get('brands/client', 'Sites\Assortment\AssortmentsController@brandsByClientComparison')->name('sites.assortment.comparison.brands.client');
                Route::get('brands/details', 'Sites\Assortment\AssortmentsController@brandsDetailsComparison')->name('sites.assortment.comparison.brands.details');
                Route::get('items', 'Sites\Assortment\AssortmentsController@itemsComparison')->name('sites.assortment.comparison.items');
                Route::get('items/client', 'Sites\Assortment\AssortmentsController@itemsByClientComparison')->name('sites.assortment.comparison.items.client');
                Route::get('items/details', 'Sites\Assortment\AssortmentsController@itemsDetailsComparison')->name('sites.assortment.comparison.items.details');

            });

        });

    });

    // Planograma
    Route::group(['prefix' => 'planogram'], function()
    {

        // Cliente
        Route::group(['prefix' => '{id}'], function()
        {

            // Periodo
            Route::group(['prefix' => '{tag}'], function()
            {

                Route::get('brands', 'Sites\Assortment\AssortmentsController@brandsPlanogram')->name('sites.assortment.planogram.brands');
                Route::post('brands', 'Sites\Assortment\AssortmentsController@brandsPlanogramGenerator')->name('sites.assortment.planogram.brands');

            });

        });

    });

    // lista de clientes
    Route::get('/clients/{option}', 'Sites\Assortment\AssortmentsController@listClients')->name('sites.assortment.clients');

    // lp
    Route::get('/about', 'Sites\Assortment\AssortmentsController@about')->name('sites.assortment.about');
    Route::post('/about', 'Sites\Assortment\AssortmentsController@about')->name('sites.assortment.sendrequest');

});