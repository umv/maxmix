<?php

// Authentication routes...
Route::get('auth/login', 'Sites\Auth\AuthController@getLogin')->name('sites.auth.login');
Route::post('auth/login', 'Sites\Auth\AuthController@postLogin')->name('sites.auth.login');
Route::get('auth/logout', 'Sites\Auth\AuthController@getLogout')->name('sites.auth.logout');

// Registration routes...
Route::get('auth/register/confirm/{token}', 'Sites\Auth\AuthController@confirmEmail')->name('sites.auth.register.confirm');
Route::get('auth/register', 'Sites\Auth\AuthController@getRegister')->name('sites.auth.register');
Route::post('auth/register', 'Sites\Auth\AuthController@postRegister')->name('sites.auth.register');

// Rotas para solicitar trocar de senha...
Route::get('password/email', 'Sites\Auth\PasswordController@getEmail')->name('sites.auth.password.email');
Route::post('password/email', 'Sites\Auth\PasswordController@postEmail')->name('sites.auth.password.email');

// Rotas para trocar a senha...
Route::get('password/reset/{token}', 'Sites\Auth\PasswordController@getReset')->name('sites.auth.password.reset');
Route::post('password/reset', 'Sites\Auth\PasswordController@postReset')->name('sites.auth.password.reset');