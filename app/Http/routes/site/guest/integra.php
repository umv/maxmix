<?php

Route::group(['prefix' => 'integra'], function()
{
    Route::post('/set-geoposition', 'Sites\IntegraController@setGeoposition')->name('sites.integra.setgeoposition');

    // Mailing
    Route::group(['prefix' => 'mailing'], function()
    {

        Route::post('/contact/add', 'Sites\Mailing\ContactsController@add')->name('sites.api1.mailing.contact.add');

    });

    // Form
    Route::group(['prefix' => 'form'], function()
    {

        Route::post('/send', 'Sites\Form\FormsController@send')->name('sites.integra.form.send');

    });

    // Tourism
    Route::group(['prefix' => 'tourism'], function()
    {

        Route::post('/flight/search/add', 'Sites\Tourism\FlightsController@addSearch')->name('sites.api1.tourism.flight.search.add');
        Route::post('/hotel/search/add', 'Sites\Tourism\HotelsController@addSearch')->name('sites.api1.tourism.hotel.search.add');

    });

});