<?php

// Destinos
Route::group(['prefix' => 'destinos'], function()
{

    Route::get('/', 'Sites\Tourism\DestinationsController@index')->name('sites.tourism.destinations.index');
    Route::get('/{slug_category}', 'Sites\Tourism\DestinationsController@category')->name('sites.tourism.destinations.category');
    Route::get('/{slug_category}/{slug_region}', 'Sites\Tourism\DestinationsController@region')->name('sites.tourism.destinations.region');
    Route::get('/{slug_category}/{slug_region}/{slug_subregion}', 'Sites\Tourism\DestinationsController@subregion')->name('sites.tourism.destinations.subregion');
    Route::get('/{slug_category}/{slug_region}/{slug_subregion}/{slug_destination}', 'Sites\Tourism\DestinationsController@destination')->name('sites.tourism.destinations.destination');

});

// Pacotes

Route::group(['prefix' => 'pacotes'], function()
{
    Route::get('/', 'Sites\Tourism\PackagesController@index')->name('sites.tourism.packages.index');
    Route::get('/{slug}', 'Sites\Tourism\PackagesController@getPage')->name('sites.tourism.packages.package');

    Route::get('/reservar/{destination_id?}/{date_id?}', 'Sites\Tourism\IntentionsController@index')->name('sites.tourism.packages.intentions.index');
    Route::post('/reservar/{destination_id?}/{date_id?}', 'Sites\Tourism\IntentionsController@store')->name('sites.tourism.packages.intentions.store');
    //Route::post('/{slug}/reservar/{date}', 'Sites\Tourism\PackagesController@intentionProcessForm')->name('sites.tourism.packages.intention.store');

    Route::post('/get-package-date', 'Sites\Tourism\PackagesController@getPackageDate')->name('sites.tourism.packages.getpackagedate');

});

// Push

Route::group(['prefix' => 'push'], function()
{

    Route::get('/', function(){

        return view('integra.push.home');

    });
});