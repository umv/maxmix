<?php

Route::get('login', 'Admin\System\Authentication\AuthController@login')->name('login');
Route::post('login', 'Admin\System\Authentication\AuthController@processLogin')->name('login');