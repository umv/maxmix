<?php

Route::group(['prefix' => 'form'], function()
{

    Route::resource('forms', 'Admin\Form\FormsController');
}
);