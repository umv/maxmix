<?php

Route::group(['prefix' => 'tourism'], function()
{
    Route::resource('attraction/types', 'Admin\Tourism\AttractionTypesController');
    Route::resource('attractions', 'Admin\Tourism\AttractionsController');
    Route::resource('services', 'Admin\Tourism\ServicesController');
    Route::resource('notes', 'Admin\Tourism\NotesController');
    Route::resource('payments', 'Admin\Tourism\PaymentsController');
    Route::resource('origins', 'Admin\Tourism\OriginsController');
    Route::resource('testimonials', 'Admin\Tourism\TestimonialsController');

    Route::post('categories/get-url', 'Admin\Tourism\CategoriesController@getUrl')->name('admin.tourism.categories.geturl');
    Route::resource('categories', 'Admin\Tourism\CategoriesController');

    Route::post('regions/get-url', 'Admin\Tourism\RegionsController@getUrl')->name('admin.tourism.regions.geturl');
    Route::resource('regions', 'Admin\Tourism\RegionsController');

    Route::post('subregions/get-url', 'Admin\Tourism\SubregionsController@getUrl')->name('admin.tourism.subregions.geturl');
    Route::resource('subregions', 'Admin\Tourism\SubregionsController');

    Route::post('packages/get-url', 'Admin\Tourism\PackagesController@getUrl')->name('admin.tourism.packages.geturl');
    Route::get('packages/{id}/get-photos', 'Admin\Tourism\PackagesController@getPhotos')->name('admin.tourism.packages.getphotos');
    Route::post('packages/{id}/upload-photo', 'Admin\Tourism\PackagesController@uploadPhoto')->name('admin.tourism.packages.uploadphoto');
    Route::post('packages/cover-photo', 'Admin\Tourism\PackagesController@coverPhoto')->name('admin.tourism.packages.coverphoto');
    Route::post('packages/crop-photo', 'Admin\Tourism\PackagesController@cropPhoto')->name('admin.tourism.packages.cropphoto');
    Route::post('packages/delete-photo', 'Admin\Tourism\PackagesController@deletePhoto')->name('admin.tourism.packages.deletephoto');
    Route::resource('packages', 'Admin\Tourism\PackagesController');

    Route::post('destinations/get-url', 'Admin\Tourism\DestinationsController@getUrl')->name('admin.tourism.destinations.geturl');
    Route::get('destinations/{id}/get-photos', 'Admin\Tourism\DestinationsController@getPhotos')->name('admin.tourism.destinations.getphotos');
    Route::post('destinations/{id}/upload-photo', 'Admin\Tourism\DestinationsController@uploadPhoto')->name('admin.tourism.destinations.uploadphoto');
    Route::post('destinations/cover-photo', 'Admin\Tourism\DestinationsController@coverPhoto')->name('admin.tourism.destinations.coverphoto');
    Route::post('destinations/crop-photo', 'Admin\Tourism\DestinationsController@cropPhoto')->name('admin.tourism.destinations.cropphoto');
    Route::post('destinations/delete-photo', 'Admin\Tourism\DestinationsController@deletePhoto')->name('admin.tourism.destinations.deletephoto');
    Route::resource('destinations', 'Admin\Tourism\DestinationsController');

    Route::resource('intention/config', 'Admin\Tourism\IntentionConfigsController');
    Route::resource('intentions', 'Admin\Tourism\IntentionsController');

}
);