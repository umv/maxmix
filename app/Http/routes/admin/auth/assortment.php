<?php

Route::group(['prefix' => 'assortment'], function()
{

    Route::get('bases/import', 'Admin\Assortment\BasesController@import')->name('admin.assortment.bases.import');
    Route::resource('bases', 'Admin\Assortment\BasesController');

    Route::resource('clients', 'Admin\Assortment\ClientsController');

    Route::post('tags/client', 'Admin\Assortment\TagsController@getTagsByClient')->name('admin.assortment.tags.client');
    Route::resource('tags', 'Admin\Assortment\TagsController');
}
);