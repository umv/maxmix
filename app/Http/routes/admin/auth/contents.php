<?php

Route::group(['prefix' => 'contents'], function()
{
    Route::post('pages/get-url', 'Admin\Contents\PagesController@getUrl')->name('admin.contents.pages.geturl');
    Route::resource('pages', 'Admin\Contents\PagesController');

    Route::resource('templates', 'Admin\Contents\TemplatesController');
}
);