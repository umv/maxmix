<?php

Route::group(['prefix' => 'planogram'], function()
{
    Route::resource('areas', 'Admin\Planogram\AreasController');
    Route::resource('categories', 'Admin\Planogram\CategoriesController');
    Route::resource('planograms', 'Admin\Planogram\PlanogramsController');

    Route::get('subcategories/import', 'Admin\Planogram\SubcategoriesController@import')->name('admin.planogram.subcategories.import');
    Route::resource('subcategories', 'Admin\Planogram\SubcategoriesController');

    Route::get('brands/import', 'Admin\Planogram\BrandsController@import')->name('admin.planogram.brands.import');
    Route::resource('brands', 'Admin\Planogram\BrandsController');

    Route::get('items/import', 'Admin\Planogram\ItemsController@import')->name('admin.planogram.items.import');
    Route::resource('items', 'Admin\Planogram\ItemsController');


}
);