<?php

Route::group(['prefix' => 'general'], function()
{
    Route::post('cities/get-city', 'Admin\General\CitiesController@getCity')->name('admin.general.cities.getcity');
    Route::resource('cities', 'Admin\General\CitiesController');
}
);