<?php

Route::group(['prefix' => 'mailing'], function()
{
    Route::resource('contacts', 'Admin\Mailing\ContactsController');
}
);