<?php

// Pagina inicial
Route::get('/', 'HomeController@index')->name('home');

// Alterar site
Route::get('/change-site/{id?}', 'HomeController@changeSite')->name('admin.change_site');
// Alterar site
Route::get('/locked', 'HomeController@locked')->name('admin.locked');