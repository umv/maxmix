<?php

Route::group(['prefix' => 'system'], function()
{

    Route::get('users/change-theme/{theme}', 'Admin\System\UsersController@changeTheme')->name('admin.system.users.changetheme');
    Route::resource('users', 'Admin\System\UsersController');

    Route::resource('accounts', 'Admin\System\AccountsController');
    Route::resource('roles', 'Admin\System\RolesController');
    Route::resource('sites', 'Admin\System\SitesController');
}
);