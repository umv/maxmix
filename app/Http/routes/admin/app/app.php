<?php

Route::get('logout', ['as'=>'logout', function()
{
    Sentinel::logout();
    return Redirect::to('/admin');
}]);