<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

function routesInDirectory($app = '') {
    $routeDir = app_path('Http/routes/' . $app . ($app !== '' ? '/' : NULL));
    $iterator = new RecursiveDirectoryIterator($routeDir);
    $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);

    foreach ($iterator as $route) {
        $isDotFile = strpos($route->getFilename(), '.') === 0;

        if (!$isDotFile && !$route->isDir()) {
            require $routeDir . $route->getFilename();
        }
    }
}

Route::get('routes', function() {
    $routeCollection = Route::getRoutes();

    echo "<table style='width:100%' cellspacing='1' cellpadding='4' border='1'>";
    echo "<tr>";
    echo "<td width='10%'><h4>HTTP Method</h4></td>";
    echo "<td width='10%'><h4>Route</h4></td>";
    echo "<td width='10%'><h4>Name</h4></td>";
    echo "<td width='70%'><h4>Corresponding Action</h4></td>";
    echo "</tr>";
    foreach ($routeCollection as $value) {
        echo "<tr>";
        echo "<td>" . $value->getMethods()[0] . "</td>";
        echo "<td>" . $value->getPath() . "</td>";
        echo "<td>" . $value->getName() . "</td>";
        echo "<td>" . $value->getActionName() . "</td>";
        echo "</tr>";

        // adicionando role

        if(!empty($value->getName())){

            $slug = $value->getName();

            $tab = \App\Models\System\Tab::where('slug', $slug)
                ->first();

            if(!$tab){

                \App\Models\System\Tab::create([
                    'name' => $slug,
                    'slug' => $slug
                ]);

            }

        }
    }
    echo "</table>";
});

/******************************/
/******** ADMINISTRACAO *******/
/******************************/

// ROTAS NAO AUTENTICADAS

Route::group(['prefix' => 'admin'],function()
{

    routesInDirectory('admin/app');

});


Route::group(['prefix' => 'admin', 'middleware' => 'sentinel.guest'], function()
{

    routesInDirectory('admin/guest');

});

// ROTAS AUTENTICADAS

Route::group(['prefix' => 'admin', 'middleware' => 'sentinel.auth'], function()
{

    routesInDirectory('admin/auth');

});

Route::group(['middleware' => 'sentinel.auth'], function()
{

    routesInDirectory('sentinel/auth');

});

/******************/
/***** SITE *******/
/******************/

routesInDirectory('site/guest');

Route::group(['middleware' => 'auth'], function()
{

    routesInDirectory('site/auth');

});

Route::get('teste', function(){

    $user = \Sentinel::findById(1); // set user permission

    $user->permissions = [
        'user.create' => true,
        'user.delete' => false,
    ];

    $user->save();

    $role = \Sentinel::findRoleById(1); // set role permissions

    $role->permissions = [
        'user.update' => true,
        'user.view' => true,
    ];

    $role->save();

    return 'teste';

});



// Paginas de erro
Route::get('errors/{code}', function ($code) {
    return view('errors.'.$code);
});

// Pagina inicial
Route::get('/', 'Sites\IndexController@index')->name('index');
Route::get('/aux', 'Sites\AuxController@index')->name('index.aux');

// Catch all page controller
Route::get('{slug}', [
    'uses' => 'Sites\IndexController@getPage'
])->where('slug', '([A-Za-z0-9\-\/]+)')
    ->name('sites.page');



