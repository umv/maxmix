<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Integra\ConfigFields;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

abstract class MasterController extends Controller
{

    /**
     * @var string
     */
    protected $config;

    protected $site;

    public $request;

    /**
     * MasterController constructor.
     */
    public function __construct($route_name = null, Request $request = null)
    {

        // Verifica se tem permissao

        /*if (!Sentinel::hasAccess(self::getRouter()->currentRouteName()))
        {

            echo Sentinel::getUser()->roles;

            echo self::getRouter()->currentRouteName();
            die('<br>Você não tem permissão para acessar este conteúdo!');
        }*/

        if(!session()->has('site')){
            Sentinel::logout();
            die('Faça login novamente!');
        }

        $this->site = session()->get('site');

        if($route_name !== null)
            $this->config = new ConfigFields($route_name);

        $this->request = $request;

        // compartilhando informacoes com as views
        view()->share([
            'request' => $this->request,
            'site' => $this->site,
            'config' => $this->config
        ]);
    }

    public function createSlug($title, $model)
    {
        $slug = Str::slug($title);
        $slugCount = count( $model->whereRaw("slug REGEXP '^{$slug}(-[0-9]*)?$'")->get() );

        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

    public function generateSlug($text, $model = null, $column = null, $id = null, $site = null)
    {
        $slug = Str::slug($text);

        if($model !== null and !empty($column))
        {

            if($id !== null)
                $model = $model->where('id', '<>', $id);

            if($site)
                $model = $model->where('site_id', $this->site->id);

            $model->whereRaw("{$column} REGEXP '^{$slug}(-[0-9]*)?$'");

            $count = $model->count();

            if( $count > 0)
            {
                $last_number = intval(str_replace($slug . '-', '', $model->orderBy($column, 'desc')->first()->{$column}));
                return $slug . '-' . ($last_number + 1);
            }
            else
                return $slug;

            //return ($count > 0) ? "{$slug}-{$count}" : $slug;
        }

        return $slug;
    }
}
