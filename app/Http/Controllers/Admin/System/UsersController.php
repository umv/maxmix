<?php

namespace App\Http\Controllers\Admin\System;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Models\System\Account;
use App\Models\System\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;

use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;

/**
 * Class UsersController
 * @package App\Http\Controllers\System
 */
class UsersController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.system.users';

    /**
     * Holds the Sentinel Users repository.
     *
     * @var \Cartalyst\Sentinel\Users\EloquentUser
     */
    protected $users;


    /**
     * UsersController constructor.
     */
    public function __construct()
    {
        parent::__construct($this->route_name);
        $this->users = Sentinel::getUserRepository();
    }

    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = $this->users->createModel()->select('id', DB::raw('CONCAT(first_name, " ", last_name) as name'), 'email');

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addColumn(['data' => 'email', 'name' => 'email', 'title' => 'E-mail'])
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new user.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating user.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating user.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified users.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyItens(Request $request)
    {
        return $this->users->createModel()->destroy($request->input('data'));
    }


    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {
        if ($id)
        {
            if ( ! $item = $this->users->createModel()->find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = $this->users->createModel();
        }

        $accounts = Account::lists('name','id');

        $roles = Role::lists('name','id');

        return view('admin.system.users.form', [
            'mode' => $mode,
            'item' => $item,
            'accounts' => $accounts,
            'roles' => $roles,
            'config' => $this->config
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        (!isset($input['active'])) ? $input['active'] = 0 : null ;

        unset($input['accounts']);
        unset($input['roles']);

        $rules = [
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'required|unique:system_users',
            //'accounts'   => 'required',
            //'roles'      => 'required',
        ];

        if ($id)
        {
            $item = $this->users->createModel()->find($id);

            $rules['email'] .= ",email,{$item->email},email";

            $messages = $this->validateUser($input, $rules);

            if ($messages->isEmpty())
            {
                $this->users->update($item, $input);
            }
        }
        else
        {
            $messages = $this->validateUser($input, $rules);

            if ($messages->isEmpty())
            {
                $item =  $this->users->create($input);

                $activation = Activation::create($item);

                Activation::complete($item, $activation->code);

            }
        }

        if($item)
        {
            (Input::get('accounts')) ? $item->accounts()->sync(Input::get('accounts')) : $item->accounts()->detach();

            //(Input::get('roles')) ? $item->roles()->sync(Input::get('roles')) : $item->roles()->detach();

            $item->roles()->detach();

            if(Input::has('roles')){
                foreach(Input::get('roles') as $id_role)
                {
                    $role = Sentinel::findRoleById($id_role);

                    $role->users()->attach($item);
                }
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a user.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateUser($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

    public function changeTheme($theme){

        if( !empty($theme) )
        {

            $user = $this->users->createModel()->find(Sentinel::getUser()->id);

            $input = [
                'theme' => $theme,
            ];

            $this->users->update($user, $input);
        }

        return Redirect::back();
    }
}
