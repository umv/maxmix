<?php

namespace App\Http\Controllers\Admin\System;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Models\System\Action;
use App\Models\System\Tab;
use App\Models\System\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Str;
use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;

/**
 * Class AccountsController
 * @package App\Http\Controllers\System
 */
class RolesController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.system.roles';

    protected $roles;

    /**
     * AccountsController constructor.
     */
    public function __construct()
    {
        parent::__construct($this->route_name);
        $this->roles = Sentinel::getRoleRepository()->createModel();
    }


    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = $this->roles->select('id', 'name');

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);

    }

    /**
     * Show the form for creating new user.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating user.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating user.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return $this->roles->destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {
        if ($id)
        {
            if ( ! $item = $this->roles->find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }

            //print_r($item->getPermissions()); die;

        }
        else
        {
            $item = new Role();
        }

        $actions = Action::all();

        $tabs = Tab::all();

        return view('admin.system.roles.form', [
            'mode' => $mode,
            'item' => $item,
            'actions' => $actions,
            'tabs' => $tabs,
            'config' => $this->config
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {
        $input = array_filter(Input::all());

        $rules = [
            'name'      => 'required'
        ];

        if ($id)
        {
            $item = $this->roles->find($id);

            if($item->slug !== Str::slug($input['name']))
                $input['slug'] = $this->createSlug($input['name'], new Role());

            $messages = $this->validateItem($input, $rules);

            if ($messages->isEmpty())
            {
                $item->fill($input)->save();

            }
        }
        else
        {
            $input['slug'] = $this->createSlug($input['name'], new Role());

            $messages = $this->validateItem($input, $rules);

            if ($messages->isEmpty())
            {
                $item = $this->roles->create($input);
            }
        }

        if($item)
        {

            if(Input::has('roles')){

                $roles = Input::get('roles');

                foreach($roles as &$role)
                    $role = true;

                $item->permissions = $roles;
            }


            $item->save();
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a user.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}
