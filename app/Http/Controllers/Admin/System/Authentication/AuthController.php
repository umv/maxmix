<?php

namespace App\Http\Controllers\Admin\System\Authentication;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {

        //var_dump(Sentinel::getUser());

        return view('admin.login.index');
    }

    /**
     * Handle posting of the form for logging the user in.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processLogin()
    {
        try
        {
            $input = Request::all();

            $rules = [
                'email'    => 'required|email',
                'password' => 'required',
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails())
            {
                return Redirect::back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $remember = (bool) Request::get('remember', false);

            if (Sentinel::authenticate(Request::all(), $remember))
            {

                // Contas
                $accounts = Sentinel::getUser()->accounts;

                // Sites
                $sites = array();
                foreach($accounts as $account){
                    $sites = array_merge($sites, $account->sites->toArray());

                }

                session()->put('config.sites', $sites);

                if(count($sites) > 1)
                    return Redirect::route('admin.change_site');
                else
                {
                    return Redirect::route('admin.change_site', $sites[0]['id']);
                    //return Redirect::intended('/admin');
                }

            }

            $errors = 'Invalid login or password.';
        }
        catch (NotActivatedException $e)
        {
            $errors = 'Account is not activated!';

            return Redirect::to('reactivate')->with('user', $e->getUser());
        }
        catch (ThrottlingException $e)
        {
            $delay = $e->getDelay();

            $errors = "Your account is blocked for {$delay} second(s).";
        }

        return Redirect::back()
            ->withInput()
            ->withErrors($errors);
    }
}
