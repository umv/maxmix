<?php

namespace App\Http\Controllers\Admin\System;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

use App\Models\System\Account;
use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;

/**
 * Class AccountsController
 * @package App\Http\Controllers\System
 */
class AccountsController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.system.accounts';

    /**
     * AccountsController constructor.
     */
    public function __construct()
    {
         parent::__construct($this->route_name);
    }


    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        /*$itens = Account::leftJoin('users', 'accounts.user_id', '=', 'users.id')
            ->select('accounts.id', 'accounts.name', 'accounts.number', DB::raw('CONCAT(users.first_name, " ", users.last_name) as user_name'), 'accounts.created_at');*/

        $itens = Account::select('id', 'name', 'number', 'created_at');

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addColumn(['data' => 'number', 'name' => 'number', 'title' => 'Número da conta'])
            //->addColumn(['data' => 'user_name', 'name' => 'user_name', 'title' => 'Usuário'])
            ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Criado em', 'render' => 'datatablesFormatDate(data)'])
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);

    }

    /**
     * Show the form for creating new user.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating user.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating user.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Account::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {
        if ($id)
        {
            if ( ! $item = Account::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Account();
        }

        $accounts = Account::lists('name','id');

        return view('admin.system.accounts.form', [
            'mode' => $mode,
            'item' => $item,
            'accounts' => $accounts,
            'config' => $this->config
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {
        $input = array_filter(Input::all());

        (!isset($input['active'])) ? $input['active'] = 0 : null ;

        (empty($input['account_id'])) ? $input['account_id'] = null : null ;

        $rules = [
            //'user_id'   => 'required',
            'name'      => 'required',
            'number'    => 'required|unique:system_accounts',
        ];

        if ($id)
        {
            $item = Account::find($id);

            $rules['number'] .= ",number,{$item->number},number";

            $messages = $this->validateItem($input, $rules);

            if ($messages->isEmpty())
            {
                $item->fill($input)->save();

            }
        }
        else
        {
            $messages = $this->validateItem($input, $rules);

            if ($messages->isEmpty())
            {
                $item = Account::create($input);
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a user.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}
