<?php

namespace App\Http\Controllers\Admin\System;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Models\System\Site;
use App\Models\System\Account;

use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class SitesController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.system.sites';

    /**
     * AccountsController constructor.
     */
    public function __construct()
    {
        parent::__construct($this->route_name);
    }

    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Site::join('system_accounts', 'system_sites.account_id', '=', 'system_accounts.id')->select('system_sites.id', 'system_sites.name', 'system_sites.url', 'system_accounts.number');

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addColumn(['data' => 'url', 'name' => 'url', 'title' => 'URL'])
            ->addColumn(['data' => 'number', 'name' => 'number', 'title' => 'Conta'])
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Site::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {
        if ($id)
        {
            if ( ! $item = Site::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Site;
        }

        $accounts = Account::lists('name','id');

        return view('admin.system.sites.form', [
            'mode' => $mode,
            'item' => $item,
            'accounts' => $accounts,
            'config' => $this->config
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $rules = [
            'account_id' => 'required',
            'slug'       => 'required|alpha_num|min:4|unique:system_sites',
            'name'       => 'required',
            'url'        => 'required'
        ];

        if ($id)
        {
            $item = Site::find($id);

            $rules['slug'] .= ",slug,{$item->slug},slug";

            $messages = $this->validateItem($input, $rules);

            if ($messages->isEmpty())
            {
                $item->fill($input)->save();
            }
        }
        else
        {
            $messages = $this->validateItem($input, $rules);

            if ($messages->isEmpty())
            {
                $item = Site::create($input);
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}
