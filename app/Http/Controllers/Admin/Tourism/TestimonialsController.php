<?php

namespace App\Http\Controllers\Admin\Tourism;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Models\Tourism\Destination;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Models\Tourism\Testimonial;

use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class TestimonialsController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.tourism.testimonials';

    public function __construct()
    {
        parent::__construct($this->route_name);
    }
    
    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Testimonial::select('tourism_destination_testimonials.id', 'tourism_destination_testimonials.name', 'tourism_destination_testimonials.rating', 'tourism_destination_testimonials.active', 'tourism_destination_testimonials.created_at', 'tourism_destinations.name AS destino')
            ->join('tourism_destinations', 'tourism_destination_testimonials.destination_id', '=', 'tourism_destinations.id')
            ->where('tourism_destinations.site_id', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addColumn(['data' => 'rating', 'name' => 'rating', 'title' => 'Nota'])
            ->addColumn(['data' => 'destino', 'name' => 'destino', 'title' => 'Destino'])
            ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Criado em', 'render' => 'datatablesFormatDate(data)'])
            ->addActive()
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Testimonial::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Testimonial::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Testimonial;
        }

        $destinations = Destination::where('site_id', '=', $this->site->id)->lists('name','id');

        return view('admin.tourism.testimonials_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config,
            'destinations' => $destinations
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        (!isset($input['active'])) ? $input['active'] = 0 : null ;

        $rules = [
            'name'        => 'required',
            'rating'        => 'required',
            'description' => 'required',
            'city_id' => 'required'
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            if ($id)
            {
                $item = Testimonial::find($id);
                $item->fill($input)->save();
            }
            else
            {
                $item = Testimonial::create($input);
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}
