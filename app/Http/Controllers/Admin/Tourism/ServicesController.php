<?php

namespace App\Http\Controllers\Admin\Tourism;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Models\Tourism\Destination;
use App\Models\Tourism\ServiceFilter;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Models\Tourism\Service;

use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class ServicesController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.tourism.services';

    protected $path_image;

    /**
     * AccountsController constructor.
     */
    public function __construct()
    {
        parent::__construct($this->route_name);

        $this->path_image = session()->get('config.path.image').'/tourism/services/';
    }

    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Service::select('id', 'name', DB::raw('CONCAT("'.$this->path_image.'", image) as image'), 'order')
            ->where('site_id', '=', $this->site->id)
            ->orderBy('order', 'asc');

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'order', 'name' => 'order', 'title' => 'Posição', 'width' => '5%'])
            ->addColumn(['data' => 'image', 'name' => 'image', 'title' => 'Ícone', 'width' => '100px', 'render' => 'datatablesFormatImage(data)'])
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        switch($request->input('type')){

            case 'DELETE':
                return $this->destroyItens($request);
                break;

            default:
                return $this->processForm('create');
        }
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Service::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Service::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Service;
        }

        return view('admin.tourism.services_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config,
            'path_image' => $this->path_image
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;

        $rules = [
            'name'        => 'required'
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {
            // Imagem
            if(!empty($input['image']))
            {

                $img = Image::make($input['image']);

                if($img->width() > 200)
                    $img->widen(200);

                $input['image'] = Str::slug($input['name']).'.png';

                $img->save(public_path($this->path_image.$input['image']));

            }
            else
                $input['image'] = null;

            if( !empty($input['filters'] )){
                $filters = $input['filters'];
                unset($input['filters']);
            }

            try {

                if ($id)
                {
                    $item = Service::find($id);
                    $item->fill($input)->save();
                }
                else
                {
                    $item = Service::create($input);
                }

                if($item && !empty($filters))
                {

                    //$item->filters()->delete();

                    $itens = [];

                    foreach ($filters as $filter) {

                        $rules = [
                            'name' => 'required'
                        ];

                        $messages = $this->validateItem($filter, $rules);

                        if ($messages->isEmpty()) {

                            if ($filter['id']) {
                                $service_filter = ServiceFilter::find($filter['id']);
                                $service_filter->fill($filter)->save();
                            } else {
                                $service_filter = $item->filters()->create($filter);
                            }

                            $itens[] = $service_filter->id;

                        }

                    }

                    if(!empty($itens))
                        $item->filters()->whereNotIn('id', $itens)->delete();
                }

            } catch (\Exception $e){

                return Redirect::route($this->config->getRouteEdit(), $id)->with('info', ['error' => true]);

            }

            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}
