<?php

namespace App\Http\Controllers\Admin\Tourism;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Models\Tourism\AttractionType;
use App\Models\Tourism\Destination;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Models\Tourism\Attraction;

use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class AttractionsController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.tourism.attractions';

    protected $path_image;

    /**
     * AccountsController constructor.
     */
    public function __construct()
    {
        parent::__construct($this->route_name);

        $this->path_image = session()->get('config.path.image').'/tourism/attractions/';
    }

    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Attraction::join('tourism_destinations', 'tourism_attractions.destination_id', '=', 'tourism_destinations.id')
            ->select('tourism_attractions.id', 'tourism_attractions.name', DB::raw('CONCAT("'.$this->path_image.'", tourism_attractions.image) as image'), 'tourism_attractions.active', 'tourism_destinations.name AS destination')
            ->where('tourism_attractions.site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'image', 'name' => 'image', 'title' => 'Imagem destacada', 'width' => '100px', 'render' => 'datatablesFormatImage(data)'])
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addColumn(['data' => 'destination', 'name' => 'destination', 'title' => 'Destino'])
            ->addActive()
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Attraction::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Attraction::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Attraction;
        }

        $attraction_types = AttractionType::where('site_id', '=', $this->site->id)->lists('name','id');
        $destinations = Destination::where('site_id', '=', $this->site->id)->lists('name','id');

        return view('admin.tourism.attractions_form', [
            'mode' => $mode,
            'item' => $item,
            'attraction_types' => $attraction_types,
            'destinations' => $destinations,
            'config' => $this->config,
            'path_image' => $this->path_image
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;

        (!isset($input['active'])) ? $input['active'] = 0 : null ;

        $rules = [
            'attraction_type_id' => 'required',
            'destination_id' => 'required',
            'name'        => 'required',
            'description'   => 'required',
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {
            // Imagem
            if(!empty($input['image']))
            {

                $img = Image::make($input['image']);

                if($img->width() > 1000)
                    $img->widen(1000);

                $input['image'] = Str::slug($input['name']).'.jpg';

                $img->save(public_path($this->path_image.$input['image']), 95);

            }
            else
                $input['image'] = null;

            if ($id)
            {
                $item = Attraction::find($id);
                $item->fill($input)->save();
            }
            else
            {
                $item = Attraction::create($input);
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}
