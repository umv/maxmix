<?php

namespace App\Http\Controllers\Admin\Tourism;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Models\Tourism\Category;
use App\Models\Tourism\Region;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Models\Tourism\Subregion;

use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class SubregionsController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.tourism.subregions';

    /**
     * AccountsController constructor.
     */
    public function __construct()
    {
        parent::__construct($this->route_name);
    }

    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Subregion::join('tourism_regions', 'tourism_subregions.region_id', '=', 'tourism_regions.id')
            ->select('tourism_subregions.id', 'tourism_subregions.name', 'tourism_regions.name AS region')
            ->where('tourism_subregions.site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addColumn(['data' => 'region', 'name' => 'region', 'title' => 'Região'])
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Subregion::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Subregion::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Subregion;
        }

        //$regions = Region::where('site_id', '=', $this->site->id)->lists('name','id');

        // Regions

        $categories = Category::where('site_id', '=', $this->site->id)->get();

        $regions = array();

        foreach($categories as $category)
        {

            $subs = $category->regions->lists('name', 'id');

            foreach($subs as $key => $value)
            {
                $regions[$category->name][$key] = $value;
            }

        }

        return view('admin.tourism.subregions_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config,
            'regions' => $regions
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;

        $rules = [
            'region_id' => 'required',
            'name'        => 'required',
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {
            $subregion = Subregion::where('region_id', $input['region_id']);

            $input['slug'] = $this->generateSlug($input['name'], $subregion, 'slug', $id, true);

            if ($id)
            {

                $item = Subregion::find($id);

                if($item->name == $input['name'])
                    unset($input['slug']);

                $item->fill($input)->save();

            }
            else
            {
                $item = Subregion::create($input);
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

    public function getUrl(Request $request)
    {

        if($request->input('region_id'))
        {
            $route_name = 'sites.tourism.destinations.subregion';

            $region = Region::find($request->input('region_id'));

            $subregion = Subregion::where('region_id', $request->input('region_id'));

            $slug = $this->generateSlug($request->input('name'), $subregion, 'slug', $request->input('id'), true);

            if ($request->input('id')) {

                $item = Subregion::find($request->input('id'));

                if ($item->name != $request->input('name'))
                    return route($route_name, [$region->category->slug, $region->slug, $slug]);
                else
                    return route($route_name, [$region->category->slug, $region->slug, $item->slug]);

            } else
                return route($route_name, [$region->category->slug, $region->slug, $slug]);
        }
        else
            return "--";

    }
}
