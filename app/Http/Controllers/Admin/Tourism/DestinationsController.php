<?php

namespace App\Http\Controllers\Admin\Tourism;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Integra\Integra;
use App\Models\Tourism\Attraction;
use App\Models\Tourism\Destination;
use App\Models\Tourism\DestinationPhoto;
use App\Models\Tourism\Region;
use App\Models\Tourism\Subregion;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class DestinationsController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.tourism.destinations';

    protected $image_path;

    protected $image_uri;

    /**
     * AccountsController constructor.
     */
    public function __construct()
    {
        parent::__construct($this->route_name);

        $this->image_path = Integra::public_path(DestinationPhoto::$src_image);
        $this->image_uri = Integra::public_uri(DestinationPhoto::$src_image);
    }

    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Destination::select('id', 'name', 'active')
            ->where('site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addActive()
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Destination::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Destination::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Destination;
        }

        // SubRegions

        $regions = Region::where('site_id', '=', $this->site->id)->get();

        $subregions = array();

        foreach($regions as $region)
        {

            $subs = $region->subregions->lists('name', 'id');

            foreach($subs as $key => $value)
            {
                $subregions[$region->name][$key] = $value;
            }

        }

        // Photos

        $photos = DestinationPhoto::select('slug','id')
            ->where('site_id', $this->site->id)
            ->where('destination_id', $id)
            ->get();

        return view('admin.tourism.destinations_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config,
            'subregions' => $subregions,
            'path_image' => $this->image_uri,
            'photos'    => $photos
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;

        (!isset($input['active'])) ? $input['active'] = 0 : null ;

        $rules = [
            'name'        => 'required',
            'subregion_id' => 'required'
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            $destination = Destination::where('subregion_id', $input['subregion_id']);

            $input['slug'] = $this->generateSlug($input['name'], $destination, 'slug', $id, true);

            if ($id)
            {
                $item = Destination::find($id);

                if($item->name == $input['name'] && $item->slug)
                    unset($input['slug']);

                $item->fill($input)->save();
            }
            else
            {
                $item = Destination::create($input);
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteEdit(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

    public function getPhotos($id, Request $request){

        if ($request->ajax()) {

            $itens = DestinationPhoto::select('id', 'slug', 'cover')
                ->where('site_id', $this->site->id)
                ->where('destination_id', $id)
                ->get();

            return $itens->toArray();
        }
    }

    public function uploadPhoto($id){

        if($id)
        {
            $file = Input::file('file');

            $slug = Str::slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));

            $input['slug'] = $slug.'-'.uniqid($id).'.'.$file->guessClientExtension();

            $path = $this->image_path.$input['slug'];

            $img = Image::make($file)->widen(1000)->save($path);

            if($img)
            {
                $input['site_id'] = $this->site->id;
                $input['destination_id'] = $id;

                $photo = DestinationPhoto::create($input);
            }

            // cover

            $count = DestinationPhoto::where('destination_id', $id)
                ->where('cover', '1')
                ->count();

            if($count < 1)
                $this->setCoverPhoto($photo->id);

            return Response::json('success', 200);
        }

        return Response::json('error', 400);

    }

    public function deletePhoto(Request $request)
    {

        $item = DestinationPhoto::select('tourism_destination_photos.slug')
            ->join('tourism_destinations', 'tourism_destination_photos.destination_id', '=', 'tourism_destinations.id')
            ->where('tourism_destinations.site_id', $this->site->id)
            ->where('tourism_destination_photos.id', $request->input('data'))
            ->first();

        if($item->slug)
            if(DestinationPhoto::destroy($request->input('data')))
                if(Storage::disk('public')->delete($this->image_path.$item['slug']))
                    return 1;

        return 0;

    }

    public function coverPhoto(Request $request)
    {
        return $this->setCoverPhoto($request->input('data'));
    }

    public function setCoverPhoto($id)
    {
        $destination = DestinationPhoto::select('destination_id')
            ->find($id);

        DestinationPhoto::where('destination_id', $destination->destination_id)
            ->update([
                'cover' => 0
            ]);

        DestinationPhoto::find($id)
            ->update([
                'cover' => 1
            ]);

        return 1;
    }

    public function cropPhoto(Request $request)
    {

        $data = $request->input('data');

        $name = explode('?', array_reverse(explode('/', $data['src']))[0])[0];

        $path = public_path($this->image_path.$name);

        Image::make($data['src'])->crop((int) $data['width'], (int) $data['height'], (int) $data['x'], (int) $data['y'])->save($path);

        return 1;
    }

    public function getUrl(Request $request)
    {

        if($request->input('subregion_id'))
        {
            $route_name = 'sites.tourism.destinations.destination';

            $subregion = Subregion::find($request->input('subregion_id'));

            $destination = Destination::where('subregion_id', $request->input('subregion_id'));

            $slug = $this->generateSlug($request->input('name'), $destination, 'slug', $request->input('id'), true);

            if ($request->input('id')) {

                $item = Destination::find($request->input('id'));

                if ($item->name != $request->input('name'))
                    return route($route_name, [$subregion->region->category->slug, $subregion->region->slug, $subregion->slug, $slug]);
                else
                    return route($route_name, [$subregion->region->category->slug, $subregion->region->slug, $subregion->slug, $item->slug]);

            } else
                return route($route_name, [$subregion->region->category->slug, $subregion->region->slug, $subregion->slug, $slug]);
        }
        else
            return "--";

    }
}
