<?php

namespace App\Http\Controllers\Admin\Tourism;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Models\Tourism\AttractionType;

use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class AttractionTypesController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.tourism.attraction.types';

    /**
     * AccountsController constructor.
     */
    public function __construct()
    {
        parent::__construct($this->route_name);
    }

    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = AttractionType::select('id', 'name', 'order', 'active')
            ->where('site_id', '=', $this->site->id)
            ->orderBy('order', 'asc');

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'order', 'name' => 'order', 'title' => 'Posição', 'width' => '5%'])
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addActive()
            ->addActions()
            ->setRowOrder()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        switch($request->input('type')){

            case 'DELETE':
                return $this->destroyItens($request);
                break;

            case 'REORDER':
                return $this->reorderItens($request);
                break;

            default:
                return $this->processForm('create');
        }

    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return AttractionType::destroy($request->input('data'));
    }

    /**
     * @param Request $request
     * @return int
     */
    public function reorderItens(Request $request)
    {

        foreach($request->input('data') as $order)
        {
            AttractionType::where('site_id', $this->site->id)
                ->where('id', $order['id'])
                ->update(['order' => $order['new']]);
        }

        return 1;
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = AttractionType::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new AttractionType;

        }

        $order_max = AttractionType::where('site_id', '=', $this->site->id)
            ->max('order');

        $order_max++;

        return view('admin.tourism.attraction_types_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config,
            'order_max' => $order_max
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;

        (!isset($input['active'])) ? $input['active'] = 0 : null ;

        /*$order_max = $input['order_max'];

        unset($input['order_max']);*/

        $rules = [
            'name'        => 'required',
            //'order' => 'required|numeric|min:1|max:'.$order_max
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            // Order
            /*if( $input['order'] != $order_max )
            {
                AttractionType::where('site_id', $this->site->id)
                    ->where('order', '>=', $input['order'])
                    ->increment('order');
            }*/

            if ($id)
            {
                $item = AttractionType::find($id);
                $item->fill($input)->save();
            }
            else
            {
                $item = AttractionType::create($input);
            }

        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}
