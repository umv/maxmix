<?php

namespace App\Http\Controllers\Admin\Tourism;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Models\Tourism\Category;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Models\Tourism\Region;

use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class RegionsController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.tourism.regions';

    /**
     * AccountsController constructor.
     */
    public function __construct()
    {
        parent::__construct($this->route_name);
    }

    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Region::join('tourism_categories', 'tourism_regions.category_id', '=', 'tourism_categories.id')
            ->select('tourism_regions.id', 'tourism_regions.name', 'tourism_categories.name AS category')
            ->where('tourism_regions.site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addColumn(['data' => 'category', 'name' => 'category', 'title' => 'Categoria'])
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Region::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Region::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Region;
        }

        $categories = Category::where('site_id', '=', $this->site->id)->lists('name','id');

        return view('admin.tourism.regions_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config,
            'categories' => $categories
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;

        $rules = [
            'category_id' => 'required',
            'name'        => 'required',
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            $region = Region::where('category_id', $input['category_id']);

            $input['slug'] = $this->generateSlug($input['name'], $region, 'slug', $id, true);

            if ($id)
            {

                $item = Region::find($id);

                if($item->name == $input['name'])
                    unset($input['slug']);

                $item->fill($input)->save();

            }
            else
            {
                $item = Region::create($input);
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

    public function getUrl(Request $request)
    {

        if($request->input('category_id'))
        {

            $route_name = 'sites.tourism.destinations.region';

            $category = Category::find($request->input('category_id'));

            $region = Region::where('category_id', $request->input('category_id'));

            $slug = $this->generateSlug($request->input('name'), $region, 'slug', $request->input('id'), true);

            if ($request->input('id')) {
                $item = Region::find($request->input('id'));

                if ($item->name != $request->input('name'))
                    return route($route_name, [$category->slug, $slug]);
                else
                    return route($route_name, [$category->slug, $item->slug]);

            } else
                return route($route_name, [$category->slug, $slug]);

        }
        else
            return "--";

    }
}
