<?php

namespace App\Http\Controllers\Admin\Tourism;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Integra\Integra;
use App\Models\Tourism\DestinationPhoto;
use App\Models\Tourism\Note;
use App\Models\Tourism\Origin;
use App\Models\Tourism\Package;
use App\Models\Tourism\PackageDate;
use App\Models\Tourism\PackagePhoto;
use App\Models\Tourism\Payment;
use App\Models\Tourism\Service;
use App\Models\Tourism\Subregion;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class PackagesController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.tourism.packages';

    protected $image_path;

    protected $image_uri;

    /**
     * AccountsController constructor.
     */
    public function __construct()
    {
        parent::__construct($this->route_name);

        $this->image_path = Integra::public_path(PackagePhoto::$src_image);
        $this->image_uri = Integra::public_uri(PackagePhoto::$src_image);
        $this->image_uri_destination = Integra::public_uri(DestinationPhoto::$src_image);
    }

    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Package::select('id', 'name')
            ->where('site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Package::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Package::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Package;
        }

        // Destinos

        $subregions = Subregion::where('site_id', '=', $this->site->id)->get();

        $destinations = array();

        foreach($subregions as $subregion)
        {

            $subs = $subregion->destinations->lists('name', 'id');

            foreach($subs as $key => $value)
            {
                $destinations[$subregion->name][$key] = $value;
            }

        }

        $services = Service::where('site_id', $this->site->id)
            ->orderBy('order')
            ->get();

        $notes = Note::where('site_id', $this->site->id)
            ->orderBy('name')
            ->get();

        $payments = Payment::where('site_id', $this->site->id)
            ->orderBy('id')
            ->get();

        $origins = Origin::lists('name', 'id');

        // Photos

        $photos = PackagePhoto::select('slug','id')
            ->where('package_id', $id)
            ->get();

        return view('admin.tourism.packages_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config,
            'destinations' => $destinations,
            'services'  => $services,
            'notes'  => $notes,
            'payments' => $payments,
            'origins' => $origins,
            'path_image' => $this->image_uri,
            'path_image_destination' => $this->image_uri_destination,
            'photos'    => $photos
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;

        (!isset($input['active'])) ? $input['active'] = 0 : null ;
        (!isset($input['url_video'])) ? $input['url_video'] = null : null ;
        (!isset($input['description'])) ? $input['description'] = null : null ;

        //$input['date_start'] = Carbon::createFromFormat('d/m/Y', $input['date_start']);

        if(isset($input['date']))
        {
            $dates = $input['date'];
            unset($input['date']);
        }

        $rules = [
            'name'        => 'required',
            'destination_id' => 'required',
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            $rules = [
                'city_id' => 'required'
            ];

            if(isset($dates))
            {

                foreach($dates as $date)
                {
                    $messages = $this->validateItem($date, $rules);

                    if (!$messages->isEmpty())
                        return Redirect::back()->withInput()->withErrors($messages);
                };

            }

            $input['slug'] = $this->generateSlug($input['name'], new Package(), 'slug', $id, true);

            //unset($input['services']);
            unset($input['notes']);
            unset($input['filters']);

            if ($id)
            {
                $item = Package::find($id);

                if($item->name == $input['name'])
                    unset($input['slug']);

                $item->fill($input)->save();

            }
            else
            {
                $item = Package::create($input);
            }

            if($item)
            {
                (Input::get('notes')) ? $item->notes()->sync(Input::get('notes')) : $item->notes()->detach();

                $services = $filters = [];

                if(Input::get('filters'))
                {
                    foreach(Input::get('filters') as $service => $filter)
                    {
                        $services[] = $service;
                        $filters = array_merge($filters, $filter);
                    }
                }

                (!empty($services)) ? $item->services()->sync($services) : $item->services()->detach();
                (!empty($filters)) ? $item->filters()->sync($filters) : $item->filters()->detach();

                // Datas

                if(isset($dates))
                {
                    foreach ($dates as $date) {
                        $date['package_id'] = $item->id;
                        $date['date_departure'] = Carbon::createFromFormat('d/m/Y', $date['date_departure']);
                        $date['date_arrival'] = Carbon::createFromFormat('d/m/Y', $date['date_arrival']);
                        $date['price_default'] = str_replace(['.', ','], ['', '.'], $date['price_default']);
                        $date['price_promotional'] = str_replace(['.', ','], ['', '.'], $date['price_promotional']);
                        $date['disabled_at'] = Carbon::createFromFormat('d/m/Y', $date['disabled_at']);
                        (!isset($date['active'])) ? $date['active'] = 0 : null;

                        $payments = $date['payments'];
                        unset($date['payments']);

                        if ($date['id']) {
                            $package_date = $item->dates()->find($date['id']);
                            $package_date->fill($date)->save();
                        } else {
                            $package_date = $item->dates()->create($date);
                        }

                        if ($package_date) {

                            $package_date->payments()->detach();

                            foreach ($payments as $payment) {

                                (isset($payment['input_value'])) ? $payment['input_value'] = str_replace([',', '%'], ['.', ''], $payment['input_value']) : $payment['input_value'] = null;
                                (isset($payment['discount_value'])) ? $payment['discount_value'] = str_replace([',', '%'], ['.', ''], $payment['discount_value']) : $payment['discount_value'] = null;

                                (!isset($payment['min_parcel'])) ? $payment['min_parcel'] = null : null;
                                (!isset($payment['max_parcel'])) ? $payment['max_parcel'] = null : null;
                                (!isset($payment['active'])) ? $payment['active'] = 0 : null;

                                $package_date->payments()->attach($payment['id'], [
                                    'input_value' => $payment['input_value'],
                                    'discount_value' => $payment['discount_value'],
                                    'min_parcel' => $payment['min_parcel'],
                                    'max_parcel' => $payment['max_parcel'],
                                    'active' => $payment['active']
                                ]);
                            }

                        }

                    }

                }
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

    public function getPhotos($id, Request $request){

        if ($request->ajax()) {

            $itens = PackagePhoto::select('id', 'slug', 'cover')
                ->where('package_id', $id)
                ->get();

            return $itens->toArray();
        }
    }

    public function uploadPhoto($id){

        if($id)
        {
            $file = Input::file('file');

            $slug = Str::slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));

            $input['slug'] = $slug.'-'.uniqid($id).'.'.$file->guessClientExtension();

            $path = $this->image_path.$input['slug'];

            $img = Image::make($file)->widen(1000)->save($path);

            if($img)
            {
                $input['package_id'] = $id;

                $photo = PackagePhoto::create($input);
            }

            // cover

            $count = PackagePhoto::where('package_id', $id)
                ->where('cover', '1')
                ->count();

            if($count < 1)
                $this->setCoverPhoto($photo->id);

            return Response::json('success', 200);
        }

        return Response::json('error', 400);

    }

    public function deletePhoto(Request $request)
    {

        $item = PackagePhoto::select('tourism_package_photos.slug')
            ->join('tourism_packages', 'tourism_package_photos.package_id', '=', 'tourism_packages.id')
            ->where('tourism_package_photos.id', $request->input('data'))
            ->first();

        if($item->slug)
            if(PackagePhoto::destroy($request->input('data')))
                if(Storage::disk('public')->delete($this->image_path.$item['slug']))
                    return 1;

        return 0;

    }

    public function coverPhoto(Request $request)
    {
        return $this->setCoverPhoto($request->input('data'));
    }

    public function setCoverPhoto($id)
    {
        $package = PackagePhoto::select('package_id')
            ->find($id);

        PackagePhoto::where('package_id', $package->package_id)
            ->update([
                'cover' => 0
            ]);

        PackagePhoto::find($id)
            ->update([
                'cover' => 1
            ]);

        return 1;
    }

    public function cropPhoto(Request $request)
    {

        $data = $request->input('data');

        $name = explode('?', array_reverse(explode('/', $data['src']))[0])[0];

        $path = public_path($this->image_path.$name);

        Image::make($data['src'])->crop((int) $data['width'], (int) $data['height'], (int) $data['x'], (int) $data['y'])->save($path);

        return 1;
    }

    public function getUrl(Request $request)
    {
        $route_name = 'sites.tourism.packages.package';

        $slug = $this->generateSlug($request->input('name'), new Package(), 'slug', $request->input('id'), true);

        if($request->input('id'))
        {
            $item = Package::find($request->input('id'));

            if($item->name != $request->input('name'))
                return route($route_name, $slug);
            else
                return route($route_name, $item->slug);

        }
        else
            return route($route_name, $slug);


    }
}
