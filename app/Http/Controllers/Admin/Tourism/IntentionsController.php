<?php

namespace App\Http\Controllers\Admin\Tourism;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Models\Tourism\Intention;

use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class IntentionsController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.tourism.intentions';

    /**
     * AccountsController constructor.
     */
    public function __construct()
    {
        parent::__construct($this->route_name);
    }

    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Intention::select('A.id', 'A.name', 'A.email', 'C.name AS destination', 'D.name AS package', 'A.active', 'A.created_at')
            ->from('tourism_intentions AS A')
            ->leftJoin('tourism_package_dates AS B', 'B.id', '=', 'A.date_id')
            ->leftJoin('tourism_destinations AS C', 'C.id', '=', 'A.destination_id')
            ->leftJoin('tourism_packages AS D', 'D.id', '=', 'B.package_id')
            ->where('A.site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addColumn(['data' => 'email', 'name' => 'email', 'title' => 'E-mail'])
            ->addColumn(['data' => 'destination', 'name' => 'destination', 'title' => 'Destino'])
            ->addColumn(['data' => 'package', 'name' => 'package', 'title' => 'Pacote'])
            ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Enviada em', 'render' => 'datatablesFormatDate(data)', 'width' => '10%'])
            ->addActive()
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        switch($request->input('type')){

            case 'DELETE':
                return $this->destroyItens($request);
                break;

            default:
                return $this->processForm('create');
        }

    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Intention::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Intention::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Intention;

        }

        return view('admin.tourism.intentions_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        (!isset($input['active'])) ? $input['active'] = 0 : null ;

        $input['date_departure'] = (!empty($input['date_departure']) ? Carbon::createFromFormat('d/m/Y', $input['date_departure']) : null);
        $input['date_arrival'] = (!empty($input['date_arrival']) ? Carbon::createFromFormat('d/m/Y', $input['date_arrival']) : null);

        $rules = [
            'email'        => 'required',
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            if ($id)
            {
                $item = Intention::find($id);
                $item->fill($input)->save();
            }
            else
            {
                $item = Intention::create($input);
            }

        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}
