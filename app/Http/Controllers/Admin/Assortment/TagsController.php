<?php

namespace App\Http\Controllers\Admin\Assortment;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Models\Assortment\Client;
use App\Models\Assortment\Product;
use App\Models\Assortment\Tag;
use App\Models\General\State;
use App\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Models\Planogram\Area;


use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class TagsController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.assortment.tags';

    public function __construct()
    {
        parent::__construct($this->route_name);
    }
    
    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Tag::select('assortment_tags.id', 'assortment_tags.name', 'assortment_clients.name AS client_name')
            ->join('assortment_clients', 'assortment_clients.id', '=', 'assortment_tags.client_id')
            ->where('assortment_tags.site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addColumn(['data' => 'client_name', 'name' => 'assortment_clients.name', 'title' => 'Cliente'])
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Tag::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Tag::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Tag;
        }

        $clients = Client::where('site_id', $this->site->id)
            ->orderBy('name')
            ->lists('name', 'id');

        $dates = Product::lists('date', 'date');

        foreach ($dates as $key => $value) {
            $dates[$key] = date('M/Y', strtotime($value));
        }

        return view('admin.assortment.tags_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config,
            'clients' => $clients,
            'dates' => $dates
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;

        unset($input['dates']);

        $rules = [
            'client_id' => 'required',
            'name'  => 'required',
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            if ($id)
            {
                $item = Tag::find($id);
                $item->fill($input)->save();
            }
            else
            {
                $item = Tag::create($input);
            }

            if($item)
                (Input::get('dates')) ? $item->dates()->sync(Input::get('dates')) : $item->dates()->detach();
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    public function getTagsByClient(Request $request){

        if($request->has('client_id'))
            $client_id = $request->input('client_id');
        else {
            return response()->json([
                'success' => 0
            ], 401);
        }

        if($client_id){

            $dates = Product::where('client_id', $client_id)
                ->lists('date', 'date');

            foreach ($dates as $key => $value) {
                $dates[$key] = IntegraMonthToString(date('m', strtotime($value))) . date('/Y', strtotime($value));
            }

            return response()->json([
                'success' => 1,
                'data' => $dates
            ]);

        }

    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}
