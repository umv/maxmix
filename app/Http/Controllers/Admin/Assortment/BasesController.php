<?php

namespace App\Http\Controllers\Admin\Assortment;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Models\Assortment\Category;
use App\Models\Assortment\Client;
use App\Models\Assortment\Subcategory;
use App\Models\Planogram\Item;
use App\Models\Product\Brand;
use App\Models\Product\Product;
use App\Models\Assortment\Product as AssortmentProduct;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class BasesController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.assortment.bases';

    public function __construct()
    {
        parent::__construct($this->route_name);
    }
    
    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = AssortmentProduct::select('assortment_products.id', 'assortment_products.amount', 'assortment_products.date', 'product_products.ean', 'product_products.name AS product_name', 'assortment_clients.name AS client_name', 'assortment_subcategories.name AS subcategory_name', 'assortment_categories.name AS category_name')
            ->join('product_products', 'product_products.id', '=', 'assortment_products.product_id')
            ->join('assortment_clients', 'assortment_clients.id', '=', 'assortment_products.client_id')
            ->join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
            ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
            ->where('assortment_categories.site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'ean', 'name' => 'ean', 'title' => 'EAN', 'width' => '10%'])
            ->addColumn(['data' => 'product_name', 'name' => 'product_products.name', 'title' => 'Item', 'width' => '30%'])
            ->addColumn(['data' => 'subcategory_name', 'name' => 'assortment_subcategories.name', 'title' => 'Subcategoria'])
            ->addColumn(['data' => 'category_name', 'name' => 'assortment_categories.name', 'title' => 'Categoria'])
            ->addColumn(['data' => 'client_name', 'name' => 'assortment_clients.name', 'title' => 'Cliente'])
            ->addColumn(['data' => 'amount', 'name' => 'amount', 'title' => 'Quantidade', 'width' => '5%', 'align' => 'center'])
            ->addColumn(['data' => 'date', 'name' => 'date', 'title' => 'Data', 'width' => '5%', 'render' => 'datatablesFormatDateToMonthYear(data)'])
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        if($request->input('type') === 'IMPORT')
            return $this->importFile($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Item::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Item::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Item;
        }

        return view('admin.planogram.items_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config,
            'products' => [],
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $rules = [
            'position'        => 'required|min:1',
            'area_id' => 'required',
            'product_id' => 'required',
            'subcategory_id' => 'required'
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            if ($id)
            {
                $item = Item::find($id);
                $item->fill($input)->save();
            }
            else
            {
                $item = Item::create($input);
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

    public function import()
    {

        $clients = Client::orderBy('name')
            ->lists('name', 'id');

        return view('admin.assortment.bases_import_form', [
            'mode' => 'create',
            'item' => new Item(),
            'clients' => $clients,
            'config' => $this->config,
        ]);

    }

    public function importFile(Request $request)
    {

        $input = array_filter(Input::all());

        $rules = [
            'client_id'        => 'required',
            'file'  => 'required|mimes:csv,xls,xlsx'
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            $client = Client::find($input['client_id']);

            $file = $request->file('file');

            $destination_path = public_path('sites/maxmix/media/sheets/assortment');
            $file_name = str_slug($client->code.date('_YmdHis_').uniqid().$client->name, '_').'.'.$file->getClientOriginalExtension();

            if ($file->isValid()) {
                $file->move($destination_path, $file_name);
            } else {
                return Redirect::back()->with('toast', ['msg' => base64_encode('Ops! A planilha enviada é inválida.')]);
            }

            $file_path = $destination_path . '/' . $file_name;

            Excel::load($file_path, function ($results) use ($client) {

                $first = $results->get()->first();

                if (
                    $first->has('ean') &&
                    $first->has('descricao') &&
                    $first->has('marca') &&
                    $first->has('quantidade') &&
                    $first->has('custo') &&
                    $first->has('preco') &&
                    $first->has('categoria') &&
                    $first->has('perecivel') &&
                    $first->has('subcategoria') &&
                    $first->has('mes') &&
                    $first->has('ano')
                ) {

                    // verificando e inserindo

                    $results->each(function ($row) use ($client) {

                        if($row->preco > 0) {

                            // formatacao

                            $month = IntegraStringToMonth($row->mes);

                            $date = date('Y-m-d', strtotime("{$row->ano}-{$month}-01"));

                            // calculos

                            $unit_gross_margin = ($row->preco - $row->custo);
                            $percentage_gross_margin = ($unit_gross_margin / $row->preco);
                            $total_cost = ($row->quantidade * $row->custo);
                            $total_revenue = ($row->quantidade * $row->preco);
                            $total_gross_margin = ($total_revenue - $total_cost);

                            $data = [
                                'amount' => $row->quantidade,
                                'unit_price' => $row->preco,
                                'unit_cost' => $row->custo,
                                'unit_gross_margin' => ($row->preco - $row->custo),
                                'percentage_gross_margin' => $percentage_gross_margin,
                                'total_cost' => $total_cost,
                                'total_revenue' => $total_revenue,
                                'total_gross_margin' => $total_gross_margin
                            ];

                            // busca do item

                            $item = AssortmentProduct::join('product_products', 'product_products.id', '=', 'assortment_products.product_id')
                                ->where('product_products.ean', $row->ean)
                                ->where('assortment_products.client_id', $client->id);

                            if ($item->where('assortment_products.date', $date)
                                //->where('assortment_products.client_id', $client->id)
                                ->first()
                            ) {

                                // apenas atualiza o item

                                $item = $item->select('assortment_products.id', 'product_products.id AS product_id')
                                    ->where('assortment_products.date', $date)
                                    //->where('assortment_products.client_id', $client->id)
                                    ->first();

                                Product::find($item->product_id)
                                    ->update([
                                        'name' => $row->descricao
                                    ]);

                                AssortmentProduct::find($item->id)
                                    ->update($data);

                            } else {

                                if ($item->first()) {

                                    // usa dados ja existentes

                                    $item = $item->first();

                                    AssortmentProduct::create(array_merge([
                                        'subcategory_id' => $item->subcategory_id,
                                        'product_id' => $item->product_id,
                                        'client_id' => $client->id,
                                        'date' => $date
                                    ], $data));

                                } else {

                                    // cria dados relacionados

                                    $product = Product::select('product_products.*')
                                        ->join('assortment_products', 'product_products.id', '=', 'assortment_products.product_id')
                                        ->where('product_products.site_id', $this->site->id)
                                        ->where('product_products.ean', $row->ean)
                                        ->where('assortment_products.client_id', $client->id)
                                        ->first();

                                    $brand = Brand::where('name', 'like', $row->marca)
                                        ->first();

                                    // inserindo marca

                                    if (!$brand) {
                                        $brand = Brand::create([
                                            'site_id' => $this->site->id,
                                            'name' => $row->marca
                                        ]);
                                    }

                                    // inserindo produto

                                    if (!$product) {

                                        $product = Product::create([
                                            'site_id' => $this->site->id,
                                            'brand_id' => $brand->id,
                                            'ean' => $row->ean,
                                            'name' => $row->descricao,
                                        ]);

                                    } else {

                                        Product::find($product->id)
                                            ->update([
                                                'name' => $row->descricao,
                                                'brand_id' => $brand->id
                                            ]);

                                    }

                                    $category = Category::where('client_id', $client->id)
                                        ->where('name', 'like', $row->categoria)
                                        ->first();

                                    // inserindo categoria

                                    if (!$category) {

                                        $category = Category::create([
                                            'site_id' => $this->site->id,
                                            'client_id' => $client->id,
                                            'name' => $row->categoria,
                                            'perishable' => (in_array($row->perecivel, ['S', 's', 'SIM', 'sim'])) ? 1 : 0
                                        ]);

                                    }

                                    $subcategory = Subcategory::where('category_id', $category->id)
                                        ->where('name', 'like', $row->subcategoria)
                                        ->first();

                                    // inserindo subcategoria

                                    if (!$subcategory) {

                                        $subcategory = Subcategory::create([
                                            'category_id' => $category->id,
                                            'name' => $row->subcategoria,
                                        ]);

                                    }

                                    // cria item

                                    AssortmentProduct::create(array_merge([
                                        'subcategory_id' => $subcategory->id,
                                        'product_id' => $product->id,
                                        'client_id' => $client->id,
                                        'date' => $date
                                    ], $data));

                                }

                            }

                        }

                    });


                } else {

                    return Redirect::back()->with('toast', ['msg' => base64_encode('Ops! A planilha enviada não contem as colunas esperadas.')]);
                }

            });

        }

        if ($messages->isEmpty())
        {
            return Redirect::back()->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);

    }
}
