<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Models\General\State;
use App\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Models\Planogram\Area;


use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class UsersController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.auth.users';

    public function __construct()
    {
        parent::__construct($this->route_name);
    }
    
    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = User::select('id', 'name', 'email')
            ->where('site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addColumn(['data' => 'email', 'name' => 'email', 'title' => 'E-mail'])
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return User::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = User::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new User;
        }

        return view('admin.auth.users_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;

        (!isset($input['verified'])) ? $input['verified'] = 0 : null ;

        if($mode == 'create') {

            $rules = [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:auth_users',
                'password' => 'required|confirmed|min:6',
            ];

        } else {

            $rules = [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255'
            ];

            if(!empty($input['password']))
                $rules['password'] = 'required|confirmed|min:6';

        }

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            unset($input['password_confirmation']);

            if(!empty($input['password']))
                $input['password'] = bcrypt($input['password']);

            if ($id)
            {
                $item = User::find($id);
                $item->fill($input)->save();
            }
            else
            {
                $item = User::create($input);
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}
