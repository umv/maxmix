<?php

namespace App\Http\Controllers\Admin\Planogram;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Integra\Integra;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Models\Planogram\Category;


use Intervention\Image\Facades\Image;
use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class CategoriesController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.planogram.categories';

    public function __construct()
    {
        parent::__construct($this->route_name);
    }
    
    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Category::select('id', 'name', 'active')
            ->where('site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addActive()
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Category::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Category::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Category;
        }

        return view('admin.planogram.categories_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;

        $rules = [
            'name'        => 'required',
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            if ($id)
            {
                $item = Category::find($id);
                $item->fill($input)->save();
            }
            else
            {
                $item = Category::create($input);
            }

            if($item)
            {

                // Imagem
                if(!empty($input['tip']))
                {

                    $img = Image::make($input['tip']);

                    if($img->width() > 1000)
                        $img->widen(1000);

                    $input['tip'] = str_slug('dicas-'.$item->name).'.jpg';

                    $img->save(public_path(Integra::public_path(Config::get('integra.planogram.tip.path')).$input['tip']));

                }
                else
                    $input['tip'] = null;

                $item->fill(['tip' => $input['tip']])->save();

            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}
