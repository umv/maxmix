<?php

namespace App\Http\Controllers\Admin\Planogram;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Integra\Integra;
use App\Models\General\City;
use App\Models\General\State;
use App\Models\Planogram\Area;
use App\Models\Planogram\Category;
use App\Models\Planogram\Item;
use App\Models\Planogram\Planogram;
use App\Models\Planogram\Subcategory;
use App\Models\Product\Product;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Planogram\Brand;


use Intervention\Image\Facades\Image;
use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class PlanogramsController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.planogram.planograms';

    public function __construct()
    {
        parent::__construct($this->route_name);
    }
    
    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Planogram::select('planogram_planograms.id', 'planogram_planograms.code', 'planogram_planograms.amount_modules', 'planogram_planograms.created_at', 'auth_users.name AS user_name', 'general_states.abbreviation AS state_abbreviation', 'planogram_subcategories.name AS subcategory_name', 'planogram_categories.name AS category_name')
            ->join('auth_users', 'auth_users.id', '=', 'planogram_planograms.user_id')
            ->join('general_states', 'general_states.id', '=', 'planogram_planograms.state_id')
            ->leftJoin('planogram_subcategories', 'planogram_subcategories.id', '=', 'planogram_planograms.subcategory_id')
            ->leftJoin('planogram_categories', 'planogram_categories.id', '=', 'planogram_planograms.category_id')
            ->where('planogram_planograms.site_id', '=', $this->site->id)
            ->orderBy('planogram_planograms.created_at', 'DESC');

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'code', 'name' => 'code', 'title' => 'Código', 'width' => '10%'])
            ->addColumn(['data' => 'user_name', 'name' => 'user_name', 'title' => 'Usuário', 'width' => '15%'])
            ->addColumn(['data' => 'state_abbreviation', 'name' => 'state_abbreviation', 'title' => 'Estado', 'width' => '5%'])
            ->addColumn(['data' => 'subcategory_name', 'name' => 'subcategory_name', 'title' => 'Subcategoria'])
            ->addColumn(['data' => 'category_name', 'name' => 'category_name', 'title' => 'Categoria'])
            ->addColumn(['data' => 'amount_modules', 'name' => 'amount_modules', 'title' => 'Módulos', 'width' => '5%'])
            ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Gerado em', 'render' => 'datatablesFormatDate(data)', 'width' => '10%'])
            ->addActions('read-delete')
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Planogram::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Planogram::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Planogram;
        }

        return view('admin.planogram.planograms_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config,
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $rules = [
            'state_id' => 'required'
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            if ($id)
            {
                $item = Planogram::find($id);
                $item->fill($input)->save();
            }
            else
            {
                $item = Planogram::create($input);
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}
