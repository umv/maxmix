<?php

namespace App\Http\Controllers\Admin\Planogram;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Integra\Integra;
use App\Models\Planogram\Area;
use App\Models\Planogram\Category;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Models\Planogram\Subcategory;

use Intervention\Image\Facades\Image;
use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class SubcategoriesController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.planogram.subcategories';

    public function __construct()
    {
        parent::__construct($this->route_name);
    }
    
    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Subcategory::select('planogram_subcategories.id', 'planogram_subcategories.name', 'planogram_subcategories.active', 'planogram_categories.name AS category_name')
            ->join('planogram_categories', 'planogram_subcategories.category_id', '=', 'planogram_categories.id')
            ->where('planogram_categories.site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addColumn(['data' => 'category_name', 'name' => 'planogram_categories.name', 'title' => 'Categoria'])
            ->addActive()
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Subcategory::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Subcategory::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Subcategory;
        }

        $categories = Category::where('site_id', $this->site->id)
            ->orderBy('name')
            ->lists('name', 'id');

        return view('admin.planogram.subcategories_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config,
            'categories' => $categories
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $rules = [
            'name'        => 'required',
            'category_id' => 'required'
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            if ($id)
            {
                $item = Subcategory::find($id);
                $item->fill($input)->save();
            }
            else
            {
                $item = Subcategory::create($input);
            }

            if($item)
            {

                // Imagem
                if(!empty($input['tip']))
                {

                    $img = Image::make($input['tip']);

                    if($img->width() > 1000)
                        $img->widen(1000);

                    $input['tip'] = str_slug('dicas-'.$item->category->name.' '.$item->name).'.jpg';

                    $img->save(public_path(Integra::public_path(Config::get('integra.planogram.tip.path')).$input['tip']));

                }
                else
                    $input['tip'] = null;

                $item->fill(['tip' => $input['tip']])->save();

            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

    public function import()
    {

        $areas = Area::lists('name', 'id');

        return view('admin.planogram.subcategories_import_form', [
            'mode' => 'create',
            'item' => new Subcategory(),
            'config' => $this->config,
            'areas' => $areas
        ]);

    }

    public function importFile(Request $request)
    {

        if ($request->file('file')->isValid()) {

            $file = $request->file('file');

            $csv = str_getcsv(utf8_encode(file_get_contents($file)), "\n");

            foreach($csv as &$map)
                $map = str_getcsv($map, ";");

            foreach ($csv[0] as &$data)
                $data = strtolower(trim($data));

            $col = array_flip($csv[0]);

            array_shift($csv);

            foreach($csv as $data)
            {

                $category = Category::where('site_id', $this->site->id)
                    ->where('name', $data[$col['categoria']])
                    ->first();

                if(!$category)
                {
                    $category = Category::create([
                        'site_id' => $this->site->id,
                        'name'  => $data[$col['categoria']]
                    ]);
                }

                if($category)
                {

                    $subcategory = Subcategory::where('category_id', $category->id)
                        ->where('name', $data[$col['subcategoria']])
                        ->first();

                    if(!$subcategory)
                    {
                        $subcategory = Subcategory::create([
                            'category_id' => $category->id,
                            'name'  => $data[$col['subcategoria']]
                        ]);

                    }

                    if($subcategory)
                    {
                        $brand = Brand::where('subcategory_id', $subcategory->id)
                            ->where('name', $data[$col['marca']])
                            ->first();

                        if(!$brand)
                        {
                            $brand = Brand::create([
                                'subcategory_id' => $subcategory->id,
                                'name'  => $data[$col['marca']],
                                'image'  => Str::slug($data[$col['categoria']].'_'.$data[$col['subcategoria']].'_'.$data[$col['marca']],"_").'.png'
                            ]);

                        }

                        if($brand)
                        {

                            $brand->areas()->detach($request->input('area_id'));

                            $brand->areas()->attach($request->input('area_id'), [
                                'share' => (int) $data[$col['share']],
                                'position' => $data[$col['posicao']]
                            ]);

                        }
                    }

                }

            }

            return Redirect::route($this->config->getRouteIndex())->with('info', ['save' => true]);

        }

        return Redirect::route($this->config->getRouteIndex())->with('info', ['save' => false]);

    }
}
