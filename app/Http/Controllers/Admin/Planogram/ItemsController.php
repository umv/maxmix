<?php

namespace App\Http\Controllers\Admin\Planogram;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Integra\Integra;
use App\Models\General\City;
use App\Models\General\State;
use App\Models\Planogram\Area;
use App\Models\Planogram\Category;
use App\Models\Planogram\Item;
use App\Models\Planogram\Subcategory;
use App\Models\Product\Product;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Planogram\Brand;


use Intervention\Image\Facades\Image;
use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class ItemsController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.planogram.items';

    public function __construct()
    {
        parent::__construct($this->route_name);
    }
    
    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Item::select('planogram_items.id', 'product_products.ean', 'planogram_items.position', 'product_products.name AS product_name', 'planogram_areas.name AS area_name', 'planogram_subcategories.name AS subcategory_name', 'planogram_categories.name AS category_name')
            ->join('product_products', 'product_products.id', '=', 'planogram_items.product_id')
            ->join('planogram_areas', 'planogram_areas.id', '=', 'planogram_items.area_id')
            ->join('planogram_subcategories', 'planogram_subcategories.id', '=', 'planogram_items.subcategory_id')
            ->join('planogram_categories', 'planogram_categories.id', '=', 'planogram_subcategories.category_id')
            ->where('planogram_categories.site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'ean', 'name' => 'ean', 'title' => 'EAN', 'width' => '10%'])
            ->addColumn(['data' => 'product_name', 'name' => 'product_name', 'title' => 'Item', 'width' => '30%'])
            ->addColumn(['data' => 'area_name', 'name' => 'area_name', 'title' => 'Área', 'width' => '5%'])
            ->addColumn(['data' => 'subcategory_name', 'name' => 'subcategory_name', 'title' => 'Subcategoria'])
            ->addColumn(['data' => 'category_name', 'name' => 'category_name', 'title' => 'Categoria'])
            ->addColumn(['data' => 'position', 'name' => 'position', 'title' => 'Posição', 'width' => '5%'])
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        if($request->input('type') === 'IMPORT')
            return $this->importFile($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Item::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Item::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Item;
        }

        $areas = Area::lists('name', 'id');

        $categories = Category::where('site_id', $this->site->id)->get();

        $subcategories = array();

        foreach($categories as $category)
        {

            $cats = $category->subcategories->lists('name', 'id');

            foreach($cats as $key => $value)
            {
                $subcategories[$category->name][$key] = $value;
            }

        }

        return view('admin.planogram.items_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config,
            'areas' => $areas,
            'products' => [],
            'subcategories' => $subcategories
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $rules = [
            'position'        => 'required|min:1',
            'area_id' => 'required',
            'product_id' => 'required',
            'subcategory_id' => 'required'
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            if ($id)
            {
                $item = Item::find($id);
                $item->fill($input)->save();
            }
            else
            {
                $item = Item::create($input);
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

    public function import()
    {

        $areas = Area::where('site_id', $this->site->id)
            ->orderBy('name')
            ->lists('name', 'id');

        return view('admin.planogram.items_import_form', [
            'mode' => 'create',
            'item' => new Item(),
            'config' => $this->config,
            'areas' => $areas
        ]);

    }

    public function importFile(Request $request)
    {

        if ($request->file('file')->isValid()) {

            $file = $request->file('file');

            $csv = str_getcsv(utf8_encode(file_get_contents($file)), "\n");

            foreach($csv as &$map)
                $map = str_getcsv($map, ";");

            foreach ($csv[0] as &$data)
                $data = strtolower(trim($data));

            $col = array_flip($csv[0]);

            array_shift($csv);

            foreach($csv as $data)
            {

                $category = Category::where('site_id', $this->site->id)
                    ->where('name', $data[$col['categoria']])
                    ->first();

                if(!$category)
                {
                    $category = Category::create([
                        'site_id' => $this->site->id,
                        'name'  => $data[$col['categoria']]
                    ]);
                }

                if($category)
                {

                    $subcategory = Subcategory::where('category_id', $category->id)
                        ->where('name', $data[$col['subcategoria']])
                        ->first();

                    if(!$subcategory)
                    {
                        $subcategory = Subcategory::create([
                            'category_id' => $category->id,
                            'name'  => $data[$col['subcategoria']]
                        ]);

                    }

                    if($subcategory)
                    {

                        // Marcas de produtos

                        $product_brand = \App\Models\Product\Brand::where('site_id', $this->site->id)
                            ->where('name', $data[$col['marca']])
                            ->first();

                        if(!$product_brand)
                        {
                            $product_brand = \App\Models\Product\Brand::create([
                                'site_id' => $this->site->id,
                                'name'  => $data[$col['marca']]
                            ]);

                        }

                        if($product_brand)
                        {

                            // Marcas do planograma

                            $brand = Brand::where('subcategory_id', $subcategory->id)
                                ->where('brand_id', $product_brand->id)
                                ->first();

                            if(!$brand)
                            {
                                $brand = Brand::create([
                                    'subcategory_id' => $subcategory->id,
                                    'brand_id'  => $product_brand->id
                                ]);

                            }

                            // Produtos do sitema

                            $product = Product::where('site_id', $this->site->id)
                                ->where('ean', IntegraRectifyNumber( $data[$col['ean']], 13 ))
                                ->first();

                            if(!$product)
                            {

                                $product = Product::create([
                                    'site_id' => $this->site->id,
                                    'brand_id' => $product_brand->id,
                                    'ean' => IntegraRectifyNumber( $data[$col['ean']], 13 ),
                                    'name'  => $data[$col['descricao']]
                                ]);

                            } else {

                                $product->update(['brand_id' => $product_brand->id]);

                            }

                            if($product)
                            {

                                $item = Item::where('area_id', $request->input('area_id'))
                                    ->where('subcategory_id', $subcategory->id)
                                    ->where('product_id', $product->id)
                                    ->first();

                                if(!$item)
                                {

                                    $item = Item::create([
                                        'area_id' => $request->input('area_id'),
                                        'subcategory_id' => $subcategory->id,
                                        'product_id' => $product->id,
                                        'position' => $data[$col['posicao']]
                                    ]);

                                } else {

                                    if($item->position != $data[$col['posicao']])
                                    {

                                        $item->update(['position' => $data[$col['posicao']]]);

                                    }

                                }

                            }

                        }
                    }

                }

            }

            return Redirect::route($this->config->getRouteIndex())->with('info', ['save' => true]);

        }

        return Redirect::route($this->config->getRouteIndex())->with('info', ['save' => false]);

    }
}
