<?php

namespace App\Http\Controllers\Admin\Planogram;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Integra\Integra;
use App\Models\General\City;
use App\Models\General\State;
use App\Models\Planogram\Area;
use App\Models\Planogram\Category;
use App\Models\Planogram\Subcategory;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Planogram\Brand;


use Intervention\Image\Facades\Image;
use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class BrandsController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.planogram.brands';

    public function __construct()
    {
        parent::__construct($this->route_name);
    }
    
    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Brand::select('planogram_brands.id', 'product_brands.name AS brand_name', DB::raw('CONCAT("'.Integra::public_path(Config::get('integra.planogram.brand.path')).'", planogram_brands.image) as image'), 'planogram_subcategories.name AS subcategory_name', 'planogram_categories.name AS category_name')
            ->join('planogram_subcategories', 'planogram_brands.subcategory_id', '=', 'planogram_subcategories.id')
            ->join('planogram_categories', 'planogram_subcategories.category_id', '=', 'planogram_categories.id')
            ->join('product_brands', 'planogram_brands.brand_id', '=', 'product_brands.id')
            ->where('planogram_categories.site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'image', 'name' => 'planogram_brands.image', 'title' => 'Imagem', 'width' => '100px', 'render' => 'datatablesFormatImage(data)'])
            ->addColumn(['data' => 'brand_name', 'name' => 'product_brands.name', 'title' => 'Nome'])
            ->addColumn(['data' => 'subcategory_name', 'name' => 'planogram_subcategories.name', 'title' => 'Subcategoria'])
            ->addColumn(['data' => 'category_name', 'name' => 'planogram_categories.name', 'title' => 'Categoria'])
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        if($request->input('type') === 'IMPORT')
            return $this->importFile($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Brand::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Brand::find($id))
            {
                return Redirect::to($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Brand;
        }

        $categories = Category::where('site_id', '=', $this->site->id)->get();

        $subcategories = array();

        foreach($categories as $category)
        {

            $cats = $category->subcategories->lists('name', 'id');

            foreach($cats as $key => $value)
            {
                $subcategories[$category->name][$key] = $value;
            }

        }

        $brands = \App\Models\Product\Brand::orderBy('name')
            ->lists('name', 'id');

        return view('admin.planogram.brands_form', [
            'mode' => $mode,
            'item' => $item,
            'config' => $this->config,
            'subcategories' => $subcategories,
            'brands' => $brands
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $rules = [
            'brand_id'        => 'required',
            'subcategory_id' => 'required'
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            if ($id)
            {
                $item = Brand::find($id);
                $item->fill($input)->save();
            }
            else
            {
                $item = Brand::create($input);
            }

            if($item)
            {

                // Imagem
                if(!empty($input['image']))
                {

                    $img = Image::make($input['image']);

                    if($img->width() > 1000)
                        $img->widen(1000);

                    $input['image'] = Str::slug($item->subcategory->category->name.' '.$item->subcategory->name.' '.$item->brand->name).'.png';

                    $img->save(public_path(Integra::public_path(Config::get('integra.planogram.brand.path')).$input['image']), 100);

                }
                else
                    $input['image'] = null;

                $item->fill(['image' => $input['image']])->save();

                //return Str::slug($item->subcategory->category->name.' '.$item->subcategory->name.' '.$input['name']); die;

            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

    public function import()
    {

        $areas = Area::where('site_id', $this->site->id)
            ->orderBy('name')
            ->lists('name', 'id');

        return view('admin.planogram.brands_import_form', [
            'mode' => 'create',
            'item' => new Brand(),
            'config' => $this->config,
            'areas' => $areas
        ]);

    }

    /*public function importFile(Request $request)
    {

        if ($request->file('file')->isValid()) {

            $file = $request->file('file');

            $csv = str_getcsv(utf8_encode(file_get_contents($file)), "\n");

            foreach ($csv as &$map)
                $map = str_getcsv($map, ";");

            foreach ($csv[0] as &$data)
                $data = strtolower(trim($data));

            $col = array_flip($csv[0]);

            array_shift($csv);

            foreach($csv as $data)
            {

                if($data[$col['cidade']] == $data[$col['local']])
                {

                    $state = State::whereRaw("UPPER(name) LIKE '{$data[$col['estado']]}'")->first();

                    if($state)
                    {

                        $city = City::where('state_id', $state->id)
                            ->whereRaw("UPPER(name) LIKE '{$data[$col['cidade']]}'")->first();

                        if($city)
                        {

                            $data = explode(',', $data[$col['dados']]);

                            City::where('id', $city->id)
                                ->update([
                                    'lng' => $data[0],
                                    'lat' => $data[1],
                                    'alt' => $data[2]
                                ]);

                        }

                    }

                }

            }

        }

    }*/

    public function importFile(Request $request)
    {

        if ($request->file('file')->isValid()) {

            $file = $request->file('file');

            $csv = str_getcsv(utf8_encode(file_get_contents($file)), "\n");

            foreach($csv as &$map)
                $map = str_getcsv($map, ";");

            foreach ($csv[0] as &$data)
                $data = strtolower(trim($data));

            $col = array_flip($csv[0]);

            array_shift($csv);

            foreach($csv as $data)
            {

                $category = Category::where('site_id', $this->site->id)
                    ->where('name', $data[$col['categoria']])
                    ->first();

                if(!$category)
                {
                    $category = Category::create([
                        'site_id' => $this->site->id,
                        'name'  => $data[$col['categoria']]
                    ]);
                }

                if($category)
                {

                    $subcategory = Subcategory::where('category_id', $category->id)
                        ->where('name', $data[$col['subcategoria']])
                        ->first();

                    if(!$subcategory)
                    {
                        $subcategory = Subcategory::create([
                            'category_id' => $category->id,
                            'name'  => $data[$col['subcategoria']]
                        ]);

                    }

                    if($subcategory)
                    {

                        // Marcas de produtos

                        $product_brand = \App\Models\Product\Brand::where('site_id', $this->site->id)
                            ->where('name', $data[$col['marca']])
                            ->first();

                        if(!$product_brand)
                        {
                            $product_brand = \App\Models\Product\Brand::create([
                                'site_id' => $this->site->id,
                                'name'  => $data[$col['marca']]
                            ]);

                        }

                        if($product_brand)
                        {

                            $brand = Brand::where('subcategory_id', $subcategory->id)
                                ->where('brand_id', $product_brand->id)
                                ->first();

                            if (!$brand) {

                                $brand = Brand::create([
                                    'subcategory_id' => $subcategory->id,
                                    'brand_id'  => $product_brand->id
                                    //'image' => Str::slug($data[$col['categoria']] . '_' . $data[$col['subcategoria']] . '_' . $data[$col['marca']], "_") . '.png'
                                ]);

                            }

                            if ($brand) {

                                $brand->areas()->detach($request->input('area_id'));

                                $brand->areas()->attach($request->input('area_id'), [
                                    'share' => (int)$data[$col['share']],
                                    'position' => $data[$col['posicao']]
                                ]);

                            }

                        }
                    }

                }

            }

            return Redirect::route($this->config->getRouteIndex())->with('info', ['save' => true]);

        }

        return Redirect::route($this->config->getRouteIndex())->with('info', ['save' => false]);

    }
}
