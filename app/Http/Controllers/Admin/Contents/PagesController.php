<?php

namespace App\Http\Controllers\Admin\Contents;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests;
use App\Models\Contents\Template;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Models\Contents\Page;

use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;


class PagesController extends MasterController
{

    /**
     * @var string
     */
    protected $route_name = 'admin.contents.pages';

    /**
     * AccountsController constructor.
     */
    public function __construct()
    {
        parent::__construct($this->route_name);
    }

    /**
     * @param Request $request
     * @param Builder $htmlBuilder
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Builder $htmlBuilder)
    {

        $itens = Page::from(Config::get('constants.table.CONTENT.PAGES').' as P')
            ->leftJoin(Config::get('constants.table.CONTENT.PAGES').' as P2', 'P.page_id', '=', 'P2.id')
            ->select('P.id', 'P.title', 'P.active', 'P2.title AS page_parent')
            ->where('P.site_id', '=', $this->site->id);

        if ($request->ajax()) {
            return Datatables::of($itens)->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->config->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'title', 'name' => 'title', 'title' => 'Título'])
            ->addColumn(['data' => 'page_parent', 'name' => 'page_parent', 'title' => 'Página Superior'])
            ->addActive()
            ->addActions()
            ->format();

        return view(Config::get('constants.admin.view.datatables'), ['datatables' => $datatables, 'config' => $this->config]);
    }

    /**
     * Show the form for creating new site.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new site.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);

        return $this->processForm('create');
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    /**
     * Show the form for updating site.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating site.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function destroyItens(Request $request)
    {
        return Page::destroy($request->input('data'));
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Page::find($id))
            {
                return Redirect::route($this->config->getRouteIndex());
            }
        }
        else
        {
            $item = new Page;
            $id = 0;
        }

        $pages = Page::where('id', '<>', $id)->orderBy('title', 'asc')->lists('title','id');

        $templates = Template::orderBy('name', 'asc')->lists('name','id');

        return view('admin.contents.pages.form', [
            'mode' => $mode,
            'item' => $item,
            'pages' => $pages,
            'templates' => $templates,
            'config' => $this->config
        ]);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;

        unset($input['files']);

        (empty($input['page_id'])) ? $input['page_id'] = null : null ;
        (empty($input['template_id'])) ? $input['template_id'] = null : null ;
        (!isset($input['active'])) ? $input['active'] = 0 : null ;

        $rules = [
            /*'slug'        => 'required|alpha_num|unique:pages',
            'title'       => 'required',*/
            'description' => 'max:160'

        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {
            $input['slug'] = $this->generateSlug($input['title'], new Page(), 'slug', $id, true);

            if ($id)
            {

                $item = Page::find($id);

                if($item->slug == $input['slug'])
                    unset($input['slug']);

                $item->fill($input)->save();

            }
            else
            {
                $item = Page::create($input);
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::route($this->config->getRouteShow(), $item->id)->with('info', ['save' => true]);
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a site.
     *
     * @param  array  $data
     * @param  mixed  $rules
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

    public function getUrl(Request $request)
    {
        $route_name = 'sites.page';

        $slug = $this->generateSlug($request->input('slug'), new Page(), 'slug', $request->input('id'), true);

        if($request->input('id'))
        {
            $item = Page::find($request->input('id'));

            if($item->slug != $request->input('slug'))
                return route($route_name, $slug);
            else
                return route($route_name, $item->slug);

        }
        else
            return route($route_name, $slug);


    }
}
