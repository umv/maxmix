<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\System\Site;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Sentinel::guest()){

            return view('admin.welcome');

        }

        return view('admin.home.index');


    }

    public function changeSite($id = null)
    {

        if(empty($id))
            return view('admin.home.change_site');
        else
        {
            // verificao de seguranca
            $ids_site = array();
            foreach(session()->get('config.sites') as $site)
            {
                $ids_site[] = $site['id'];
            }

            if(in_array($id, $ids_site))
            {
                $site = Site::find($id);
                session()->put('site', $site);
                $this->makeConfigSite();
                return Redirect::intended('/admin');
            }
            else
                return Redirect::route('admin.change_site');
        }


    }

    protected function makeConfigSite(){

        // Contas
        session()->forget('accounts');
        $accounts = Sentinel::getUser()->accounts;
        session()->put('accounts', $accounts);

        // Sites
        $sites = session()->get('config.sites');
        session()->forget('config');
        session()->put('config.sites', $sites);

        // Caminhos de pasta
        $parameters = [
            'default' => 'sites/'.session()->get('site')->slug,
            'image' => 'sites/'.session()->get('site')->slug.'/media/images',
        ];

        session()->put('config.path', $parameters);

    }

}
