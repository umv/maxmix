<?php
namespace app\Http\Controllers\Sites\Tourism;

use App\Http\Controllers\Sites\MasterController;
use App\Models\Tourism\AttractionType;
use App\Models\Tourism\Destination;
use Illuminate\Support\Facades\DB;

class DestinationsController extends MasterController
{

    public function index(){

        return $this->view('tourism.destinations');

    }

    public function category($slug_category){

        return $this->view('tourism.destination_category', [
            'slug_category' => $slug_category
        ]);

    }

    public function region($slug_category, $slug_region){

        return $this->view('tourism.destination_region', [
            'slug_category' => $slug_category,
            'slug_region' => $slug_region
        ]);

    }

    public function subregion($slug_category, $slug_region, $slug_subregion){

        return $this->view('tourism.destination_subregion', [
            'slug_category' => $slug_category,
            'slug_region' => $slug_region,
            'slug_subregion' => $slug_subregion,
        ]);

    }

    public function destination($slug_category, $slug_region, $slug_subregion, $slug_destination){

        $destination = Destination::where('site_id', $this->site->id)
            ->where('slug', $slug_destination)
            ->first();

        $destination->photos;

        $attraction_types = AttractionType::where('site_id', $this->site->id)
            ->whereExists(function ($query) use ($destination) {
                $query->select(DB::raw(1))
                    ->from('tourism_attractions')
                    ->where('tourism_attractions.destination_id', $destination->id)
                    ->whereRaw('tourism_attractions.attraction_type_id = tourism_attraction_types.id');
            })
            ->orderBy('order', 'asc')
            ->get();

        return $this->view('tourism.destination', [
            'slug_category' => $slug_category,
            'slug_region' => $slug_region,
            'slug_subregion' => $slug_subregion,
            'slug_destination' => $slug_destination,
            'destination' => $destination,
            'attraction_types' => $attraction_types
        ]);

    }
}