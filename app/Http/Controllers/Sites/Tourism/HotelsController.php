<?php
namespace app\Http\Controllers\Sites\Tourism;

use App\Http\Controllers\Sites\MasterController;
use App\Models\Tourism\HotelSearch;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class HotelsController extends MasterController
{

    public function addSearch()
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;
        $input['destination'] = (!empty($input['destination'])) ? $input['destination'] : null;
        $input['date_checkin'] = (!empty($input['date_checkin'])) ? Carbon::createFromFormat('d/m/Y', $input['date_checkin']) : null;
        $input['date_checkout'] = (!empty($input['date_checkout'])) ? Carbon::createFromFormat('d/m/Y', $input['date_checkout']) : null;
        $input['amount_adult'] = (!empty($input['amount_adult'])) ? $input['amount_adult'] : null;
        $input['amount_child'] = (!empty($input['amount_child'])) ? $input['amount_child'] : null;

        $rules = [

        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {
            HotelSearch::create($input);
        }

        return 1;

    }
}