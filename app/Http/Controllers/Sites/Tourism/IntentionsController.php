<?php
namespace app\Http\Controllers\Sites\Tourism;

use App\Http\Controllers\Sites\MasterController;
use App\Models\General\City;
use App\Models\Marketing\Origin;
use App\Models\Tourism\AttractionType;
use App\Models\Tourism\Category;
use App\Models\Tourism\Destination;
use App\Models\Tourism\IntentionConfig;
use App\Models\Tourism\Package;
use App\Models\Tourism\PackageDate;
use App\Models\Tourism\Intention;
use App\Models\Tourism\Region;
use App\Models\Tourism\Service;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Vinkla\Hashids\Facades\Hashids;

class IntentionsController extends MasterController
{

    public function __construct(Request $request)
    {

        parent::__construct($request);

    }

    public function index($destination_id = null, $date_id = null){

        $destination = null;
        $date = null;

        if(!empty($destination_id))
            $destination = Destination::find(Hashids::decode($destination_id))->first();

        if(!empty($date_id))
            $date = PackageDate::find(Hashids::decode($date_id))->first();

        $origins = Origin::where('site_id', $this->site->id)
            ->where('active', 1)
            ->orderBy('name')
            ->lists('name', 'id');

        return $this->view('tourism.intention', [
            'destination' => $destination,
            'date' => $date,
            'origins' => $origins
        ]);

    }

    public function store($destination_id = null, $date_id = null)
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;

        if(!empty($destination_id) && $destination_id != 'cotacao-personsalizada')
            $input['destination_id'] = Hashids::decode($destination_id)[0];

        if(!empty($date_id))
            $input['date_id'] = Hashids::decode($date_id)[0];

        $input['date_departure'] = (!empty($input['date_departure'])) ? Carbon::createFromFormat('d/m/Y', $input['date_departure']) : null;
        $input['date_arrival'] = (!empty($input['date_arrival'])) ? Carbon::createFromFormat('d/m/Y', $input['date_arrival']) : null;

        $rules = [
            'name'        => 'required|min:3|max:250',
            'email' => 'required|email|max:250',
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {
            $intention = Intention::create($input);

            $config = IntentionConfig::where('site_id', $this->site->id)
                ->first();

            // User

            Mail::send($this->config['view'].$config->template, ['intention' => $intention], function ($m) use ($intention, $config) {
                $m->from($config->email_sender, $config->email_name);
                $m->to($intention->email)->subject($config->subject);
            });

            // Admins

            Mail::send($this->config['view'].$config->template, ['intention' => $intention], function ($m) use ($intention, $config) {

                $m->from($config->email_sender, $config->email_name);

                $senders = explode(',', $config->senders);

                foreach($senders as $sender)
                    $m->to($sender)->subject('Cotação enviada pelo site!');

            });

            return Redirect::route('sites.tourism.packages.intentions.index', [
                'destination_id'=>$destination_id,
                'date_id'=>$date_id
            ])->with('info', [
                'intention' => $intention,
                'send' => true
            ]);

        }

        return Redirect::back()->withInput()->withErrors($messages);

    }
}