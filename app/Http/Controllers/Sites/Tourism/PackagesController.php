<?php
namespace app\Http\Controllers\Sites\Tourism;

use App\Http\Controllers\Sites\MasterController;
use App\Models\Tourism\AttractionType;
use App\Models\Tourism\Category;
use App\Models\Tourism\Package;
use App\Models\Tourism\PackageDate;
use App\Models\Tourism\PackageDateIntention;
use App\Models\Tourism\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Vinkla\Hashids\Facades\Hashids;

class PackagesController extends MasterController
{

    public function __construct(Request $request)
    {

        parent::__construct($request);

        $services = Service::where('site_id', $this->site->id)
            ->orderBy('order')
            ->get();

        view()->share([
            'services' => $services,
        ]);

    }

    public function index(Request $request){

        return $this->category(null, $request);
        //return $this->view('tourism.packages');

    }

    public function getPage($slug, Request $request = null){

        $category = $this->isCategory($slug);

        if($category)
            return $this->category($category, $request);
        else
        {

            $package = $this->isPackage($slug);

            if($package)
                return $this->package($package);

        }


        return Redirect::to('/errors/404');

    }

    public function isCategory($slug){

        // Verifica se e categoria
        $category = Category::where('site_id', $this->site->id)
            ->where('slug', $slug)
            ->first();

        if($category)
            return $category;
        else
            return false;

    }

    public function isPackage($slug){

        // Verifica se e pacote
        $package = Package::where('site_id', $this->site->id)
            ->where('slug', $slug)
            ->first();

        if($package)
            return $package;
        else
            return false;

    }

    public function getCategory($category = null, $request){

        $packages = PackageDate::select('tourism_package_dates.*')
            ->join('tourism_packages', 'tourism_packages.id', '=', 'tourism_package_dates.package_id')
            ->leftJoin('tourism_package_service_filter', 'tourism_packages.id', '=', 'tourism_package_service_filter.package_id')
            ->join('tourism_destinations', 'tourism_destinations.id', '=', 'tourism_packages.destination_id')
            ->join('tourism_subregions', 'tourism_subregions.id', '=', 'tourism_destinations.subregion_id')
            ->join('tourism_regions', 'tourism_regions.id', '=', 'tourism_subregions.region_id')
            ->where('tourism_packages.site_id', $this->site->id)
            ->where('tourism_packages.active', 1)
            ->whereRaw("DATE(tourism_package_dates.date_departure) >= DATE(NOW())")
            ->whereRaw("DATE(tourism_package_dates.disabled_at) >= DATE(NOW())");
            //->groupBy('tourism_package_dates.package_id');

        if($category)
            $packages = $packages->where('tourism_regions.category_id', $category->id);

        // query
        if($request->has('q')){

            $query = $request->input('q');

            $packages = $packages->whereRaw("(tourism_packages.name LIKE '%{$query}%' OR tourism_destinations.name LIKE '%{$query}%')");

        }

        // services
        if($request->has('filters.service')){

            $filter_service = [];

            foreach ($request->input('filters.service') as $filter)
                $filter_service = array_merge($filter_service, $filter);

            $packages = $packages->whereIn('tourism_package_service_filter.filter_id', $filter_service)
                ->havingRaw("COUNT(DISTINCT tourism_package_service_filter.filter_id) = ".count($filter_service));

        }

        // price
        if($request->has('filters.price')){

            $filter_price = [];

            foreach ($request->input('filters.price') as $filter)
                $filter_price[] = explode('-', $filter);

            $where = [];

            foreach($filter_price as $filter){

                $aux = [];

                if($filter[0] > 0)
                    $aux[] = "tourism_package_dates.price_promotional >= {$filter[0]}";

                if($filter[1] > 0)
                    $aux[] = "tourism_package_dates.price_promotional <= {$filter[1]}";

                if(!empty($aux))
                    $where[] = '( ' . implode(' AND ', $aux) . ' )';

            }

            if(!empty($where)){

                $where = implode(' OR ', $where);

                $packages = $packages->whereRaw($where);

            }

        }

        // region
        if($request->has('filters.region')){

            $filter_destination = $request->input('filters.region');
            $packages = $packages->whereIn('tourism_subregions.region_id', $filter_destination);

        }

        // duration
        if($request->has('filters.duration')){

            //DATEDIFF(day,'2014-08-05','2014-06-05')

        }

        // ammount of people
        if($request->has('filters.amount_people')){

            $filter_amount_people = $request->input('filters.amount_people');
            $packages = $packages->whereIn('tourism_package_dates.amount_people', $filter_amount_people);

        }

        $packages->groupBy(['tourism_packages.id', 'tourism_package_dates.package_id']);

        // order
        if($request->has('order')) {
            if($request->input('order') == 'price-asc'){
                $packages = $packages->orderBy('tourism_package_dates.price_promotional', 'asc')->orderBy('tourism_package_dates.price_default', 'asc');
            } elseif($request->input('order') == 'price-desc'){
                $packages = $packages->orderBy('tourism_package_dates.price_promotional', 'desc')->orderBy('tourism_package_dates.price_default', 'desc');
            } elseif($request->input('order') == 'relevance')
                $packages = $packages->orderBy('tourism_package_dates.id', 'desc');
        }

        $packages = $packages->paginate(15);

        //return $packages;

        $data =  [
            'category' => $category,
            'dates'  => $packages
        ];

        return $data;

    }

    public function getPackage($package){

        $destination = $package->destination;

        $attraction_types = AttractionType::where('site_id', $this->site->id)
            ->whereExists(function ($query) use ($destination) {
                $query->select(DB::raw(1))
                    ->from('tourism_attractions')
                    ->where('tourism_attractions.destination_id', $destination->id)
                    ->whereRaw('tourism_attractions.attraction_type_id = tourism_attraction_types.id');
            })
            ->orderBy('order', 'asc')
            ->get();

        $data = [
            'package' => $package,
            'attraction_types' => $attraction_types
        ];

        return $data;

    }

    public function category($category, $request){

        return $this->view('tourism.packages', $this->getCategory($category, $request));

    }

    public function package($package){

        return $this->view('tourism.package', $this->getPackage($package));

    }

    public function getPackageDate(Request $request)
    {

        $date_id = $request->input('data');

        $package_date = PackageDate::find($date_id);

        if(!$package_date)
            return 0;

        $package_date->payments;
        $package_date['date_id'] = Hashids::encode($package_date->id);

        return $package_date;

    }

}