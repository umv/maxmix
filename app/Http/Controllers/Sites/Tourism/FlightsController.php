<?php
namespace app\Http\Controllers\Sites\Tourism;

use App\Http\Controllers\Sites\MasterController;
use App\Models\Tourism\FlightSearch;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class FlightsController extends MasterController
{

    public function addSearch()
    {

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;
        $input['type'] = (!empty($input['type'])) ? $input['type'] : null;
        $input['local_departure'] = (!empty($input['local_departure'])) ? $input['local_departure'] : null;
        $input['local_arrival'] = (!empty($input['local_arrival'])) ? $input['local_arrival'] : null;
        $input['date_departure'] = (!empty($input['date_departure'])) ? Carbon::createFromFormat('d/m/Y', $input['date_departure']) : null;
        $input['date_arrival'] = (!empty($input['date_arrival'])) ? Carbon::createFromFormat('d/m/Y', $input['date_arrival']) : null;
        $input['amount_adult'] = (!empty($input['amount_adult'])) ? $input['amount_adult'] : null;
        $input['amount_child'] = (!empty($input['amount_child'])) ? $input['amount_child'] : null;
        $input['amount_baby'] = (!empty($input['amount_baby'])) ? $input['amount_baby'] : null;

        $rules = [

        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {
            FlightSearch::create($input);
        }

        return 1;

    }
}