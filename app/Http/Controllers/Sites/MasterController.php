<?php

namespace App\Http\Controllers\Sites;

use App\Http\Controllers\Controller;
use App\Integra\ConfigFields;
use App\Integra\Integra;
use App\Models\System\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

abstract class MasterController extends Controller
{

    /**
     * @var Request
     */
    public $request;

    /**
     * @var Site
     */
    public $site;

    /**
     * @var
     */
    public $config;

    public $user;

    public $resource;

    /**
     * MasterController constructor.
     * @param Request $request
     */
    public function __construct($route_name = null, Request $request)
    {

        //self::__construct();

        // busca do site
        $this->request = $request;

        $this->site = Site::where('url', $request->getHost())->first();

        if( $this->site == null )
            die('A URL do site não está configurada!');

        // configuracoes
        $this->config = [
            'slug' => $this->site->slug,
            'view'=> 'sites.'.$this->site->slug.'.'
        ];

        if($route_name !== null)
            $this->resource = new ConfigFields($route_name);

        // Auth
        $this->user = Auth::user();

        // compartilhando informacoes com as views
        view()->share([
            'request' => $this->request,
            'site' => $this->site,
            'config' => $this->config,
            'user' => $this->user,
            'resource' => $this->resource
        ]);

        // compartilhando informacao na session
        session([
            'cliente' => [
                'config' => $this->config
            ]
        ]);

        Integra::setSessionSite($this->site->id);

    }

    /**
     * @param $path
     * @param bool $make
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($path, $data = [], $make = true)
    {

        if($make)
            $path = $this->config['view'].$path;

        if (view()->exists($path))
            return view($path, $data);
        else
            return Redirect::to('/errors/404');
    }

    public function setGeoposition(Request $request)
    {

        session([
            'geoposition' => $request->input('data')
        ]);

        return 1;

    }

    protected function validateItem($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

}
