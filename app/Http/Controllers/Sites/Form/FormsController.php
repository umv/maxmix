<?php
namespace app\Http\Controllers\Sites\Form;

use App\Http\Controllers\Sites\MasterController;
use App\Models\Form\Form;
use App\Models\General\City;
use App\Models\Tourism\AttractionType;
use App\Models\Tourism\Category;
use App\Models\Tourism\Destination;
use App\Models\Tourism\IntentionConfig;
use App\Models\Tourism\Package;
use App\Models\Tourism\PackageDate;
use App\Models\Tourism\Intention;
use App\Models\Tourism\Region;
use App\Models\Tourism\Service;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Vinkla\Hashids\Facades\Hashids;

class FormsController extends MasterController
{

    public function send()
    {
        $input = array_filter(Input::all());

        $slug = $input['slug'];

        $form = Form::where('slug', $slug)->first();

        if($form)
        {
            $content = $this->parseContent( $form->code_recipient, $input );

            //return $content; die;

            unset($input['_token']);
            unset($input['slug']);
            unset($input['request_uri']);

            Mail::send('integra.form.blank', ['content' => $content, 'input' => $input], function ($m) use ($form) {

                $m->from('contato@lazertur.com.br', 'Lazertur Viagens');

                $recipients = explode(',', $form->recipients);

                foreach($recipients as $recipient)
                    $m->to($recipient)->subject($form->subject);

            });

            $send = true;


            //return 'enviado';


            return Redirect::back()
                ->with('info', [
                'text' => ($send) ? $form->text_success : $form->text_error,
                'send' => true
            ]);

        } else {

            return "Formulário nao encontrado";


        }

    }

    public function parseContent( $content, $input = [] ) {

        //$content = str

        //$html_content = preg_replace( array( '/({#)/', '/(#})/' ), array( '{{$', '}}' ), $html_content );

        /*$filepath = Config::get('view.paths')[0];

        $file = 'integra_form_' . uniqid() . '.blade.php';

        $handle = fopen( $filepath . $file, 'w+' );

        fwrite( $handle, $html_content );
        fclose( $handle );

        $input = array_filter(Input::all());

        if( !empty( $input ) )
            foreach( $input as $key => $value )
                $this->_view->assign( $key, utf8_decode( $value ) );

        $conteudo = $this->_view->fetch( $file );

        unlink( $filepath . $file );*/

        return $content;

    }
}