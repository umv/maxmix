<?php

namespace App\Http\Controllers\Sites;

use App\Models\Planogram\Item;
use App\Models\Product\Product;

class AuxController extends MasterController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $area = [
            7 => 13,
            8 => 14,
            9 => 15,
            10 => 16,
            11 => 17,
            12 => 18,
        ];

        $subcategory = [
            66 => 68,
            67 => 69,
        ];

        $planogram_items = Item::whereIn('subcategory_id', [66,67])->get();

        foreach ($planogram_items as $planogram_item) {

            $product_antigo = Product::find($planogram_item->product_id);

            $product = Product::where('ean', $product_antigo->ean)
                ->where('site_id', 16)
                ->first();

            Item::create([
                'area_id' => $area[$planogram_item->area_id],
                'subcategory_id' => $subcategory[$planogram_item->subcategory_id],
                'product_id' => $product->id,
                'position' => $planogram_item->position,
            ]);

        }

    }



}
