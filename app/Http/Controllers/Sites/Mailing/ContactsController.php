<?php
namespace app\Http\Controllers\Sites\Mailing;

use App\Http\Controllers\Sites\MasterController;
use App\Models\Mailing\Contact;
use Illuminate\Support\Facades\Input;

class ContactsController extends MasterController
{

    public function add(){

        $input = array_filter(Input::all());

        $input['site_id'] = $this->site->id;

        $rules = [
            'name'        => 'required',
            'email'        => 'required|email'
        ];

        $messages = $this->validateItem($input, $rules);

        if ($messages->isEmpty())
        {

            $item = Contact::where('email', 'like', $input['email'])
                ->first();

            if($item)
            {
                $item->update([
                    'name' => $input['name'],
                    'active' => 1
                ]);
            } else {

                $item = Contact::create($input);

            }

            return ['status' => true];

        }

        return ['status' => false];

    }

}