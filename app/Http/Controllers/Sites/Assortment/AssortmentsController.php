<?php
namespace app\Http\Controllers\Sites\Assortment;

use App\Http\Controllers\Sites\MasterController;
use App\Models\Assortment\Category;
use App\Models\Assortment\Client;
use App\Models\Assortment\Product;
use App\Models\Assortment\Subcategory;
use App\Models\Assortment\Tag;
use App\Models\Planogram\Planogram;
use App\Models\Product\Brand;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Khill\Lavacharts\Laravel\LavachartsFacade;
use Vinkla\Hashids\Facades\Hashids;


class AssortmentsController extends MasterController
{

    protected $route_name = 'sites.assortment.resource';

    protected $table = [
        'A' => [
            'category' => 0,
            'subcategory' => 0,
            'sku' => 0
        ],
        'B' => [
            'category' => 0,
            'subcategory' => 0,
            'sku' => 0
        ],
        'C' => [
            'category' => 0,
            'subcategory' => 0,
            'sku' => 0
        ],
        'D' => [
            'category' => 0,
            'subcategory' => 0,
            'sku' => 0
        ]
    ];

    public function __construct(Request $request)
    {
        parent::__construct($this->route_name, $request);
    }

    public function about(Request $request){

        if($request->has('email')){

            $site = $this->site;

            $data = [
                'user' => $this->user,
                'site' => $site,
                'request' => $request
            ];

            //try {

                Mail::send($this->config['view'].'assortment.email_request', $data, function ($m) use ($site, $request) {
                    $m->from('contato@maxmix.net.br', 'MaxMix');
                    $m->bcc('samuel@wingmidia.com.br')->subject('Solicitaçao de sortimento - MaxMix - ' . $request->input('name'));
                    //$m->to('samuelkbca@gmail.com')->subject('Solicitaçao de sortimento - MaxMix - ' . $request->input('name'));
                    $m->to('denis.salum@martins.com.br')->subject('Solicitaçao de sortimento - MaxMix - ' . $request->input('name'));
                    $m->to('ester.jorge@martins.com.br')->subject('Solicitaçao de sortimento - MaxMix - ' . $request->input('name'));
                    $m->to('blurkil@hotmail.com')->subject('Solicitaçao de sortimento - MaxMix - ' . $request->input('name'));
                });

            //} catch(\Exception $e){

                // Get error here
                //return var_dump($e);
            //}

            return response()->json(['msg'=>'success']);

        }

        return $this->view('assortment.about');

    }

    public function listClients($option)
    {

        $route = "sites.assortment.{$option}.client" ;

        $clients = $this->user->assortmentClients();

        if($clients->count() == 0)
            return Redirect::route('sites.assortment.about');

        if($clients->count() == 1)
            return Redirect::route($route, Hashids::encode($clients->first()->id));


        return $this->view('assortment.clients', [
            'clients' => $clients->orderBy('name')->get(),
            'option' => $option,
            'route' => $route
        ]);

    }

    public function clientValuation($id)
    {

        $client = $this->getClient($id);

        $tags = Tag::where('client_id', $client->id)
            ->get();

        foreach ($tags as &$tag)
        {

            $tag->total = $this->getTotalTag($client->id, $tag->id);

        }

        return $this->view('assortment.valuation.client', [
            'client' => $client,
            'tags' => $tags
        ]);

    }

    public function initValuation($client_id, $tag_id){

        $client = $this->getClient($client_id);

        $tag = $this->getTag($tag_id);

        return $this->view('assortment.valuation.init', [
            'client' => $client,
            'tag' => $tag
        ]);

    }

    public function categoriesValuation($client_id, $tag_id)
    {

        $client_id = Hashids::decode($client_id)[0];
        $tag_id = Hashids::decode($tag_id)[0];

        $data = $this->getCategoryValuation($client_id, $tag_id);

        $table = $data['table'];

        $chart_table = LavachartsFacade::DataTable();

        $chart_table->addStringColumn(trans('sites/general.group'))
            ->addNumberColumn(trans('sites/general.percentage'))
            ->addRow([trans('sites/maxmix/assortment.groups.a.title'), (int) $table['A']['category']])
            ->addRow([trans('sites/maxmix/assortment.groups.b.title'), (int) $table['B']['category']])
            ->addRow([trans('sites/maxmix/assortment.groups.c.title'), (int) $table['C']['category']])
            ->addRow([trans('sites/maxmix/assortment.groups.d.title'), (int) $table['D']['category']]);

        LavachartsFacade::PieChart('ChartTable', $chart_table, [
            'legend' => 'none',
            'colors' => ['#008000', '#92d050', '#ffa500', '#ff0000'],
            'fontSize' => 18,
            'tooltip' => [
                'showColorCode' => true
            ],
            'slices' => [
                0 => [
                    'offset' => 0.2
                ]
            ],
            'chartArea' => [
                'width' => '100%',
                'height' => '80%'
            ]
        ]);

        return $this->view('assortment.valuation.categories', $data);
    }

    public function subcategoriesValuation($client_id, $tag_id)
    {

        $client_id = Hashids::decode($client_id)[0];
        $tag_id = Hashids::decode($tag_id)[0];

        $data = $this->getSubcategoryValuation($client_id, $tag_id);

        $table = $data['table'];

        $chart_table = LavachartsFacade::DataTable();

        $chart_table->addStringColumn(trans('sites/general.group'))
            ->addNumberColumn(trans('sites/general.percentage'))
            ->addRow([trans('sites/maxmix/assortment.groups.a.title'), (int) $table['A']['subcategory']])
            ->addRow([trans('sites/maxmix/assortment.groups.b.title'), (int) $table['B']['subcategory']])
            ->addRow([trans('sites/maxmix/assortment.groups.c.title'), (int) $table['C']['subcategory']])
            ->addRow([trans('sites/maxmix/assortment.groups.d.title'), (int) $table['D']['subcategory']]);

        LavachartsFacade::PieChart('ChartTable', $chart_table, [
            'legend' => 'none',
            'colors' => ['#008000', '#92d050', '#ffa500', '#ff0000'],
            'fontSize' => 18,
            'tooltip' => [
                'showColorCode' => true
            ],
            'slices' => [
                0 => [
                    'offset' => 0.2
                ]
            ],
            'chartArea' => [
                'width' => '100%',
                'height' => '80%'
            ]
        ]);

        return $this->view('assortment.valuation.subcategories', $data);
    }

    public function compositionCategoriesValuation($client_id, $tag_id)
    {

        $client_id = Hashids::decode($client_id)[0];
        $tag_id = Hashids::decode($tag_id)[0];

        $category_id = $this->request->c;

        $data = $this->getCompositionCategory($client_id, $tag_id, $category_id);

        $template = ($this->request->a == 1) ? 'assortment.valuation.categories_composition_ajax' : 'assortment.valuation.categories_composition';

        return $this->view($template, $data);
    }

    public function brandsValuation($client_id, $tag_id)
    {

        $client_id = Hashids::decode($client_id)[0];
        $tag_id = Hashids::decode($tag_id)[0];

        $subcategory_id = ($this->request->s) ? $this->request->s : null;

        $data = $this->getBrandValuation($client_id, $tag_id, $subcategory_id);

        /*if(!empty($data['subcategory']) && count($data['brands']) > 1)
        {

            // grafico

            $chart_table = LavachartsFacade::DataTable();

            $chart_table->addStringColumn(trans('sites/general.brand'))
                ->addNumberColumn(trans('sites/general.participation'))
                ->addNumberColumn(trans('sites/general.margin'));

            foreach ($data['brands'] as $key => &$item) {

                $chart_table->addRow([
                    $item['brand'],
                    (float) number_format($item['participation'], 2),
                    (float) number_format($item['percentage_gross_margin'], 2)
                ]);

            }

            LavachartsFacade::AreaChart('ChartTable', $chart_table, [
                'legend' => [
                    'position' => 'top',
                    'alignment' => 'center'
                ],
                'series' => [
                    1 => [
                        'color' => 'green',
                    ]
                ],
                'pointSize' => 3,
                'chartArea' => [
                    'width' => '90%',
                    'height' => '50%'
                ]
            ]);

        }*/

        return $this->view('assortment.valuation.brands', $data);

    }

    public function itemsValuation($client_id, $tag_id)
    {

        $client_id = Hashids::decode($client_id)[0];
        $tag_id = Hashids::decode($tag_id)[0];

        $subcategory_id = ($this->request->s) ? $this->request->s : null;

        $data = $this->getItemValuation($client_id, $tag_id, $subcategory_id);

        $data['mix'] = ($this->request->mix == 1) ? true: false;
        $data['brand_size'] = (!empty($this->request->brand_size)) ? unserialize($this->request->brand_size) : false;

        if($data['mix']){

            $itens_aux = [];
            $itens_sorted = [];
            $total_margin = [];

            foreach ($data['itens'] as $item) {

                if($item['brand_valuation']['status'] && $item['recomendation']['W'] != 0) {

                    if(!isset($total_margin[$item['brand_id']]))
                        $total_margin[$item['brand_id']] = 0;

                    $total_margin[$item['brand_id']] += $item['total_gross_margin'];

                    $itens_aux[$item['brand_valuation']['participation'] . $item['brand_id']][] = $item;

                }
            }

            ksort($itens_aux, SORT_NUMERIC);

            $itens_aux = array_reverse($itens_aux);

            foreach ($itens_aux as $item)
                $itens_sorted = array_merge($itens_sorted, $item);

            foreach ($itens_sorted as &$item) {
                $item['participation_sorted'] = $item['total_gross_margin'] / $total_margin[$item['brand_id']] * 100;
                $item['length'] = ($data['brand_size'][$item['brand_id']] * $item['participation_sorted']) / 100;
            }

            $data['itens'] = $itens_sorted;

        }

        $template = ($this->request->print == 1) ? 'assortment.valuation.items_print' : 'assortment.valuation.items';

        return $this->view($template, $data);
    }

    public function cockpitValuation($client_id, $tag_id){

        $client = $this->getClient($client_id);

        $tag = $this->getTag($tag_id);

        return $this->view('assortment.valuation.cockpit', [
            'client' => $client,
            'tag' => $tag
        ]);

    }

    public function itemsByImportanceValuation($client_id, $tag_id, $importance)
    {

        $client_id = Hashids::decode($client_id)[0];
        $tag_id = Hashids::decode($tag_id)[0];

        $data = $this->getItemByImportance($client_id, $tag_id, $importance);

        $template = ($this->request->a == 1) ? 'assortment.valuation.items_by_importante_ajax' : 'assortment.valuation.items_by_importante_print';

        return $this->view($template, $data);
    }

    public function clientMonitoring($id)
    {

        $client = $this->getClient($id);

        $tags = $this->getTagsClient($client->id);

        if(count($tags) <= 1)
            return Redirect::route('sites.assortment.client', $id);


        return $this->view('assortment.monitoring.client', [
            'client' => $client,
            'tags' => $tags
        ]);

    }

    public function initMonitoring($client_id, $tags_url){

        $client = $this->getClient($client_id);

        $tags = $this->getTags($tags_url);

        foreach ($tags as &$tag)
        {

            $tag->total = Product::select([
                    'assortment_products.date',
                    DB::raw('SUM(assortment_products.total_revenue) AS total_revenue'),
                    DB::raw('SUM(assortment_products.total_gross_margin) AS total_gross_margin')
                ])
                ->where('assortment_products.client_id', $client->id)
                ->whereIn('assortment_products.date', $tag->getDate($tag->id))
                ->orderBy('assortment_products.date')
                ->first();

            $tag->total->percentage_gross_margin = ($tag->total->total_gross_margin  * 100) / $tag->total->total_revenue;

        }

        return $this->view('assortment.monitoring.init', [
            'client' => $client,
            'tags' => $tags,
            'tags_url' => $tags_url
        ]);

    }

    public function categoriesMonitoring($client_id, $tags_url)
    {

        $client = $this->getClient($client_id);

        $tags = $this->getTags($tags_url);

        foreach ($tags as &$tag) {

            $tag->valuation = $this->getCategoryValuation($client->id, $tag->id);

            foreach ($tag->valuation['categories'] as $item) {

                $data[$item['category_id']]['category'] = $item['category'];

                $data[$item['category_id']]['data'][$tag->id] = $item['total_gross_margin'];

            }

            $chart['series'][$tag->id] = [
                'name' => $tag->name,
                'data' => []
            ];

        }

        foreach ($data as $category_id => $d){

            if( count($tags) == count($d['data']) ){

                $first = current($d['data']);

                $x = (( end($d['data']) * 100 ) / $first)-100;

                $color = ($x > 0) ? 'green' : 'red';

                $x = ($x > 0) ? '+'.number_format($x, 2, ',', '.') : number_format($x, 2, ',', '.');

                $chart['categories'][] = "{$d['category']}: <span style='color: {$color}; float: right;'>{$x}%</span>";

                $chart['data']['categories'][str_replace(' ', '', $d['category'])] = $category_id;

                foreach ($d['data'] as $tag_id => $percentage) {

                    $chart['series'][$tag_id]['data'][] = (float) number_format($percentage, 2, '.', '');

                }

            }

        }

        sort($chart['series']);

        return $this->view('assortment.monitoring.categories', [
            'client' => $client,
            'tags' => $tags,
            'tags_url' => $tags_url,
            'chart' => $chart,
        ]);

    }

    public function subcategoriesMonitoring($client_id, $tags_url)
    {

        $client = $this->getClient($client_id);

        $tags = $this->getTags($tags_url);

        foreach ($tags as &$tag) {

            $tag->valuation = $this->getSubcategoryValuation($client->id, $tag->id);

            foreach ($tag->valuation['subcategories'] as $item) {

                $data[$item['subcategory_id']]['subcategory'] = $item['subcategory'];

                if(!isset($data[$item['subcategory_id']]['data'][$tags[0]->id]))
                    $data[$item['subcategory_id']]['data'][$tags[0]->id] = 0;

                if(!isset($data[$item['subcategory_id']]['data'][$tags[1]->id]))
                    $data[$item['subcategory_id']]['data'][$tags[1]->id] = 0;

                $data[$item['subcategory_id']]['data'][$tag->id] = $item['total_gross_margin'];

            }

            $chart['series'][$tag->id] = [
                'name' => $tag->name,
                'data' => []
            ];

        }

        foreach ($data as $subcategory_id => $d){

            $first = reset($d['data']);

            if($first > 0)
                $x = ((end($d['data']) * 100) / $first) - 100;
            else
                $x = 100;

            $color = '#3498db';

            if($x == -100)
                $x = 'SUBCATEGORIA REMOVIDA';
            elseif($x == 100)
                $x = 'SUBCATEGORIA ADICIONADA';
            else {
                $x = ($x > 0) ? '+' . number_format($x, 2, ',', '.') . '%' : number_format($x, 2, ',', '.') . '%';
                $color = ($x > 0) ? 'green' : 'red';
            }

            $chart['categories'][] = "{$d['subcategory']}: {$subcategory_id}: <span style='color:{$color}; float:right;'>{$x}</span>";

            foreach ($tags as $t)
                $chart['series'][$t->id]['data'][] = (isset($d['data'][$t->id])) ? (float)number_format($d['data'][$t->id], 2, '.', '') : 0;


            //$chart['data']['subcategories'][str_replace(' ', '', $d['subcategory'])] = $subcategory_id;

            /*if( count($tags) == count($d['data']) ){

                $first = current($d['data']);

                $x = (( end($d['data']) * 100 ) / $first)-100;

                $color = ($x > 0) ? 'green' : 'red';

                $x = ($x > 0) ? '+'.number_format($x, 2, ',', '.') : number_format($x, 2, ',', '.');

                $chart['categories'][] = "{$d['subcategory']}: <span style='color: {$color}; float: right;'>{$x}%</span>";

                $chart['data']['subcategories'][str_replace(' ', '', $d['subcategory'])] = $subcategory_id;

                foreach ($d['data'] as $tag_id => $percentage) {

                    $chart['series'][$tag_id]['data'][] = (float) number_format($percentage, 2, '.', '');

                }

            }*/

        }

        sort($chart['series']);

        return $this->view('assortment.monitoring.subcategories', [
            'client' => $client,
            'tags' => $tags,
            'tags_url' => $tags_url,
            'chart' => $chart,
        ]);

    }

    public function brandsMonitoring($client_id, $tags_url)
    {

        $client = $this->getClient($client_id);

        $tags = $this->getTags($tags_url);

        $subcategory_id = ($this->request->s) ? $this->request->s : null;

        foreach ($tags as &$tag) {

            $tag->valuation = $this->getBrandValuation($client->id, $tag->id, $subcategory_id);

            if(!empty($tag->valuation['subcategory']) && count($tag->valuation['brands']) > 1) {

                foreach ($tag->valuation['brands'] as $item) {

                    $data[$item['brand_id']]['brand'] = $item['brand'];

                    if(!isset($data[$item['brand_id']]['data'][$tags[0]->id]))
                        $data[$item['brand_id']]['data'][$tags[0]->id] = 0;

                    if(!isset($data[$item['brand_id']]['data'][$tags[1]->id]))
                        $data[$item['brand_id']]['data'][$tags[1]->id] = 0;

                    $data[$item['brand_id']]['data'][$tag->id] = $item['total_gross_margin'];

                }

            }

            $tag_subcategories[] = $tag->valuation['subcategories'];
            $tag_subcategories_grouped[] = $tag->valuation['subcategories_grouped'];

            $chart['series'][$tag->id] = [
                'name' => $tag->name,
                'data' => []
            ];

        }

        $subcategories = $tag_subcategories[0]->intersect($tag_subcategories[1]);
        $subcategories = $subcategories->intersect($tag_subcategories[0]);

        foreach ($tag_subcategories_grouped[0] as $category => $subcategory)
            if(array_key_exists($category, $tag_subcategories_grouped[1]))
                $subcategories_grouped[$category] = array_intersect($tag_subcategories_grouped[0][$category], $tag_subcategories_grouped[1][$category]);

        if($subcategory_id && isset($data)) {

            foreach ($data as $brand_id => $d) {

                //if (count($tags) == count($d['data'])) {

                    $first = reset($d['data']);

                    if($first > 0)
                        $x = ((end($d['data']) * 100) / $first) - 100;
                    else
                        $x = 100;

                    $color = '#3498db';

                    if($x == -100)
                        $x = 'MARCA REMOVIDA';
                    elseif($x == 100)
                        $x = 'MARCA ADICIONADA';
                    else {
                        $x = ($x > 0) ? '+' . number_format($x, 2, ',', '.') . '%' : number_format($x, 2, ',', '.') . '%';
                        $color = ($x > 0) ? 'green' : 'red';
                    }

                    $chart['categories'][] = "{$d['brand']}: {$brand_id}: <span style='color:{$color}; float:right;'>{$x}</span>";

                //} else {

                    //$chart['categories'][] = "{$d['brand']}: <span style='color: #666666; float: right;'></span>";

                //}

                foreach ($tags as $t)
                    $chart['series'][$t->id]['data'][] = (isset($d['data'][$t->id])) ? (float)number_format($d['data'][$t->id], 2, '.', '') : 0;


                //$chart['data']['brands'][str_replace(' ', '', $d['brand'])] = $brand_id;

            }

            sort($chart['series']);

        }

        return $this->view('assortment.monitoring.brands', [
            'client' => $client,
            'tags' => $tags,
            'tags_url' => $tags_url,
            'chart' => $chart,
            'subcategories' => $subcategories,
            'subcategory_id' => $subcategory_id,
            'subcategories_grouped' => $subcategories_grouped
        ]);


    }

    public function detailsMonitoring($client_id, $tags_url, $type)
    {

        $client = $this->getClient($client_id);

        $tags = $this->getTags($tags_url);

        switch ($type){

            case 'category' :

                $id = $this->request->c;

                foreach ($tags as &$tag) {

                    $data = $this->getCategoryValuation($client->id, $tag->id);

                    foreach ($data['categories'] as $category) {

                        if( $category['category_id'] == $id ){

                            $tag->valuation = $category;

                            CONTINUE 2;

                        }

                    }

                }

                break;

            case 'subcategory' :

                $id = $this->request->s;

                foreach ($tags as &$tag) {

                    $data = $this->getSubcategoryValuation($client->id, $tag->id);

                    foreach ($data['subcategories'] as $subcategory) {

                        if( $subcategory['subcategory_id'] == $id ){

                            $tag->valuation = $subcategory;
                            $tag->data = [
                                'itens' => $this->getItemValuation($client->id, $tag->id, $id)
                            ];

                            CONTINUE 2;

                        }

                    }

                }

                break;

            case 'brand' :

                $id = $this->request->b;

                $subcategory_id = $this->request->s;

                foreach ($tags as &$tag) {

                    $data = $this->getBrandValuation($client->id, $tag->id, $subcategory_id);

                    foreach ($data['brands'] as $brand) {

                        if( $brand['brand_id'] == $id ){

                            $tag->valuation = $brand;
                            $tag->data = [
                                'subcategory' => $data['subcategory'],
                                'margin' => $data['margin'],
                                'itens' => $this->getItemValuation($client->id, $tag->id, $subcategory_id)
                            ];

                            CONTINUE 2;

                        }

                    }

                }

                break;

        }

        $first = reset($tags);
        $end = end($tags);

        $table['sku'] = ($first->valuation['sku'] > 0) ? $end->valuation['sku'] - $first->valuation['sku'] : 0;

        $table['add'] = ($first->valuation['sku'] <= 0) ? 1 : 0;
        $table['removed'] = ($end->valuation['sku'] <= 0) ? 1 : 0;

        if($first->valuation['total_revenue'] > 0)
            $table['total_revenue'] = (($end->valuation['total_revenue'] * 100) / $first->valuation['total_revenue']) - 100;
        else
            $table['total_revenue'] = 100;

        if($first->valuation['total_gross_margin'] > 0)
            $table['total_gross_margin'] = (($end->valuation['total_gross_margin'] * 100) / $first->valuation['total_gross_margin']) - 100;
        else
            $table['total_gross_margin'] = 100;

        if($first->valuation['participation'] > 0)
            $table['participation'] = (($end->valuation['participation'] * 100) / $first->valuation['participation']) - 100;
        else
            $table['participation'] = 100;

        $table['info'] = [];

        //if($type == 'subcategory') {

            //$table['itens'] = [];

            if(!empty($first->data['itens']['itens']) && !empty($end->data['itens']['itens'])){

                $table['itens'] = [
                    'first' => [],
                    'end' => [],
                    'remove' => []
                ];

                foreach($first->data['itens']['itens'] as $item_first){

                    if($type == 'subcategory') {

                        $table['itens']['first'][$item_first['product_id']] = $item_first['product_id'];

                        if ($item_first['recomendation']['status'] == 0 && $item_first['recomendation']['X'] == 1)
                            $table['itens']['remove'][$item_first['product_id']] = $item_first['product_id'];

                    }

                    if($type == 'brand') {

                        if($item_first['brand_id'] == $id){

                            $table['itens']['first'][$item_first['product_id']] = $item_first['product_id'];

                            if ($item_first['recomendation']['status'] == 0 && $item_first['recomendation']['X'] == 1)
                                $table['itens']['remove'][$item_first['product_id']] = $item_first['product_id'];

                        }

                    }

                }

                foreach ($end->data['itens']['itens'] as $item_end) {

                    if($type == 'subcategory') {

                        $table['itens']['end'][$item_end['product_id']] = $item_end['product_id'];

                    }

                    if($type == 'brand') {

                        if($item_end['brand_id'] == $id){

                            $table['itens']['end'][$item_end['product_id']] = $item_end['product_id'];

                        }

                    }

                }

                $table['info']['itens']['removed'] = array_diff($table['itens']['first'], $table['itens']['end']);
                $table['info']['itens']['added'] = array_diff($table['itens']['end'], $table['itens']['first']);
                $table['info']['itens']['not_removed'] = array_intersect($table['itens']['remove'], $table['itens']['end']);
                $table['info']['itens']['yes_removed'] = array_diff($table['itens']['remove'], $table['itens']['end']);

            }

        //}

        if($type == 'brand') {

            $table['subcategory'] = (isset($first->data['subcategory'])) ? $first->data['subcategory'] : $end->data['subcategory'];

            if(!$table['add'] || !$table['removed']) {

                $table['info']['brand_status'] = ($end->valuation['sku'] > 0) ? $first->valuation['status'] : 2;

                if ($first->valuation['percentage_gross_margin'] < $first->data['margin']) // margem menor
                    if ($end->valuation['percentage_gross_margin'] < $end->data['margin'])
                        $table['info']['margin'] = 0; // margem continua abaixo da media
                    else
                        $table['info']['margin'] = 1; // margem passou a ficar acima da media
                elseif ($first->valuation['percentage_gross_margin'] > $first->data['margin'] && $end->valuation['sku'] > 0) // margem maior
                    if ($end->valuation['percentage_gross_margin'] > $end->data['margin'])
                        $table['info']['margin'] = 2; // estava acima e conseguiu aumentar a margem
                    else
                        $table['info']['margin'] = 3; // margem estava acima da media e baixou
                else
                    $table['info']['margin'] = null; // margem estagnada

            } else {

                $table['info']['brand_status'] = ($end->valuation['sku'] > 0) ? 3 : 2;

            }

        }

        return $this->view('assortment.monitoring.details', [
            'client' => $client,
            'tags' => $tags,
            'tags_url' => $tags_url,
            'table' => $table,
            'type' => $type
        ]);

    }

    public function clientComparison(){

        $clients = $this->user->assortmentClients();

        if($clients->count() == 0)
            return Redirect::route('sites.assortment.about');

        return $this->view('assortment.comparison.client', [
            'clients' => $clients->get()
        ]);

    }

    public function periodComparison(Request $request){

        if(!$request->has('clients_id')){

            flash("Nenhuma data selecionada");
            return Redirect::route('sites.assortment.comparison.client');

        } else {

            $clients_id = $request->input('clients_id');

            $clients = $this->user->assortmentClients()
                ->whereIn('id', $clients_id)
                ->lists('name', 'id')
                ->toArray();

            $dates = [];

            foreach ($clients_id as $id){

                $dates[] = Product::where('client_id', $id)
                    ->groupBy('date')
                    ->lists('date', 'date')
                    ->toArray();

            }

            if(count($clients_id) > 1)
                $dates = call_user_func_array('array_intersect',$dates);
            else
                $dates = $dates[0];

            $period = [];

            foreach ($dates as $date)
                $period[date('Y', strtotime($date))][] = $date;

            return $this->view('assortment.comparison.period', [
                'dates' => $period,
                'clients_selected' => $clients
            ]);

        }
    }

    public function processComparison(Request $request){

        if(!$request->has('clients_id') || !$request->has('dates')){

            flash("Nenhum cliente ou período encontrado");
            return Redirect::route('sites.assortment.comparison.client');

        } else {

            $clients_id = implode('.', $request->input('clients_id'));
            $dates = implode('.', $request->input('dates'));

            return Redirect::route('sites.assortment.comparison.subcategories',[
                base64_encode($clients_id),
                base64_encode($dates)
            ]);

        }

    }

    public function categoriesComparison($clients_id, $dates){

        $parameters['clients'] = $clients_id;
        $parameters['dates'] = $dates;

        $clients_id = explode('.', base64_decode($clients_id));
        $dates = explode('.', base64_decode($dates));

        $clients = $this->user->assortmentClients()
            ->whereIn('id', $clients_id)
            ->lists('name', 'id')
            ->toArray();

        $data = [];

        foreach ($clients as $id => $name)
            $data[$id] = $this->getCategoryValuation($id, null, $dates);

        return $this->view('assortment.comparison.categories', [
            'dates' => $dates,
            'clients_selected' => $clients,
            'data' => $data,
            'parameters' => $parameters
        ]);

    }

    public function subcategoriesComparison($clients_id, $dates){

        $parameters['clients'] = $clients_id;
        $parameters['dates'] = $dates;

        $clients_id = explode('.', base64_decode($clients_id));
        $dates = explode('.', base64_decode($dates));

        $clients = $this->user->assortmentClients()
            ->whereIn('id', $clients_id)
            ->lists('name', 'id')
            ->toArray();

        $data = [];

        foreach ($clients as $id => $name)
            $data[$id]['tag'] = $this->getTotalTag($id, null, $dates);

        return $this->view('assortment.comparison.subcategories', [
            'dates' => $dates,
            'clients_selected' => $clients,
            'data' => $data,
            'parameters' => $parameters
        ]);

    }

    public function subcategoriesByClientComparison($clients_id, $dates){

        $clients_id = explode('.', base64_decode($clients_id));
        $dates = explode('.', base64_decode($dates));

        $client_id = ($this->request->c) ? $this->request->c : null;

        $data = $this->getSubcategoryValuation($client_id, null, $dates);

        return $this->view('assortment.comparison.subcategories_by_client', $data);

    }

    public function subcategoriesDetailsComparison($clients_id, $dates){

        $clients_id = explode('.', base64_decode($clients_id));
        $dates = explode('.', base64_decode($dates));

        $client_id = ($this->request->c) ? $this->request->c : null;
        $subcategory_name = ($this->request->s) ? $this->request->s : null;

        $clients = $this->user->assortmentClients()
            ->whereIn('id', $clients_id)
            ->lists('name', 'id')
            ->toArray();

        if($subcategory_name){

            $valuations = [];

            foreach ($clients as $id => $name){

                $valuations[$id] = $this->getSubcategoryValuation($id, null, $dates);

            }

            $results = [];

            $current = [];

            foreach ($valuations as $id => $valuation){

                foreach ($valuation['subcategories'] as $subcategory){

                    if($subcategory['subcategory'] == $subcategory_name){

                        $subcategory['client'] = $valuation['client'];

                        $results[] = $subcategory;

                        if($id == $client_id)
                            $current = $subcategory;

                    }

                }

            }

            $results = collect($results)->sortBy('total_gross_margin');

            $avg['amount'] = $results->avg('amount');
            $avg['percentage_gross_margin'] = $results->avg('percentage_gross_margin');
            $avg['participation'] = $results->avg('participation');
            $avg['sku'] = $results->avg('sku');
            $avg['total_gross_margin'] = $results->avg('total_gross_margin');
            $avg['total_gross_margin_alt'] = $results->avg('total_gross_margin_alt');
            $avg['total_revenue'] = $results->avg('total_revenue');
            $avg['total_unit_cost'] = $results->avg('total_unit_cost');

            $min = $results->first();
            $max = $results->last();

            $data = [
                'presence_percentage' => ($results->count() * 100) / count($clients_id),
                'presence_count' => $results->count(),
                'current' => $current,
                'avg' => $avg,
                'min' => $min,
                'max' => $max
            ];

            return $this->view('assortment.comparison.subcategories_details', $data);

        } else {

            return 'Nenhum dado encontrado.';

        }

    }

    public function brandsComparison($clients_id, $dates){

        $parameters['clients'] = $clients_id;
        $parameters['dates'] = $dates;

        $clients_id = explode('.', base64_decode($clients_id));
        $dates = explode('.', base64_decode($dates));

        $subcategory_name = ($this->request->s) ? $this->request->s : null;

        $clients = $this->user->assortmentClients()
            ->whereIn('id', $clients_id)
            ->lists('name', 'id')
            ->toArray();

        $categories = Product::join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
            ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
            ->where('assortment_categories.perishable', 0)
            ->whereIn('assortment_products.client_id', array_keys($clients))
            ->whereIn('assortment_products.date', $dates)
            ->groupBy('assortment_categories.name')
            ->orderBy('assortment_categories.name')
            ->lists('assortment_categories.name', 'assortment_categories.name')
            ->toArray();

        $subcategories_grouped = [];

        foreach ($categories as $key => $category_name){

            $category_name = trim($category_name);

            $subcategories_grouped[$category_name] = Product::join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
                ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
                ->where('assortment_categories.perishable', 0)
                ->whereIn('assortment_products.client_id', array_keys($clients))
                ->whereIn('assortment_products.date', $dates)
                ->where('assortment_categories.name', 'LIKE', $category_name)
                ->groupBy('assortment_subcategories.name')
                ->orderBy('assortment_subcategories.name')
                ->lists('assortment_subcategories.name', 'assortment_subcategories.name')
                ->toArray();

        }

        return $this->view('assortment.comparison.brands', [
            'dates' => $dates,
            'clients_selected' => $clients,
            'parameters' => $parameters,
            'subcategories_grouped' => $subcategories_grouped,
            'subcategory_name' => $subcategory_name,
        ]);

    }

    public function brandsByClientComparison($clients_id, $dates){

        $clients_id = explode('.', base64_decode($clients_id));
        $dates = explode('.', base64_decode($dates));

        $client_id = ($this->request->c) ? $this->request->c : null;
        $subcategory_name = ($this->request->s) ? $this->request->s : null;

        if($subcategory_name){

            $subcategory = Subcategory::select('assortment_subcategories.*')
                ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
                ->where('assortment_subcategories.name', 'LIKE', $subcategory_name)
                ->where('assortment_categories.client_id', $client_id)
                ->first();

            if($subcategory) {

                $data = $this->getBrandValuation($client_id, null, $subcategory->id, $dates);

                return $this->view('assortment.comparison.brands_by_client', $data);

            } else {

                return 'SUBCATEGORIA INEXISTENTE';

            }


        } else {

            return 'NENHUM DADO ENCONTRADO';

        }

    }

    public function brandsDetailsComparison($clients_id, $dates){

        $clients_id = explode('.', base64_decode($clients_id));
        $dates = explode('.', base64_decode($dates));

        $client_id = ($this->request->c) ? $this->request->c : null;
        $subcategory_name = ($this->request->s) ? $this->request->s : null;
        $brand_name = ($this->request->b) ? $this->request->b : null;

        $clients = $this->user->assortmentClients()
            ->whereIn('id', $clients_id)
            ->lists('name', 'id')
            ->toArray();

        if($brand_name && $subcategory_name){

            $valuations = [];

            foreach ($clients as $id => $name){

                $subcategory = Subcategory::select('assortment_subcategories.*')
                    ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
                    ->where('assortment_subcategories.name', 'LIKE', $subcategory_name)
                    ->where('assortment_categories.client_id', $id)
                    ->first();

                if($subcategory) {

                    $valuations[$id] = $this->getBrandValuation($id, null, $subcategory->id, $dates);

                }

            }

            $results = [];

            $current = [];

            foreach ($valuations as $id => $valuation){

                foreach ($valuation['brands'] as $brand){

                    if($brand['brand'] == $brand_name){

                        $brand['client'] = $valuation['client'];

                        $results[] = $brand;

                        if($id == $client_id)
                            $current = $brand;

                    }

                }

            }

            $results = collect($results)->sortBy('total_gross_margin');

            $avg['amount'] = $results->avg('amount');
            $avg['percentage_gross_margin'] = $results->avg('percentage_gross_margin');
            $avg['participation'] = $results->avg('participation');
            $avg['sku'] = $results->avg('sku');
            $avg['total_gross_margin'] = $results->avg('total_gross_margin');
            $avg['total_gross_margin_alt'] = $results->avg('total_gross_margin_alt');
            $avg['total_revenue'] = $results->avg('total_revenue');
            $avg['total_unit_cost'] = $results->avg('total_unit_cost');

            $min = $results->first();
            $max = $results->last();

            $data = [
                'presence_percentage' => ($results->count() * 100) / count($clients_id),
                'presence_count' => $results->count(),
                'current' => $current,
                'avg' => $avg,
                'min' => $min,
                'max' => $max
            ];

            return $this->view('assortment.comparison.brands_details', $data);

        } else {

            return 'Nenhum dado encontrado.';

        }

    }

    public function itemsComparison($clients_id, $dates){

        $parameters['clients'] = $clients_id;
        $parameters['dates'] = $dates;

        $clients_id = explode('.', base64_decode($clients_id));
        $dates = explode('.', base64_decode($dates));

        $subcategory_name = ($this->request->s) ? $this->request->s : null;

        $clients = $this->user->assortmentClients()
            ->whereIn('id', $clients_id)
            ->lists('name', 'id')
            ->toArray();

        $categories = Product::join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
            ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
            ->where('assortment_categories.perishable', 0)
            ->whereIn('assortment_products.client_id', array_keys($clients))
            ->whereIn('assortment_products.date', $dates)
            ->groupBy('assortment_categories.name')
            ->orderBy('assortment_categories.name')
            ->lists('assortment_categories.name', 'assortment_categories.name')
            ->toArray();

        $subcategories_grouped = [];

        foreach ($categories as $key => $category_name){

            $category_name = trim($category_name);

            $subcategories_grouped[$category_name] = Product::join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
                ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
                ->where('assortment_categories.perishable', 0)
                ->whereIn('assortment_products.client_id', array_keys($clients))
                ->whereIn('assortment_products.date', $dates)
                ->where('assortment_categories.name', 'LIKE', $category_name)
                ->groupBy('assortment_subcategories.name')
                ->orderBy('assortment_subcategories.name')
                ->lists('assortment_subcategories.name', 'assortment_subcategories.name')
                ->toArray();

        }

        return $this->view('assortment.comparison.items', [
            'dates' => $dates,
            'clients_selected' => $clients,
            'parameters' => $parameters,
            'subcategories_grouped' => $subcategories_grouped,
            'subcategory_name' => $subcategory_name,
        ]);

    }

    public function itemsByClientComparison($clients_id, $dates){

        $clients_id = explode('.', base64_decode($clients_id));
        $dates = explode('.', base64_decode($dates));

        $client_id = ($this->request->c) ? $this->request->c : null;
        $subcategory_name = ($this->request->s) ? $this->request->s : null;

        if($subcategory_name){

            $subcategory = Subcategory::select('assortment_subcategories.*')
                ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
                ->where('assortment_subcategories.name', 'LIKE', $subcategory_name)
                ->where('assortment_categories.client_id', $client_id)
                ->first();

            if($subcategory) {

                $data = $this->getItemValuation($client_id, null, $subcategory->id, $dates);

                return $this->view('assortment.comparison.items_by_client', $data);

            } else {

                return 'SUBCATEGORIA INEXISTENTE';

            }


        } else {

            return 'NENHUM DADO ENCONTRADO';

        }

    }

    public function itemsDetailsComparison($clients_id, $dates){

        $clients_id = explode('.', base64_decode($clients_id));
        $dates = explode('.', base64_decode($dates));

        $client_id = ($this->request->c) ? $this->request->c : null;
        $subcategory_name = ($this->request->s) ? $this->request->s : null;
        $item_name = ($this->request->i) ? $this->request->i : null;

        $clients = $this->user->assortmentClients()
            ->whereIn('id', $clients_id)
            ->lists('name', 'id')
            ->toArray();

        if($item_name && $subcategory_name){

            $valuations = [];

            foreach ($clients as $id => $name){

                $subcategory = Subcategory::select('assortment_subcategories.*')
                    ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
                    ->where('assortment_subcategories.name', 'LIKE', $subcategory_name)
                    ->where('assortment_categories.client_id', $id)
                    ->first();

                if($subcategory) {

                    $valuations[$id] = $this->getItemValuation($id, null, $subcategory->id, $dates);

                }

            }

            $results = [];

            $current = [];

            foreach ($valuations as $id => $valuation){

                foreach ($valuation['itens'] as $item){

                    if($item['ean'] == $item_name){

                        $item['client'] = $valuation['client'];

                        $results[] = $item;

                        if($id == $client_id)
                            $current = $item;

                    }

                }

            }

            $results = collect($results)->sortBy('total_gross_margin');

            $avg['amount'] = $results->avg('amount');
            $avg['percentage'] = $results->avg('percentage');
            $avg['participation'] = $results->avg('participation');
            $avg['unit_price'] = $results->avg('unit_price');
            $avg['total_gross_margin'] = $results->avg('total_gross_margin');
            $avg['total_gross_margin_alt'] = $results->avg('total_gross_margin_alt');
            $avg['total_revenue'] = $results->avg('total_revenue');
            $avg['total_unit_cost'] = $results->avg('total_unit_cost');

            $min = $results->first();
            $max = $results->last();

            $data = [
                'presence_percentage' => ($results->count() * 100) / count($clients_id),
                'presence_count' => $results->count(),
                'current' => $current,
                'avg' => $avg,
                'min' => $min,
                'max' => $max
            ];

            return $this->view('assortment.comparison.items_details', $data);

        } else {

            return 'Nenhum dado encontrado.';

        }

    }

    protected function getCategoryValuation($client_id, $tag_id = null, $dates = null)
    {

        // dados do cliente

        $client = Client::find($client_id);

        // dados do periodo

        if(!empty($tag_id)) {

            $tag = Tag::find($tag_id);

            $dates = $tag->getDate($tag_id);

        } else {

            $tag = null;

        }

        if( !$client || !$dates )
            return false;

        $count_dates = count($dates);

        // sortimento

        $itens = Product::select([
                'assortment_categories.id AS category_id',
                'assortment_categories.name AS category',
                'assortment_categories.perishable AS perishable',
                DB::raw('COUNT( DISTINCT assortment_products.product_id) AS sku'),
                DB::raw('SUM(assortment_products.total_gross_margin) / ' . $count_dates . ' AS total_gross_margin'),
                DB::raw('SUM(assortment_products.total_revenue) / ' . $count_dates . ' AS total_revenue'),
                //DB::raw('AVG(assortment_products.total_gross_margin) AS total_gross_margin'),
            ])
            ->join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
            ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
            ->join('product_products', 'product_products.id', '=', 'assortment_products.product_id')
            ->where('assortment_products.client_id', $client->id)
            ->whereIn('assortment_products.date', $dates)
            ->groupBy('assortment_categories.id')
            ->orderBy('total_gross_margin', 'desc')
            ->get()
            ->toArray();

        $total_margin = 0;

        foreach($itens as $item) {

            $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

            $item['percentage_gross_margin'] = $this->getPercentageGrossMargin($item['total_gross_margin'], $item['total_revenue']);

            if($item['percentage_gross_margin'] > 75) {
                $item['total_gross_margin_alt'] = $item['total_gross_margin_alt'] * 0.35;
            }

            $total_margin += $item['total_gross_margin_alt'];

        }

        $cumulative_margin = 0;
        $table = $this->table;

        foreach($itens as &$item){

            $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

            $item['percentage_gross_margin'] = $this->getPercentageGrossMargin($item['total_gross_margin'], $item['total_revenue']);

            if($item['percentage_gross_margin'] > 75) {
                $item['percentage_gross_margin'] = 35;
                $item['total_gross_margin'] = $item['total_gross_margin'] * 0.35;
                $item['total_gross_margin_alt'] = $item['total_gross_margin_alt'] * 0.35;
            }

            $item['participation'] = $this->getParticipation($item['total_gross_margin_alt'], $total_margin);

            $cumulative_margin += $item['participation'];

            $item['acumulado'] = $cumulative_margin;

            if($cumulative_margin <= 50 || $item['participation'] >= 50)
            {
                $group = 'A';

                $item['group'] = $group;
                $table[$group]['category']++;
                $table[$group]['sku'] += $item['sku'];
            }
            elseif($cumulative_margin > 50 && $cumulative_margin <= 75)
            {
                $group = 'B';

                $item['group'] = $group;
                $table[$group]['category']++;
                $table[$group]['sku'] += $item['sku'];
            }
            elseif($cumulative_margin > 75 && $cumulative_margin <= 90)
            {
                $group = 'C';

                $item['group'] = $group;
                $table[$group]['category']++;
                $table[$group]['sku'] += $item['sku'];
            }
            else
            {
                $group = 'D';

                $item['group'] = $group;
                $table[$group]['category']++;
                $table[$group]['sku'] += $item['sku'];
            }

        }

        $itens = collect($itens)->sortByDesc('total_gross_margin_alt')->values();

        $count = count($itens);

        $table['A']['percentage'] = number_format(($table['A']['category']/$count) * 100, 0);
        $table['B']['percentage'] = number_format(($table['B']['category']/$count) * 100, 0);
        $table['C']['percentage'] = number_format(($table['C']['category']/$count) * 100, 0);
        $table['D']['percentage'] = number_format(($table['D']['category']/$count) * 100, 0);

        return [
            'client' => $client,
            'tag' => $tag,
            'dates' => $dates,
            'categories' => $itens,
            'table' => $table
        ];
    }

    protected function getSubcategoryValuation($client_id, $tag_id = null, $dates = null)
    {

        // dados do cliente

        $client = Client::find($client_id);

        // dados do periodo

        if(!empty($tag_id)) {

            $tag = Tag::find($tag_id);

            $dates = $tag->getDate($tag_id);

        } else {

            $tag = null;

        }

        if( !$client || !$dates )
            return false;

        $count_dates = count($dates);

        // sortimento

        $itens = Product::select([
                'assortment_subcategories.id AS subcategory_id',
                'assortment_subcategories.name AS subcategory',
                'assortment_categories.perishable AS perishable',
                DB::raw('COUNT( DISTINCT assortment_products.product_id) AS sku'),
                DB::raw('SUM(assortment_products.amount) / ' . $count_dates . ' AS amount'),
                DB::raw('SUM(assortment_products.total_gross_margin) / ' . $count_dates . ' AS total_gross_margin'),
                DB::raw('SUM(assortment_products.total_revenue) / ' . $count_dates . ' AS total_revenue'),
                //DB::raw('AVG(assortment_products.total_gross_margin) AS total_gross_margin'),
            ])
            ->join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
            ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
            ->where('assortment_products.client_id', $client->id)
            ->whereIn('assortment_products.date', $dates)
            ->groupBy('assortment_subcategories.id')
            ->orderBy('total_gross_margin', 'desc')
            ->get()
            ->toArray();

        $total_margin = 0;

        foreach($itens as $item) {

            $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

            $item['percentage_gross_margin'] = $this->getPercentageGrossMargin($item['total_gross_margin'], $item['total_revenue']);

            if($item['percentage_gross_margin'] > 75) {
                $item['total_gross_margin_alt'] = $item['total_gross_margin_alt'] * 0.35;
            }

            $total_margin += $item['total_gross_margin_alt'];

        }

        $cumulative_margin = 0;
        $table = $this->table;

        foreach($itens as &$item){

            $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

            $item['percentage_gross_margin'] = $this->getPercentageGrossMargin($item['total_gross_margin'], $item['total_revenue']);

            if($item['percentage_gross_margin'] > 75) {
                $item['percentage_gross_margin'] = 35;
                $item['total_gross_margin'] = $item['total_gross_margin'] * 0.35;
                $item['total_gross_margin_alt'] = $item['total_gross_margin_alt'] * 0.35;
            }

            $item['participation'] = $this->getParticipation($item['total_gross_margin_alt'], $total_margin);

            $cumulative_margin += $item['participation'];

            $item['acumulado'] = $cumulative_margin;

            if($cumulative_margin <= 50 || $item['participation'] >= 50)
            {
                $group = 'A';

                $item['group'] = $group;
                $table[$group]['subcategory']++;
                $table[$group]['sku'] += $item['sku'];
            }
            elseif($cumulative_margin > 50 && $cumulative_margin <= 75)
            {
                $group = 'B';

                $item['group'] = $group;
                $table[$group]['subcategory']++;
                $table[$group]['sku'] += $item['sku'];
            }
            elseif($cumulative_margin > 75 && $cumulative_margin <= 90)
            {
                $group = 'C';

                $item['group'] = $group;
                $table[$group]['subcategory']++;
                $table[$group]['sku'] += $item['sku'];
            }
            else
            {
                $group = 'D';

                $item['group'] = $group;
                $table[$group]['subcategory']++;
                $table[$group]['sku'] += $item['sku'];
            }

        }

        $itens = collect($itens)->sortByDesc('total_gross_margin_alt')->values();

        $count = count($itens);

        $table['A']['percentage'] = number_format(($table['A']['subcategory']/$count) * 100, 0);
        $table['B']['percentage'] = number_format(($table['B']['subcategory']/$count) * 100, 0);
        $table['C']['percentage'] = number_format(($table['C']['subcategory']/$count) * 100, 0);
        $table['D']['percentage'] = number_format(($table['D']['subcategory']/$count) * 100, 0);

        return [
            'client' => $client,
            'tag' => $tag,
            'dates' => $dates,
            'subcategories' => $itens,
            'table' => $table
        ];
    }

    protected function getCompositionCategory($client_id, $tag_id, $category_id)
    {

        // dados do cliente

        $client = Client::find($client_id);

        // dados do periodo

        $tag = Tag::find($tag_id);

        if( !$client || !$tag )
            return false;

        // dados da categoria

        $category = Category::where('id', $category_id)
            ->first();

        // grupos da lista de subcategorias

        $groups = $this->getGroupSubcategories($client->id, $tag->id);

        // sortimento

        $itens = Product::select([
                'assortment_subcategories.id AS subcategory_id',
                'assortment_subcategories.name AS subcategory',
                'assortment_categories.id AS category_id',
                'assortment_categories.perishable AS perishable',
                DB::raw('COUNT( DISTINCT assortment_products.product_id) AS sku'),
                DB::raw('SUM(assortment_products.total_gross_margin) / ' . count($tag->getDate($tag_id)) . ' AS total_gross_margin'),
                DB::raw('SUM(assortment_products.total_revenue) / ' . count($tag->getDate($tag_id)) . ' AS total_revenue'),
            ])
            ->join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
            ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
            ->where('assortment_products.client_id', $client->id)
            ->where('assortment_categories.id', $category->id)
            ->whereIn('assortment_products.date', $tag->getDate($tag_id))
            ->groupBy('assortment_subcategories.id')
            ->orderBy('total_gross_margin', 'desc')
            ->get()
            ->toArray();

        $total_margin = 0;

        foreach($itens as $item) {

            $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

            $item['percentage_gross_margin'] = $this->getPercentageGrossMargin($item['total_gross_margin'], $item['total_revenue']);

            if($item['percentage_gross_margin'] > 75)
                $item['total_gross_margin_alt'] = $item['total_gross_margin_alt'] * 0.35;

            $total_margin += $item['total_gross_margin_alt'];

        }

        $table = $this->table;
        $count = 0;

        foreach($itens as &$item){

            $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

            $item['percentage_gross_margin'] = $this->getPercentageGrossMargin($item['total_gross_margin'], $item['total_revenue']);

            if($item['percentage_gross_margin'] > 75)
                $item['total_gross_margin_alt'] = $item['total_gross_margin_alt'] * 0.35;

            $item['percentage'] = $this->getParticipation($item['total_gross_margin_alt'], $total_margin);

            $item['products'] = Product::where('subcategory_id', $item['subcategory_id'])
                ->whereIn('date', $tag->getDate($tag_id))
                ->where('client_id', $client->id)
                ->groupBy('product_id')
                ->lists('product_id', 'product_id');

            $item['brands'] = Brand::whereIn('product_products.id', $item['products'])
                ->join('product_products', 'product_products.brand_id', '=', 'product_brands.id')
                ->lists('product_brands.name', 'brand_id');

            if(!array_key_exists($item['subcategory_id'], $groups))
                $groups[$item['subcategory_id']] = '';

            $item['classification'] = $groups[$item['subcategory_id']];

            $table[$item['classification']]['sku'] += $item['sku'];

            $count += $item['sku'];

        }

        $itens = collect($itens)->sortByDesc('total_gross_margin_alt')->values();

        $table['A']['percentage'] = number_format(($table['A']['sku']/$count) * 100, 0);
        $table['B']['percentage'] = number_format(($table['B']['sku']/$count) * 100, 0);
        $table['C']['percentage'] = number_format(($table['C']['sku']/$count) * 100, 0);
        $table['D']['percentage'] = number_format(($table['D']['sku']/$count) * 100, 0);

        return [
            'client' => $client,
            'tag' => $tag,
            'category_id' => $category_id,
            'category' => $category,
            //'categories' => $categories,
            'subcategories' => $itens,
            'table' => $table
        ];
    }

    protected function getBrandValuation($client_id, $tag_id = null, $subcategory_id = null, $dates = null)
    {

        // dados do cliente

        $client = Client::find($client_id);

        // dados do periodo

        if(!empty($tag_id)){

            $tag = Tag::find($tag_id);

            $dates = $tag->getDate($tag_id);

        } else {

            $tag = null;
        }

        if( !$client || !$dates )
            return false;

        $count_dates = count($dates);

        // lista de subcategorias

        $subcategories = Product::join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
            ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
            ->where('assortment_categories.perishable', 0)
            ->where('assortment_products.client_id', $client->id)
            ->whereIn('assortment_products.date', $dates)
            ->groupBy('assortment_subcategories.id')
            ->orderBy('assortment_subcategories.name')
            ->lists('assortment_subcategories.name', 'assortment_subcategories.id');

        $subcategories_grouped = $this->getSubcategoriesGrouped($client_id, $tag_id, 0, null, $dates);

        $data = [
            'client' => $client,
            'tag' => $tag,
            'dates' => $dates,
            'subcategories' => $subcategories,
            'subcategories_grouped' => $subcategories_grouped,
            'subcategory_id' => $subcategory_id,
        ];

        if($subcategory_id) {

            // dados da subcategoria selecionada

            $subcategory = Subcategory::where('id', $subcategory_id)
                ->first();

            // grupos da lista de subcategorias

            $groups = $this->getGroupSubcategories($client->id, $tag_id, $dates);

            // sortimento

            $itens = Product::select([
                    'product_brands.id AS brand_id',
                    'product_brands.name AS brand',
                    DB::raw('COUNT( DISTINCT assortment_products.product_id) AS sku'),
                    DB::raw('SUM(assortment_products.amount) / ' . $count_dates . ' AS amount'),
                    DB::raw('SUM(assortment_products.total_gross_margin) / ' . $count_dates . ' AS total_gross_margin'),
                    DB::raw('SUM(assortment_products.total_revenue) / ' . $count_dates . ' AS total_revenue'),
                    DB::raw('SUM(assortment_products.unit_cost) / ' . $count_dates . ' AS total_unit_cost')
                ])
                ->join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
                ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
                ->join('product_products', 'product_products.id', '=', 'assortment_products.product_id')
                ->join('product_brands', 'product_brands.id', '=', 'product_products.brand_id')
                ->where('assortment_products.client_id', $client->id)
                ->where('assortment_subcategories.id', $subcategory_id)
                ->whereIn('assortment_products.date', $dates)
                ->groupBy('product_brands.id')
                ->orderBy('total_gross_margin', 'desc')
                ->get()
                ->toArray();

            $total = 0;

            foreach($itens as $item) {

                $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

                $item['percentage_gross_margin'] = $this->getPercentageGrossMargin($item['total_gross_margin'], $item['total_revenue']);

                if($item['percentage_gross_margin'] > 75)
                    $item['total_gross_margin_alt'] = $item['total_gross_margin_alt'] * 0.35;

                $total += $item['total_gross_margin_alt'];
            }

            $sku = 0;
            $amount = 0;
            $total_gross_margin = 0;
            $total_revenue = 0;

            $cumulative_margin = 0;

            $count = count($itens);

            foreach ($itens as $key => &$item) {

                $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

                $item['percentage_gross_margin'] = $this->getPercentageGrossMargin($item['total_gross_margin'], $item['total_revenue']);

                if($item['percentage_gross_margin'] > 75) {
                    $item['percentage_gross_margin'] = 35;
                    $item['total_gross_margin'] = $item['total_gross_margin'] * 0.35;
                    $item['total_gross_margin_alt'] = $item['total_gross_margin_alt'] * 0.35;
                }

                $sku += $item['sku'];
                $amount += $item['amount'];
                $total_gross_margin += $item['total_gross_margin'];
                $total_revenue += $item['total_revenue'];

                $item['participation'] = $this->getParticipation($item['total_gross_margin_alt'], $total);

                $item['acumulado'] = $cumulative_margin += ( $item['total_gross_margin_alt'] / $total );

                $item['status'] = ($count < 3) ? 1 : ($item['participation'] >= 10) ? 1 : ($cumulative_margin < 0.95) ? 1 : 0 ;

                $item['working_capital'] = ($item['status']) ? 0 : $this->getWorkingCapital($item['amount'], $item['total_unit_cost']) ;

            }

            $itens = collect($itens)->sortByDesc('total_gross_margin_alt')->values();

            $subcategory->sku = $sku;
            $subcategory->amount = $amount;
            $subcategory->total_gross_margin = $total_gross_margin;
            $subcategory->total_revenue = $total_revenue;

            $data = array_merge([
                'subcategory' => $subcategory,
                'margin' => ($total_revenue) ? ($total_gross_margin / $total_revenue * 100) : 0,
                'classification' => (array_key_exists($subcategory_id, $groups)) ? $groups[$subcategory_id] : null,
                'brands' => $itens,
                'working_capital' => $itens->sum('working_capital'),
            ], $data);

        }

        return $data;

    }

    protected function getItemValuation($client_id, $tag_id = null, $subcategory_id = null, $dates = null)
    {

        // dados do cliente

        $client = Client::find($client_id);

        // dados do periodo

        if(!empty($tag_id)){

            $tag = Tag::find($tag_id);

            $dates = $tag->getDate($tag_id);

        } else {

            $tag = null;
        }

        if( !$client || !$dates )
            return false;

        $count_dates = count($dates);

        // lista de subcategorias

        $subcategories = Product::join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
            ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
            ->where('assortment_categories.perishable', 0)
            ->where('assortment_products.client_id', $client->id)
            ->whereIn('assortment_products.date', $dates)
            ->groupBy('assortment_subcategories.id')
            ->orderBy('assortment_subcategories.name')
            ->lists('assortment_subcategories.name', 'assortment_subcategories.id');

        $subcategories_grouped = $this->getSubcategoriesGrouped($client_id, $tag_id, 0, null, $dates);

        $data = [
            'client' => $client,
            'tag' => $tag,
            'subcategory_id' => $subcategory_id,
            'subcategories' => $subcategories,
            'subcategories_grouped' => $subcategories_grouped,
        ];

        if($subcategory_id) {

            // marcas

            $brand_valuation = $this->getBrandValuation($client_id, $tag_id, $subcategory_id, $dates);

            // dados da subcategoria selecionada

            $subcategory = Subcategory::where('id', $subcategory_id)
                ->first();

            // grupos da lista de subcategorias

            $groups = $this->getGroupSubcategories($client->id, $tag_id, $dates);

            $i = 0;
            while (key($groups) != $subcategory_id) {
                next($groups);
                $i++;
            }

            $position = $i + 1;

            // sortimento

            $itens = Product::select([
                    'product_products.id AS product_id',
                    'product_products.ean',
                    'product_products.name AS product',
                    'product_brands.id AS brand_id',
                    'product_brands.name AS brand',
                    'assortment_products.unit_price',
                    DB::raw('SUM(assortment_products.total_gross_margin) / '.$count_dates.' AS total_gross_margin'),
                    DB::raw('SUM(assortment_products.total_revenue) / ' . $count_dates. ' AS total_revenue'),
                    DB::raw('SUM(assortment_products.unit_cost) / ' . $count_dates. ' AS total_unit_cost'),
                    DB::raw('SUM(assortment_products.amount) / ' . $count_dates. ' AS amount'),
                ])
                ->join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
                ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
                ->join('product_products', 'product_products.id', '=', 'assortment_products.product_id')
                ->join('product_brands', 'product_brands.id', '=', 'product_products.brand_id')
                ->where('assortment_products.client_id', $client->id)
                ->where('assortment_subcategories.id', $subcategory->id)
                ->whereIn('assortment_products.date', $dates)
                ->groupBy('product_products.id')
                ->orderBy('total_gross_margin', 'desc')
                ->get()
                ->toArray();

            $total_margin = 0;
            $total_amount = 0;
            $total_gross_margin = 0;
            $total_revenue = 0;

            foreach ($itens as $item) {

                $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

                $item['percentage_gross_margin'] = $this->getPercentageGrossMargin($item['total_gross_margin'], $item['total_revenue']);

                if($item['percentage_gross_margin'] > 75)
                    $item['total_gross_margin_alt'] = $item['total_gross_margin_alt'] * 0.35;

                $total_margin += $item['total_gross_margin_alt'];
                $total_amount += $item['amount'];
                $total_gross_margin += $item['total_gross_margin'];
                $total_revenue += $item['total_revenue'];
            }

            $cumulative_margin = 0;

            $margin_subcategory = $total_gross_margin / $total_revenue * 100;

            foreach ($itens as &$item) {

                $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

                $item['percentage'] = $this->getPercentageGrossMargin($item['total_gross_margin'], $item['total_revenue']);

                if($item['percentage'] > 75) {
                    $item['percentage'] = 35;
                    $item['total_gross_margin'] = $item['total_gross_margin'] * 0.35;
                    $item['total_gross_margin_alt'] = $item['total_gross_margin_alt'] * 0.35;
                }

                $item['participation'] = $this->getParticipation($item['total_gross_margin_alt'], $total_margin);

                $total_amount = ($total_amount) ? $total_amount : 1;

                $item['amount_participation'] = $item['amount'] / $total_amount * 100;

                $cumulative_margin += $item['participation'];

                $item['acumulado'] = $cumulative_margin;

                if($item['participation'] >= 30)
                    $item['classification'] = 'A';
                else {

                    if ($cumulative_margin <= 70)
                        $item['classification'] = 'A';
                    elseif ($cumulative_margin > 70 && $cumulative_margin <= 80)
                        $item['classification'] = 'B';
                    elseif ($cumulative_margin > 80 && $cumulative_margin <= 90)
                        $item['classification'] = 'C';
                    else
                        $item['classification'] = 'D';

                    if($item['classification'] == 'D' && $item['participation'] >= 10)
                        $item['classification'] = 'C';

                }

                $item['brand_valuation'] = $brand_valuation['brands'][array_search($item['brand'], array_column($brand_valuation['brands']->toArray(), 'brand'))];

                $r = [
                    'S' => $item['amount_participation'],
                    'T' => ($item['percentage'] < $margin_subcategory) ? 0 : 1,
                    'U' => ($item['amount_participation'] <= 1) ? 0 : 1,
                    'V' => ($item['classification'] == 'D') ? 0 : 1,
                ];

                $r['W'] = ($r['V'] == 0) ? ( ($r['U'] == 0) ? 'Avaliar Exclusão' : ( ($r['T'] == 0) ? 'Avaliar Exclusão' : ( (!$item['brand_valuation']['status']) ? 'Avaliar Exclusão' : 'Produto Ok!' ) ) ) : 1;

                $r['X'] = ($r['W'] == 1) ? ( ($r['T'] == 0) ? 'Aumentar Margem' : ( ($r['U'] == 0) ? 'Aumentar Volume' : ( (!$item['brand_valuation']['status']) ? 'Avaliar Marca' : 'Produto Ok!' ) ) ) : 1;

                $r['info'] = ($r['W'] == 1) ? $r['X'] : $r['W'];

                $r['status'] = ($r['info'] == 'Produto Ok!') ? 1 : 0;

                $item['recomendation'] = $r;

                $item['working_capital'] = ($r['W'] != 0) ? 0 : $this->getWorkingCapital($item['amount'], $item['total_unit_cost']) ;
            }

            $itens = collect($itens)->sortByDesc('total_gross_margin_alt')->values();

            $data = array_merge([
                'subcategory' => $subcategory,
                'position' => $position,
                'margin' => $margin_subcategory,
                'classification' => (array_key_exists($subcategory_id, $groups)) ? $groups[$subcategory_id] : null,
                'itens' => $itens,
                'matriz' => $this->getMatrizImportance(),
                'brand_valuation' => $brand_valuation,
                'working_capital' => $itens->sum('working_capital')
            ], $data);

        }

        return $data;

    }

    protected function getItemByImportance($client_id, $tag_id, $importance_matrix)
    {

        switch ($importance_matrix){

            case 1 :
                $group = ['A'];
                $class = ['A'];
                $importance = [
                    ['group' => 'A', 'class' => 'A']
                ];
                break;

            case 2 :
                $group = ['A', 'B'];
                $class = ['B', 'A'];
                $importance = [
                    ['group' => 'A', 'class' => 'B'],
                    ['group' => 'B', 'class' => 'A']
                ];
                break;

            case 3 :
                $group = ['A', 'B', 'C'];
                $class = ['C', 'B', 'A'];
                $importance = [
                    ['group' => 'A', 'class' => 'C'],
                    ['group' => 'B', 'class' => 'B'],
                    ['group' => 'C', 'class' => 'A']
                ];
                break;

            case 4 :
                $group = ['A', 'B', 'C', 'D'];
                $class = ['D', 'C', 'B', 'A'];
                $importance = [
                    ['group' => 'A', 'class' => 'D'],
                    ['group' => 'B', 'class' => 'C'],
                    ['group' => 'C', 'class' => 'B'],
                    ['group' => 'D', 'class' => 'A']
                ];
                break;

            case 5 :
                $group = ['B', 'C', 'D'];
                $class = ['D', 'C', 'B'];
                $importance = [
                    ['group' => 'B', 'class' => 'D'],
                    ['group' => 'C', 'class' => 'C'],
                    ['group' => 'D', 'class' => 'B']
                ];
                break;

            case 6 :
                $group = ['C', 'D'];
                $class = ['D', 'C'];
                $importance = [
                    ['group' => 'C', 'class' => 'D'],
                    ['group' => 'D', 'class' => 'C']
                ];
                break;

            default:
                $group = ['D'];
                $class = ['D'];
                $importance = [
                    ['group' => 'D', 'class' => 'D']
                ];

        }

        // dados do cliente

        $client = Client::find($client_id);

        // dados do periodo

        $tag = Tag::find($tag_id);

        if( !$client || !$tag )
            return false;

        // grupos da lista de subcategorias

        $groups = collect($this->getGroupSubcategories($client->id, $tag->id));

        $groups_alt = $groups->filter(function ($item) use ($group) {
            //return $item == $group;
            return in_array($item, $group) == true;
        });//->keys();

        $groups = $groups_alt->keys();

        $groups_alt = $groups_alt->toArray();

        //return print_r($groups_alt); die;

        // total da loja toda

        $total = $this->getTotalTag($client_id, $tag_id);

        // sortimento

        $itens = Product::select([
                'product_products.id AS product_id',
                'product_products.ean',
                'product_products.name AS product',
                'assortment_categories.id AS category_id',
                'assortment_categories.name AS category',
                'assortment_categories.perishable AS perishable',
                'assortment_subcategories.id AS subcategory_id',
                'assortment_subcategories.name AS subcategory',
                'product_brands.id AS brand_id',
                'product_brands.name AS brand',
                'assortment_products.unit_price',
                DB::raw('SUM(assortment_products.total_gross_margin) / '.count($tag->getDate($tag_id)).' AS total_gross_margin'),
                DB::raw('SUM(assortment_products.total_revenue) / ' . count($tag->getDate($tag_id)) . ' AS total_revenue'),
                DB::raw('SUM(assortment_products.unit_cost) / ' . count($tag->getDate($tag_id)) . ' AS total_unit_cost'),
                DB::raw('SUM(assortment_products.amount) / ' . count($tag->getDate($tag_id)) . ' AS amount')
            ])
            ->join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
            ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
            ->join('product_products', 'product_products.id', '=', 'assortment_products.product_id')
            ->join('product_brands', 'product_brands.id', '=', 'product_products.brand_id')
            ->where('assortment_products.client_id', $client->id)
            ->whereIn('assortment_subcategories.id', $groups->all())
            ->whereIn('assortment_products.date', $tag->getDate($tag_id))
            ->groupBy('product_products.id')
            ->orderBy('total_gross_margin', 'desc')
            ->get()
            ->toArray();

        $total_margin = [];
        $data = [];

        foreach ($itens as $item) {

            $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

            $item['percentage_gross_margin'] = $this->getPercentageGrossMargin($item['total_gross_margin'], $item['total_revenue']);

            if($item['percentage_gross_margin'] > 75)
                $item['total_gross_margin_alt'] = $item['total_gross_margin_alt'] * 0.35;

            if(!array_key_exists($item['subcategory_id'], $total_margin))
                $total_margin[$item['subcategory_id']] = 0;

            $total_margin[$item['subcategory_id']] += $item['total_gross_margin_alt'];
        }

        $total_gross_margin = 0;
        $total_revenue = 0;

        $cumulative_margin = [];

        $data['total_gross_margin'] = 0;
        $data['total_revenue'] = 0;

        $list = collect();

        foreach ($itens as $key => &$item) {

            $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

            $item['percentage'] = $this->getPercentageGrossMargin($item['total_gross_margin'], $item['total_revenue']);

            if($item['percentage'] > 75) {
                $item['percentage'] = 35;
                $item['total_gross_margin'] = $item['total_gross_margin'] * 0.35;
                $item['total_gross_margin_alt'] = $item['total_gross_margin_alt'] * 0.35;
            }

            $total_gross_margin += $item['total_gross_margin'];
            $total_revenue += $item['total_revenue'];

            $item['participation'] = $this->getParticipation($item['total_gross_margin_alt'], $total_margin[$item['subcategory_id']]);

            if(!array_key_exists($item['subcategory_id'], $cumulative_margin))
                $cumulative_margin[$item['subcategory_id']] = 0;

            $cumulative_margin[$item['subcategory_id']] += $item['participation'];

            $item['acumulado'][$item['subcategory_id']] = $cumulative_margin[$item['subcategory_id']];

            if($item['participation'] >= 30)
                $item['classification'] = 'A';
            else {

                if ($cumulative_margin[$item['subcategory_id']] <= 70)
                    $item['classification'] = 'A';
                elseif ($cumulative_margin[$item['subcategory_id']] > 70 && $cumulative_margin[$item['subcategory_id']] <= 80)
                    $item['classification'] = 'B';
                elseif ($cumulative_margin[$item['subcategory_id']] > 80 && $cumulative_margin[$item['subcategory_id']] <= 90)
                    $item['classification'] = 'C';
                else
                    $item['classification'] = 'D';

                if($item['classification'] == 'D' && $item['participation'] >= 10)
                    $item['classification'] = 'C';

            }

            //if($item['classification'] != $class)

            $item['working_capital'] = ($importance_matrix != 7) ? 0 : $this->getWorkingCapital($item['amount'], $item['total_unit_cost']);

            foreach ($importance as $i){

                if( ($groups_alt[$item['subcategory_id']] == $i['group'] && $item['classification'] == $i['class'] ) ){
                    $list->push($item);
                    $data['item'][$item['ean']] = $item['ean'];
                    $data['brand'][$item['brand_id']] = $item['brand'];
                    $data['category'][$item['category_id']] = $item['category'];
                    $data['subcategory'][$item['subcategory_id']] = $item['subcategory'];
                    $data['total_gross_margin'] += $item['total_gross_margin'];
                    $data['total_revenue'] += $item['total_revenue'];
                }

            }

        }

        $list = $list->sortByDesc('total_gross_margin_alt')->values();

        $data['percentage_revenue'] = ($data['total_revenue'] / $total['total_revenue']) * 100;
        $data['percentage_gross_margin'] = ($data['total_gross_margin'] / $total['total_gross_margin']) * 100;
        $data['working_capital'] = $list->sum('working_capital');

        return [
            'client' => $client,
            'tag' => $tag,
            'itens' => $list,//$itens,
            //'group' => $group,
            //'class' => $class,
            'data' => $data,
            'importance' => $this->getMatrizImportance()[$importance_matrix],
            'importance_matrix' => $importance_matrix
        ];
    }

    protected function getTotalTag($client_id, $tag_id, $dates = null)
    {

        if(!empty($tag_id)) {
            $tag = Tag::find($tag_id);
            $dates = $tag->getDate($tag_id);
        } else {
            $tag = null;
        }

        $count_dates = count($dates);

        $total = Product::select([
            DB::raw('COUNT( DISTINCT product_products.ean) AS sku'),
            DB::raw('SUM(assortment_products.amount) / ' . $count_dates . ' AS amount'),
            DB::raw('SUM(assortment_products.total_revenue) / ' . $count_dates . ' AS total_revenue'),
            DB::raw('SUM(assortment_products.total_cost) / ' . $count_dates . ' AS total_cost'),
            DB::raw('SUM(assortment_products.total_gross_margin) / '.$count_dates.' AS total_gross_margin')
        ])
            ->join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
            ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
            ->join('product_products', 'product_products.id', '=', 'assortment_products.product_id')
            ->join('product_brands', 'product_brands.id', '=', 'product_products.brand_id')
            ->where('assortment_products.client_id', $client_id)
            ->whereIn('assortment_products.date', $dates)
            ->orderBy('total_gross_margin', 'desc')
            ->first()
            ->toArray();

        if($total['total_revenue'] > 0) {
            $total['percentage_gross_margin'] = ($total['total_gross_margin'] * 100) / $total['total_revenue'];

            if ($total['percentage_gross_margin'] > 75) {
                $total['percentage_gross_margin'] = 35;
                $total['total_gross_margin'] = $total['total_gross_margin'] * 0.35;
            }

        }

        return $total;

    }

    public function getGroupSubcategories($client_id, $tag_id = null, $dates = null)
    {

        $client = Client::find($client_id);

        if(!empty($tag_id)) {
            $tag = Tag::find($tag_id);
            $dates = $tag->getDate($tag_id);
        } else {
            $tag = null;
        }

        $count_dates = count($dates);

        // sortimento

        $itens = Product::select([
                'assortment_subcategories.id AS subcategory_id',
                DB::raw('SUM(assortment_products.total_gross_margin) / ' . $count_dates . ' AS total_gross_margin'),
                DB::raw('SUM(assortment_products.total_revenue) / ' . $count_dates . ' AS total_revenue'),
            ])
            ->join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
            ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
            ->join('product_products', 'product_products.id', '=', 'assortment_products.product_id')
            ->join('product_brands', 'product_brands.id', '=', 'product_products.brand_id')
            ->where('assortment_products.client_id', $client->id)
            ->whereIn('assortment_products.date', $dates)
            ->groupBy('assortment_subcategories.id')
            ->orderBy('total_gross_margin', 'desc')
            ->get()
            ->toArray();

        $total_margin = 0;

        foreach($itens as $item) {

            $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

            $item['percentage_gross_margin'] = $this->getPercentageGrossMargin($item['total_gross_margin'], $item['total_revenue']);

            if($item['percentage_gross_margin'] > 75)
                $item['total_gross_margin_alt'] = $item['total_gross_margin_alt'] * 0.35;

            $total_margin += $item['total_gross_margin_alt'];
        }

        $cumulative_margin = 0;

        $group = [];

        foreach($itens as &$item){

            $item['total_gross_margin_alt'] = $this->getWeightedAverage($item['total_gross_margin'], $item['total_revenue']);

            $item['participation'] = $this->getParticipation($item['total_gross_margin_alt'], $total_margin);

            $cumulative_margin += $item['participation'];

            if($cumulative_margin <= 50 || $item['participation'] >= 50)
                $group[$item['subcategory_id']] = 'A';
            elseif($cumulative_margin > 50 && $cumulative_margin <= 75)
                $group[$item['subcategory_id']] = 'B';
            elseif($cumulative_margin > 75 && $cumulative_margin <= 90)
                $group[$item['subcategory_id']] = 'C';
            else
                $group[$item['subcategory_id']] = 'D';

        }

        return $group;
    }

    public function getMatrizImportance(){

        $msg = [
            1 => 'Extremamente Importante',
            2 => 'Muito Importante',
            3 => 'Importante',
            4 => 'Intermediário',
            5 => 'Pouco Importante',
            6 => 'Avaliar Exclusão',
            7 => 'Excluir',
        ];

        $matriz = [
            'AA' => $msg[1],
            'AB' => $msg[2],
            'AC' => $msg[3],
            'AD' => $msg[4],
            'BA' => $msg[2],
            'BB' => $msg[3],
            'BC' => $msg[4],
            'BD' => $msg[5],
            'CA' => $msg[3],
            'CB' => $msg[4],
            'CC' => $msg[5],
            'CD' => $msg[6],
            'DA' => $msg[4],
            'DB' => $msg[5],
            'DC' => $msg[6],
            'DD' => $msg[7],
            1 => $msg[1],
            2 => $msg[2],
            3 => $msg[3],
            4 => $msg[4],
            5 => $msg[5],
            6 => $msg[6],
            7 => $msg[7]
        ];

        return $matriz;

    }

    protected function getClient($id)
    {

        if(!is_numeric($id))
            $id = Hashids::decode($id)[0];

        $client = Client::find($id);

        return $client;

    }

    protected function getTag($id){

        if(!is_numeric($id))
            $id = Hashids::decode($id)[0];

        $tag = Tag::find($id);

        return $tag;

    }

    protected function getTagsClient($id){

        if(!is_numeric($id))
            $id = Hashids::decode($id)[0];

        $tags = Tag::where('client_id', $id)
            ->get();

        return $tags;

    }

    protected function getTags($tags_url)
    {

        $arr_tags = explode('-', $tags_url);

        $tags = [];

        foreach ($arr_tags as $tag)
            $tags[] = $this->getTag($tag);

        return $tags;
    }

    protected function getCategories($client_id, $tag_id, $perishable = 0)
    {

        // lista de categorias

        $tag = new Tag;

        $categories = Product::select('assortment_categories.*')
            ->join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
            ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
            ->where('assortment_categories.perishable', $perishable)
            ->where('assortment_products.client_id', $client_id)
            ->whereIn('assortment_products.date', $tag->getDate($tag_id))
            ->groupBy('assortment_categories.id')
            ->orderBy('assortment_categories.name')
            ->get();

        return $categories;

    }

    protected function getSubCategories($client_id, $tag_id = null, $perishable = 0, $category = null, $dates = null)
    {

        // lista de subcategorias

        if($tag_id !== null) {

            $tag = new Tag;

            $dates = $tag->getDate($tag_id);

        }

        $subcategories = Product::select('assortment_subcategories.*', 'assortment_categories.name AS category', 'assortment_categories.perishable')
            ->join('assortment_subcategories', 'assortment_subcategories.id', '=', 'assortment_products.subcategory_id')
            ->join('assortment_categories', 'assortment_categories.id', '=', 'assortment_subcategories.category_id')
            ->where('assortment_categories.perishable', $perishable)
            ->where('assortment_products.client_id', $client_id)
            ->whereIn('assortment_products.date', $dates)
            ->groupBy('assortment_subcategories.id')
            ->orderBy('assortment_subcategories.name')
            ->get();

        return $subcategories;

    }

    protected function getSubcategoriesGrouped($client_id, $tag_id = null, $perishable = 0, $category = null, $dates = null)
    {

        $grouped = [];

        $subcategories = $this->getSubCategories($client_id, $tag_id, $perishable, $category, $dates);

        foreach ($subcategories as $subcategory)
            $grouped[$subcategory->category][$subcategory->id] = $subcategory->name;

        ksort($grouped, SORT_STRING);

        return $grouped;

    }

    public function brandsPlanogram($client_id, $tag_id)
    {

        $subcategory_id = ($this->request->s) ? $this->request->s : null;

        return $this->view('assortment.planogram.brands', [
            'client_id' => $client_id,
            'tag_id' => $tag_id,
            'subcategory_id' => $subcategory_id
        ]);

    }

    public function brandsPlanogramGenerator($client_id, $tag_id)
    {

        $client_id = Hashids::decode($client_id)[0];
        $tag_id = Hashids::decode($tag_id)[0];

        $subcategory_id = ($this->request->s) ? $this->request->s : null;

        $shelf_size = ($this->request->shelf_size) ? $this->request->shelf_size : 1;

        $gondola = ($this->request->gondola) ? $this->request->gondola : [1 => 6];

        $shelf_size_full = array_sum($gondola) * $shelf_size;

        $data = $this->getBrandValuation($client_id, $tag_id, $subcategory_id);

        // Porcentagens

        $items = [];

        $others = [
            'id' => 0,
            'name' => 'OUTRAS',
            'participation' => 100
        ];

        $position = 0;

        foreach ($data['brands'] as $item) {

            $participation = number_format($item['participation'], 2);

            if ($item['status']) {

                $position++;

                $items[$position] = [
                    'id' => $item['brand_id'],
                    'name' => $item['brand'],
                    'position' => $position,
                    'participation' => $participation,
                    'length' => $shelf_size_full * $participation
                ];

                $others['participation'] -= $participation;

            }

        }

        if ($others['participation'] > 1){

            $others['position'] = $position + 1;
            $others['length'] = $shelf_size_full * $others['participation'];
            $items[$position+1] = $others;

        }

        // Geracao do planograma

        $planogram = Planogram::generate($items, $gondola, $shelf_size);

        foreach ($planogram['items_original'] as $item)
            $brand_size[$item['id']] = $item['length'];

        return $this->view('assortment.planogram.generated', [
            'client_id' => $client_id,
            'tag_id' => $tag_id,
            'subcategory_id' => $subcategory_id,
            'planogram' => $planogram,
            'brand_size' => $brand_size
        ]);

    }

    protected function getParticipation($total_gross_margin, $total_margin){

        if($total_margin == 0)
            return 0;

        return ($total_gross_margin / $total_margin) * 100;

    }

    protected function getPercentageGrossMargin($total_gross_margin, $total_revenue){

        if($total_revenue == 0)
            return 0;

        return ($total_gross_margin / $total_revenue) * 100;

    }

    protected function getWeightedAverage($total_gross_margin, $total_revenue){

        return (($total_gross_margin * 0.6) + ($total_revenue * 0.4) / 100 );

    }

    protected function getWorkingCapital($amount, $unit_cost){

        return ($amount * 1.33) * $unit_cost;

    }
}