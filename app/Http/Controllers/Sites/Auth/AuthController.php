<?php

namespace App\Http\Controllers\Sites\Auth;

use App\User;
use App\Mailers\AppMailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Sites\MasterController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Validator;

class AuthController extends MasterController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/';

    protected $redirectPath = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        parent::__construct(null, $request);
        $this->middleware('guest', ['except' => ['getLogout', 'getAccount', 'setAccount']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:auth_users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'site_id' => $this->site->id,
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLogin()
    {
        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        return $this->view('auth.login');
    }

    protected function getCredentials(Request $request)
    {
        return [
            'email'    => $request->input($this->loginUsername()),
            'password' => $request->input('password'),
            'verified' => true
        ];
    }

    public function getRegister()
    {
        return $this->view('auth.register');
    }

    public function postRegister(Request $request, AppMailer $mailer)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->create($request->all());

        $mailer->sendEmailConfirmationTo($user);

        flash('Por favor, confirme seu endereço de e-mail.');

        return redirect()->back();
    }

    /**
     * Confirm a user's email address.
     *
     * @param  string $token
     * @return mixed
     */
    public function confirmEmail($token)
    {
        $user = User::whereToken($token)->firstOrFail();

        User::whereToken($token)->firstOrFail()->confirmEmail();

        flash('Obrigado! Seu endereço de e-mail foi confirmado com sucesso.');

        Auth::login($user);

        return redirect($this->redirectPath());
    }

    public function getAccount()
    {

        return $this->view('auth.account', [
            'user' => Auth::user()
        ]);

    }

    public function setAccount()
    {

        $input = array_filter(Input::all());

        $id = Auth::user()->id;

        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255'
        ];

        if(!empty($input['password']))
            $rules['password'] = 'required|confirmed|min:6';

        $validator = Validator::make($input, $rules);

        $validator->passes();

        $messages = $validator->errors();

        if ($messages->isEmpty())
        {

            unset($input['password_confirmation']);

            if(!empty($input['password']))
                $input['password'] = bcrypt($input['password']);

            $item = User::find($id);
            $item->fill($input)->save();

            flash('Dados alterados com sucesso!');

            return Redirect::route('sites.auth.account');

        }

        return Redirect::back()->withInput()->withErrors($messages);

    }

}
