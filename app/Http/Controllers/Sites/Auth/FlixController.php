<?php

namespace App\Http\Controllers\Sites\Auth;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Sites\MasterController;


class FlixController extends MasterController
{

    protected $url_validation = "https://www.flixdovarejo.com.br/validar-o-token-de-parceria";
    protected $url_decryption = "https://www.flixdovarejo.com.br/validar-o-acesso-do-assinante";

    protected $key_partner = "9FEF2C66A9E91F936C7BBCFCC76BEF44";

    public function __construct(Request $request)
    {
        parent::__construct(null, $request);
        $this->middleware('guest');
    }

    public function getLogin(Request $request)
    {

        if(!$request->has('KC'))
            return response('KEY CLIENT INVALID', 404);

        if(!$request->has('KA'))
            return response('KEY AUTH INVALID');


        $xmlstr =  $this->validation($request->input('KC'), $request->input('KA'));

        libxml_use_internal_errors(true);

        $xml_encrypted = simplexml_load_string($xmlstr);

        if (!$xml_encrypted) {

            $xmlstr = $this->descryption($xmlstr);

            $xml_descrypted = simplexml_load_string($xmlstr);

            if ($xml_descrypted && !empty($xml_descrypted->cpf)) {

                if($xml_descrypted->assinatura == 'GRATUITO' && strpos($_SERVER['HTTP_HOST'],'freemium') === false)
                    return redirect("https://freemium.maxmix.net.br/auth/flix/?KC={$request->input('KC')}&KA={$request->input('KA')}");

                //return response()->json($xml_descrypted);

                // Busca o cliente pelo cpf
                // --> se nao existir, faz o cadastro

                $user = User::where('cpf', $xml_descrypted->cpf)
                    ->first();

                $data = [
                    'site_id' => $this->site->id,
                    'cpf' => $xml_descrypted->cpf,
                    'cnpj' => $xml_descrypted->cnpj,
                    'name' => $xml_descrypted->razaoSocial,
                    'email' => $xml_descrypted->email,
                    'password' => bcrypt($xml_descrypted->senha),
                    'verified' => true
                ];

                if($user){

                    $user->fill($data)->save();

                } else {

                    $user = User::create($data);

                }

                //return response()->json($user);

                // Loga o cliente

                if($user->id){

                    $user_login = Auth::loginUsingId($user->id, true);

                    if($user_login)
                        return redirect('/');

                }

                return redirect('http://www.flixdovarejo.com.br');

            } else {

                // RESPONSE DESCRYPTION INVALID

                return redirect("https://www.flixdovarejo.com.br/complete-os-dados-do-administrador?KC={$request->input('KC')}&KA={$request->input('KA')}&KU=".base64_encode('https://www.maxmix.net.br/auth/flix/')."&PA=MaxMix");

            }

        } else {

            // RESPONSE VALIDATION INVALID

            return redirect('http://www.flixdovarejo.com.br');

        }



    }

    protected function validation($key_client, $key_autorization){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url_validation,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "KC=".$key_client."&KA=".$key_autorization."&KP=".$this->key_partner,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error";
        } else {
            return $response;
        }

    }

    protected function descryption($code){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url_decryption,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "COD=".$code."&KP=".$this->key_partner,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error";
        } else {
            return $response;
        }

    }

}
