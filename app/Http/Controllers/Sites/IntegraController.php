<?php

namespace App\Http\Controllers\Sites;

use Illuminate\Http\Request;

class IntegraController extends MasterController
{

    public function setGeoposition(Request $request)
    {

        session([
            'geoposition' => $request->input('geoposition')
        ]);

        return session()->get('geoposition');

    }

}
