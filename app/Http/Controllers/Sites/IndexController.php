<?php

namespace App\Http\Controllers\Sites;

use App\Http\Requests;
use App\Models\Contents\Page;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;

class IndexController extends MasterController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return $this->view('index');

    }

    public function getPage($slug = null)
    {

        // Buscando por páginas

        $page = Page::from(Config::get('constants.table.CONTENT.PAGES').' as P')
            ->leftJoin(Config::get('constants.table.CONTENT.TEMPLATES').' as T', 'P.template_id', '=', 'T.id')
            ->select('P.*', "T.slug AS template")
            ->where('P.slug', $slug)
            ->first();

        if(!empty($page->template))
            return $this->view($page->template)->with('page', $page);


        //return Redirect::to('/errors/404');
    }

}
