<?php
namespace app\Http\Controllers\Sites\Planogram;

use App\Http\Controllers\Sites\MasterController;
use App\Integra\Integra;
use App\Models\General\State;
use App\Models\Planogram\Area;
use App\Models\Planogram\Category;
use App\Models\Planogram\Planogram;
use Illuminate\Http\Request;
use App\Models\Planogram\Brand;
use App\Models\Planogram\Subcategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use Yajra\Datatables\Datatables;
use App\Integra\DatatablesBuilder as Builder;

class PlanogramsController extends MasterController
{

    protected $route_name = 'sites.planogram.resource';

    public function __construct(Request $request)
    {
        parent::__construct($this->route_name, $request);
    }

    public function index(Request $request, Builder $htmlBuilder)
    {

        if($request->input('order.0.column') == 0)
            $request->merge(['order.0.dir' => 'desc']);

        $itens = Planogram::select('planogram_planograms.id', 'planogram_planograms.code', 'planogram_planograms.amount_modules', 'planogram_planograms.created_at', 'general_states.abbreviation AS state_abbreviation', 'planogram_subcategories.name AS subcategory_name', 'planogram_categories.name AS category_name')
            ->join('general_states', 'general_states.id', '=', 'planogram_planograms.state_id')
            ->leftJoin('planogram_subcategories', 'planogram_subcategories.id', '=', 'planogram_planograms.subcategory_id')
            ->leftJoin('planogram_categories', 'planogram_categories.id', '=', 'planogram_planograms.category_id')
            ->where('planogram_planograms.site_id', '=', $this->site->id)
            ->where('planogram_planograms.user_id', $this->user->id);

        if ($request->ajax()) {
            return Datatables::of($itens)
                ->make(true);
        }

        $datatables = $htmlBuilder
            ->ajax(route($this->resource->getRouteIndex()))
            ->addCheckboxs()
            ->addColumn(['data' => 'code', 'name' => 'planogram_planograms.code', 'title' => 'Código', 'width' => '10%'])
            ->addColumn(['data' => 'state_abbreviation', 'name' => 'general_states.abbreviation', 'title' => 'Estado', 'width' => '5%'])
            ->addColumn(['data' => 'subcategory_name', 'name' => 'planogram_subcategories.name', 'title' => 'Subcategoria'])
            ->addColumn(['data' => 'category_name', 'name' => 'planogram_categories.name', 'title' => 'Categoria'])
            ->addColumn(['data' => 'amount_modules', 'name' => 'planogram_planograms.amount_modules', 'title' => 'Módulos', 'width' => '5%'])
            ->addColumn(['data' => 'created_at', 'name' => 'planogram_planograms.created_at', 'title' => 'Gerado em', 'render' => 'datatablesFormatDate(data)', 'width' => '10%', 'order' => 'desc'])
            ->addActions('read-delete')
            ->format();

        return $this->view('planogram.datatables.index', [
            'datatables' => $datatables
        ]);
    }

    public function store(Request $request)
    {
        if($request->input('type') === 'DELETE')
            return $this->destroyItens($request);
    }

    public function show($id)
    {
        return $this->showForm('show', $id);
    }

    public function destroyItens(Request $request)
    {
        return Planogram::destroy($request->input('data'));
    }

    protected function showForm($mode, $id = null)
    {

        if ($id)
        {
            if ( ! $item = Planogram::find($id))
            {
                return Redirect::to($this->resource->getRouteIndex());
            }

            $brands = $this->getBrandsByPlanogram($item);


        } else {

            return 'Ops!';

        }

        return $this->view('planogram.generated', [
            'mode' => $mode,
            'planogram' => $item,
            'img' => Image::make(Integra::public_uri($item->getSrc())),
            'brands' => $brands,
            'resource' => $this->resource,
        ]);
    }

    protected function getBrandsByPlanogram($item)
    {
        $marcas = false;

        //$area_id = $item->state->areas[0]['pivot']['area_id'];
        $area = Area::join('planogram_area_state', 'planogram_area_state.area_id', '=', 'planogram_areas.id')
            ->where('planogram_areas.site_id', $this->site->id)
            ->where('planogram_area_state.state_id', $item->state_id)
            ->first();

        $area_id = $area->id;

        $qtd_prateleira = array_sum(json_decode($item->amount_shelves, true));

        $brands = Brand::select('planogram_brands.*', 'product_brands.name AS brand_name', 'planogram_brand_area.*')
            ->join('planogram_brand_area', 'planogram_brand_area.brand_id', '=', 'planogram_brands.id')
            ->join('product_brands', 'product_brands.id', '=', 'planogram_brands.brand_id')
            ->where('planogram_brands.subcategory_id', $item->subcategory->id)
            ->where('planogram_brand_area.area_id', $area_id)
            ->orderBy('planogram_brand_area.position')
            ->get();

        foreach($brands as $brand) {

            $position = $brand->areas->find($area_id)['pivot']['position'];

            $share = $brand->areas->find($area_id)['pivot']['share'];

            if ($position > 0 && $share > 0)
            {

                $marcas[$position] = [
                    'id' => $brand->id,
                    'nome' => $brand->brand_name,
                    'share' => number_format($share, 0),
                    'position' => $position,
                    'cm' => ($item->shelf_size * $qtd_prateleira) * number_format($share, 0)
                ];

            }
        }

        return $marcas;
    }

    public function sendEmail($code, Request $request)
    {

        $planogram = Planogram::where('code', $code)
                        ->first();

        $user = Auth::user();

        $site = $this->site;

        $data = [
            'planogram' => $planogram,
            'user' => $user,
            'site' => $site,
            'request' => $request,
            'brands' => $this->getBrandsByPlanogram($planogram)
        ];

        $path_planogram = Integra::public_uri($planogram->getSrc());

        Mail::send($this->config['view'].'planogram.email', $data, function ($m) use ($planogram, $request, $site, $path_planogram) {
            $m->from($site->email_sender, $site->email_name);
            $m->attach($path_planogram, ['as' => 'Planograma - '.$planogram->subcategory->name.'.jpg', 'mime' => 'image/jpeg']);
            $m->to($request->email)->subject('Planograma - '.$planogram->subcategory->name);
        });

    }

    public function getGenerator(){

        $subcategories = Subcategory::join('planogram_categories', 'planogram_categories.id', '=', 'planogram_subcategories.category_id')
            ->where('planogram_categories.site_id', $this->site->id)
            ->orderBy('planogram_subcategories.name')
            ->lists('planogram_subcategories.name', 'planogram_subcategories.id');

        $categories = Category::where('site_id', $this->site->id);

        $states = State::whereExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('planogram_area_state')
                ->join('planogram_areas', 'planogram_area_state.area_id', '=', 'planogram_areas.id')
                ->where('planogram_areas.site_id', $this->site->id)
                ->whereRaw('planogram_area_state.state_id = general_states.id');
            })
            ->lists('name', 'id');

        return $this->view('planogram.generator', [
            'categories' => $categories,
            'subcategories' => $subcategories,
            'states'    => $states
        ]);
    }

    public function postGenerator(Request $request){

        $arr_gondola = array_filter($request->input('gondola'));

        $amount_modules = count($arr_gondola);

        // Marcas

        $subcategory_id = $request->input('subcategory_id');
        $state_id = $request->input('state_id');
        $number = $request->input('number');

        //$area = State::find($state_id)->areas->first();
        $area = Area::join('planogram_area_state', 'planogram_area_state.area_id', '=', 'planogram_areas.id')
            ->where('planogram_areas.site_id', $this->site->id)
            ->where('planogram_area_state.state_id', $state_id)
            ->first();

        if(!$area)
            return 'Área não configurada!';

        $subcategory = Subcategory::find($subcategory_id);

        $area_id = $area->id;
        $marcas = [];

        $brands = Brand::select('planogram_brands.*', 'product_brands.name AS brand_name')
            ->join('planogram_brand_area', 'planogram_brand_area.brand_id', '=', 'planogram_brands.id')
            ->join('product_brands', 'product_brands.id', '=', 'planogram_brands.brand_id')
            ->where('planogram_brands.subcategory_id', $subcategory_id)
            ->where('planogram_brand_area.area_id', $area_id)
            ->orderBy('planogram_brand_area.position')
            ->get();

        if(!$brands)
            return 'Não foram encontradas marcas para esta área!';

        foreach($brands as $brand) {

            $position = $brand->areas->find($area_id)['pivot']['position'];

            $share = $brand->areas->find($area_id)['pivot']['share'];

            if ($position > 0 && $share > 0)
            {

                $marcas[$position] = [
                    'id' => $brand->id,
                    'nome' => $brand->brand_name,
                    'imagem' => $brand->image,
                    'share' => number_format($share, 0),
                    'position' => $position,
                    'color' => [ 'red' => rand(-40,-10), 'green' => rand(-40,-10), 'blue' => rand(-40,-10) ],
                    'error' => rand(1,5)
                ];

            }
        }

        // Largura da prateleira
        $prateleira['largura'] = 1 * 1021;

        // Quantidade de prateleiras
        $prateleira['quantidade'] = $number;

        // Criacao da gondola
        $img = Image::canvas(($prateleira['largura'])*count($arr_gondola), ((6*240)));

        // Insercao das prateleiras
        $matriz = [];

        $qtd_prateleira = 0;

        for($g = 0; $g < count($arr_gondola); $g++) {

            $qtd_prateleira += $arr_gondola[$g+1];

            // Matriz
            for ($i = 1; $i <= $arr_gondola[$g+1]; $i++)
                $matriz[$g+1][$i] = array_pad([], 10, null);

        }

        // largura total de espaco na gondola
        $prateleira['largura_total'] = $prateleira['largura'] * $qtd_prateleira;

        foreach($marcas as $pos => &$marca) {
            $aux_largura = $prateleira['largura_total'] * ($marca['share'] / 100);
            $marca['largura_real'] = $marca['largura_original'] = $aux_largura;

            $marca['removeu'] = 0;

            if($aux_largura < 100) {
                $marca['largura'] = 100;
                $marca['maior'] = 100 - $aux_largura;
            }elseif($aux_largura >= 100) {
                $marca['largura'] = $aux_largura;
                $marca['maior'] = 0;
            }

            $marca['cm'] = ( ( $request->input('shelf_size') * $qtd_prateleira ) * $marca['share'] );
        }

        foreach($marcas as $pos => $mar){

            $maior = $mar['maior'];

            $i = 0;

            while( $maior > 0 && $i < 20){

                foreach($marcas as $p => $m){

                    if( ( $m['largura'] - $maior ) > 100 && ($marcas[$p]['removeu'] < 50)  ) {
                        $marcas[$p]['largura'] = $marcas[$p]['largura_real'] = ($m['largura']) - $maior;
                        $marcas[$p]['removeu'] = $marcas[$p]['removeu'] + $maior;
                        $maior = 0;
                    }

                }

                $marcas[$pos]['removeu'] = 0;
                $i++;

            }

        }

        $brands = $marcas;

        // Montagem na matriz
        $matriz2 = $matriz;

        foreach ($matriz as $g => &$gondola) {

            foreach ($gondola as $l => &$linha) {

                foreach ($linha as $c => &$coluna) {

                    foreach ($marcas as &$marca) {

                        if ($marca['largura'] >= 55) {

                            $coluna = $marca;

                            $marca['largura'] = $marca['largura'] - 100;

                            CONTINUE 2;

                        }

                    }

                }

            }

        }

        if(count($arr_gondola) > 1) {

            $arr_marcas = [];

            foreach ($matriz as $g => $gond)
                foreach ($gond as $p => $prat)
                    foreach ($prat as $m => $marca)
                        $arr_marcas[] = $marca;


            $m = null;

            while (!empty($arr_marcas)) {

                // Gondola
                foreach ($matriz2 as $g2 => &$gond2) {

                    $m = reset($arr_marcas)['id'];

                    // Prateleira
                    foreach ($gond2 as $p2 => &$prat2)
                        foreach ($prat2 as $c2 => &$col2) // Colunas
                            if (empty($col2) && $m == reset($arr_marcas)['id'])
                                $col2 = array_shift($arr_marcas);

                }

            }

            $matriz = $matriz2;

        }

        // Montagem dos produtos

        for($g = 1; $g <= count($arr_gondola); $g++) {

            for ($i = 1; $i <= $arr_gondola[$g]; $i++) {

                // Prateleiras
                $y1 = ($i == 1) ? 200 : 200 * $i + (40 * ($i - 1));

                // Produtos
                $aux_brand = '';

                for ($n = 0; $n < 10; $n++) {

                    $x1b = ($n * 100) + ( ($g-1) * 1000 ) + ( ($g-1)*21 );
                    $y1b = $y1 - 200;

                    if (!empty($matriz[$g][$i][$n]))
                        $aux = $n;
                    else
                        $aux = $n - 1;

                    if (empty($matriz[$g][$i][$aux]))
                        $aux = $aux - 1;

                    $image_name = $matriz[$g][$i][$aux]['imagem'];

                    $image_path = 'sites/maxmix/media/images/planogram/brands/' . $image_name;

                    if (!Storage::disk('public')->has($image_path) || empty($image_name)) {

                        $image_path = 'sites/maxmix/media/images/planogram/brands/error-'.$matriz[$g][$i][$aux]['error'].'.png';

                        $aux_img = Image::make(public_path($image_path))
                            ->colorize($matriz[$g][$i][$aux]['color']['red'],$matriz[$g][$i][$aux]['color']['green'],$matriz[$g][$i][$aux]['color']['blue']);

                        $img->insert($aux_img, null, $x1b, $y1b + 20);

                    } else
                        $img->insert(public_path($image_path), null, $x1b, $y1b + 20);

                    if ($aux_brand != $matriz[$g][$i][$aux]['nome']) {
                        $matriz_name[$g][$i][] = ['nome' => $matriz[$g][$i][$aux]['nome'], 'x' => $x1b, 'y' => $y1b + 230];
                    }

                    $aux_brand = $matriz[$g][$i][$aux]['nome'];

                }

            }

        }

        // Montagem das estiquetas
        foreach ($matriz_name as $g => &$gond) {

            foreach ($gond as $key => &$prat) {

                foreach ($prat as $k => &$m) {

                    if (isset($prat[$k + 1]['x'])) {
                        $m['text']['x'] = ($prat[$k + 1]['x'] / 2) + ($m['x'] / 2);
                        $m['w'] = $prat[$k + 1]['x'] - $prat[$k]['x'];
                        $m['c'] = (abs($m['w']) >= 100) ? true : false;
                    } else {
                        $m['text']['x'] = (((1000*$g + 21*($g-1)) - $m['x']) / 2) + $m['x'];
                        $m['w'] = (1000*$g + 21*($g-1)) - $m['x'];
                        $m['c'] = (abs($m['w']) > 100) ? true : false;
                    }

                    if ($m['text']['x'] <= 50)
                        $m['text']['x'] = 0;
                    elseif ($m['text']['x'] >= 950 && $m['text']['x'] <= 1000)
                        $m['text']['x'] = 900;
                    elseif ($m['text']['x'] >= 1971 && $m['text']['x'] <= 2021)
                        $m['text']['x'] = 1921;
                    elseif ($m['text']['x'] >= 3092)
                        $m['text']['x'] = 3042;

                    $m['text']['y'] = $m['y'];

                    $img->text($m['nome'], $m['text']['x'], $m['text']['y'], function ($font) use ($m) {

                        $font->file('sites/maxmix/media/fonts/planogram/BebasNeue.otf');
                        $font->color('#ffffff');

                        if (strlen($m['nome']) > 8 && $m['w'] == 100)
                            $font->size(240 / strlen($m['nome']));
                        else
                            $font->size(30);

                        //if(( $m['c'] ) && $m['x'] < 900)
                        if ($m['c'] === true && $m['text']['x'] >= 50)
                            $font->align('center');
                        else
                            $font->align('left');

                    });

                }

            }

        }

        $arr_width = [
            1 => 1074,
            2 => 2096,
            3 => 3118
        ];

        $stand = Image::canvas($arr_width[count($arr_gondola)], 6*240);

        for($g = 0; $g < count($arr_gondola); $g++) {

            for ($i = 1; $i <= 6; $i++) {

                if ($i == 1)
                    $stand->insert(public_path('sites/maxmix/media/images/planogram/stand_top.png'), null, $g * 1021);
                else
                    $stand->insert(public_path('sites/maxmix/media/images/planogram/stand.png'), null, $g * 1021, 240 * ($i - 1));

            }

        }

        $stand->insert($img, null, 37, 0);

        // Salvando planograma

        $data = [
            'site_id' => $this->site->id,
            'user_id' => $this->user->id,
            'subcategory_id' => $request->input('subcategory_id'),
            'state_id' => $request->input('state_id'),
            'code' => uniqid(),
            'store_size' => $request->input('store_size'),
            'amount_modules' => $amount_modules,
            'amount_shelves' => json_encode($arr_gondola),
            'shelf_size' => $request->input('shelf_size'),
        ];

        $planogram = Planogram::create($data);

        if($planogram){

            $path = Integra::public_path(Planogram::$src_image).$planogram->code.'.jpg';
            $stand->save($path, 60);

        }

        return $this->view('planogram.generated', [
            'planogram' => $planogram,
            'subcategory' => $subcategory,
            'area' => $area,
            'img' => $stand,
            'brands'    => $brands,
            'store_size' => $request->input('store_size')
        ]);

    }

}