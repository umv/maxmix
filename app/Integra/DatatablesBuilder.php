<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 06/01/2016
 * Time: 15:34
 */

namespace App\Integra;

use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Html\Column;

class DatatablesBuilder extends Builder
{
    /**
     * Generate datatable js parameters.
     *
     * @param  array $attributes
     * @return string
     */
    public function parameterize($attributes = [])
    {
        $json = parent::parameterize($attributes);

        //$json = str_replace("\"#search#\"", 'function(){datatablesFormatActions();this.api().columns().every(function(){var a=this,e=document.createElement("input");e.setAttribute("class","form-control");$(e).appendTo($(a.footer()).empty()).on("change",function(){a.search($(this).val()).draw()})});$("#dataTableBuilder thead").append($("#dataTableBuilder tfoot").html());}', $json);

        return $json;
    }

    /**
     * Add a id column.
     *
     * @return $this
     */
    public function addIndex(array $attributes = [])
    {
        $attributes = [
            'data'           => 'id',
            'name'           => 'id',
            'title'          => 'ID',
            'render'         => null,
            'orderable'      => true,
            'searchable'     => true,
            'class'         => 'datatables-column-id'
        ];
        $this->collection->push(new Column($attributes));

        return $this;
    }

    /**
     * Add a active column.
     *
     * @return $this
     */
    public function addActive()
    {
        $attributes = [
            'data'           => 'active',
            'name'           => 'active',
            'title'          => 'Ativo',
            'render'         => 'datatablesFormatActive(data, type, full, meta)',
            'orderable'      => true,
            'searchable'     => true,
            'class'         => 'datatables-column-active text-center',
            'width' => '5%'
        ];
        $this->collection->push(new Column($attributes));

        return $this;
    }

    /**
     * Add a id column.
     *
     * @return $this
     */
    public function addActions($option = null)
    {

        switch($option){
            case 'read':
                $render = 'datatablesMakeActionRead(data, type, full, meta)';
                break;
            case 'read-delete':
                $render = 'datatablesMakeActionReadDelete(data, type, full, meta)';
                break;
            default:
                $render = 'datatablesMakeActions(data, type, full, meta)';
        }

        $attributes = [
            'data'           => 'id',
            'name'           => 'action',
            'title'          => 'Ações',
            'render'         => $render,
            'orderable'      => false,
            'searchable'     => false,
            'class'         => 'datatables-column-action',
            'width'         => '100px'
        ];

        $this->collection->push(new Column($attributes));

        return $this;
    }

    /**
     * Add a id column.
     *
     * @return $this
     */
    public function addCheckboxs()
    {

        $attributes = [
            'data'           => 'id',
            'name'           => 'checkbox',
            'title'          => '',
            'render'         => 'datatablesMakeCheckbox(data, type, full, meta)',
            'orderable'      => false,
            'searchable'     => false,
            'class'         => 'datatables-column-checkbox',
            'width'         => '10px'
        ];

        $this->collection->push(new Column($attributes));

        return $this;
    }

    /**
     * Configure DataTable's format.
     *
     * @return $this
     */
    public function format()
    {

        $attributes = array(
            "dom" => '<"row"<"col-lg-12"<"card"<"card-body no-padding"<"table-responsive no-margin"t>>>>><"row"<"col-lg-6"l><"col-lg-6 text-right"i>>',
            "pagingType" => "simple",
            "language" => array (
                "sEmptyTable" => "Nenhum registro encontrado",
                "sInfo" => "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty" => "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered" => "(Filtrados de _MAX_ registros)",
                "sInfoPostFix" => "",
                "sInfoThousands" => ".",
                "sLengthMenu" => "_MENU_ resultados por página",
                "sLoadingRecords" => "Carregando...",
                "sProcessing" => "Processando...",
                "sZeroRecords" => "Nenhum registro encontrado",
                "sSearch" => '<i class="fa fa-search"></i> Pesquisar',
                "oPaginate" => array(
                    "sNext" => '<i class="fa fa-angle-right" title="Próximo"></i>',
                    "sPrevious" => '<i class="fa fa-angle-left" title="Anterior"></i>',
                    "sFirst" => "Primeiro",
                    "sLast" => "Último"
                ),
                "oAria" => array(
                    "sSortAscending" => ": Ordenar colunas de forma ascendente",
                    "sSortDescending" => ": Ordenar colunas de forma descendente"
                )
            )
        );

        $this->attributes = array_merge($this->attributes, $attributes);

        return $this;
    }

    public function setRowOrder()
    {

        $attributes = array(
            "rowReorder" => array(
                "selector" => "td:nth-child(2)"
            ),
            "responsive" => true
        );

        $this->attributes = array_merge($this->attributes, $attributes);

        return $this;
    }

    /**
     * Generate DataTable's table html.
     *
     * @param  array $attributes
     * @return string
     */
    public function table(array $attributes = [], $drawFooter = false)
    {
        $this->tableAttributes = array_merge($this->tableAttributes, $attributes);

        $table = '<table ' . $this->html->attributes($this->tableAttributes) . '>';

        $table .= '<thead><tr>';

        /*foreach ($this->collection->toArray() as $i => $column) {

            $table .= '<th><input></th>';

        }

        $table .= '</tr>';

        $table .= '<tr>';*/

        foreach ($this->collection->toArray() as $i => $column) {

            $table .= '<th>'.$column['title'].'</th>';

        }

        $table .= '</tr></thead><tbody></tbody>';

        $table .= '<tfoot><tr>';

        foreach ($this->collection->toArray() as $i => $column) {

            $table .= '<th id="integra-datatables-filter-idx-'.$i.'">'.$column['title'].'</th>';

        }

        $table .= '</tr></tfoot>';

        $table .= '</table>';

        return $table;
    }


}