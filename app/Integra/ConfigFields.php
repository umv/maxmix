<?php

namespace App\Integra;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;


/**
 * Class ConfigFields
 * @package App\Integra
 */
class ConfigFields
{

    /**
     * @var
     */
    private $route;

    private $name;

    private $icon;

    /**
     * @var
     */
    private $columns;

    /**
     * ConfigFields constructor.
     * @param $url
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->route = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getRouteIndex()
    {
        $route = $this->route.'.index';
        return (Route::has($route)) ? $route : false;
    }

    /**
     * @return mixed
     */
    public function getRouteCreate()
    {
        $route = $this->route.'.create';
        return (Route::has($route)) ? $route : false;
    }

    /**
     * @return mixed
     */
    public function getRouteStore()
    {
        $route = $this->route.'.store';
        return (Route::has($route)) ? $route : false;
    }

    /**
     * @return mixed
     */
    public function getRouteShow()
    {
        $route = $this->route.'.show';
        return (Route::has($route)) ? $route : false;
    }

    /**
     * @return mixed
     */
    public function getRouteEdit()
    {
        $route = $this->route.'.edit';
        return (Route::has($route)) ? $route : false;
    }

    /**
     * @return mixed
     */
    public function getRouteUpdate()
    {
        $route = $this->route.'.update';
        return (Route::has($route)) ? $route : false;
    }

    /**
     * @return mixed
     */
    public function getRouteDestroy()
    {
        $route = $this->route.'.destroy';
        return (Route::has($route)) ? $route : false;
    }

    /**
     * @return mixed
     */
    public function getRouteSlug()
    {
        return str_replace('.','_', $this->getName());
    }

    /**
     * @return mixed
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param mixed $columns
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }




}