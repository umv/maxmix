<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22/01/2016
 * Time: 17:00
 */

namespace App\Integra;


use App\Models\Contents\Page;
use App\Models\Form\Form;
use App\Models\System\Site;
use App\Models\Tourism\Category;
use App\Models\Tourism\Destination;
use App\Models\Tourism\PackageDate;
use App\Models\Tourism\Testimonial;
use Illuminate\Support\Str;

class Integra
{

    public static $site;

    public function __construct()
    {
        self::$site = session()->get('site');
    }

    public static function setSessionSite($id)
    {

        $site = Site::find($id);
        session()->set('site', $site);

        return 1;

    }

    public function page($tag = null, $cols = '*'){

        $page = Page::select($cols);

        if($tag)
            $page = $page->where('tag', $tag);

        if($tag)
            return $page->get();
        else
            return $page->first();

    }

    public function model($model){

        switch($model){

            case 'Tourism\Category':
                return Category::where('site_id', session()->get('site')->id);
                break;

            case 'Tourism\Destination':
                return Destination::where('site_id', session()->get('site')->id);
                break;
        }

    }

    public static function assets($path)
    {
        return asset('assets/sites/'. session()->get('site')->slug . '/' . $path);
    }

    public static function asset($path)
    {
        return asset('sites/'. session()->get('site')->slug . '/assets/' . $path);
    }

    public static function media($path)
    {
        return asset('sites/'. session()->get('site')->slug . '/media/' . $path);
    }

    public static function view($path)
    {
        return view('sites'.DIRECTORY_SEPARATOR.session()->get('site')->slug.DIRECTORY_SEPARATOR.$path);
    }

    public static function path_view($path)
    {
        return 'sites.'.session()->get('site')->slug.'.'.$path;
    }

    public static function public_uri($path)
    {
        return asset('sites/'. session()->get('site')->slug . '/' . $path);
    }

    public static function public_path($path)
    {
        return ('sites/'. session()->get('site')->slug . '/' . $path);
    }

    public static function site(){
        return session()->get('site');
    }

    public static function getSiteSlug()
    {

        if(session()->has('site'))
            return 'lazertur';
        else
            return 'lazertur';

        //$slug = session()->get('site');

    }

    public static function slug($text)
    {
        return Str::slug($text);
    }

    public static function getParamsUrlYoutube($url)
    {
        if(!empty($url)) {

            $itens = parse_url($url);
            parse_str($itens['query'], $params);

            return $params;

        }
    }

    public static function getIframeYoutube($url, $w = null, $h = null, $c = null){

        if(!empty($url)) {

            (!$w) ? $w = '100%' : null;
            (!$h) ? $h = '360' : null;
            (!$c) ? $c = '' : null;

            return '<iframe frameborder="0" src="//www.youtube.com/embed/' . self::getParamsUrlYoutube($url)['v'] . '" width="' . $w . '" height="' . $h . '" class="' . $c . '"></iframe>';

        }

    }

    public static function listTourismDestination($category_id = null, $limit = null)
    {

        $destinations = Destination::select('tourism_destinations.*')
            ->leftJoin('tourism_subregions', 'tourism_subregions.id', '=', 'tourism_destinations.subregion_id')
            ->leftJoin('tourism_regions', 'tourism_regions.id', '=', 'tourism_subregions.region_id')
            ->where('tourism_destinations.site_id', self::$site->id)
            ->where('tourism_destinations.active', 1)
            ->orderBy('tourism_destinations.name', 'asc');

        if($category_id)
            $destinations = $destinations->where('tourism_regions.category_id', $category_id);

        if($limit)
            $destinations = $destinations->skip(0)->take($limit);

        return $destinations->get();

    }

    public static function listTourismPackageDate($category_id = null, $limit = null)
    {

        $dates = PackageDate::select('tourism_package_dates.*')
            ->leftJoin('tourism_packages', 'tourism_packages.id', '=', 'tourism_package_dates.package_id')
            ->leftJoin('tourism_package_service_filter', 'tourism_packages.id', '=', 'tourism_package_service_filter.package_id')
            ->leftJoin('tourism_destinations', 'tourism_destinations.id', '=', 'tourism_packages.destination_id')
            ->leftJoin('tourism_subregions', 'tourism_subregions.id', '=', 'tourism_destinations.subregion_id')
            ->leftJoin('tourism_regions', 'tourism_regions.id', '=', 'tourism_subregions.region_id')
            ->where('tourism_packages.site_id', self::$site->id)
            ->where('tourism_packages.active', 1)
            ->whereRaw("DATE(tourism_package_dates.date_departure) >= DATE(NOW())")
            ->whereRaw("DATE(tourism_package_dates.disabled_at) >= DATE(NOW())")
            ->groupBy('tourism_package_dates.package_id')
            ->orderBy('tourism_package_dates.price_promotional', 'asc')
            ->orderBy('tourism_package_dates.price_default', 'asc');

        if($category_id)
            $dates = $dates->where('tourism_regions.category_id', $category_id);

        if($limit)
            $dates = $dates->skip(0)->take($limit);

        return $dates->get();

    }

    public static function listTourismDestionationTestimonial($destination_id = null, $limit = null)
    {

        $testimonials = Testimonial::select('tourism_destination_testimonials.*')
            ->join('tourism_destinations', 'tourism_destinations.id', '=', 'tourism_destination_testimonials.destination_id')
            ->where('tourism_destination_testimonials.active', 1)
            ->orderBy('tourism_destination_testimonials.created_at');

        if($destination_id)
            $testimonials = $testimonials->where('tourism_destination_testimonials.destination_id', $destination_id);

        if($limit)
            $testimonials = $testimonials->skip(0)->take($limit);

        return $testimonials->get();

    }

    public static function showForm($slug, $hidden = [], $vars = [])
    {

        if( $slug === null )
            return;

        $hiddens = '';

        if( !empty($hidden) ) {
            $hidden = explode(',', $hidden);

            foreach ($hidden as $h) {
                $h = trim($h);
                $hiddens .= "<input type='hidden' name='{$h}' value='{$vars[$h]}'>";
            }
        }

        $form = Form::where('slug', $slug)->first();

        $form_content = '' . PHP_EOL . $form->code_form;

        $form_content = str_replace( '</form>', '', $form_content );

        $csrf_field = csrf_field();

        $form_content = <<<FORM
{$form_content}
{$csrf_field}
<input type='hidden' name='slug' value='{$form->slug}'>
<input type='hidden' name='request_uri' value='{$_SERVER['REQUEST_URI']}'>
{$hiddens}
</form>
FORM;

        return stripslashes( str_replace( '{_URL_FORM_}', route('sites.integra.form.send'), $form_content ) );

    }

}