<?php

function IntegraFormatItem($item, $column){

    $value  = $item[$column['fieldname']];
    $format = $column['format'];

    if(is_array($format)){

        foreach($format as $k => $f)
        {
            $column['format'] = $f;
            $value = $item[$column['fieldname']] = IntegraFormatItem($item, $column);
        }

        return $value;

    } else {

        switch ($format) {

            case 'date':
                return IntegraFormatDate('d/m/Y', $value);
                break;

            case 'url':
                return IntegraFomatURL($value);
                break;

            case 'bold':
                return IntegraFormatBold($value);
                break;

            case 'link':
                return IntegraFormatLink($value, $item, $column);
                break;

            case 'link_alt':
                return IntegraFormatLinkAlt($value, $column);
                break;

            default:
                return $value;
        }
    }
}

function IntegraFormatDate($format, $value)
{
    return date($format, strtotime($value));
}

function IntegraFomatURL($value)
{
    return str_replace(array('http://', 'https://', 'www.'), '', $value);
}

function IntegraFormatBold($value)
{
    return "<strong>{$value}</strong>";
}

function IntegraFormatLink($value, $item, $column)
{
    return "<a href='".URL::route($column['params']['route'], $item->id)."'>{$value}</a>";
}

function IntegraFormatLinkAlt($value, $column)
{
    return "<a href='".URL::route($column['params']['route'], $value->id)."'>{$value[$column['params']['name']]}</a>";
}

function IntegraRectifyNumber( $number, $l = 14 ){

    if( empty( $number ) )
        return false;

    $length = strlen( $number );
    $complement = '';

    if( $length < $l ){

        $fault = $l - $length;

        for ($i=0; $i < $fault; $i++)
            $complement .= '0';

    }

    return $complement.$number;

}

function IntegraMonthToString($value){

    switch ($value){

        case 1 :
            return 'Janeiro';
            break;

        case 2 :
            return 'Fevereiro';
            break;

        case 3 :
            return 'Março';
            break;

        case 4 :
            return 'Abril';
            break;

        case 5 :
            return 'Maio';
            break;

        case 6 :
            return 'Junho';
            break;

        case 7 :
            return 'Julho';
            break;

        case 8 :
            return 'Agosto';
            break;

        case 9 :
            return 'Setembro';
            break;

        case 10 :
            return 'Outubro';
            break;

        case 11 :
            return 'Novembro';
            break;

        case 12 :
            return 'Dezembro';
            break;

    }

}

function IntegraStringToMonth($value){

    $dates = [

        1 => [
            'jan', 'janeiro'
        ],

        2 => [
            'fev', 'fevereiro'
        ],

        3 => [
            'mar', 'marco'
        ],

        4 => [
            'abr', 'abril'
        ],

        5 => [
            'mai', 'maio'
        ],

        6 => [
            'jun', 'junho'
        ],

        7 => [
            'jul', 'julho'
        ],

        8 => [
            'ago', 'agosto'
        ],

        9 => [
            'set', 'setembro'
        ],

        10 => [
            'out', 'outubro'
        ],

        11 => [
            'nov', 'novembro'
        ],

        12 => [
            'dez', 'dezembro'
        ],

    ];

    $value = str_slug($value);

    switch($value)
    {

        case in_array($value, $dates[1]) :
            return '01';
            break;

        case in_array($value, $dates[2]) :
            return '02';
            break;

        case in_array($value, $dates[3]) :
            return '03';
            break;

        case in_array($value, $dates[4]) :
            return '04';
            break;

        case in_array($value, $dates[5]) :
            return '05';
            break;

        case in_array($value, $dates[6]) :
            return '06';
            break;

        case in_array($value, $dates[7]) :
            return '07';
            break;

        case in_array($value, $dates[8]) :
            return '08';
            break;

        case in_array($value, $dates[9]) :
            return '09';
            break;

        case in_array($value, $dates[10]) :
            return '10';
            break;

        case in_array($value, $dates[11]) :
            return '11';
            break;

        case in_array($value, $dates[12]) :
            return '12';
            break;

        default :
            return null;

    }

}

/**
 * Set a flash message in the session.
 *
 * @param  string $message
 * @return void
 */
function flash($message) {
    session()->flash('message', $message);
}