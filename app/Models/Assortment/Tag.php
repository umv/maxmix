<?php

namespace App\Models\Assortment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tag extends Model
{
    /**
     * @var string
     */
    protected $table = 'assortment_tags';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $clientsModel = 'App\Models\Assortment\Client';

    protected static $productModel = 'App\Models\Assortment\Product';

    protected static $sitesModel = 'App\Models\System\Site';

    public function dates()
    {
        return $this->belongsToMany(static::$productModel, 'assortment_tag_date', 'tag_id', 'product_date')->withTimestamps();
    }

    public function sites()
    {
        return $this->hasMany(static::$sitesModel);
    }

    public function clients()
    {
        return $this->hasMany(static::$clientsModel);
    }

    public function getDate($id = null)
    {
        if(empty($id))
            $id = $this->id;

        return DB::table('assortment_tag_date')
            ->where('tag_id', $id)
            ->lists('product_date', 'product_date');
    }

}
