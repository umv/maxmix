<?php

namespace App\Models\Assortment;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @var string
     */
    protected $table = 'assortment_products';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $productModel = 'App\Models\Product\Product';

    protected static $subcategoryModel = 'App\Models\Assortment\Subcategory';

    protected static $clientModel = 'App\Models\Assortment\Client';

    protected static $tagModel = 'App\Models\Assortment\Tag';

    public function product()
    {
        return $this->belongsTo(static::$productModel);
    }

    public function subcategory()
    {
        return $this->belongsTo(static::$subcategoryModel);
    }

    public function client()
    {
        return $this->belongsTo(static::$clientModel);
    }

    public function tags()
    {
        return $this->belongsToMany(static::$tagModel, 'assortment_tag_date', 'product_date', 'tag_id')->withTimestamps();
    }

}
