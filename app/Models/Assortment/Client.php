<?php

namespace App\Models\Assortment;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * @var string
     */
    protected $table = 'assortment_clients';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $productModel = 'App\Models\Assortment\Product';

    protected static $usersModel = 'App\User';

    protected static $sitesModel = 'App\Models\System\Site';

    public function users()
    {
        return $this->belongsToMany(static::$usersModel, 'assortment_client_user', 'client_id', 'user_id')->withTimestamps();
    }

    public function sites()
    {
        return $this->hasMany(static::$sitesModel);
    }

    public function products()
    {
        return $this->hasMany(static::$productModel);
    }

}
