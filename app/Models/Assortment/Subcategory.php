<?php

namespace App\Models\Assortment;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    /**
     * @var string
     */
    protected $table = 'assortment_subcategories';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $categoryModel = 'App\Models\Assorment\Category';

    protected static $productModel = 'App\Models\Assortment\Product';

    public function category()
    {
        return $this->belongsTo(static::$categoryModel);
    }

    public function products()
    {
        return $this->hasMany(static::$productModel);
    }

}
