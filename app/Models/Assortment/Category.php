<?php

namespace App\Models\Assortment;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @var string
     */
    protected $table = 'assortment_categories';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $siteModel = 'App\Models\System\Site';

    protected static $subcategoryModel = 'App\Models\Assortment\Subcategory';

    public function site()
    {
        return $this->belongsTo(static::$siteModel);
    }

    public function subcategories()
    {
        return $this->hasMany(static::$subcategoryModel);
    }

}
