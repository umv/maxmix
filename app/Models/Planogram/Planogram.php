<?php

namespace App\Models\Planogram;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class Planogram extends Model
{
    /**
     * @var string
     */
    protected $table = 'planogram_planograms';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $siteModel = 'App\Models\System\Site';

    protected static $userModel = 'App\User';

    protected static $categoryModel = 'App\Models\Planogram\Category';

    protected static $subcategoryModel = 'App\Models\Planogram\Subcategory';

    protected static $stateModel = 'App\Models\General\State';

    public static $src_image = 'media/images/planogram/planograms/';

    public function site()
    {
        return $this->belongsTo(static::$siteModel);
    }

    public function user()
    {
        return $this->belongsTo(static::$userModel);
    }

    public function category()
    {
        return $this->belongsTo(static::$categoryModel);
    }

    public function subcategory()
    {
        return $this->belongsTo(static::$subcategoryModel);
    }

    public function state()
    {
        return $this->belongsTo(static::$stateModel);
    }

    public function getSrc()
    {
        return static::$src_image.$this->code.'.jpg';
    }

    public function getStoreSize()
    {

        $sizes = [
            1 => 'Entre 300 a 600m²',
            2 => 'Entre 600 a 1.000m²',
            3 => 'Acima de 1.000m²'
        ];

        return $sizes[$this->store_size];

    }

    public static function generate($items_original, $arr_gondola, $shelf_size = 1)
    {

        // Dados

        /*$arr_gondola = [
            1 => 4,
            2 => 0,
            3 => 0
        ];

        $items = [
            1 => [ // position
                'id' => 1,
                'name' => 'Nome',
                'position' => 1,
                'participation' => 50,
                'color' => [ 'red' => rand(-40,-10), 'green' => rand(-40,-10), 'blue' => rand(-40,-10) ],
            ]
        ];
        
        $shelf_size = 1;*/

        $items = $items_original;

        foreach ($items as &$item) {
            $item['color'] = ['red' => rand(-40, -10), 'green' => rand(-40, -10), 'blue' => rand(-40, -10)];
            $item['error'] = rand(1,5);
        }

        // Gerador

        $arr_gondola = array_filter($arr_gondola);

        $gondola_count = count($arr_gondola);

        // Largura da prateleira
        $shelves_width = 1 * 1021;

        // Criacao da gondola
        $img = Image::canvas(($shelves_width)*$gondola_count, ((6*240)));

        // Insercao das prateleiras
        $matriz = [];

        $shelves_count = 0;

        for($g = 0; $g < count($arr_gondola); $g++) {

            $shelves_count += $arr_gondola[$g+1];

            // Matriz
            for ($i = 1; $i <= $arr_gondola[$g+1]; $i++)
                $matriz[$g+1][$i] = array_pad([], 10, null);

        }

        // largura total de espaco na gondola
        $shelves_width_full = $shelves_width * $shelves_count;

        foreach($items as $position => &$item) {
            
            $aux_width = $shelves_width_full * ($item['participation'] / 100);
            
            $item['width_real'] = $item['width_original'] = $aux_width;

            $item['removed'] = 0;

            if($aux_width < 100) {
                $item['width'] = 100;
                $item['bigger'] = 100 - $aux_width;
            }elseif($aux_width >= 100) {
                $item['width'] = $aux_width;
                $item['bigger'] = 0;
            }

            $item['cm'] = ( ( $shelf_size * array_sum($arr_gondola) ) * $item['participation'] );
        }

        foreach($items as $position2 => $item2){

            $bigger = $item2['bigger'];

            $i = 0;

            while( $bigger > 0 && $i < 20){

                foreach($items as $p => $item3){

                    if( ( $item3['width'] - $bigger ) > 100 && ($items[$p]['removed'] < 50)  ) {
                        $items[$p]['width'] = $items[$p]['width_real'] = ($item3['width']) - $bigger;
                        $items[$p]['removed'] = $items[$p]['removed'] + $bigger;
                        $bigger = 0;
                    }

                }

                $items[$position2]['removed'] = 0;
                $i++;

            }

        }

        $items_final = $items;

        // Montagem na matriz
        $matriz2 = $matriz;

        foreach ($matriz as $g => &$gondola) {

            foreach ($gondola as $l => &$line) {

                foreach ($line as $c => &$column) {

                    foreach ($items as &$item) {

                        if ($item['width'] >= 55) {

                            $column = $item;

                            $item['width'] = $item['width'] - 100;

                            CONTINUE 2;

                        }

                    }

                }

            }

        }

        if($gondola_count > 1) {

            $arr_items = [];

            foreach ($matriz as $g => $gond)
                foreach ($gond as $p => $shelf)
                    foreach ($shelf as $m => $item)
                        $arr_items[] = $item;


            $m = null;

            while (!empty($arr_items)) {

                // Gondola
                foreach ($matriz2 as $g2 => &$gond2) {

                    $m = reset($arr_items)['id'];

                    // Prateleira
                    foreach ($gond2 as $p2 => &$shelf2)
                        foreach ($shelf2 as $c2 => &$col2) // Colunas
                            if (empty($col2) && $m == reset($arr_items)['id'])
                                $col2 = array_shift($arr_items);

                }

            }

            $matriz = $matriz2;

        }

        // Montagem dos produtos

        for($g = 1; $g <= $gondola_count; $g++) {

            for ($i = 1; $i <= $arr_gondola[$g]; $i++) {

                // Prateleiras
                $y1 = ($i == 1) ? 200 : 200 * $i + (40 * ($i - 1));

                // Items
                $aux_item = '';

                for ($n = 0; $n < 10; $n++) {

                    $x1b = ($n * 100) + ( ($g-1) * 1000 ) + ( ($g-1)*21 );
                    $y1b = $y1 - 200;

                    if (!empty($matriz[$g][$i][$n]))
                        $aux = $n;
                    else
                        $aux = $n - 1;

                    if (empty($matriz[$g][$i][$aux]))
                        $aux = $aux - 1;

                    $image_path = 'sites/maxmix/media/images/planogram/brands/error-'.$matriz[$g][$i][$aux]['error'].'.png';

                    $aux_img = Image::make(public_path($image_path))
                        ->colorize($matriz[$g][$i][$aux]['color']['red'],$matriz[$g][$i][$aux]['color']['green'],$matriz[$g][$i][$aux]['color']['blue']);

                    $img->insert($aux_img, null, $x1b, $y1b + 20);

                    if ($aux_item != $matriz[$g][$i][$aux]['name']) {
                        $matriz_name[$g][$i][] = ['name' => $matriz[$g][$i][$aux]['name'], 'x' => $x1b, 'y' => $y1b + 230];
                    }

                    $aux_item = $matriz[$g][$i][$aux]['name'];

                }

            }

        }

        // Montagem das estiquetas
        foreach ($matriz_name as $g => &$gond) {

            foreach ($gond as $key => &$prat) {

                foreach ($prat as $k => &$m) {

                    if (isset($prat[$k + 1]['x'])) {
                        $m['text']['x'] = ($prat[$k + 1]['x'] / 2) + ($m['x'] / 2);
                        $m['w'] = $prat[$k + 1]['x'] - $prat[$k]['x'];
                        $m['c'] = (abs($m['w']) >= 100) ? true : false;
                    } else {
                        $m['text']['x'] = (((1000*$g + 21*($g-1)) - $m['x']) / 2) + $m['x'];
                        $m['w'] = (1000*$g + 21*($g-1)) - $m['x'];
                        $m['c'] = (abs($m['w']) > 100) ? true : false;
                    }

                    if ($m['text']['x'] <= 50)
                        $m['text']['x'] = 0;
                    elseif ($m['text']['x'] >= 950 && $m['text']['x'] <= 1000)
                        $m['text']['x'] = 900;
                    elseif ($m['text']['x'] >= 1971 && $m['text']['x'] <= 2021)
                        $m['text']['x'] = 1921;
                    elseif ($m['text']['x'] >= 3092)
                        $m['text']['x'] = 3042;

                    $m['text']['y'] = $m['y'];

                    $img->text($m['name'], $m['text']['x'], $m['text']['y'], function ($font) use ($m) {

                        $font->file('sites/maxmix/media/fonts/planogram/BebasNeue.otf');
                        $font->color('#ffffff');

                        if (strlen($m['name']) > 8 && $m['w'] == 100)
                            $font->size(240 / strlen($m['name']));
                        else
                            $font->size(30);

                        //if(( $m['c'] ) && $m['x'] < 900)
                        if ($m['c'] === true && $m['text']['x'] >= 50)
                            $font->align('center');
                        else
                            $font->align('left');

                    });

                }

            }

        }

        $arr_width = [
            1 => 1074,
            2 => 2096,
            3 => 3118
        ];

        $stand = Image::canvas($arr_width[$gondola_count], 6*240);

        for($g = 0; $g < $gondola_count; $g++) {

            for ($i = 1; $i <= 6; $i++) {

                if ($i == 1)
                    $stand->insert(public_path('sites/maxmix/media/images/planogram/stand_top.png'), null, $g * 1021);
                else
                    $stand->insert(public_path('sites/maxmix/media/images/planogram/stand.png'), null, $g * 1021, 240 * ($i - 1));

            }

        }

        $stand->insert($img, null, 37, 0);

        return [
            'img' => $stand,
            'items'    => $items_final,
            'items_original' => $items_original
        ];

    }

}
