<?php

namespace App\Models\Planogram;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @var string
     */
    protected $table = 'planogram_categories';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    public static $src_tip = 'media/images/planogram/tips/';

    protected static $siteModel = 'App\Models\System\Site';

    protected static $subcategoryModel = 'App\Models\Planogram\Subcategory';

    public function site()
    {
        return $this->belongsTo(static::$siteModel);
    }

    public function subcategories()
    {
        return $this->hasMany(static::$subcategoryModel);
    }

    public function getSrcTip()
    {
        return static::$src_tip.$this->tip;
    }

}
