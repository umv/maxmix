<?php

namespace App\Models\Planogram;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * @var string
     */
    protected $table = 'planogram_items';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $subcategoryModel = 'App\Models\Planogram\Subcategory';

    protected static $productModel = 'App\Models\Product\Product';

    protected static $areaModel = 'App\Models\Planogram\Area';

    public function subcategory()
    {
        return $this->belongsTo(static::$subcategoryModel);
    }

    public function product()
    {
        return $this->belongsTo(static::$productModel);
    }

    public function area()
    {
        return $this->belongsTo(static::$areaModel);
    }

}
