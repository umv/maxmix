<?php

namespace App\Models\Planogram;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    /**
     * @var string
     */
    protected $table = 'planogram_subcategories';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    public static $src_tip = 'media/images/planogram/tips/';

    protected static $categoryModel = 'App\Models\Planogram\Category';

    protected static $brandModel = 'App\Models\Planogram\Brand';

    public function category()
    {
        return $this->belongsTo(static::$categoryModel);
    }

    public function brands()
    {
        return $this->hasMany(static::$brandModel);
    }

    public function getSrcTip()
    {
        return static::$src_tip.$this->tip;
    }

    public function getBrands($area_id)
    {
        $brands = Brand::select('product_brands.id', 'product_brands.name')
            ->join('planogram_brand_area', 'planogram_brand_area.brand_id', '=', 'planogram_brands.id')
            ->join('product_brands', 'product_brands.id', '=', 'planogram_brands.brand_id')
            ->where('planogram_brands.subcategory_id', $this->id)
            ->where('planogram_brand_area.area_id', $area_id)
            ->orderBy('planogram_brand_area.position')
            ->get();

        return $brands;
    }

    public function getMix($area_id, $brand_id){

        $items = Item::select('planogram_items.position', 'product_products.ean', 'product_products.name')
            ->join('product_products', 'product_products.id', '=', 'planogram_items.product_id')
            ->join('product_brands', 'product_brands.id', '=', 'product_products.brand_id')
            ->where('planogram_items.area_id', $area_id)
            ->where('planogram_items.subcategory_id', $this->id)
            ->where('product_brands.id', $brand_id)
            ->orderBy('position')
            ->get();

        return $items;

    }

}
