<?php

namespace App\Models\Planogram;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    /**
     * @var string
     */
    protected $table = 'planogram_brands';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $areaModel = 'App\Models\Planogram\Area';

    protected static $subcategoryModel = 'App\Models\Planogram\Subcategory';

    protected static $brandModel = 'App\Models\Product\Brand';

    public function subcategory()
    {
        return $this->belongsTo(static::$subcategoryModel);
    }

    public function areas()
    {
        return $this->belongsToMany(static::$areaModel, 'planogram_brand_area', 'brand_id', 'area_id')->withPivot('share', 'position');
    }

    public function brand()
    {
        return $this->belongsTo(static::$brandModel);
    }

}
