<?php

namespace App\Models\Planogram;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    /**
     * @var string
     */
    protected $table = 'planogram_areas';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $stateModel = 'App\Models\General\State';

    protected static $brandModel = 'App\Models\Planogram\Brand';

    public function states()
    {
        return $this->belongsToMany(static::$stateModel, 'planogram_area_state', 'area_id', 'state_id')->withTimestamps();
    }

    public function brands()
    {
        return $this->belongsToMany(static::$brandModel, 'planogram_brand_area', 'area_id', 'brand_id')->withTimestamps();
    }

}
