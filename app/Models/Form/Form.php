<?php

namespace App\Models\Form;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    /**
     * @var string
     */
    protected $table = 'form_forms';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $siteModel = 'App\Models\System\Site';

    public function site()
    {
        return $this->belongsTo(static::$stateModel);
    }

}
