<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_services';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $serviceModel = 'App\Models\Tourism\ServiceFilter';

    protected static $packagesModel = 'App\Models\Tourism\Package';

    public function filters()
    {
        return $this->hasMany(static::$serviceModel);
    }

    public function packages()
    {
        return $this->belongsToMany(static::$packagesModel, 'tourism_package_service', 'service_id', 'package_id')->withTimestamps();
    }
}
