<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Destinations
 * @package App\Models\Tourism
 */
class DestinationPhoto extends Model
{

    /**
     * @var string
     */
    protected $table = 'tourism_destination_photos';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    public static $src_image = 'media/images/tourism/destinations/';

    protected static $destinationModel = 'App\Models\Tourism\Destination';

    public function destination()
    {
        return $this->belongsTo(static::$destinationModel);
    }

    public function getSrc()
    {
        return static::$src_image.$this->slug;
    }

}