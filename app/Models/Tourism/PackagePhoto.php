<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Destinations
 * @package App\Models\Tourism
 */
class PackagePhoto extends Model
{

    /**
     * @var string
     */
    protected $table = 'tourism_package_photos';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    public static $src_image = 'media/images/tourism/packages/';

    protected static $packageModel = 'App\Models\Tourism\Package';

    public function package()
    {
        return $this->belongsTo(static::$packageModel);
    }

    public function getSrc()
    {
        return static::$src_image.$this->slug;
    }

}