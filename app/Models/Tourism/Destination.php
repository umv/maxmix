<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Destinations
 * @package App\Models\Tourism
 */
class Destination extends Model
{

    /**
     * @var string
     */
    protected $table = 'tourism_destinations';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $subregionModel = 'App\Models\Tourism\Subregion';

    protected static $attractionModel = 'App\Models\Tourism\Attraction';

    protected static $testimonialModel = 'App\Models\Tourism\Testimonial';

    protected static $destinationPhotoModel = 'App\Models\Tourism\DestinationPhoto';

    protected static $packageModel = 'App\Models\Tourism\Package';

    protected static $intentionModel = 'App\Models\Tourism\Intention';

    public function subregion()
    {
        return $this->belongsTo(static::$subregionModel);
    }

    public function attractions()
    {
        return $this->hasMany(static::$attractionModel)->where('destination_id', $this->id);
    }

    public function testimonials()
    {
        return $this->hasMany(static::$testimonialModel)->where('active', 1)->orderBy('created_at', 'desc');
    }

    public function photos()
    {
        return $this->hasMany(static::$destinationPhotoModel)->orderBy('cover', 'desc');
    }

    public function packages()
    {
        return $this->hasMany(static::$packageModel)->where('active', 1);
    }

    public function intentions()
    {
        return $this->hasMany(static::$intentionModel);
    }

    public function getUrl()
    {
        return route('sites.tourism.destinations.destination', [
            $this->subregion->region->category->slug,
            $this->subregion->region->slug,
            $this->subregion->slug,
            $this->slug
        ]);
    }

    public function getPhotoCover()
    {
        $cover = DestinationPhoto::where('destination_id', $this->id)
            ->where('cover', '1')
            ->first();

        return $cover;
    }

}