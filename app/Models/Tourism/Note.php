<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_notes';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $packagesModel = 'App\Models\Tourism\Package';

    public function packages()
    {
        return $this->belongsToMany(static::$packagesModel, 'tourism_package_note', 'note_id', 'package_id')->withTimestamps();
    }
}
