<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_categories';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $regionsModel = 'App\Models\Tourism\Region';

    public function regions()
    {
        return $this->hasMany(static::$regionsModel);
    }

    public function getUrl()
    {
        return route('sites.tourism.destinations.category', $this->slug );
    }
}
