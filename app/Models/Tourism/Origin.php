<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_origins';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

}
