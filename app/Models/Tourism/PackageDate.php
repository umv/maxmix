<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

class PackageDate extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_package_dates';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $packageModel = 'App\Models\Tourism\Package';

    protected static $cityModel = 'App\Models\General\City';

    protected static $paymentModel = 'App\Models\Tourism\Payment';

    public function package()
    {
        return $this->belongsTo(static::$packageModel);
    }

    public function city()
    {
        return $this->belongsTo(static::$cityModel);
    }

    public function payments()
    {
        return $this->belongsToMany(static::$paymentModel, 'tourism_package_date_payment', 'date_id', 'payment_id')
            ->withPivot('input_value', 'discount_value', 'min_parcel', 'max_parcel', 'active')
            ->withTimestamps();
    }

}
