<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_payments';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $packageDateModel = 'App\Models\Tourism\PackageDate';

    public function dates()
    {
        return $this->belongsToMany(static::$packageDateModel, 'tourism_package_date_payment', 'payment_id', 'date_id')
            ->withPivot('input_value', 'discount_value', 'min_parcel', 'max_parcel', 'active')
            ->withTimestamps();
    }

}
