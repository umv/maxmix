<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

class Intention extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_intentions';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $dateModel = 'App\Models\Tourism\PackageDate';

    protected static $destinationModel = 'App\Models\Tourism\Destination';

    protected static $originModel = 'App\Models\Marketing\Origin';

    public function date()
    {
        return $this->belongsTo(static::$dateModel);
    }

    public function destination()
    {
        return $this->belongsTo(static::$destinationModel);
    }

    public function origin()
    {
        return $this->belongsTo(static::$originModel);
    }

}
