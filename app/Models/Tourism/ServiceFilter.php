<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

class ServiceFilter extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_service_filters';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $serviceModel = 'App\Models\Tourism\Service';

    protected static $packagesModel = 'App\Models\Tourism\Package';

    public function service()
    {
        return $this->belongsTo(static::$serviceModel);
    }

    public function packages()
    {
        return $this->belongsToMany(static::$packagesModel, 'tourism_package_service_filter', 'filter_id', 'package_id')->withTimestamps();
    }
}
