<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

class FlightSearch extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_flight_searches';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $siteModel = 'App\Models\System\Site';

    public function site()
    {
        return $this->belongsTo(static::$siteModel);
    }

}
