<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Template
 * @package App\Models
 */
class AttractionType extends Model
{

    /**
     * @var string
     */
    protected $table = 'tourism_attraction_types';

    protected $guarded = [
        'created_at'
    ];

    protected static $attractionModel = 'App\Models\Tourism\Attraction';

    public function attractions()
    {
        return $this->hasMany(static::$attractionModel);
    }

}