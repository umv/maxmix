<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_packages';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $destinationModel = 'App\Models\Tourism\Destination';

    protected static $datesModel = 'App\Models\Tourism\PackageDate';

    protected static $servicesModel = 'App\Models\Tourism\Service';

    protected static $filtersModel = 'App\Models\Tourism\ServiceFilter';

    protected static $notesModel = 'App\Models\Tourism\Note';

    protected static $destinationPhotoModel = 'App\Models\Tourism\PackagePhoto';

    public function destination()
    {
        return $this->belongsTo(static::$destinationModel);
    }

    public function dates()
    {
        return $this->hasMany(static::$datesModel);
    }

    public function datesAvailables()
    {
        return $this->hasMany(static::$datesModel)
            ->where('active', 1)
            ->whereRaw("DATE(date_departure) >= DATE(NOW())")
            ->whereRaw("DATE(disabled_at) >= DATE(NOW())")
            ->orderBy('price_promotional')
            ->orderBy('price_default');
    }

    public function dateFirstAvailable()
    {
        return $this->datesAvailables()->first();
    }

    public function services()
    {
        return $this->belongsToMany(static::$servicesModel, 'tourism_package_service', 'package_id', 'service_id')->withTimestamps();
    }

    public function filters()
    {
        return $this->belongsToMany(static::$filtersModel, 'tourism_package_service_filter', 'package_id', 'filter_id');
    }

    public function notes()
    {
        return $this->belongsToMany(static::$notesModel, 'tourism_package_note', 'package_id', 'note_id')->withTimestamps();
    }

    public function photos()
    {
        return $this->hasMany(static::$destinationPhotoModel)->orderBy('cover', 'desc');
    }

    public function getUrl()
    {
        return route('sites.tourism.packages.package', $this->slug );
    }

    public function getPhotoCover()
    {
        $cover = PackagePhoto::where('package_id', $this->id)
            ->where('cover', '1')
            ->first();

        return $cover;
    }
}
