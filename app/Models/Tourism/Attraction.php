<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Template
 * @package App\Models
 */
class Attraction extends Model
{

    /**
     * @var string
     */
    protected $table = 'tourism_attractions';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $attractionTypeModel = 'App\Models\Tourism\AttractionType';

    protected static $destinationModel = 'App\Models\Tourism\Destination';

    public function attraction_type()
    {
        return $this->belongsTo(static::$attractionTypeModel);
    }

    public function destination()
    {
        return $this->belongsTo(static::$destinationModel);
    }

}