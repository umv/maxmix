<?php

namespace App\Models\Tourism;

use App\Models\General\State;
use Illuminate\Database\Eloquent\Model;

class Subregion extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_subregions';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $regionModel = 'App\Models\Tourism\Region';

    protected static $destinationModel = 'App\Models\Tourism\Destination';

    public function region()
    {
        return $this->belongsTo(static::$regionModel);
    }

    public function destinations()
    {
        return $this->hasMany(static::$destinationModel);
    }

    public function getUrl()
    {
        return route('sites.tourism.destinations.subregion', [$this->region->category->slug, $this->region->slug, $this->slug] );
    }

    public function getState()
    {
        $state = State::where('name', 'like', $this->name)->first();

        return $state;
    }

}
