<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

class IntentionConfig extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_intention_config';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $siteModel = 'App\Models\System\Site';

    public function site()
    {
        return $this->belongsTo(static::$siteModel);
    }

}
