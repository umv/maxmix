<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_destination_testimonials';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $destinationModel = 'App\Models\Tourism\Destination';

    protected static $cityModel = 'App\Models\General\City';

    public function destination()
    {
        return $this->belongsTo(static::$destinationModel);
    }

    public function city()
    {
        return $this->belongsTo(static::$cityModel);
    }
}
