<?php

namespace App\Models\Tourism;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    /**
     * @var string
     */
    protected $table = 'tourism_regions';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $categoryModel = 'App\Models\Tourism\Category';

    protected static $subregionsModel = 'App\Models\Tourism\Subregion';

    public function category()
    {
        return $this->belongsTo(static::$categoryModel);
    }

    public function subregions()
    {
        return $this->hasMany(static::$subregionsModel);
    }

    public function getUrl()
    {
        return route('sites.tourism.destinations.region', [$this->category->slug, $this->slug] );
    }
}
