<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @var string
     */
    protected $table = 'product_products';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $siteModel = 'App\Models\System\Site';

    protected static $brandModel = 'App\Models\Product\Brand';

    public function site()
    {
        return $this->belongsTo(static::$siteModel);
    }

    public function brand()
    {
        return $this->belongsTo(static::$brandModel);
    }
}
