<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    /**
     * @var string
     */
    protected $table = 'product_brands';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $siteModel = 'App\Models\System\Site';

    public function site()
    {
        return $this->belongsTo(static::$siteModel);
    }
}
