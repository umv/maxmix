<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * @var string
     */
    protected $table = 'general_cities';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $stateModel = 'App\Models\General\State';

    protected static $datesModel = 'App\Models\Tourism\PackageDate';

    public function state()
    {
        return $this->belongsTo(static::$stateModel);
    }

    public function dates()
    {
        return $this->hasMany(static::$datesModel);
    }

}
