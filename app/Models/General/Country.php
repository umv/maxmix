<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * @var string
     */
    protected $table = 'general_countrys';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

}
