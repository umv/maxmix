<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /**
     * @var string
     */
    protected $table = 'general_states';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

    protected static $countryModel = 'App\Models\General\Country';

    protected static $cityModel = 'App\Models\General\City';

    protected static $areaModel = 'App\Models\Planogram\Area';

    public function country()
    {
        return $this->belongsTo(static::$countryModel);
    }

    public function cities()
    {
        return $this->belongsToMany(static::$cityModel)->withTimestamps();
    }

    public function areas()
    {
        return $this->belongsToMany(static::$areaModel, 'planogram_area_state', 'state_id', 'area_id')->withTimestamps();
    }

}
