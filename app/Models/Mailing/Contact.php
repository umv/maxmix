<?php

namespace App\Models\Mailing;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Template
 * @package App\Models
 */
class Contact extends Model
{

    /**
     * @var string
     */
    protected $table = 'mailing_contacts';

    protected $guarded = [
        'created_at'
    ];

}