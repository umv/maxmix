<?php

namespace App\Models\System;

use \Cartalyst\Sentinel\Activations\EloquentActivation;

class Activation extends EloquentActivation
{

    protected $table = 'system_activations';

}
