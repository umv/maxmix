<?php

namespace App\Models\System;

use \Cartalyst\Sentinel\Users\EloquentUser;

class User extends EloquentUser
{

    protected $table = 'system_users';

    protected $guarded = [
        'created_at'
    ];

    protected $fillable = array();

    /**
     * The Eloquent roles model name.
     *
     * @var string
     */
    protected static $rolesModel = 'App\Models\System\Role';

    /**
     * The Eloquent roles model name.
     *
     * @var string
     */
    protected static $accountModel = 'App\Models\System\Account';

    /**
     * Returns the roles relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(static::$rolesModel, 'system_role_users', 'user_id', 'role_id')->withTimestamps();
    }

    /**
     * Returns the roles relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function accounts()
    {
        return $this->belongsToMany(static::$accountModel, 'system_account_user', 'user_id', 'account_id')->withTimestamps();
        //return $this->belongsToMany(static::$accountModel);
    }

}
