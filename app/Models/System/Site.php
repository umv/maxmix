<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Site
 * @package App\Models
 */
class Site extends Model
{

    /**
     * @var string
     */
    protected $table = 'system_sites';

    protected $guarded = [
        'created_at'
    ];

    /**
     * The Eloquent account model name.
     *
     * @var string
     */
    protected static $accountModel = 'App\Models\System\Account';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(static::$accountModel);
    }
}
