<?php

namespace App\Models\System;

use \Cartalyst\Sentinel\Reminders\EloquentReminder;

class Reminder extends EloquentReminder
{

    protected $table = 'system_reminders';

}
