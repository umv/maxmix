<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/12/2015
 * Time: 16:05
 */

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{

    /**
     * {@inheritDoc}
     */
    protected $table = 'system_accounts';

    protected $guarded = [
        'created_at'
    ];

    /**
     * The Eloquent users model name.
     *
     * @var string
     */
    protected static $usersModel = 'App\Models\System\User';

    /**
     * The Eloquent sites model name.
     *
     * @var string
     */
    protected static $sitesModel = 'App\Models\System\Site';

    /**
     * The Users relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(static::$usersModel, 'system_account_user', 'account_id', 'user_id')->withTimestamps();
    }

    public function sites()
    {
        return $this->hasMany(static::$sitesModel);
    }

}