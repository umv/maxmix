<?php

namespace App\Models\System;

use \Cartalyst\Sentinel\Roles\EloquentRole;

class Role extends EloquentRole
{

    protected $table = 'system_roles';

    /**
     * The Eloquent users model name.
     *
     * @var string
     */
    protected static $usersModel = 'App\Models\System\User';

    /**
     * The Users relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(static::$usersModel, 'system_role_users', 'role_id', 'user_id')->withTimestamps();
    }

}
