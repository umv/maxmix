<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Action
 * @package App\Models
 */
class Action extends Model
{

    /**
     * @var string
     */
    protected $table = 'actions';

    protected $guarded = [
        'created_at'
    ];

}