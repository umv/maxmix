<?php

namespace App\Models\System;

use \Cartalyst\Sentinel\Persistences\EloquentPersistence;

class Persistence extends EloquentPersistence
{

    protected $table = 'system_persistences';

}
