<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class Tab extends Model
{

    protected $table = 'system_tabs';

    protected $guarded = [
        'created_at'
    ];

}