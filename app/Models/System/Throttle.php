<?php

namespace App\Models\System;

use \Cartalyst\Sentinel\Throttling\EloquentThrottle;

class Throttle extends EloquentThrottle
{

    protected $table = 'system_throttle';

}
