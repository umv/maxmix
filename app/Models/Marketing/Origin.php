<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
    /**
     * @var string
     */
    protected $table = 'marketing_origins';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at'
    ];

}
