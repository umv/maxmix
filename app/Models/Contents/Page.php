<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/12/2015
 * Time: 16:05
 */

namespace App\Models\Contents;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    /**
     * {@inheritDoc}
     */
    protected $table = 'content_pages';

    protected $guarded = [
        'created_at'
    ];

}