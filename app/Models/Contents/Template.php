<?php

namespace App\Models\Contents;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Template
 * @package App\Models
 */
class Template extends Model
{

    /**
     * @var string
     */
    protected $table = 'content_templates';

    protected $guarded = [
        'created_at'
    ];

}