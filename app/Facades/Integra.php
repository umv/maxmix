<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22/01/2016
 * Time: 17:05
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Integra extends Facade
{
    protected static function getFacadeAccessor() {
        return 'integra';
    }
}