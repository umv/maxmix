<?php
return array(

    'admin' => [

        'prefix' => [
            'route' => 'admin.'
        ],

        'view' => [
            'datatables' => 'admin.datatables.index'
        ]

    ],

    'table' => [

        'CONTENT' => [

            'PAGES' => 'content_pages',
            'TEMPLATES' => 'content_templates',

        ],

    ]

);